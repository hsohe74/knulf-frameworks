package com.knulf.core.util;

/**
 * String utilities
 * @author Hyounseok Ohe
 *
 */
public class StringUtil {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Common
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Get string from array by delimiter
	 * @param data
	 * @param delimiter
	 * @return
	 */
	public static String join(Object[] data, String delimiter) {
		StringBuilder result = new StringBuilder();
		int idx = 0;
		
		if (data != null && data.length > 0) {
			for(Object item : data) {
				if (idx == 0)
					result.append(item.toString());
				else
					result.append(delimiter + item.toString());
				idx++;
			}
		} else {
			return "";
		}

		return result.toString();
	}

    /**
	 * Get string that first capital charater
	 * @param data
	 * @return
	 */
	public static String firstCapitalize(String data) {
		String result = null;

		if (data != null && data.length() > 0) {
			result = data.substring(0,1).toUpperCase();
			if (data.length() > 1) result += data.substring(1).toLowerCase();
		} else {
			result = data;
		}

		return result;
	}
	
	/**
	 * If the input object is null, returns the empty string.
	 * @param data
	 * @return
	 */
	public static String nvl(Object data) {
		return nvl(data, "");
	}

	/**
	 * If the input object is null, returns default string.
	 * @param data
	 * @param defaultString
	 * @return
	 */
	public static String nvl(Object data, String defaultString) {
		String result = defaultString;
		if (data != null) {
			result = data.toString();
		}
		return result;
	}
	
	/**
	 * Indicates whether the specified string is null or an Empty string.
	 * @param data
	 * @return
	 */
	public static Boolean isNullOrEmpty(Object data) {
		Boolean result = false;
		
		if (data == null || String.valueOf(data).isEmpty())
			result = true;
		
		return result;
	}
	
	/**
	 * Left padding
	 * @param source
	 * @param alter
	 * @param length
	 * @return
	 */
	public static String lpad(String source, String alter, Integer length) {
		String result = null;
		
		if (source.length() >= length) {
			return source;
		} else {
			result = source;
			for (int i = source.length(); i < length; i++) {
				result = alter + result;
			}
		}
		
		return result;
	}
	
	/**
	 * Right padding
	 * @param source
	 * @param alter
	 * @param length
	 * @return
	 */
	public static String rpad(String source, String alter, Integer length) {
		String result = null;
		
		if (source.length() >= length) {
			return source;
		} else {
			result = source;
			for (int i = source.length(); i < length; i++) {
				result = result + alter;
			}
		}
		
		return result;
	}
	
	/**
	 * Convert item with quotation mark
	 * @param source
	 * @param delimiter
	 * @param quotation
	 * @return
	 */
	public static String convertWithDelimiter(String source, String delimiter, String quotation) {
		String result = "";
		if (StringUtil.isNullOrEmpty(source)) {
			return null;
		} else {
			String[] sourceList = source.split(delimiter);
			for (int i = 0; i < sourceList.length; i++) {
				result += quotation + sourceList[i] + quotation + delimiter;
			}
			result = result.substring(0, result.length() - 1);
		}
		
		return result;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Check format
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Check input data string was number format
	 * @param data
	 * @return
	 */
	public static boolean isNumber(String data) {
		if (data == null) {
			return false;
		}
		if (data.isEmpty()) {
			return false;
		}
		int i = 0;
		if (data.charAt(0) == '-') {
			if (data.length() == 1) {
				return false;
			}
			i = 1;
		}
		for (; i < data.length(); i++) {
			char c = data.charAt(i);
			if (c < '0' || c > '9') {
				return false;
			}
		}
		return true;
	}

}
