package com.knulf.core.util.xml;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.knulf.core.util.TypeUtil;

/**
 * Xml serialize base class
 * This class only support List<Object>, HashMap<String, Object> in generic type
 * @author Hyounseok Ohe
 *
 */
public abstract class XmlObjectBase {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// MEMBER
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// CONSTRUCTER & INITIALIZE
	////////////////////////////////////////////////////////////////////////////////////////////////////

	

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// getter & setter
	////////////////////////////////////////////////////////////////////////////////////////////////////

	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Xml Deserialize
	////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Load object with xml string
	 */
	public void load(String xml) throws Exception {
		DocumentBuilder docBuild = null;
		Document xmlDoc = null;
		NodeList nodes = null;
		java.lang.reflect.Field field = null;
		
		docBuild = XmlUtil.getXmlDocumentBuilder();
		xmlDoc = XmlUtil.getXmlDocument(docBuild, xml);
		
		nodes = xmlDoc.getDocumentElement().getChildNodes();
		for (int i=0; i<nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == 1) {
				field = this.getClass().getField(nodes.item(i).getNodeName());
				switch (getItemObjectType(field.getType(), field.getGenericType())) {
					case Base:
						field.set(this, TypeUtil.toBasicObject(field.getType(), nodes.item(i).getTextContent()));
						break;
					case Simple:
						field.set(this, this.getItemObject(nodes.item(i)));
						break;
					case List:
						field.set(this, this.getListObject(nodes.item(i)));
						break;
					case HashMap:
						field.set(this, this.getHashMapObject(nodes.item(i)));
						break;
				}
			}
		}
	}

	/**
	 * Get item object (General object)
	 * @param itemNode
	 * @return
	 * @throws Exception
	 */
	private Object getItemObject(Node itemNode) {
		Object itemObject = null;
		Class<?> itemClass = null; 
		Node fieldNode = null;
		
		try {
			itemClass = Class.forName(this.getClass().getPackage().getName() + "." + itemNode.getNodeName());
			itemObject = (Object)itemClass.newInstance();
			
			java.lang.reflect.Field[] fields = itemObject.getClass().getFields();
			for (java.lang.reflect.Field field : fields) {
				fieldNode = XmlXPathUtil.evaluateNode("./" + field.getName(), itemNode);
				if (fieldNode != null) {
					switch (getItemObjectType(field.getType(), field.getGenericType())) {
						case Base:
							field.set(this, TypeUtil.toBasicObject(field.getType(), fieldNode.getTextContent()));
							break;
						case Simple:
							field.set(itemObject, this.getItemObject(fieldNode));
							break;
						case List:
							field.set(itemObject, this.getListObject(fieldNode));
							break;
						case HashMap:
							field.set(itemObject, this.getHashMapObject(fieldNode));
							break;
					}
				}
			}
		} catch (Exception ex) {
			itemObject = null;
		}
		
		return itemObject;
	}
	
	/**
	 * Get List object (List<?> object)
	 * @param itemNode
	 * @return
	 * @throws Exception
	 */
	private List<Object> getListObject(Node itemNode) {
		List<Object> itemList = new ArrayList<Object>();
		Object itmeObject = null;
		
		try {
			for (int i=0; i<itemNode.getChildNodes().getLength(); i++) {
				if (itemNode.getChildNodes().item(i).getNodeType() == 1) {
					itmeObject = this.getItemObject(itemNode.getChildNodes().item(i));
					if (itmeObject != null) itemList.add(itmeObject);
				}
			}
		} catch (Exception ex) {
			itemList = null;
		}
		
		return itemList;
	}
	
	/**
	 * Get HashMap object (HashMap<String, Object> object)
	 * @param itemNode
	 * @return
	 * @throws Exception
	 */
	private HashMap<String, Object> getHashMapObject(Node itemNode) {
		HashMap<String, Object> itemList = new HashMap<String, Object>();
		Object itmeObject = null;
		
		try {
			for (int i=0; i<itemNode.getChildNodes().getLength(); i++) {
				if (itemNode.getChildNodes().item(i).getNodeType() == 1) {
					itmeObject = this.getItemObject(itemNode.getChildNodes().item(i));
					if (itmeObject != null) itemList.put(String.valueOf(i), itmeObject);
				}
			}
		} catch (Exception ex) {
			itemList = null;
		}
		
		return itemList;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Xml Serialize
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Save xml string from Object
	 * @return
	 * @throws Exception
	 */
	public String save() throws Exception {
		DocumentBuilder docBuild = XmlUtil.getXmlDocumentBuilder();
		Document xmlDoc = docBuild.newDocument();
		
		xmlDoc.appendChild(XmlUtil.getXmlNode(xmlDoc, this.getClass().getSimpleName()));
		
		Field[] fields = this.getClass().getFields();
		for (Field field : fields) {
			xmlDoc.getDocumentElement().appendChild(this.getFieldNode(xmlDoc, this, field));
		}
		
		return XmlUtil.getXmlString(xmlDoc);
	}
	
	/**
	 * Get item object node
	 * @param xmlDoc
	 * @param nodeName
	 * @param itemObject
	 * @return
	 */
	private Node getItemObjectNode(Document xmlDoc, String nodeName, Object itemObject) {
		Node itemNode = null;
		Node subNode = null;
		
		try {
			itemNode = XmlUtil.getXmlNode(xmlDoc, nodeName);
			
			Field[] fields = itemObject.getClass().getFields();
			for (Field field : fields) {
				subNode = this.getFieldNode(xmlDoc, itemObject, field);
				if (subNode != null) itemNode.appendChild(subNode);
			}
		} catch (Exception ex) {
			itemNode = null;
		}
		
		return itemNode;
	}
	
	/**
	 * Get List object node
	 * @param xmlDoc
	 * @param nodeName
	 * @param itemList
	 * @return
	 */
	private Node getListObjectNode(Document xmlDoc, String nodeName, List<Object> itemList) {
		Node itemNode = null;
		Node subNode = null;
		
		try {
			itemNode = XmlUtil.getXmlNode(xmlDoc, nodeName);
			if (itemList.size() > 0) {
				for (int i=0; i<itemList.size(); i++) {
					subNode = this.getItemObjectNode(xmlDoc, itemList.get(i).getClass().getSimpleName(), itemList.get(i));
					if (subNode != null) itemNode.appendChild(subNode);
				}
			}
		} catch (Exception ex) {
			itemNode = null;
		}
		
		return itemNode;
	}
	
	/**
	 * Get HashMap object node
	 * @param xmlDoc
	 * @param nodeName
	 * @param itemList
	 * @return
	 */
	private Node getHashObjectNode(Document xmlDoc, String nodeName, HashMap<String, Object> itemList) {
		Node itemNode = null;
		Node subNode = null;

		try {
			itemNode = XmlUtil.getXmlNode(xmlDoc, nodeName);
			if (itemList.size() > 0) {
				for (String key : itemList.keySet()) {
					subNode = this.getItemObjectNode(xmlDoc, itemList.get(key).getClass().getSimpleName(), itemList.get(key));
					if (subNode != null) itemNode.appendChild(subNode);
				}
			}
		} catch (Exception ex) {
			itemNode = null;
		}
		
		return itemNode;
	}
	
	/**
	 * Get item field node
	 * @param xmlDoc
	 * @param itemObject
	 * @param field
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Node getFieldNode(Document xmlDoc, Object itemObject, java.lang.reflect.Field field) {
		Node itemNode = null;
		
		try {
			switch (getItemObjectType(field.getType(), field.getGenericType())) {
			case Base:
				itemNode = XmlUtil.getXmlNode(xmlDoc, field.getName(), String.valueOf(field.get(itemObject)));
				break;
			case Simple:
				itemNode = this.getItemObjectNode(xmlDoc, field.getName(), field.get(itemObject));
				break;
			case List:
				itemNode = this.getListObjectNode(xmlDoc, field.getName(), (List<Object>)field.get(itemObject));
				break;
			case HashMap:
				itemNode = this.getHashObjectNode(xmlDoc, field.getName(), (HashMap<String, Object>)field.get(itemObject));
				break;
		}
			
		} catch (Exception ex) {
			itemNode = null;
		}
		
		return itemNode;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Library
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Get item object type
	 * @return
	 */
	private XML_OBJECT_TYPE getItemObjectType(Class<?> type, java.lang.reflect.Type genericType) {
		XML_OBJECT_TYPE itemObjectType = XML_OBJECT_TYPE.Base;
		ParameterizedType paramType = null;
		
		if (genericType instanceof ParameterizedType) {
			paramType = (ParameterizedType)genericType;
	    	if (paramType.getRawType().toString().equals("interface java.util.List")) {
	    		itemObjectType = XML_OBJECT_TYPE.List;
	    	} else if (paramType.getRawType().toString().equals("class java.util.HashMap")) {
	    		itemObjectType = XML_OBJECT_TYPE.HashMap;
	    	}
	    } else {
	    	if (TypeUtil.isBasicType(type)) {
	    		itemObjectType = XML_OBJECT_TYPE.Base;
	    	} else {
	    		itemObjectType = XML_OBJECT_TYPE.Simple;
	    	}
	    }
		
		return itemObjectType;
	}
	
}
