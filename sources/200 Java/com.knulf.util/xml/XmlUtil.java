package com.knulf.core.util.xml;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

import com.knulf.core.util.TypeUtil;

public class XmlUtil {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// MEMBER
	////////////////////////////////////////////////////////////////////////////////////////////////////

	

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// CONSTRUCTER & INITIALIZE
	////////////////////////////////////////////////////////////////////////////////////////////////////



	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Object xml
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Get list xml
	 * @param listObject
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static String getConcurrentHashMapListXml(Object listObject) throws Exception {
		if (listObject instanceof ConcurrentHashMap) {
			String parentNodeName = null;
			DocumentBuilder docBuild = XmlUtil.getXmlDocumentBuilder();
			Document xmlDoc = docBuild.newDocument();
			
			// Get sub nodes
			List<Node> subNodes = new ArrayList<Node>();
			Iterator<Entry<String, Object>> it = ((ConcurrentHashMap<String, Object>)listObject).entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, Object> item = it.next();
				Object itemObject = item.getValue();
				subNodes.add(getItemNode(xmlDoc, itemObject));
				if (parentNodeName == null) parentNodeName = itemObject.getClass().getSimpleName();
			}
			
			// Make root node
			xmlDoc.appendChild(XmlUtil.getXmlNode(xmlDoc, parentNodeName + "s"));
			
			// Append sub node
			if (subNodes.size() > 0) {
				for(Node node : subNodes) {
					xmlDoc.getDocumentElement().appendChild(node);
				}
			}
			
			return XmlUtil.getXmlString(xmlDoc);
		} else {
			return null;
		}
	}
	
	/**
	 * Get item node xml
	 * @param itemObject
	 * @return
	 * @throws Exception
	 */
	public static String getItemNodeXml(Object itemObject) throws Exception {
		DocumentBuilder docBuild = XmlUtil.getXmlDocumentBuilder();
		Document xmlDoc = docBuild.newDocument();
		
		xmlDoc.appendChild(getItemNode(xmlDoc, itemObject));
		
		return XmlUtil.getXmlString(xmlDoc);
	}
	
	/**
	 * Get object item node
	 * @param xmlDoc
	 * @param itemObject
	 * @return
	 * @throws Exception
	 */
	public static Node getItemNode(Document xmlDoc, Object itemObject) throws Exception {
		Node itemNode = XmlUtil.getXmlNode(xmlDoc, itemObject.getClass().getSimpleName());
		
		Field[] fields = itemObject.getClass().getFields();
		for (int i=0; i<fields.length; i++) {
			if (fields[i].get(itemObject) != null)
				itemNode.appendChild(XmlUtil.getXmlNode(xmlDoc, fields[i].getName(), String.valueOf(fields[i].get(itemObject))));
		}
		
		return itemNode;
	}
	
	/**
	 * Get object from xml string
	 * @param xml
	 * @param itemType
	 * @return
	 * @throws Exception
	 */
	public static Object getItemObject(String xml, Class<?> itemType) throws Exception {
		DocumentBuilder docBuild = XmlUtil.getXmlDocumentBuilder();
		Document xmlDoc = docBuild.parse(new org.xml.sax.InputSource(new StringReader(xml)));
		
		Object itemObject = getItemObject(xmlDoc.getDocumentElement(), itemType);
		
		return itemObject;
	}
	
	/**
	 * Get object from xml node
	 * @param itemNode
	 * @param itemType
	 * @return
	 * @throws Exception
	 */
	public static Object getItemObject(Node itemNode, Class<?> itemType) throws Exception {
		Object itemObject = (Object)itemType.newInstance();
		
		Field[] fields = itemObject.getClass().getFields();
		for (int i=0; i<fields.length; i++) {
			String fieldValue = XmlXPathUtil.evaluate("./" + fields[i].getName(), itemNode);
			if (fieldValue != null) {
				fields[i].set(itemObject, TypeUtil.toBasicObject(fields[i].getType(), fieldValue));
			}
		}
		
		return itemObject;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// PUBLIC
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Get XML document builder instance
	 * @return
	 * @throws ParserConfigurationException
	 */
	public static DocumentBuilder getXmlDocumentBuilder() throws ParserConfigurationException {
		DocumentBuilderFactory docBuildFact = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuild = docBuildFact.newDocumentBuilder();
		return docBuild;
	}
	
	/**
	 * Get XML document
	 * @return
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public static Document getXmlDocument() throws SAXException, IOException, ParserConfigurationException {
		return getXmlDocument(null, null);
	}
	
	/**
	 * Get XML document
	 * @param xml
	 * @return
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public static Document getXmlDocument(String xml) throws SAXException, IOException, ParserConfigurationException {
		return getXmlDocument(null, xml);
	}
	
	/**
	 * Get XML document
	 * @param docBuild
	 * @param xml
	 * @return
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException 
	 */
	public static Document getXmlDocument(DocumentBuilder docBuild, String xml) throws SAXException, IOException, ParserConfigurationException {
		
		if (docBuild == null) docBuild = getXmlDocumentBuilder();
		
		Document xmlDoc = null;
		
		if (xml == null)
			xmlDoc = docBuild.newDocument();
		else
			xmlDoc = docBuild.parse(new org.xml.sax.InputSource(new StringReader(xml)));
			
		return xmlDoc;
	}
	
	/**
	 * Get XML XPath instance
	 * @return
	 */
	public static XPath getXmlXPath() {
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		return xpath;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Xml Node
	////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get XML node by document, element name
	 * @param xmlDoc
	 * @param elementName
	 * @return
	 */
	public static Node getXmlNode(Document xmlDoc, String elementName) {
		return getXmlNode(xmlDoc, elementName, null);
	}
	
	/**
	 * Get XML node by document, element name, node text
	 * node text will be escaped
	 * @param xmlDoc
	 * @param elementName
	 * @param value
	 * @return
	 */
	public static Node getXmlNode(Document xmlDoc, String elementName, String value) {
		Node node = xmlDoc.createElement(elementName);
		if (value != null && value != "") {
			node.setTextContent(getXmlEncode(value));
		}
		return node;
	}
	
	/**
	 * Get XML node from other document node
	 * @param xmlDoc
	 * @param targetNode
	 * @return
	 */
	public static Node getXmlNodeOtherDocument(Document xmlDoc, Node targetNode) {
		Node node = xmlDoc.adoptNode(targetNode).cloneNode(true);
		return node;
	}
	
	/**
	 * Get XML Single node by node name
	 * @param xmlDoc
	 * @param elementName
	 * @return
	 */
	public static Node getXmlSingleNode(Document xmlDoc, String elementName) {
		Node node = null;
		XPath xpath = null;
		
		try {
			xpath = XmlUtil.getXmlXPath();
			node = getXmlSingleNode( xmlDoc, xpath, elementName);
		} catch (Exception ex) {
			node = null;
		} finally {
			xpath = null;
		}
		
		return node;
	}
	
	/**
	 * Get XML Single node by node name
	 * @param xmlDoc
	 * @param xpath
	 * @param elementName
	 * @return
	 */
	public static Node getXmlSingleNode(Document xmlDoc, XPath xpath, String elementName) {
		return XmlXPathUtil.evaluateNode("./" + elementName, xmlDoc.getDocumentElement());
	}
	
	/**
	 * Remove all child node
	 * @param node
	 */
	public static void removeAllSubNode(Node node) {
		if (node.hasChildNodes()) {
			for (int i=0; i<node.getChildNodes().getLength(); i++) {
				Node subNode = node.getChildNodes().item(i);
				if (subNode.hasChildNodes()) {
					removeAllSubNode(subNode);
					node.removeChild(subNode);
				} else {
					node.removeChild(subNode);
				}
			}
		}
	}
	
	/**
	 * Set attribute to node
	 * @param node
	 * @param name
	 * @param value
	 */
	public static void setAttribute(Node node, String name, String value) {
		((Element)node).setAttribute(name, value);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Xml Node
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Get XML string from xml document
	 * @param xmlDoc
	 * @return
	 * @throws Exception
	 */
	public static String getXmlString(Document xmlDoc) throws Exception {
		String xmlString = null;
		
		if (xmlDoc == null) {
			xmlString = null;
		} else {
			//xmlDoc.setXmlStandalone(true);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer t = tf.newTransformer();
			t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			t.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			
			StringWriter sw = new StringWriter();
			t.transform(new DOMSource(xmlDoc), new StreamResult(sw));
			
			xmlString = sw.getBuffer().toString();
		}
		
		return xmlString;
	}
	
	/**
	 * Get XML node string with xml format
	 * @param node
	 * @return
	 */
	public static String getInnerXml(Node node) throws Exception {
		String xml = null;
		if (node != null) {
			try {
				/**
				StringWriter sw = new StringWriter();
				Transformer t = TransformerFactory.newInstance().newTransformer();
				t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
				t.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
				t.setOutputProperty(OutputKeys.INDENT, "yes");
				t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
				t.transform(new DOMSource(node.getChildNodes().item(0)), new StreamResult(sw));
				xml = sw.toString();
				**/
				/**/
				DOMImplementationLS lsImpl = (DOMImplementationLS)node.getOwnerDocument().getImplementation().getFeature("LS", "3.0");
			    LSSerializer lsSerializer = lsImpl.createLSSerializer();
			    lsSerializer.getDomConfig().setParameter("xml-declaration", false);
			    NodeList childNodes = node.getChildNodes();
			    StringBuilder sb = new StringBuilder();
			    for (int i = 0; i < childNodes.getLength(); i++) {
			       sb.append(lsSerializer.writeToString(childNodes.item(i)));
			    }
				xml = sb.toString();
				/**/
			} catch (Exception ex) {
				xml = node.getTextContent();
			}
		}
		return xml;
	 }
	
	/**
	 * Get XML node string 
	 * @param node
	 * @return
	 * @throws Exception
	 */
	public static String getInnerText(Node node) throws Exception {
		String result = null;
		
		if (node != null)
			result = node.getTextContent();
		
		return result;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Xml encoding & decoding
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static String getXmlTagEncode(String value) {
		String result = value;
		if (value != null) {
			result = value.trim().replaceAll("[\\s\\t\\`\\~\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\_\\[\\{\\]\\}\\:\\;\\'\\\"\\,\\<\\.\\>\\/\\?]", "_");
		}
		return result;
	}
	
	/**
	 * Get escape xml string
	 * @param value
	 * @return
	 */
	public static String getXmlEncode(String value) {
		if (value == null || value.isEmpty()) {
			return value;
		}
		else {
			String returnString = value;
			returnString = returnString.replace("&", "&amp;");
			returnString = returnString.replace(">", "&gt;");
			returnString = returnString.replace("<", "&lt;");
			returnString = returnString.replace("&#0;", "");
			returnString = returnString.replace(";amp;", ";");
			
			return returnString;
		}
	}
	
	/**
	 * Get unescape xml string
	 * @param value
	 * @return
	 */
	public static String getXmlDecode(String value) {
		if (value == null || value.isEmpty()) {
			return value;
		}
		else {
			String returnString = value;
			returnString = returnString.replace("&apos;", "'");
			returnString = returnString.replace("&quot;", "\"");
			returnString = returnString.replace("&gt;", ">");
			returnString = returnString.replace("&lt;", "<");
			returnString = returnString.replace("&amp;", "&");
			return returnString;
		}
	}
}