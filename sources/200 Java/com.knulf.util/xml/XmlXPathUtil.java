package com.knulf.core.util.xml;

import java.util.Iterator;

import org.apache.commons.jxpath.CompiledExpression;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.JXPathNotFoundException;
import org.apache.commons.jxpath.Pointer;
import org.w3c.dom.Node;

/**
 * XML XPath helper
 * @author Hyounseok Ohe
 *
 */
public class XmlXPathUtil {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// XPath
	////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get string by xpath
	 * @param xpath
	 * @param target
	 * @return
	 */
	public static String evaluate(String xpath, Node target) {
		return evaluate(xpath, JXPathContext.newContext(target));
	}

	/**
	 * Get string by xpath
	 * @param xpath
	 * @param context
	 * @return
	 */
	public static String evaluate(String xpath,  JXPathContext context) {
		String result = null;
		
		try {
			result = (String) context.getValue(xpath);
		} catch (JXPathNotFoundException ignored) {
		}

		return result;
	}

	/**
	 * Get pointer by xpath
	 * @param xpath
	 * @param target
	 * @return
	 */
	public static Pointer evaluatePointer(String xpath, Node target) {
		return evaluatePointer(xpath, JXPathContext.newContext(target));
	}

	/**
	 * Get pointer by xpath
	 * @param xpath
	 * @param context
	 * @return
	 */
	public static Pointer evaluatePointer(String xpath, JXPathContext context) {
		Pointer result = null;

		try {
			result = context.getPointer(xpath);
		} catch (JXPathNotFoundException ignored) {
		}

		return result;
	}

	/**
	 * Get node by xpath
	 * @param xpath
	 * @param target
	 * @return
	 */
	public static Node evaluateNode(String xpath, Node target) {
		return evaluateNode(xpath, JXPathContext.newContext(target));
	}

	/**
	 * Get node by xpath
	 * @param xpath
	 * @param context
	 * @return
	 */
	public static Node evaluateNode(String xpath, JXPathContext context) {
		Node result = null;

		try {
			Pointer ptr = context.getPointer(xpath);
			if (ptr != null) {
				result = (Node) ptr.getNode();
			}
		} catch (JXPathNotFoundException ignored) {
		}

		return result;
	}

	/**
	 * Get iterator strings by xpath
	 * @param xpath
	 * @param target
	 * @return
	 */
	public static Iterator<String> evaluateIterator(String xpath, Node target) {
		return evaluateIterator(xpath, JXPathContext.newContext(target));
	}

	/**
	 * Get iterator strings by xpath
	 * @param xpath
	 * @param context
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Iterator<String> evaluateIterator(String xpath, JXPathContext context) {
		Iterator<String> result = null;

		try {
			result = context.iterate(xpath);
		} catch (JXPathNotFoundException ignored) {
		}

		return result;
	}

	/**
	 * Get iterator strings by evaluate compiledExpression
	 * @param expr
	 * @param target
	 * @return
	 */
	public static Iterator<String> evaluateIterator(CompiledExpression expr, Node target) {
		return evaluateIterator(expr, JXPathContext.newContext(target));
	}

	/**
	 * Get iterator strings by evaluate compiledExpression
	 * @param expr
	 * @param context
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Iterator<String> evaluateIterator(CompiledExpression expr, JXPathContext context) {
		Iterator<String> result = null;

		try {
			result = expr.iterate(context);
		} catch (JXPathNotFoundException ignored) {
		}

		return result;
	}

	/**
	 * Get iterator pointers by evaluate xpath
	 * @param xpath
	 * @param target
	 * @return
	 */
	public static Iterator<Pointer> evaluateIteratorPointers(String xpath, Node target) {
		return evaluateIteratorPointers(xpath, JXPathContext.newContext(target));
	}

	/**
	 * Get iterator pointers by evaluate xpath
	 * @param xpath
	 * @param context
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Iterator<Pointer> evaluateIteratorPointers(String xpath, JXPathContext context) {
		Iterator<Pointer> result = null;

		try {
			result = context.iteratePointers(xpath);
		} catch (JXPathNotFoundException ignored) {
		}

		return result;
	}

	/**
	 * Get iterator pointers by evaluate xpath
	 * @param expr
	 * @param target
	 * @return
	 */
	public static Iterator<Pointer> evaluateIteratorPointers(CompiledExpression expr, Node target) {
		return evaluateIteratorPointers(expr, JXPathContext.newContext(target));
	}

	/**
	 * Get iterator pointers by evaluate xpath
	 * @param expr
	 * @param context
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Iterator<Pointer> evaluateIteratorPointers(CompiledExpression expr, JXPathContext context) {
		Iterator<Pointer> result = null;

		try {
			result = expr.iteratePointers(context);
		} catch (JXPathNotFoundException ignored) {
		}

		return result;
	}
}
