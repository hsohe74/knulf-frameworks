package com.knulf.core.util.xml;

import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class KeyValueListData {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// MEMBER
	////////////////////////////////////////////////////////////////////////////////////////////////////

	public HashMap<String, String> Items;

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// CONSTRUCTER & INITIALIZE
	////////////////////////////////////////////////////////////////////////////////////////////////////

	public KeyValueListData(String xml) throws Exception{
		this();
		this.load(xml);
	}

	public KeyValueListData(){
		Items = new HashMap<String, String>();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// METHOD
	////////////////////////////////////////////////////////////////////////////////////////////////////

	public void load(String xml) throws Exception {
		String key = null;
		String value = null;
		
		DocumentBuilder docBuild = XmlUtil.getXmlDocumentBuilder();
		Document xmlDoc = XmlUtil.getXmlDocument(docBuild, xml);
		
		NodeList nodes = xmlDoc.getDocumentElement().getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == 1) {
				NodeList subNodes = nodes.item(i).getChildNodes();
				for (int j = 0; j < subNodes.getLength(); j++) {
					if (subNodes.item(j).getNodeType() == 1) {
						if ("Key".equals(subNodes.item(j).getNodeName())) {
							key = subNodes.item(j).getTextContent();
						}
						if ("Value".equals(subNodes.item(j).getNodeName())) {
							value = subNodes.item(j).getTextContent();
						}
					}
				}
				Items.put(key, value);
			}
		}
	}
}
