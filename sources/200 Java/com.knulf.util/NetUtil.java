package com.knulf.core.util;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Network utilities
 * @author Hyounseok Ohe
 *
 */
public class NetUtil {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Common
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Get all ip of machine
	 * @return
	 * @throws SocketException
	 */
	public static List<String> getAllIp() throws SocketException {
		List<String> returnAllIp = new ArrayList<String>();

		Enumeration<NetworkInterface> nics = NetworkInterface.getNetworkInterfaces();
		while (nics.hasMoreElements()) {
			NetworkInterface nic = nics.nextElement();
			if (!nic.isUp() || nic.isVirtual()) {
				continue;
			}

			Enumeration<InetAddress> ipAddresses = nic.getInetAddresses();
			while (ipAddresses.hasMoreElements()) {
				InetAddress ipAddress = ipAddresses.nextElement();
				String ip = ipAddress.getHostAddress();
				if ("0.0.0.0".equals(ip) || "::0".equals(ip)) {
					continue;
				}
				returnAllIp.add(ip);
			}
		}

		return returnAllIp;
	}
	
	/**
	 * Whether IP is Loopback address
	 * @param ipAddress
	 * @return
	 */
	public static boolean isLoopbackIp(InetAddress ipAddress) {
		boolean result = false;

		if (ipAddress instanceof Inet4Address) {
			result = isLoopbackIp((Inet4Address) ipAddress);
		} else if (ipAddress instanceof Inet6Address) {
			result = isLoopbackIp((Inet6Address) ipAddress);
		}

		return result;
	}

	/**
	 * Whether IP is Loopback address
	 * @param ipAddress (IPv4)
	 * @return
	 */
	public static boolean isLoopbackIp(Inet4Address ipAddress) {
		boolean result = false;

		byte[] address = ipAddress.getAddress();

		if ((address[0] & 0x000000ff) == 0x7f) {
			// 127.*.*.*
			result = true;
		}

		return result;
	}

	/**
	 * Whether IP is Loopback address
	 * @param ipAddress (IPv6)
	 * @return
	 */
	public static boolean isLoopbackIp(Inet6Address ipAddress) {
		boolean result = false;

		byte[] address = ipAddress.getAddress();

		boolean isAllZero = true;
		for (byte abyte : address) {
			if (abyte != 0) {
				isAllZero = false;
				break;
			}
		}

		if (isAllZero && address[address.length - 1] == 1) {
			result = true;
		}

		return result;
	}
}
