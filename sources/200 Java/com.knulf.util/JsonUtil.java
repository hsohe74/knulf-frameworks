package com.knulf.core.util;

/**
 * Json utilities
 * @author Hyounseok Ohe
 *
 */
public class JsonUtil {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Common list
	////////////////////////////////////////////////////////////////////////////////////////////////////

	
	/**
	 * Get JSONArray from collection data
	 * @param data
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static org.codehaus.jettison.json.JSONArray getJSONArray(java.util.Collection data) {
		return new org.codehaus.jettison.json.JSONArray(data);
	}
}
