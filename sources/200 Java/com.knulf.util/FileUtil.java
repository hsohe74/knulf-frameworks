package com.knulf.core.util;

import java.io.*;
import java.nio.file.Paths;
import java.util.regex.Pattern;

import org.apache.commons.io.*;

import com.knulf.core.GlobalString;

/**
 * File utilities
 * @author Hyounseok Ohe
 *
 */
public class FileUtil {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Binary file handing
	////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Write binary file
	 * @param filePath
	 * @param inputStream
	 */
	public static void writeBinaryFile(String filePath, InputStream inputStream) {
		
		try {
			int read = 0;
			byte[] bytes = new byte[1024];
	
			OutputStream out = new FileOutputStream(new File(filePath));
			while ((read = inputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Text file handing
	////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Write text to file with UTF-8 format
	 * 
	 * @param filePath
	 * @param value
	 * @throws Exception
	 */
	public static void writeTextFile(String filePath, String value) throws Exception {
		writeTextFile(filePath, value, false, GlobalString.CHARSET_NAME_UTF_8);
	}

	/**
	 * Write text to file
	 * @param filePath
	 * @param value
	 * @param isAppend
	 * @throws Exception
	 */
	public static void writeTextFile(String filePath, String value, boolean isAppend) throws Exception {
		writeTextFile(filePath, value, isAppend, GlobalString.CHARSET_NAME_UTF_8);
	}

	/**
	 * Write text to file
	 * @param filePath
	 * @param value
	 * @param encoding
	 * @throws Exception
	 */
	public static void writeTextFile(String filePath, String value, String encoding) throws Exception {
		writeTextFile(filePath, value, false, encoding);
	}

	/**
	 * Write text to file
	 * @param filePath
	 * @param value
	 * @param isAppend
	 * @param encoding
	 * @throws Exception
	 */
	public static void writeTextFile(String filePath, String value, boolean isAppend, String encoding) throws Exception {
		if (filePath == null) {
			throw new NullPointerException("filename can't be null");
		}

		File file = new File(filePath);
		String filepath = file.getAbsolutePath();
		BufferedOutputStream bos = null;

		try {
			bos = new BufferedOutputStream(new FileOutputStream(filepath, isAppend));
			bos.write(value.getBytes(encoding));
		} finally {
			if (bos != null) {
				bos.close();
			}
		}
	}
	
	/**
	 * Get text file's all text
	 * 
	 * @param filePath
	 * @return
	 * @throws IOException
	 */
	public static String readTextFile(String filePath) throws IOException {
		return readTextFile(new File(filePath));
	}
	
	/**
	 * Get text file's all text
	 * 
	 * @param filePath
	 * @param encoding (UTF-8)
	 * @return
	 * @throws IOException
	 */
	public static String readTextFile(String filePath, String encoding) throws IOException {
		return readTextFile(new File(filePath), encoding);
	}

	/**
	 * Get text file's all text
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static String readTextFile(File file) throws IOException {
		return readTextFile(file, GlobalString.CHARSET_NAME_UTF_8);
	}

	/**
	 * Get text file's all text
	 * 
	 * @param file
	 * @param encoding (UTF-8)
	 * @return
	 * @throws IOException
	 */
	public static String readTextFile(File file, String encoding) throws IOException {
		String returnString = null;
		String filepath = file.getAbsolutePath();
		FileInputStream fis = null;
		byte[] buffer = null;

		try {
			fis = new FileInputStream(filepath);
			buffer = new byte[fis.available()];
			fis.read(buffer);
			returnString = readTextFile(new ByteArrayInputStream(buffer), encoding);
		} finally {
			if (fis != null) {
				fis.close();
			}
		}		

		return returnString;
	}

	/**
	 * Get text file's all text
	 * @param input
	 * @param encoding (UTF-8)
	 * @return
	 * @throws IOException
	 */
	private static String readTextFile(InputStream input, String encoding) throws IOException {
		String returnString = null;
		BufferedReader br = null;
		StringBuilder sb = null;

		try {
			br = new BufferedReader(new InputStreamReader(input, encoding));
			sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				line = TypeUtil.removeUTF8BOM(line);
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			returnString = sb.toString();
		} finally {
			if (br != null) {
				br.close();
			}
		}

		return returnString;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// File system
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Get combined path
	 * @param first
	 * @param more
	 * @return
	 */
	public static String getCombinePath(String first, String... more) {
		return Paths.get(first, more).toString();
	}
	
	/**
	 * Get releative path
	 * @param targetPath
	 * @param basePath
	 * @param pathSeparator
	 * @return
	 */
	public static String getRelativePath(String targetPath, String basePath) {
        // Normalize the paths
        String normalizedBasePath = FilenameUtils.normalizeNoEndSeparator(basePath);
        String normalizedTargetPath = FilenameUtils.normalizeNoEndSeparator(targetPath);

        // Undo the changes to the separators made by normalization
        if (File.separator.equals("/")) {
            normalizedBasePath = FilenameUtils.separatorsToUnix(normalizedBasePath);
            normalizedTargetPath = FilenameUtils.separatorsToUnix(normalizedTargetPath);
        } else if (File.separator.equals("\\")) {
            normalizedBasePath = FilenameUtils.separatorsToWindows(normalizedBasePath);
            normalizedTargetPath = FilenameUtils.separatorsToWindows(normalizedTargetPath);
        } else {
            throw new IllegalArgumentException("Unrecognised dir separator '" + File.separator + "'");
        }

        // Set array of path
        String[] base = normalizedBasePath.split(Pattern.quote(File.separator));
        String[] target = normalizedTargetPath.split(Pattern.quote(File.separator));

        // First get all the common elements. Store them as a string,
        // and also count how many of them there are.
        StringBuffer common = new StringBuffer();
        int commonIndex = 0;
        while (commonIndex < target.length
        		&& commonIndex < base.length
        		&& target[commonIndex].equals(base[commonIndex])) {
            common.append(target[commonIndex] + File.separator);
            commonIndex++;
        }

        if (commonIndex == 0) {
        	return targetPath;
        }   

        // The number of directories we have to backtrack depends on whether the base is a file or a dir
        // For example, the relative path from
        //
        // /foo/bar/baz/gg/ff to /foo/bar/baz
        // 
        // ".." if ff is a file
        // "../.." if ff is a directory
        //
        // The following is a heuristic to figure out if the base refers to a file or dir. It's not perfect, because
        // the resource referred to by this path may not actually exist, but it's the best I can do
        boolean baseIsFile = true;

        File baseResource = new File(normalizedBasePath);
        if (baseResource.exists()) {
            baseIsFile = baseResource.isFile();

        } else if (basePath.endsWith(File.separator)) {
            baseIsFile = false;
        }

        StringBuffer relative = new StringBuffer();
        if (base.length != commonIndex) {
            int numDirsUp = baseIsFile ? base.length - commonIndex - 1 : base.length - commonIndex;

            for (int i = 0; i < numDirsUp; i++) {
                relative.append(".." + File.separator);
            }
        }
        relative.append(normalizedTargetPath.substring(common.length()));
        return relative.toString();
    }

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// File handling
	////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get file name from full file path
	 * 
	 * @param filepath
	 * @return
	 * @throws Exception
	 */
	public static String getFilename(String filepath) {
		String filename = null;

		try {
			File file = new File(filepath);
			filename = file.getName();
		} catch (Exception e) {
			filename = null;
		}

		return filename;
	}

	/**
	 * Get file name from File
	 * 
	 * @param file
	 * @return
	 * @throws Exception
	 */
	public static String getFilename(File file) {
		String filename = null;

		try {
			filename = file.getName();
		} catch (Exception e) {
			filename = null;
		}

		return filename;
	}

	/**
	 * Get file name without extension from file path
	 * 
	 * @param filepath
	 * @return
	 */
	public static String getFileNameWithoutExtension(String filepath) {
		String filename = null;

		try {
			filename = org.apache.commons.io.FilenameUtils.getBaseName(filepath);
		} catch (Exception e) {
			filename = null;
		}

		return filename;
	}

	/**
	 * Get file extension from file path
	 * 
	 * @param filepath
	 * @return
	 */
	public static String getFileExtension(String filePath) {
		String fileName = null;

		try {
			fileName = org.apache.commons.io.FilenameUtils.getExtension(filePath);
		} catch (Exception e) {
			fileName = null;
		}

		return fileName;
	}

	/**
	 * Get unique file name
	 * 
	 * @param filepath
	 * @return
	 */
	public static String getUniqueFilename(String filePath) {
		String uniqueFilePath = null;
		boolean isExist = false;
		File file = null;
		String directory = null;
		String fileName = null;
		String fileExtension = null;
		Integer renameIndex = 1;

		try {
			uniqueFilePath = filePath;
			directory = FileUtil.getDirectory(filePath).getPath();
			fileName = FileUtil.getFileNameWithoutExtension(filePath);
			fileExtension = FileUtil.getFileExtension(filePath);
			file = new File(uniqueFilePath);
			if (file.exists()) {
				isExist = true;
				while (isExist) {
					uniqueFilePath = directory + File.separator + fileName + "_(" + String.valueOf(renameIndex) + ")." + fileExtension;
					file = new File(uniqueFilePath);
					if (file.exists()) {
						isExist = true;
						renameIndex++;
					} else {
						isExist = false;
						break;
					}
				}
			} else {
				isExist = false;
				uniqueFilePath = filePath;
			}

		} catch (Exception e) {
			uniqueFilePath = null;
		}

		return uniqueFilePath;
	}
	
	/**
	 * Copy file
	 * @param sourceFilePath
	 * @param destinationFilePath
	 * @param isOverwrite
	 * @return
	 * @throws IOException
	 */
	public static Boolean copyFile(String sourceFilePath, String destinationFilePath, Boolean isOverwrite) throws IOException {
		Boolean result = true;

		File sourceFile = new File(sourceFilePath);
		File destinationFile = new File(destinationFilePath);
		
		if (destinationFile.exists()) {
			if (destinationFile.canWrite() == true && isOverwrite) {
				destinationFile.delete();
				FileUtils.copyFile(sourceFile, destinationFile);
			} else {
				result = false;
			}
		} else {
			FileUtils.copyFile(sourceFile, destinationFile);
		}
		
		return result;
	}
	
	/**
	 * Move file
	 * @param sourceFilePath
	 * @param destinationFilePath
	 * @param isOverwrite
	 * @return
	 * @throws IOException
	 */
	public static Boolean moveFile(String sourceFilePath, String destinationFilePath, Boolean isOverwrite) throws IOException {
		Boolean result = true;

		File sourceFile = new File(sourceFilePath);
		File destinationFile = new File(destinationFilePath);
		
		if (destinationFile.exists()) {
			if (destinationFile.canWrite() == true && isOverwrite) {
				destinationFile.delete();
				FileUtils.moveFile(sourceFile, destinationFile);
			} else {
				result = false;
			}
		} else {
			FileUtils.moveFile(sourceFile, destinationFile);
		}
		
		return result;
	}
	
	/**
	 * Delete file
	 * @param destinationFilePath
	 * @return
	 * @throws IOException
	 */
	public static Boolean deleteFile(String destinationFilePath) throws IOException {
		Boolean result = true;

		File destinationFile = new File(destinationFilePath);

		if (destinationFile.exists() && destinationFile.canWrite() == true) {
			destinationFile.delete();
		}
		
		return result;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Directory handling
	////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get directory by file path
	 * 
	 * @param filepath
	 * @return
	 */
	public static File getDirectory(String filePath) {
		File directory = null;

		try {
			File file = new File(filePath);
			directory = new File(file.getParent());
		} catch (Exception e) {
			directory = null;
		}

		return directory;
	}

	/**
	 * Get directory by file path
	 * 
	 * @param file
	 * @return
	 * @throws Exception
	 */
	public static File getDirectory(File file) {
		File directory = null;

		try {
			directory = new File(file.getParent());
		} catch (Exception e) {
			directory = null;
		}

		return directory;
	}
	
	/**
	 * @param directoryPath
	 * @return
	 */
	public static Boolean createDirectory(String directoryPath) {
		Boolean result = true;
		
		File directory = new File(directoryPath);
		result = directory.mkdir();
		
		return result;
	}
	
	/**
	 * @param directoryPath
	 * @return
	 * @throws IOException
	 */
	public static Boolean deleteDirectory(String directoryPath) throws IOException {
		Boolean result = true;
		
		File directory = new File(directoryPath);
		FileUtils.deleteDirectory(directory);
		
		return result;
	}
	
	/**
	 * Get sub directory list
	 * @param directoryPath
	 * @return
	 * @throws IOException
	 */
	public static File[] getSubDirectories(String directoryPath) throws IOException {
		File directory = new File(directoryPath);
		return directory.listFiles(new FileFilter() {
		    @Override
		    public boolean accept(File file) {
		        return file.isDirectory();
		    }
		});
	}
	
	/**
	 * Get directory's file list
	 * @param directoryPath
	 * @return
	 * @throws IOException
	 */
	public static File[] getFileListInDirectory(String directoryPath) throws IOException {
		File directory = new File(directoryPath);
		return directory.listFiles(new FileFilter() {
		    @Override
		    public boolean accept(File file) {
		        return file.isFile();
		    }
		});
	}
}
