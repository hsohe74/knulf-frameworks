package com.knulf.core.util;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * Image utilities
 * @author Hyounseok Ohe
 *
 */
public class ImageUtil {
	
	/**
	 * @param sourceFilePath
	 * @param targetFilePath
	 * @param baseWidth (720)
	 * @param baseHeight (1280)
	 * @param formatName (jpg)
	 * @throws IOException
	 */
	public static void createThumbImage(String sourceFilePath, String targetFilePath, int baseWidth, int baseHeight, String formatName) throws IOException
	{
		File sourceFile = new File(sourceFilePath);
		File targetFile = new File(targetFilePath);
		
		BufferedImage sourceImage = ImageIO.read(sourceFile);
		int sourceWidth = sourceImage.getWidth();
		int sourceHeight = sourceImage.getHeight();
		
		if (sourceWidth <= baseWidth && sourceHeight <= baseHeight) {
			FileUtil.copyFile(sourceFilePath, targetFilePath, true);
		} else {
			if (sourceWidth > baseWidth) {
				float widthRatio = ((float)baseWidth) / ((float)sourceWidth);
				sourceWidth = (int)(sourceWidth * widthRatio);
				sourceHeight = (int)(sourceHeight * widthRatio);
			}
			
			if (sourceHeight > baseHeight) {
				float heightRatio = ((float)baseHeight) / ((float)sourceHeight);
				sourceWidth = (int)(sourceWidth * heightRatio);
				sourceHeight = (int)(sourceHeight * heightRatio);
			}
			
			Image targetImage = sourceImage.getScaledInstance(sourceWidth, sourceHeight, Image.SCALE_SMOOTH);
			
			int pixels[] = new int[sourceWidth * sourceHeight]; 
		    PixelGrabber pg = new PixelGrabber(targetImage, 0, 0, sourceWidth, sourceHeight, pixels, 0, sourceWidth); 
		    try {
		        pg.grabPixels();
		    } catch (InterruptedException e) {
		        throw new IOException(e.getMessage());
		    } 
		    BufferedImage destImage = new BufferedImage(sourceWidth, sourceHeight, BufferedImage.TYPE_INT_RGB); 
		    destImage.setRGB(0, 0, sourceWidth, sourceHeight, pixels, 0, sourceWidth);
		    
		    ImageIO.write(destImage, formatName, targetFile);
	
		    // 이전 방식은 Quality가 떨어지는 경우가 발생
//			BufferedImage thumbImage = new BufferedImage(sourceWidth, sourceHeight, 1);
//			Graphics2D g2 = thumbImage.createGraphics();
//			g2.drawImage(sourceImage, 0, 0, sourceWidth, sourceHeight, null);
//			ImageIO.write(thumbImage, formatName, targetFile);
		}
	}
}
