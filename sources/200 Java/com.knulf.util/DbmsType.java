package com.knulf.core.util;

/**
 * DBMS type
 * @author Hyounseok Ohe
 *
 */
public enum DbmsType {
	DB2, ORACLE, MSSQL, POSTGRESQL, MYSQL, MARIADB, H2, CUBRID
}
