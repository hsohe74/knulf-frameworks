package com.knulf.core.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder.BorderSide;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExcelUtil {

    protected static final Logger LOG = LoggerFactory.getLogger(ExcelUtil.class);

    // ====================================================================================================
    // Excel
    // ====================================================================================================

    public static String saveExcelFile(List<Map<String, String>> list, String fileName, String sheetName,
            String title) {

        String successYn = "N";

        FileOutputStream outFile;

        HSSFRow row;

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(sheetName);

        row = sheet.createRow(0);
        row.createCell(0).setCellValue(title);

        Map<String, String> colMap = list.get(0);
        Set<String> key = colMap.keySet();

        row = sheet.createRow(1);
        int i = 0;
        for (Iterator<String> it = key.iterator(); it.hasNext();) {
            String keyName = it.next();
            row.createCell(i).setCellValue(keyName);
            i = i + 1;
        }

        for (int j = 0; j < list.size(); j++) {
            row = sheet.createRow(j + 2);
            Map<String, String> map = list.get(j);
            int k = 0;
            for (Iterator<String> it = key.iterator(); it.hasNext();) {
                String keyName = it.next();
                String valueName = String.valueOf(map.get(keyName));
                row.createCell(k).setCellValue(valueName);
                k = k + 1;
            }
        }

        try {
            File file = new File(fileName);
            outFile = new FileOutputStream(file);
            workbook.write(outFile);
            outFile.flush();
            outFile.close();
            successYn = "Y";

        } catch (Exception e) {
            LOG.debug("saveExcelFile");
        } finally {
            try {
                workbook.close();
            } catch (IOException e) {
                LOG.debug("saveExcelFile");
            }
        }
        return successYn;
    }

    /**
     * @Description 엑셀파일 저장
     * @param filePath
     * @param sheetName
     * @param excelColumns
     * @param excelData
     * @return
     * @throws Exception
     */
    public static boolean saveExcelXlsxFile(String filePath, String sheetName,
            LinkedHashMap<String, String> excelColumns, List<Map<String, Object>> excelData) throws Exception {
        return saveExcelXlsxFile(filePath, sheetName, excelColumns, excelData, true);
    }

    /**
     * @Description 엑셀파일 저장
     * @param filePath
     * @param sheetName
     * @param excelColumns
     * @param excelData
     * @param isHasHeader
     * @return
     * @throws Exception
     */
    @SuppressWarnings("resource")
    public static boolean saveExcelXlsxFile(String filePath, String sheetName,
            LinkedHashMap<String, String> excelColumns, List<Map<String, Object>> excelData, Boolean isHasHeader)
            throws Exception {

        boolean result = true;

        Integer rowIndex = 0;
        Integer columnIndex = 0;

        XSSFColor cellBorderColor = new XSSFColor(new java.awt.Color(0, 0, 0), new DefaultIndexedColorMap());
        XSSFColor cellBackColor = new XSSFColor(new java.awt.Color(255, 255, 255), new DefaultIndexedColorMap());

        // Workbook
        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFFont headerFont = workbook.createFont();
        headerFont.setFontName("돋움체");
        headerFont.setFontHeightInPoints((short) 10);
        headerFont.setColor(IndexedColors.WHITE.getIndex());

        XSSFCellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setBorderTop(BorderStyle.THIN);
        headerStyle.setBorderColor(BorderSide.TOP, cellBorderColor);
        headerStyle.setBorderRight(BorderStyle.THIN);
        headerStyle.setBorderColor(BorderSide.RIGHT, cellBorderColor);
        headerStyle.setBorderBottom(BorderStyle.THIN);
        headerStyle.setBorderColor(BorderSide.BOTTOM, cellBorderColor);
        headerStyle.setBorderLeft(BorderStyle.THIN);
        headerStyle.setBorderColor(BorderSide.LEFT, cellBorderColor);
        headerStyle.setFillForegroundColor(IndexedColors.GREY_80_PERCENT.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        headerStyle.setFont(headerFont);

        XSSFFont cellFont = workbook.createFont();
        cellFont.setFontName("돋움체");
        cellFont.setFontHeightInPoints((short) 10);

        XSSFCellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderColor(BorderSide.TOP, cellBorderColor);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderColor(BorderSide.RIGHT, cellBorderColor);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderColor(BorderSide.BOTTOM, cellBorderColor);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setBorderColor(BorderSide.LEFT, cellBorderColor);
        cellStyle.setFillBackgroundColor(cellBackColor);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyle.setFont(cellFont);

        // Sheet
        XSSFSheet sheet = workbook.createSheet(sheetName);

        // Header row
        if (isHasHeader) {
            if (excelColumns != null && excelColumns.size() > 0) {
                rowIndex++;

                XSSFRow headerRow = sheet.createRow(0);
                for (String key : excelColumns.keySet()) {
                    XSSFCell cell = headerRow.createCell(columnIndex);
                    cell.setCellValue(excelColumns.get(key));
                    cell.setCellStyle(headerStyle);
                    CellUtil.setAlignment(cell, HorizontalAlignment.CENTER);
                    columnIndex++;
                }
            } else {
                throw new Exception("헤더정보가 없습니다.");
            }
        }

        // Cell data
        if (excelData.size() > 0) {
            for (int j = 0; j < excelData.size(); j++) {
                XSSFRow dataRow = sheet.createRow(j + rowIndex);
                Map<String, Object> excelRow = excelData.get(j);

                columnIndex = 0;
                for (String key : excelColumns.keySet()) {
                    XSSFCell cell = dataRow.createCell(columnIndex);
                    if (excelRow.containsKey(key) && excelRow.get(key) != null) {
                        cell.setCellValue(String.valueOf(excelRow.get(key)));
                    } else {
                        cell.setCellValue("");
                    }
                    cell.setCellStyle(cellStyle);

                    columnIndex++;
                }
            }
        } else {
            throw new Exception("데이터가 없습니다.");
        }

        // Sheet
        sheet.setDefaultColumnWidth(12);
        for (int i = 0; i < excelColumns.size(); i++) {
            sheet.autoSizeColumn(i);
        }

        // Save file
        FileOutputStream fileOutputStream = new FileOutputStream(filePath);
        workbook.write(fileOutputStream);

        return result;
    }

    @SuppressWarnings("resource")
    public static List<Object> readExcelFile(String filePath, Boolean isHasHeader, Class<?> itemType) throws Exception {

        // Result
        List<Object> result = new ArrayList<Object>();

        // Read excel by type
        if ("XLS".equals(FileUtil.getFileExtension(filePath).toUpperCase())) {

            // Workbook
            FileInputStream fileInputStream = new FileInputStream(filePath);
            HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);

            // Sheet
            HSSFSheet sheet = workbook.getSheetAt(0);

            // Row count
            Integer rowCount = sheet.getPhysicalNumberOfRows();

            // Read
            Integer startIndex = (isHasHeader ? 1 : 0);
            for (int i = startIndex; i < rowCount; i++) {
                HSSFRow row = sheet.getRow(i);
                if (row != null) {

                    Constructor<?> constructor = itemType.getConstructor();
                    Object itemObject = constructor.newInstance();
                    Field[] fields = itemObject.getClass().getFields();

                    int cellCount = row.getPhysicalNumberOfCells();
                    for (int j = 0; j < cellCount; j++) {
                        HSSFCell cell = row.getCell(j);
                        String cellValue = "";
                        if (cell != null) {
                            switch (cell.getCellType()) {
                            case FORMULA:
                                cellValue = cell.getCellFormula();
                                break;
                            case NUMERIC:
                                cellValue = String.valueOf(cell.getNumericCellValue());
                                break;
                            case STRING:
                                cellValue = cell.getStringCellValue();
                                break;
                            case BLANK:
                                cellValue = String.valueOf("");
                                break;
                            case ERROR:
                                cellValue = String.valueOf(cell.getErrorCellValue());
                                break;
                            default:
                                cellValue = cell.getStringCellValue();
                                break;
                            }
                        }
                        if (fields.length > j) {
                            fields[j].set(itemObject, TypeUtil.toBasicObject(fields[j].getType(), cellValue));
                        }
                    }

                    result.add(itemObject);
                }
            }

        } else if ("XLSX".equals(FileUtil.getFileExtension(filePath).toUpperCase())) {

            // Workbook
            FileInputStream fileInputStream = new FileInputStream(filePath);
            XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);

            // Sheet
            XSSFSheet sheet = workbook.getSheetAt(0);

            // Row count
            Integer rowCount = sheet.getPhysicalNumberOfRows();

            // Read
            Integer startIndex = (isHasHeader ? 1 : 0);
            for (int i = startIndex; i < rowCount; i++) {
                XSSFRow row = sheet.getRow(i);
                if (row != null) {

                    Constructor<?> constructor = itemType.getConstructor();
                    Object itemObject = constructor.newInstance();
                    Field[] fields = itemObject.getClass().getFields();

                    int cellCount = row.getPhysicalNumberOfCells();
                    for (int j = 0; j < cellCount; j++) {
                        XSSFCell cell = row.getCell(j);
                        String cellValue = "";
                        if (cell != null) {
                            switch (cell.getCellType()) {
                            case FORMULA:
                                cellValue = cell.getCellFormula();
                                break;
                            case NUMERIC:
                                cellValue = String.valueOf(cell.getNumericCellValue());
                                break;
                            case STRING:
                                cellValue = cell.getStringCellValue();
                                break;
                            case BLANK:
                                cellValue = String.valueOf("");
                                break;
                            case ERROR:
                                cellValue = String.valueOf(cell.getErrorCellValue());
                                break;
                            default:
                                cellValue = cell.getStringCellValue();
                                break;
                            }
                        }
                        if (fields.length > j) {
                            fields[j].set(itemObject, TypeUtil.toBasicObject(fields[j].getType(), cellValue));
                        }
                    }

                    result.add(itemObject);
                }
            }

        } else {
            throw new Exception(
                    "Not support old excel type (" + FileUtil.getFileExtension(filePath).toUpperCase() + ").");
        }

        return result;
    }

    // ====================================================================================================
    // CSV
    // ====================================================================================================

    public static void saveCsvFile(List<Map<String, String>> list, String fileName) throws Exception {

        FileOutputStream outFile;

        HSSFWorkbook workbook = new HSSFWorkbook();
        StringBuffer csvData = new StringBuffer("");

        Map<String, String> colMap = list.get(0);
        Set<String> key = colMap.keySet();

        for (Iterator<String> it = key.iterator(); it.hasNext();) {
            String keyName = it.next();
            if (it.hasNext()) {
                csvData.append("\"").append(keyName).append("\"").append(",");
            } else {
                csvData.append("\"").append(keyName).append("\"");
            }
        }
        csvData.append("\r\n");

        for (int j = 0; j < list.size(); j++) {
            Map<String, String> map = list.get(j);

            for (Iterator<String> it = key.iterator(); it.hasNext();) {
                String keyName = it.next();
                String valueName = String.valueOf(map.get(keyName));
                if (it.hasNext()) {
                    csvData.append("\"").append(valueName).append("\"").append(",");
                } else {
                    csvData.append("\"").append(valueName).append("\"");
                }
            }
            csvData.append("\r\n");
        }

        String data = csvData.toString();
        try {
            File file = new File(fileName);
            outFile = new FileOutputStream(file);
            outFile.write(data.getBytes("EUC-KR"));
            outFile.flush();
            outFile.close();

        } catch (Exception e) {
            LOG.debug("saveCsvFile");
        } finally {
            try {
                workbook.close();
            } catch (IOException e) {
                LOG.debug("saveCsvFile");
            }
        }

    }

    /**
     * @Description
     * @param list
     * @param fileName
     * @throws Exception
     */
    public static void saveCsvFileOnlyData(List<List<String>> list, String fileName) throws Exception {

        FileOutputStream outFile;

        StringBuffer csvData = new StringBuffer("");

        for (int j = 0; j < list.size(); j++) {
            List<String> itemList = list.get(j);

            for (Iterator<String> it = itemList.iterator(); it.hasNext();) {
                String valueName = String.valueOf(it.next());
                if (it.hasNext()) {
                    csvData.append(valueName).append(",");
                } else {
                    csvData.append(valueName);
                }
            }
            csvData.append("\r\n");
        }

        String data = csvData.toString();
        try {
            File file = new File(fileName);
            outFile = new FileOutputStream(file);
            outFile.write(data.getBytes("EUC-KR"));
            outFile.flush();
            outFile.close();

        } catch (Exception e) {
            LOG.debug("saveCsvFile");
        }
    }
}
