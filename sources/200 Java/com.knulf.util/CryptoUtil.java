package com.knulf.core.util;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.MessageDigest;
import java.util.Enumeration;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
//import org.apache.log4j.Logger;

public class CryptoUtil {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// MEMBER
	////////////////////////////////////////////////////////////////////////////////////////////////////

	// Logger
	//private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SecurityUtil.class);

	public static final int NUMERIC_SYSTEM_SCALE = 36;
	private static final int ALPHABETIC_BASE_CODE = 55;
	private static final String AES_KEY_COMMON = "hsohe74seokohe23.comhyoun/@gmail";

	public static final String KEY_NAME_COMMON = "CommonKey";

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Computer Unique ID
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Get computer's unique value
	 * @return
	 */
	public static String getComputerID() throws SocketException {
		String serverKey = null;
		String macAddress = null;
		
		Enumeration<NetworkInterface> nics = NetworkInterface.getNetworkInterfaces();
		while (nics.hasMoreElements()) {
			NetworkInterface nic = nics.nextElement();
			if (nic.isVirtual() || nic.isLoopback()) {
				continue;
			}
			
			if (nic != null) {
				byte[] mac = nic.getHardwareAddress();

				if (mac != null) {
					StringBuilder sb = new StringBuilder(18);
					for (byte b : mac) {
						sb.append(String.format("%02X", b));
					}
					
					macAddress = sb.toString();
					
					if (macAddress == null || macAddress.length() == 0 || macAddress.equals("00000000000000E0"))
						continue;
					else
						macAddress = CryptoUtil.getAESEncrypt(macAddress);
					
					serverKey = macAddress;
					break;
				}
			}
		}
		
		return serverKey;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Security key
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Get security key
	 * @return
	 */
	public static String getSecurityKey() {
		return AES_KEY_COMMON;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// AES
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 키 이름으로 키를 반환
	 * @param keyName
	 * @return
	 */
	public static String getAESKey(String keyName) {
		String key = null;
		
		if (keyName.equals(KEY_NAME_COMMON))
			key = AES_KEY_COMMON;
		else
			key = null;
		
		return key;
	}
	
	/**
	 * Encrypt string by key
	 * @param text
	 * @return
	 */
	public static String getAES256Encrypt(String text) {
		return getAES256Encrypt(text, getSecurityKey());
	}
	
	/**
	 * Encrypt string by key
	 * @param text
	 * @param key
	 * @return
	 */
	public static String getAES256Encrypt(String text, String key) {
		String returnVal = text;
		
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			byte[] keyBytes = new byte[32];
			byte[] ivBytes = new byte[16];
			byte[] b = key.getBytes("UTF-8");
			int len = b.length;
			if (len > keyBytes.length) len = keyBytes.length;
			
			System.arraycopy(b, 0, keyBytes, 0, 32);
			System.arraycopy(b, 0, ivBytes, 0, 16);
			
			SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
			IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
			cipher.init(javax.crypto.Cipher.ENCRYPT_MODE,keySpec,ivSpec);
		
			byte[] results = cipher.doFinal(text.getBytes("UTF-8"));
			returnVal = new String(Base64.encodeBase64(results));
		}
		catch (Exception ex) {
			returnVal = text;
		}
		
		return returnVal;
	}
	
	/**
	 * Decrypt string by key
	 * @param text
	 * @return
	 */
	public static String getAES256Decrypt(String text) {
		return getAES256Decrypt(text, getSecurityKey());
	}
	
	/**
	 * Decrypt string by key
	 * @param text
	 * @param key
	 * @return
	 */
	public static String getAES256Decrypt(String text, String key) {
		String returnVal = text;
		
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			byte[] keyBytes = new byte[32];
			byte[] ivBytes = new byte[16];
			byte[] b = key.getBytes("UTF-8");
			int len = b.length;
			if (len > keyBytes.length) len = keyBytes.length;
			
			System.arraycopy(b, 0, keyBytes, 0, 32);
			System.arraycopy(b, 0, ivBytes, 0, 16);
			
			SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
			IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
			cipher.init(Cipher.DECRYPT_MODE,keySpec,ivSpec);
	
			byte [] results = cipher.doFinal(Base64.decodeBase64(text));
			returnVal = new String(results,"UTF-8");
		}
		catch (Exception ex) {
			returnVal = text;
		}
		
		return returnVal;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Hash
	////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get SHA-256 hash code
	 * @param data
	 * @return
	 */
	public static String getSHA256Hash(String data){
		String SHA = ""; 
		try {
			MessageDigest sh = MessageDigest.getInstance("SHA-256"); 
			sh.update(data.getBytes()); 
			byte byteData[] = sh.digest();
			StringBuilder sb = new StringBuilder(); 
			for(int i = 0 ; i < byteData.length ; i++){
				sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
			}
			SHA = sb.toString();
		}catch(java.security.NoSuchAlgorithmException e){
			e.printStackTrace(); 
			SHA = null; 
		}
		return SHA;
	}

	/**
	 * Get SHA-512 hash code
	 * @param data
	 * @return
	 */
	public static String getSHA512Hash(String data){
		String SHA = ""; 
		try {
			MessageDigest sh = MessageDigest.getInstance("SHA-512"); 
			sh.update(data.getBytes()); 
			byte byteData[] = sh.digest();
			StringBuilder sb = new StringBuilder(); 
			for(int i = 0 ; i < byteData.length ; i++){
				sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
			}
			SHA = sb.toString();
		}catch(java.security.NoSuchAlgorithmException e){
			e.printStackTrace(); 
			SHA = null; 
		}
		return SHA;
	}
	
	/**
	 * Get MD5 hash code
	 * @param data
	 * @return
	 */
	public static String getMd5Hash(String data){
		String MD5 = ""; 
		try {
			java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5"); 
			md.update(data.getBytes()); 
			byte byteData[] = md.digest();
			StringBuilder sb = new StringBuilder(); 
			for(int i = 0 ; i < byteData.length ; i++){
				sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
			}
			MD5 = sb.toString();
			
		}catch(java.security.NoSuchAlgorithmException e){
			e.printStackTrace(); 
			MD5 = null; 
		}
		return MD5;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Nuberic system
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Get power base
	 * @param x
	 * @param base
	 * @return
	 */
	public static int getPowerByBase(double x, double base) {
		return (int)(Math.log(x) / Math.log(base));
	}
	
	/**
	 * Get scale number of numeric system
	 * @param systemScale
	 * @param num
	 * @return
	 */
	public static String getScaleNumByDecimal(int systemScale, long num) {
		int totalPower = getPowerByBase(num, systemScale);
		long totalSum = num;
		StringBuilder totalScaleNum = new StringBuilder(); 
		
		for (int i=totalPower; i>=0; i--) {
			long base = (long)Math.pow(systemScale, i);
			int power = (int)(totalSum / base);
			int nowNum = power;
			totalSum = totalSum - base * power;
			totalScaleNum.append( getScaleNumCharater(nowNum) );
		}

		return totalScaleNum.toString();
	}
	
	/**
	 * Get decimal number from other numberic system
	 * @param systemScale
	 * @param num
	 * @return
	 */
	public static long getDecimalNumByScale(int systemScale, String num) {
		long totalDecimalNum = 0;
		
		char[] scaleChars = num.toCharArray();
		for (int i=0; i<scaleChars.length; i++) {
			long power = (long)Math.pow(systemScale, (scaleChars.length - i - 1));
			totalDecimalNum = totalDecimalNum + power * getDecimalNumCharater(String.valueOf(scaleChars[i]));
		}
		
		return totalDecimalNum;
	}
	
	/**
	 * Get scale number character on numberic system
	 * @param num
	 * @return
	 */
	public static String getScaleNumCharater(int num) {
		if (num >= 10)
			return Character.toString(Character.toChars(num + ALPHABETIC_BASE_CODE)[0]);
		else
			return String.valueOf(num);
	}
	
	/**
	 * Get decimal number chracter
	 * @param num
	 * @return
	 */
	public static int getDecimalNumCharater(String num) {
		int scale10Num = (int)(num.toCharArray()[0]);
		
		if (scale10Num < 65)
			scale10Num = scale10Num - 48;
		else
			scale10Num = scale10Num - 55;
		
		return scale10Num;
	}
	
}
