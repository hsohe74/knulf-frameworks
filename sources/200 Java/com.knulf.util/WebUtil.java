package com.knulf.core.util;

import java.io.*;
import java.net.*;
import java.util.*;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringEscapeUtils;

import com.knulf.core.ReturnData;
import com.knulf.core.GlobalString;

/**
 * Web utilities
 * @author Hyounseok Ohe
 *
 */
public class WebUtil {


	////////////////////////////////////////////////////////////////////////////////////////////////////
	// MEMBER
	////////////////////////////////////////////////////////////////////////////////////////////////////

	// Whether specific path is relative
	public static Boolean isRelativeForSpecificPath = true;
	
	// Specific path that is relative checking
	public static String RelativeForSpecificPath = null;

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// File handling
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Get physical path from web virtual path
	 * @param context
	 * @param virtualPath
	 * @return
	 */
	public static String getPhysicalPath(ServletContext context, String virtualPath) {
		return getPhysicalPath(context, virtualPath, isRelativeForSpecificPath, RelativeForSpecificPath);
	}
	
	/**
	 * Get physical path from web virtual path
	 * @param context
	 * @param virtualPath
	 * @param isRelativePath
	 * @param containPath
	 * @return
	 */
	public static String getPhysicalPath(ServletContext context, String virtualPath, Boolean isRelativePath, String specificPath) {
		if (StringUtil.isNullOrEmpty(specificPath) == false && virtualPath.indexOf(specificPath) > -1) {
			if (isRelativePath) {
				if (virtualPath.startsWith("/"))
					return context.getRealPath(virtualPath);
				else
					return context.getRealPath("/" + virtualPath);
			} else {
				return virtualPath;
			}
		} else {
			if (virtualPath.startsWith("/"))
				return context.getRealPath(virtualPath);
			else
				return context.getRealPath("/" + virtualPath);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Common
	////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get web resource
	 * @param url
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public static String getWebResource(String url, String data) throws Exception {
		String outputData = null;
		URL httpUrl = null;
		URLConnection httpConn = null;
		BufferedReader in = null;
		OutputStreamWriter out = null;
		
		try {
			// Make new connection
			httpUrl = new URL(url);
			httpConn = httpUrl.openConnection();
			httpConn.setDoOutput(true);
			httpConn.setUseCaches(false);
			// XXX 타임아웃 설정
			// httpConn.setConnectTimeout(10 * 1000);
			
			// Make output stream to server
			if (StringUtil.isNullOrEmpty(data) == false) {
				out = new OutputStreamWriter(httpConn.getOutputStream());
				out.write(data);
				out.close();
			}
			
			// Make input stream from server
			InputStream is = httpConn.getInputStream();
			in = new BufferedReader(new InputStreamReader(is), 8*1024);
			
			// Get data
			String line = null;
			StringBuilder sb = new StringBuilder();
			while( (line=in.readLine())!= null ) {
				sb.append(line);
	            sb.append("\n");
			}
			in.close();
			outputData = sb.toString();
		} catch (Exception ex) {
			outputData = null;
			throw ex;
		}
		
		// Return
		return outputData;
	}
	
	/**
	 * Send data via post method
	 * @param url
	 * @param sendData
	 * @return
	 * @throws Exception 
	 */
	public static String sendPostData(String sendUrl, String sendData) throws Exception {
		String outputData = null;

		try {
			
			URL url = new URL(sendUrl);
	        byte[] postDataBytes = sendData.getBytes("UTF-8");

	        // 연결을 맺고 전송한다.
	        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
	        conn.setRequestMethod("POST");
	        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
	        conn.setDoOutput(true);
	        conn.getOutputStream().write(postDataBytes);

	        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
	        
	        // 반환 데이터를 정리해서 반환한다.
	        StringBuilder sb = new StringBuilder();
	        for (int c; (c = in.read()) >= 0; sb.append((char)c));
			outputData = sb.toString();
		} catch (Exception ex) {
			throw ex;
		}
		
		return outputData;
	}
	
	/**
	 * Test web url connection
	 * @param webType
	 * @param url
	 * @return
	 */
	public static ReturnData testConnection(String webType, String url) {
		ReturnData returnData = null;
		
		try {
			returnData = new ReturnData();
			
	        URL urlObj = new URL(url);
	        HttpURLConnection urlConn = (HttpURLConnection) urlObj.openConnection();
	        urlConn.connect();

			returnData.setIsSuccess(true);
			returnData.setMessage(GlobalString.COMMON_MSG_SUCCESS);
	    } catch (UnknownHostException ex) {
	    	return new ReturnData(ex);
	    } catch (IOException ex) {
	    	return new ReturnData(ex);
	    } catch (Exception ex) {
	    	return new ReturnData(ex);
		}
		
		return returnData;
	}
	
	/**
	 * Encode url parameter
	 * @param data
	 * @return
	 * @throws Exception 
	 */
	public static String encodeUrl(java.util.Hashtable<String, String> data) throws Exception {
		String returnString = null;
		
		try {
			if ( data == null ) throw new IllegalArgumentException("argument is null");
			StringBuilder buf = new StringBuilder();
			boolean isFirst = true;
			
			Iterator<String> it = data.keySet().iterator();
			while(it.hasNext()) {
				if (isFirst) isFirst = false;
				else buf.append('&');
				String key = (String)it.next();
				String value = (String)data.get(key);
				buf.append(java.net.URLEncoder.encode(key, "UTF-8"));
				buf.append('=');
				buf.append(java.net.URLEncoder.encode(value, "UTF-8"));
			}
			returnString = buf.toString();
		} catch (Exception ex) {
			returnString = null;
			throw ex;
		}
		
		return returnString;
	}
	
	/**
	 * Get escape string
	 * @param value
	 * @return
	 */
	public static String getUrlEncode(String value) {
		if (value == null || value.isEmpty()) return value;
		return StringEscapeUtils.escapeXml(value);
	}
	
	/**
	 * Get unescape string
	 * @param value
	 * @return
	 */
	public static String getUrlDecode(String value) {
		if (value == null || value.isEmpty()) return value;
		return StringEscapeUtils.unescapeXml(value);
	}


}
