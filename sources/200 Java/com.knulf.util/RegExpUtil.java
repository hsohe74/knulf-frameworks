package com.knulf.core.util;

import java.util.regex.*;

public class RegExpUtil {

	public static String regPatternGuid = "^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$";
	public static String regPatternNumber = "^\\d+$";
	public static String regPatternNumberReal = "^(\\d+|\\d{1,3}(,\\d{3})*)(\\.\\d+)?$";
	public static String regPatternAlphabet = "^[A-Za-z]+$";
	public static String regPatternKorean = "^[가-힣]+$";
	public static String regPatternSpecial = "^[~`!@#$%^&*()_\\-+={}[\\]|\\\\;:'\"\"<>,.?/]+$";
	public static String regPatternText = "^[0-9a-zA-Z]+$";
	public static String regPatternTextKorean = "^[0-9a-zA-Z가-힣]+$";
	public static String regPatternFullText = "^[0-9a-zA-Z~`!@#$%^&*()_\\-+={}[\\]|\\\\;:'\"\"<>,.?/]+$";
	public static String regPatternFullTextKorean = "^[0-9a-zA-Z가-힣~`!@#$%^&*()_\\-+={}[\\]|\\\\;:'\"\"<>,.?/]+$";
	public static String regPatternLoginId = "^[a-zA-Z0-9_-]+$";
	public static String regPatternEnglishName = "^[A-Za-z0-9\\s&_\\.\\-\\\\]+$";
	public static String regPatternKoreanName = "^[ㄱ-ㅎ|ㅏ-ㅣ|가-힣|0-9\\s&_\\.\\-\\\\]+$";
	public static String regPatternEmail = "^([a-zA-Z0-9_.+-])+\\@(([a-zA-Z0-9-])+\\.)+([a-zA-Z0-9]{2,4})+$";
	public static String regPatternDateShort = "^\\d{4}-?(0[1-9]|1[012])-?(0[1-9]|[12][0-9]|3[01])$";
	public static String regPatternDateTime = "^\\d{4}-?(0[1-9]|1[012])-?(0[1-9]|[12][0-9]|3[01])\\s?(0[1-9]|1[0-9]|2[0-3]):?(0[0-9]|[1-5][0-9])(:?(0[0-9]|[1-5][0-9]))?$";
	public static String regPatternTelephone = "^((0[1-9]\\d?)?-?(\\d{3,4})-?(\\d{4}))$";
	public static String regPatternTelephoneInternational = "/^((\\d{1,5})-(\\d{1,3})-(\\d{3,4})-(\\d{4}))$/";
	public static String regPatternCellphone = "^((01\\d)-?(\\d{3,4})-?(\\d{4}))$";
	public static String regPatternCellphoneWithoutHyphen = "^((01\\d)(\\d{3,4})(\\d{4}))$";
	public static String regPatternTailNumber = "[\\S\\s]*\\D+(\\d+)$";
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Common
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * @param pattern
	 * @param source
	 * @return
	 */
	public static Boolean match(String pattern, String source) {
		Boolean result = false;
		
		try {
			result = Pattern.matches(pattern, source);
		} catch (Exception ex) {
			result = false;
		}
		
		return result;
	}
	
	/**
	 * @param pattern
	 * @param source
	 * @param replace
	 * @return
	 */
	public static String replace(String pattern, String source, String replace) {
		String result = null;
		
		try {
			result = source.replaceAll(pattern, replace);
		} catch (PatternSyntaxException e) {
			result = null;
		}
		
		return result;
	}
	
	/**
	 * @param data
	 * @return
	 */
	public static String getTailNumber(String data) {
		String result = null;
		
		try {
			if (match(regPatternTailNumber, data)) {
				Pattern pattern = Pattern.compile(regPatternTailNumber); 
				Matcher groupMatcher = pattern.matcher(data);
				while(groupMatcher.find()) {
					result = groupMatcher.group(1);
				}
			}
		} catch (PatternSyntaxException e) {
			result = null;
		}
		
		return result;
	}
}
