package com.knulf.core.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * DateTime utilities
 * @author Hyounseok Ohe
 *
 */
public class DateTimeUtil {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// MEMBER
	////////////////////////////////////////////////////////////////////////////////////////////////////

	/** Default datetime format */
	public static final DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	public static final DateFormat timeFormat = new SimpleDateFormat("HHmmss");
	public static final DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	public static final DateFormat dateTimeFullFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// DateTime
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Get date string by default format
	 * @return
	 */
	public static String getDateString() {
		return getDateTimeString(dateFormat);
	}
	
	/**
	 * Get time string by default format
	 * @return
	 */
	public static String getTimeString() {
		return getDateTimeString(timeFormat);
	}

	/**
	 * Get datetime string by default format
	 * @return
	 */
	public static String getDateTimeString() {
		return getDateTimeString(dateTimeFormat);
	}

	/**
	 * Get datetime string by default format
	 * @return
	 */
	public static String getDateTimeFullString() {
		return getDateTimeString(dateTimeFullFormat);
	}
	
	/**
	 * Get datetime string by default format
	 * @param milliseconds
	 * @return
	 */
	public static String getDateTimeString(Long milliseconds) {
		return getDateTimeString(new Date(milliseconds), dateTimeFormat);
	}
	
	/**
	 * Get datetime string by format string
	 * @param format
	 * @return
	 */
	public static String getDateTimeString(String format) {
		return getDateTimeString(new SimpleDateFormat(format));
	}
	
	/**
	 * Get datetime string by format
	 * @param format
	 * @return
	 */
	public static String getDateTimeString(DateFormat format) {
		return getDateTimeString(new java.util.Date(), format);
	}
	
	/**
	 * Get datetime string by format
	 * @param milliseconds
	 * @param format
	 * @return
	 */
	public static String getDateTimeString(Long milliseconds, String format) {
		return getDateTimeString(new Date(milliseconds), format);
	}
	
	/**
	 * Get datetime string by format
	 * @param datetime
	 * @param format
	 * @return
	 */
	public static String getDateTimeString(Date datetime, String format) {
		return getDateTimeString(datetime, new SimpleDateFormat(format));
	}
	
	/**
	 * Get datetime string by format
	 * @param milliseconds
	 * @param format
	 * @return
	 */
	public static String getDateTimeString(Long milliseconds, DateFormat format) {
		return getDateTimeString(new Date(milliseconds), format);
	}
	
	/**
	 * Get datetime string by format
	 * @param datetime
	 * @param format
	 * @return
	 */
	public static String getDateTimeString(Date datetime, DateFormat format) {
		return format.format(datetime);
	}
	
	/**
	 * Get datetime with datetime string and format
	 * @param datetime
	 * @param format
	 * @return
	 */
	public static java.util.Date parseDateTime(String datetime, String format) throws Exception {
		Date result;
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		result = simpleDateFormat.parse(datetime);
		
		return result;
	}
	
	/**
	 * Add years to input date
	 * @param datetime
	 * @param years
	 * @return
	 */
	public static java.util.Date addYears(Date datetime, int years) {
		return addDatetimeValue(datetime, Calendar.YEAR, years);
	}
	
	/**
	 * Add months to input date
	 * @param datetime
	 * @param months
	 * @return
	 */
	public static java.util.Date addMonths(Date datetime, int months) {
		return addDatetimeValue(datetime, Calendar.MONTH, months);
	}
	
	/**
	 * Add days to input date
	 * @param datetime
	 * @param days
	 * @return
	 */
	public static java.util.Date addDays(Date datetime, int days) {
		return addDatetimeValue(datetime, Calendar.DATE, days);
	}
	
	/**
	 * Add hours to input date
	 * @param datetime
	 * @param hours
	 * @return
	 */
	public static java.util.Date addHours(Date datetime, int hours) {
		return addDatetimeValue(datetime, Calendar.HOUR, hours);
	}
	
	/**
	 * Add value to input datetime
	 * @param datetime
	 * @param type
	 * @param addValue
	 * @return
	 */
	public static java.util.Date addDatetimeValue(Date datetime, int type, int addValue) {
		Date result;
		
		Calendar c = Calendar.getInstance();
		c.setTime(datetime); 
		c.add(type, addValue);
		result = c.getTime();		
		
		return result;
	}
	
	/**
	 * Get differen days of two date
	 * @param newDate
	 * @param oldDate
	 * @return
	 */
	public static int getDifferentDays(Date newDate, Date oldDate) {
		int result;
		
		result = (int)((newDate.getTime() - oldDate.getTime()) / (1000 * 60 * 60 * 24));
		
		return result;
	}

}
