package com.knulf.core.util;

import java.io.*;

import org.apache.commons.compress.archivers.zip.*;

import com.knulf.core.GlobalString;

/**
 * Compress utilities
 * @author Hyounseok Ohe
 *
 */
public class CompressUtil {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Zip file
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Create zip file by directory
	 * @param zipFilepath
	 * @param directoryPath
	 * @param encoding
	 * @throws IOException
	 */
//	public static void createZipByDirectory(String zipFilepath, String directoryPath, String encoding) throws IOException {
//		
//	}
	
	/**
	 * Create zip file by source files
	 * @param zipFilepath
	 * @param targetFiles
	 * @throws IOException
	 */
	public static void createZip(String zipFilepath, String[] targetFiles) throws IOException {
		createZip(zipFilepath, targetFiles, GlobalString.CHARSET_NAME_UTF_8);
	}

	/**
	 * Create zip file by source files, encoding
	 * @param zipFilepath
	 * @param targetFiles
	 * @throws IOException
	 */
	public static void createZip(String zipFilepath, String[] targetFiles, String encoding) throws IOException {
		File zipFile = new File(zipFilepath);
		
		// Output stream
		if (!zipFile.exists())
			zipFile.createNewFile();
		OutputStream zipFileStream = new FileOutputStream(zipFile);

		// Prepare zip
		ZipArchiveOutputStream zipOutStream = new ZipArchiveOutputStream(zipFileStream);
		zipOutStream.setEncoding(encoding);

		try {
			for (String targetFile : targetFiles) {
				
				File relativeFile = new File(FileUtil.getRelativePath(targetFile, zipFilepath));
				addFileToZip(zipOutStream, targetFile, relativeFile.getParent());
			}
		} finally {
			if (zipOutStream != null) {
				zipOutStream.close();
			}
		}
	}
	
	/**
	 * Get zip data after creating zip
	 * @param targetFile
	 * @return
	 * @throws IOException
	 */
	public static byte[] getZipData(String[] targetFiles) throws IOException {
		return getZipData(targetFiles, GlobalString.CHARSET_NAME_UTF_8);
	}

	/**
	 * Get zip data after creating zip by encoding
	 * @param targetFile
	 * @param encoding
	 * @return
	 * @throws IOException
	 */
	public static byte[] getZipData(String[] targetFiles, String encoding) throws IOException {
		// Output stream
		ByteArrayOutputStream ms = new ByteArrayOutputStream();

		// Prepare zip
		ZipArchiveOutputStream zipOutStream = new ZipArchiveOutputStream(ms);
		zipOutStream.setEncoding(encoding);

		try {
			for (String targetFile : targetFiles) {
				addFileToZip(zipOutStream, targetFile, "");
			}
		} finally {
			if (zipOutStream != null) {
				zipOutStream.close();
			}
		}

		return ms.toByteArray();
	}
	
	/**
	 * Add file to zip
	 * @param zipOutStream
	 * @param targetFile
	 * @throws IOException
	 */
	private static void addFileToZip(ZipArchiveOutputStream zipOutStream, String targetFile, String basePath) throws IOException {
		byte[] buf = new byte[1024 * 8];
		int length;
		
		ZipArchiveEntry zipEntry = null;
		
		if (basePath == null || basePath.isEmpty())
			zipEntry = new ZipArchiveEntry(FileUtil.getFilename(targetFile));
		else
			zipEntry = new ZipArchiveEntry(basePath + "/" + FileUtil.getFilename(targetFile));
		
		zipOutStream.putArchiveEntry(zipEntry);
		FileInputStream fis = new FileInputStream(targetFile);
		try {
			while ((length = fis.read(buf, 0, buf.length)) >= 0) {
				zipOutStream.write(buf, 0, length);
			}
		} finally {
			if (fis != null) {
				fis.close();
			}
			zipOutStream.closeArchiveEntry();
		}
	}

	/**
	 * Unzip from zp file
	 * @param zipFilename
	 * @throws Exception
	 */
	public static void unzip(String zipFilename) throws Exception {
		unzip(zipFilename, GlobalString.CHARSET_NAME_UTF_8);
	}
	
	/**
	 * Unzip from zp file by encoding
	 * @param zipFilename
	 * @param encoding
	 * @throws Exception
	 */
	public static void unzip(String zipFilename, String encoding) throws Exception {
		String targetDirectoryPath = FileUtil.getDirectory(zipFilename).getAbsolutePath();
		unzip(zipFilename, targetDirectoryPath, encoding);
	}
	
	/**
	 * Unzip from zp file by target directory, encoding
	 * @param zipFilename
	 * @param targetDirectoryPath
	 * @param encoding
	 * @throws Exception
	 */
	public static void unzip(String zipFilename, String targetDirectoryPath, String encoding) throws Exception {
		java.io.FileInputStream fis = new FileInputStream(zipFilename);
		unzip(fis, targetDirectoryPath, encoding);
	}
	
	/**
	 * Unzip from file stream by target directory
	 * @param inputStream
	 * @param targetDirectory
	 * @throws Exception
	 */
	public static void unzip(InputStream inputStream, String targetDirectoryPath) throws Exception {
		unzip(inputStream, targetDirectoryPath, GlobalString.CHARSET_NAME_UTF_8);
	}

	/**
	 * Unzip from file stream by target directory, encoding
	 * @param inputStream
	 * @param targetDirectory
	 * @param encoding
	 * @throws Exception
	 */
	public static void unzip(InputStream inputStream, String targetDirectoryPath, String encoding) throws Exception {
		File targetDirectory;
		ZipArchiveInputStream zis = null;
		ZipArchiveEntry entry = null;
		int nWritten = 0;
		byte[] buf = new byte[1024 * 8];

		try {
			targetDirectory = new File(targetDirectoryPath);
			zis = new ZipArchiveInputStream(inputStream, encoding, false);

			while ((entry = zis.getNextZipEntry()) != null) {
				File target = new File(targetDirectory, entry.getName());
				if (entry.isDirectory()) {
					target.mkdirs();
				} else {
					File f = new File(target.getParent());
					if (f.exists() == false) {
						f.mkdir();
					}
					target.createNewFile();
					BufferedOutputStream bos = null;

					try {
						bos = new BufferedOutputStream(new FileOutputStream(target));
						while ((nWritten = zis.read(buf)) >= 0) {
							bos.write(buf, 0, nWritten);
						}
					} finally {
						if (bos != null) {
							bos.close();
						}
					}
				}
			}
		} finally {
			if (zis != null) {
				zis.close();
			}
		}
	}

}