package com.knulf.core.util.datatypes;

import java.sql.ResultSetMetaData;

/**
 * DBMS별 data type 구분자의 구현에 사용.<br/>
 * Handler들의 이름은 sql.xml의 DBMS tag의 값으로 해야한다.<br/>
 * 
 * @author Sangmin Lee
 */
public interface IDataTypeHandler {
	/**
	 * 해당 typename을 SmartBuilder에서 인식하는 typename 으로 변환
	 * 
	 * @param typename
	 *            {@link ResultSetMetaData#getColumnType(int)}에서 반환된 type명
	 * @return {@link DataTypes}의 값 또는 null(targetname이 null인 경우)
	 */
	String getType(String typename);
}
