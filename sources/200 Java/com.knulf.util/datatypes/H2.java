package com.knulf.core.util.datatypes;

/**
 * H2의 data type handler
 * 
 * @author Sangmin Lee
 */
public class H2 implements IDataTypeHandler {
	public enum IntType {
		INT, INTEGER, MEDIUMINT, INT4, SIGNED, TINYINT, SMALLINT, INT2, YEAR, BIGINT, INT8, IDENTITY
	}

	public enum FixedPointType {
		DECIMAL, NUMERIC, DEC, NUMBER
	}

	public enum FloatType {
		DOUBLE, FLOAT, FLOAT8, REAL, FLOAT4
	}

	@Override
	public String getType(String typename) {
		DataTypes result = null;

		if (typename != null) {
			String actualName = typename.toUpperCase();

			if (DataTypeHelper.contains(IntType.values(), actualName)) {
				result = DataTypes.Int;
			} else if (DataTypeHelper.startsWith(FixedPointType.values(), actualName)) {
				result = DataTypes.Double;
			} else if (DataTypeHelper.startsWith(FloatType.values(), actualName)) {
				result = DataTypes.Double;
			} else {
				result = DataTypes.String;
			}
		}

		return result.toString();
	}
}
