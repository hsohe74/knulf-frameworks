package com.knulf.core.util.datatypes;

/**
 * MySQL의 data type handler
 * 
 * @author Sangmin Lee
 */
public class INFORMIX implements IDataTypeHandler {
	public enum IntType {
		INTEGER, SMALLINT, INT8, SERIAL, SERIAL8
	}

	public enum FixedPointType {
		DECIMAL
	}

	public enum FloatType {
		FLOAT, SMALLFLOAT
	}

	@Override
	public String getType(String typename) {
		DataTypes result = null;

		if (typename != null) {
			String actualName = typename.toUpperCase();

			if (DataTypeHelper.contains(IntType.values(), actualName)) {
				result = DataTypes.Int;
			} else if (DataTypeHelper.startsWith(FixedPointType.values(), actualName)) {
				result = DataTypes.Double;
			} else if (DataTypeHelper.contains(FloatType.values(), actualName)) {
				result = DataTypes.Double;
			} else {
				result = DataTypes.String;
			}
		}

		return result.toString();
	}
}
