package com.knulf.core.util.datatypes;

/**
 * Oracle의 data type handler
 * 
 * @author Sangmin Lee
 */
public class ORACLE implements IDataTypeHandler {
	public enum IntType {
		INTEGER
	}
	
	public enum FixedPointType {
		NUMBER
	}

	public enum FloatType {
		BINARY_FLOAT, BINARY_DOUBLE;
	}

	@Override
	public String getType(String typename) {
		DataTypes result = null;

		if (typename != null) {
			String actualName = typename.toUpperCase();

			if (DataTypeHelper.contains(IntType.values(), actualName)) {
				result = DataTypes.Int;
			} else if (DataTypeHelper.startsWith(FixedPointType.values(), actualName)) {
				result = DataTypes.Double;
			} else if (DataTypeHelper.contains(FloatType.values(), actualName)) {
				result = DataTypes.Double;
			} else {
				result = DataTypes.String;
			}
		}

		return result.toString();
	}
}
