package com.knulf.core.util.datatypes;

/**
 * CUBRID의 data type handler
 * 
 * @author Sangmin Lee
 */
public class CUBRID implements IDataTypeHandler {
	public enum IntType {
		SHORT, SMALLINT, INT, BIGINT
	}

	public enum FixedPointType {
		DECIMAL, NUMERIC
	}

	public enum FloatType {
		FLOAT, REAL, DOUBLE
	}

	@Override
	public String getType(String typename) {
		DataTypes result = null;

		if (typename != null) {
			String actualName = typename.toUpperCase();

			if (DataTypeHelper.contains(IntType.values(), actualName)) {
				result = DataTypes.Int;
			} else if (DataTypeHelper.startsWith(FixedPointType.values(), actualName)) {
				result = DataTypes.Double;
			} else if (DataTypeHelper.startsWith(FloatType.values(), actualName)) {
				result = DataTypes.Double;
			} else {
				result = DataTypes.String;
			}
		}

		return result.toString();
	}
}
