package com.knulf.core.util.datatypes;

/**
 * Data type을 다루기 편하게 돕는 helper
 * 
 * @author Sangmin Lee
 */
public class DataTypeHelper {
	/**
	 * 해당 enum type들 중에 target과 동일한 것이 있는지 검사
	 * 
	 * @param enumTypes
	 *            enum class에서 values()가 반환하는 값
	 * @param target
	 *            검사할 대상
	 * @return true: 동일한 항목이 존재
	 */
	public static boolean contains(Enum<?>[] enumTypes, String target) {
		boolean result = false;

		for (Enum<?> enumType : enumTypes) {
			if (enumType.toString().equals(target)) {
				result = true;
				break;
			}
		}

		return result;
	}
	
	/**
	 * 해당 enum type들 중에 target으로 시작하는 항목이 있는지 검사
	 * 
	 * @param enumTypes
	 *            enum class에서 values()가 반환하는 값
	 * @param target
	 *            검사할 대상
	 * @return true: target으로 시작하는 항목이 존재
	 */
	public static boolean startsWith(Enum<?>[] enumTypes, String target) {
		boolean result = false;
		
		for (Enum<?> enumType : enumTypes) {
			if (enumType.toString().startsWith(target)) {
				result = true;
				break;
			}
		}
		
		return result;
	}
}
