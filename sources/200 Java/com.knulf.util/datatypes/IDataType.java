package com.knulf.core.util.datatypes;

/**
 * Type name을 정의하는 enum의 구현에 사용
 * 
 * @author Sangmin Lee
 */
public interface IDataType {
	boolean contains(String value);
}
