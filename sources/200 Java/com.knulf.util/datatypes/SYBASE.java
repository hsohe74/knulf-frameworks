package com.knulf.core.util.datatypes;

/**
 * Sybase의 data type handler
 * 
 * @author Sangmin Lee
 */
public class SYBASE implements IDataTypeHandler {
	public enum IntType {
		BIGINT, INT, SMALLINT, TINYINT
	}
	
	public enum FixedPointType {
		DECIMAL, NUMERIC, UNSIGNED
	}

	public enum FloatType {
		FLOAT, REAL, DOUBLE
	}

	@Override
	public String getType(String typename) {
		DataTypes result = null;

		if (typename != null) {
			String actualName = typename.toUpperCase();

			if (DataTypeHelper.startsWith(IntType.values(), actualName)) {
				result = DataTypes.Int;
			} else if (DataTypeHelper.startsWith(FixedPointType.values(), actualName)) {
				result = DataTypes.Double;
			} else if (DataTypeHelper.startsWith(FloatType.values(), actualName)) {
				result = DataTypes.Double;
			} else {
				result = DataTypes.String;
			}
		}

		return result.toString();
	}
}
