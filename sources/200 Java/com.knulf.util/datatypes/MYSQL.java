package com.knulf.core.util.datatypes;

/**
 * MySQL의 data type handler
 * 
 * @author Sangmin Lee
 */
public class MYSQL implements IDataTypeHandler {
	public enum IntType {
		INTEGER, INT, SMALLINT, TINYINT, MEDIUMINT, BIGINT
	}

	public enum FixedPointType {
		DECIMAL, NUMERIC, DEC, FIXED
	}

	public enum FloatType {
		FLOAT, DOUBLE
	}

	@Override
	public String getType(String typename) {
		DataTypes result = null;

		if (typename != null) {
			String actualName = typename.toUpperCase();

			if (DataTypeHelper.contains(IntType.values(), actualName)) {
				result = DataTypes.Int;
			} else if (DataTypeHelper.startsWith(FixedPointType.values(), actualName)) {
				result = DataTypes.Double;
			} else if (DataTypeHelper.contains(FloatType.values(), actualName)) {
				result = DataTypes.Double;
			} else {
				result = DataTypes.String;
			}
		}

		return result.toString();
	}
}
