package com.knulf.core.util.datatypes;

/**
 * SmartBuilder에서 인식/사용하는 DBMS의 data type들
 * 
 * @author Sangmin Lee
 */
public enum DataTypes {
	String, Int, Double
}
