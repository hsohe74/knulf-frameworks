package com.knulf.core.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.knulf.core.ReturnData;
import com.knulf.core.GlobalString;

/**
 * DBMS utilities
 * @author Hyounseok Ohe
 *
 */
public class DbmsUtil {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// MEMBER
	////////////////////////////////////////////////////////////////////////////////////////////////////

	public static final int DBMS_TIMEOUT = 2;

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Common
	////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get DB class
	 * 
	 * @param dbmsType
	 * @return
	 */
	public static String getDbClass(String dbmsType) {
		String className = null;
		switch (DbmsType.valueOf(dbmsType)) {
		case DB2:
			className = "com.ibm.db2.jcc.DB2Driver";
			break;
		case ORACLE:
			className = "oracle.jdbc.driver.OracleDriver";
			break;
		case MSSQL:
			className = "net.sourceforge.jtds.jdbc.Driver";
			break;
		case POSTGRESQL:
			className = "org.postgresql.Driver";
			break;
		case MYSQL:
			className = "com.mysql.jdbc.Driver";
			break;
		case MARIADB:
			className = "org.mariadb.jdbc.Driver";
			break;
		case H2:
			className = "org.h2.Driver";
			break;
		case CUBRID:
			className = "cubrid.jdbc.driver.CUBRIDDriver";
			break;
		}

		return className;
	}

	/**
	 * Get DBMS connection url
	 * 
	 * @param dbmsType
	 * @param host
	 * @param port
	 * @param database
	 * @return
	 */
	public static String getDbConnectionUrl(String dbmsType, String host, Integer port, String database, String databaseServer, String loginID,
			String loginPW) {
		String dbconnUrl = null;

		switch (DbmsType.valueOf(dbmsType)) {
		case DB2:
			dbconnUrl = "jdbc:db2://" + host + ":" + port + "/" + database;
			break;
		case ORACLE:
			dbconnUrl = "jdbc:oracle:thin:@" + host + ":" + port + ":" + database;
			break;
		case MSSQL:
			dbconnUrl = "jdbc:jtds:sqlserver://" + host + ":" + port + ";databaseName=" + database;
			break;
		case POSTGRESQL:
			dbconnUrl = "jdbc:postgresql://" + host + ":" + port + "/" + database;
			break;
		case MYSQL:
			dbconnUrl = "jdbc:mysql://" + host + ":" + port + "/" + database;
			break;
		case MARIADB:
			dbconnUrl = "jdbc:mariadb://" + host + ":" + port + "/" + database;
			break;
		case H2:
			dbconnUrl = "jdbc:h2:tcp://" + host + ":" + port + "/" + database;
			break;
		case CUBRID:
			dbconnUrl = "jdbc:CUBRID:" + host + ":" + port + ":" + database + ":::";
			break;
		}

		return dbconnUrl;
	}

	/**
	 * Get DB sql statement by max count
	 * 
	 * @param sql
	 * @param dbmsType
	 * @return
	 * @throws Exception
	 */
	public static String getMaxCountResultSql(String sql, String dbmsType, Integer maxCount) {
		String reSql = null;

		try {
			if (maxCount == 0) {
				reSql = sql;
			} else {
				switch (DbmsType.valueOf(dbmsType)) {
				case DB2:
					reSql = "select * from (" + sql + ") fetch first " + String.valueOf(maxCount) + " rows only";
					break;
				case ORACLE:
					reSql = sql;
					break;
				case MSSQL:
					if (sql.matches("^\\s*(?i)select\\s*(?i)top[\\s|\\S]*"))
						reSql = sql;
					else
						reSql = sql.replaceFirst("(?i)select", "select top " + String.valueOf(maxCount));
					break;
				case POSTGRESQL:
					reSql = sql + " limit " + String.valueOf(maxCount);
					break;
				case MYSQL:
					reSql = sql;
					break;
				case MARIADB:
					reSql = sql;
					break;
				case H2:
					reSql = sql;
					break;
				case CUBRID:
					reSql = "select * from (" + sql + ") T limit 0, " + String.valueOf(maxCount);
					break;
				}
			}
		} catch (Exception ex) {
			reSql = sql;
		}

		return reSql;
	}
	
	/**
	 * Test DB connection
	 * 
	 * @param dbmsType
	 * @param host
	 * @param port
	 * @param database
	 * @param loginID
	 * @param loginPW
	 * @return common return data object
	 */
	public static ReturnData testConnection(String dbmsType, String host, Integer port, String database, String databaseServer, String loginID, String loginPW) {
		ReturnData returnData = new ReturnData();
		Connection conn = null;

		// Class
		try {
			Class.forName(DbmsUtil.getDbClass(dbmsType));
		} catch (ClassNotFoundException ex) {
			return new ReturnData(ex);
		}

		// Connection URL
		String connUrl = DbmsUtil.getDbConnectionUrl(dbmsType, host, port, database, databaseServer, loginID, loginPW);

		// Test connection
		try {
			DriverManager.setLoginTimeout(DBMS_TIMEOUT);
			conn = DriverManager.getConnection(connUrl, loginID, loginPW);
			returnData.setIsSuccess(true);
			returnData.setMessage(GlobalString.COMMON_MSG_SUCCESS);
		} catch (java.sql.SQLException ex) {
			return new ReturnData(ex);
		} catch (Exception ex) {
			return new ReturnData(ex);
		} finally {
			DbmsUtil.closeObject(conn);
		}

		return returnData;
	}
	
	/**
	 * Close object
	 * 
	 * @param obj
	 */
	public static void closeObject(Connection obj) {
		if (obj != null) {
			try {
				obj.close();
			} catch (SQLException e) {
			}
		}
	}

	/**
	 * Close object
	 * 
	 * @param obj
	 */
	public static void closeObject(Statement obj) {
		if (obj != null) {
			try {
				obj.close();
			} catch (SQLException e) {
			}
		}
	}

	/**
	 * Close object
	 * 
	 * @param obj
	 */
	public static void closeObject(ResultSet obj) {
		if (obj != null) {
			try {
				obj.close();
			} catch (SQLException e) {
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Get result data
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Execute query for result data set with select sql
	 * @param conn
	 * @param sql
	 * @return
	 * @throws Exception 
	 */
	public static List<HashMap<String, Object>> executeResultQuery(Connection conn, String sql) throws Exception {
		List<HashMap<String, Object>> result = null;
		Statement stmt = null;
		ResultSet rsData = null;
		
		try {
			stmt = conn.createStatement();
			rsData = stmt.executeQuery(sql);
			result = convertResultSetToList(rsData);
		} finally {
			DbmsUtil.closeObject(rsData);
			DbmsUtil.closeObject(stmt);
		}
		
		return result;
	}
	
	/**
	 * Execute query with update sql
	 * @param conn
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	public static int executeUpdateQuery(Connection conn, String sql) throws Exception {
		int result = 0;
		Statement stmt = null;
		
		try {
			stmt = conn.createStatement();
			result = stmt.executeUpdate(sql);
		} finally {
			DbmsUtil.closeObject(stmt);
		}
		
		return result;
	}
	
	/**
	 * Convert ResultSet to List with HashMap
	 * @param rsData
	 * @return
	 * @throws Exception 
	 * @throws SQLException
	 */
	public static List<HashMap<String, Object>> convertResultSetToList(ResultSet rsData) throws Exception {
		List<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>();;
		
		ResultSetMetaData rsMD = rsData.getMetaData();
		int columnCount = rsMD.getColumnCount();
		
		while (rsData.next()) {
			HashMap<String, Object> row = new HashMap<String, Object>(columnCount);
			for (int i = 0; i < columnCount; i++) {
				row.put(rsMD.getColumnName(i), rsData.getObject(i));
			}
			result.add(row);
		}
		
		return result;
	}
}
