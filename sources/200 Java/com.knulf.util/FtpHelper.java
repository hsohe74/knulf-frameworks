package com.knulf.core.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FtpHelper {

    FTPClient ftpClient = null;

    /**
     * @Description FTP 초기화 및 접속
     * @param host
     * @param user
     * @param password
     * @throws Exception
     */
    public void init(String host, String user, String password) throws Exception {
        this.init(host, 21, 60, user, password);
    }

    /**
     * @Description FTP 초기화 및 접속
     * @param host
     * @param port
     * @param user
     * @param password
     * @throws Exception
     */
    public void init(String host, int port, String user, String password) throws Exception {
        this.init(host, port, 60, user, password);
    }

    /**
     * @Description FTP 초기화 및 접속
     * @param host
     * @param port
     * @param timeout
     * @param user
     * @param password
     * @throws Exception
     */
    public void init(String host, int port, int timeout, String user, String password) throws Exception {

        ftpClient = new FTPClient();
        ftpClient.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));

        int reply;
        ftpClient.setConnectTimeout(timeout * 1000);
        ftpClient.connect(host, port);
        reply = ftpClient.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            ftpClient.disconnect();
            throw new Exception("Exception in connecting to FTP Server");
        }
        ftpClient.login(user, password);
        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
        ftpClient.enterLocalPassiveMode();
    }

    /**
     * @Description 접속해제
     */
    public void disconnect() {
        if (this.ftpClient.isConnected()) {
            try {
                this.ftpClient.logout();
                this.ftpClient.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @Description 로컬로 파일 다운로드
     * @param remoteDirectory
     * @param localDirectory
     * @param fileName
     * @throws Exception
     */
    public void getFileFromFtpServer(String remoteDirectory, String localDirectory, String fileName) throws Exception {
        try {
            ftpClient.changeWorkingDirectory(remoteDirectory);

            File file = new File(localDirectory, fileName);
            try (BufferedOutputStream fo = new BufferedOutputStream(new FileOutputStream(file))) {
                if (ftpClient.retrieveFile(fileName, fo)) {
                    System.out.println("Download - " + file);
                }
            }
        } catch (Exception e) {
            disconnect();
            log.debug("Remote directory : [{}] \\nLocal File : [{}] \\nFile Name : [{}]", remoteDirectory,
                    localDirectory, fileName);
            throw e;
        }
    }

    /**
     * @Description 서버로 파일 업로드
     * @param localFile
     * @param remoteFile
     * @throws Exception
     */
    public void putFileToFtpServer(String localFile, String remoteFile) throws Exception {
        try {
            try (InputStream input = new FileInputStream(new File(localFile))) {
                ftpClient.storeFile(remoteFile, input);
            }
        } catch (Exception e) {
            disconnect();
            log.debug("Source File : [{}] \nDestination File : [{}]", localFile, remoteFile);
            throw e;
        }
    }

    /**
     * @Description 원격파일 삭제
     * @param remoteDirectory
     * @param fileName
     * @return
     * @throws Exception
     */
    public boolean removeFileInFtpServer(String remoteDirectory, String fileName) throws Exception {
        boolean result = false;

        try {
            ftpClient.changeWorkingDirectory(remoteDirectory);

            result = ftpClient.deleteFile(fileName);
        } catch (Exception e) {
            disconnect();
            log.debug("Remote directory : [{}] \\nFile Name : [{}]", remoteDirectory, fileName);
            throw e;
        }

        return result;
    }
}
