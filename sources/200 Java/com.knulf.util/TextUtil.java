package com.knulf.core.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.codec.binary.Base64;

import com.knulf.core.GlobalString;

/**
 * Text utilities
 * @author Hyounseok Ohe
 *
 */
public class TextUtil {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Common
	////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get system default encoding
	 * @return
	 */
	public static String getDefaultEncoding() {
		String defaultEncoding = null;

		defaultEncoding = new OutputStreamWriter(System.out).getEncoding();

		return defaultEncoding;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Base64
	////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Get encoded Base64 from byte array
     * @param data
     * @return
     */
	public static String getBase64Encode(byte[] data) throws Exception {
		return Base64.encodeBase64String(data);
	}

	/**
	 * Get encoded Base64 from string
	 * @param data
	 * @return
	 */
	public static String getBase64Encode(String data) throws Exception {
		return Base64.encodeBase64String(data.getBytes());
	}

	/**
	 * Get decoded Base64
	 * @param base64String
	 * @return
	 */
	public static byte[] getBase64Decode(String base64String) throws Exception {
		return Base64.decodeBase64(base64String);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// GZip
	////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Get compressed string by GZip
	 * @param plainString
	 * @return
	 * @throws Exception
	 */
	public static String compressGZip(String plainString) throws Exception {
		return compressGZip(plainString, GlobalString.CHARSET_NAME_UTF_8);
	}

	/**
	 * Get compressed string by GZip
	 * @param plainString
	 * @return
	 * @throws IOException 
	 */
	public static String compressGZip(String plainString, String encoding) throws Exception {
		String compressedString = null;
		byte[] rawData = null;
		byte[] compressedData = null;

		ByteArrayOutputStream compressStream = null;
		GZIPOutputStream gZipOutStream = null;
		BufferedOutputStream bufferedOutputStream = null;

		if (plainString == null || plainString.length() == 0) {
			return plainString;
		}

		try {
			rawData = plainString.getBytes(encoding);
			compressStream = new ByteArrayOutputStream()  ;
			gZipOutStream = new GZIPOutputStream(compressStream, rawData.length);
			bufferedOutputStream = new BufferedOutputStream(gZipOutStream);
			gZipOutStream.write(rawData, 0, rawData.length);
			gZipOutStream.finish();

			compressedData = compressStream.toByteArray();
			compressedString = getBase64Encode(compressedData);
		} finally {
			if (bufferedOutputStream != null) bufferedOutputStream.close();
			if (gZipOutStream != null) gZipOutStream.close();
			if (compressStream != null) compressStream.close();
		}

		return compressedString;
	}

	/**
	 * Get decompressed string by GZip
	 * @param compressedString
	 * @return
	 * @throws Exception
	 */
	public static String decompressGZip(String compressedString) throws Exception {
		return decompressGZip(compressedString, GlobalString.CHARSET_NAME_UTF_8);
	}

	/**
	 * Get decompressed string by GZip
	 * @param compressedString
	 * @return
	 * @throws Exception
	 */
	public static String decompressGZip(String compressedString, String encoding) throws Exception {
		String plainString = null;
		byte[] compressedData = null;

		ByteArrayInputStream compressedStream = null;
		ByteArrayOutputStream decompressStream = null;
		GZIPInputStream gZipInStream = null;
		BufferedInputStream bufferedInputStream = null;

		byte[] buffer = new byte[1024*8];
		int length;

		if (compressedString == null || compressedString.length() == 0) {
			return compressedString;
		}

		try {
			compressedData = getBase64Decode(compressedString);
			compressedStream = new ByteArrayInputStream(compressedData);
			decompressStream = new ByteArrayOutputStream();
			gZipInStream = new GZIPInputStream(compressedStream); 
			bufferedInputStream = new BufferedInputStream(gZipInStream);

			while ((length = bufferedInputStream.read(buffer)) > 0) {
				decompressStream.write(buffer, 0, length);
			}

			plainString = decompressStream.toString(encoding);
		} finally {
			if (bufferedInputStream != null) bufferedInputStream.close();
			if (gZipInStream != null) gZipInStream.close();
			if (compressedStream != null) compressedStream.close();
			if (decompressStream != null) decompressStream.close();
		}

		return plainString;
	}

}