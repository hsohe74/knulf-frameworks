package com.knulf.core.util;

import java.util.List;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Type utilities
 * @author Hyounseok Ohe
 *
 */
public class TypeUtil {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// MEMBER
	////////////////////////////////////////////////////////////////////////////////////////////////////

	/** FEFF because this is the Unicode char represented by the UTF-8 byte order mark (EF BB BF). */
	public static final String UTF8_BOM = "\uFEFF";

    /** Default datetime format */
	public static final DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Common
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Remove UTF-8 BOM
	 * @param s
	 * @return
	 */
	public static String removeUTF8BOM(String s) {
		if (s.startsWith(UTF8_BOM)) {
			s = s.substring(1);
		}
		return s;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Enum
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Get list of enum name
	 * @param enumType
	 * @return
	 */
	public static <T extends Enum<T>> List<String> getEnumNames(Class<T> enumType) {
		List<String> result = new ArrayList<String>();
		
		for (T c : enumType.getEnumConstants()) {
			result.add(c.name());
		}
		
		return result;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Type
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Get is basic type
	 * @param type
	 * @return
	 */
	public static Boolean isBasicType(Class<?> type) {
		if (Boolean.class == type
				|| Double.class == type
				|| Float.class == type
				|| Integer.class == type
				|| Long.class == type
				|| Short.class == type
				|| String.class == type) {
			return true;
    	} else {
    		return false;
    	}
	}
	
	/**
	 * Get basic object
	 * @param type
	 * @param value
	 * @return
	 */
	public static Object toBasicObject(Class<?> type, String value) {
		try {
			if (Boolean.class == type) return Boolean.parseBoolean(value);
			if (Double.class == type) return Double.parseDouble(value);
			if (Float.class == type) return Float.parseFloat(value);
			if (Integer.class == type) return Integer.parseInt(value);
			if (Long.class == type) return Long.parseLong(value);
			if (Short.class == type) return Short.parseShort(value);
			return value;
		} catch (java.lang.NumberFormatException ex) {
			return 0;
		}
	}
	
}