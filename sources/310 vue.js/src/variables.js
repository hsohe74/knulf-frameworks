/**
 * NAME:    variables.js
 * DESC:    전역 변수 설정
 * DATE:    2021-11-18
 * CREATOR: hsohe74
 * HISTORY:
 *     2021-11-18 hsohe74 생성
 */
export const setting = {
    apiBaseURL: '/api',
    refreshInterval: 120,
    defaultPageSize: 20
}
