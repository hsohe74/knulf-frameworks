import Vue from 'vue'

/* focus */
const focus = Vue.directive('focus', {
    // 바인딩 된 엘리먼트가 DOM에 삽입되었을 때...
    inserted: function(el) {
        // 엘리먼트에 포커스를 줍니다
        el.focus()
    }
})

export default focus
