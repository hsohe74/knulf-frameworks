import Vue from 'vue'

const onlynumber = Vue.directive('onlynumber', {
    inserted: function(el) {
        el.oninput = event => {
            const formattedValue = parseInt(event.target.value)
            el.value = isNaN(formattedValue) ? '' : formattedValue
        }
    }
})

export default onlynumber
