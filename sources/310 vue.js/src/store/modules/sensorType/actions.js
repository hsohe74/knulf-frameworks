import axios from '@/plugins/axios'
import * as dataUtil from '@/utils/data'
export default {
    /**********************************************************************/
    /* 센서유형 */
    /**********************************************************************/

    /**
     * 센서 대응메뉴얼
     */
    initSensorManual({ commit }) {
        commit('initSensorManual')
    },
    getSensorManual({ commit }, applicationID) {
        return new Promise((resolve, reject) => {
            axios
                .post('/SensorType/SensorTypeManualDetail', { ApplicationID: applicationID })
                .then((response) => {
                    if (response.data.Success) {
                        commit('setSensorManual', response.data.Data[0])
                        resolve(response)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    }
}
