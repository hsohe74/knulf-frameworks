export default {
    /**********************************************************************/
    /* 센서유형 */
    /**********************************************************************/

    /**
     * 센서 대응메뉴얼
     */
    initSensorManual: (state) => {
        state.sensorManualData = {
            ApplicationID: 0,
            SensorTypeNm: '',
            IconFilePath: '',
            ManualContent: ''
        }
    },
    setSensorManual: (state, paramObj) => {
        state.sensorManualData = paramObj
    }
}
