export default {
    /**********************************************************************/
    /* 센서 */
    /**********************************************************************/

    /**
     * 센서알람 조치
     */
    initSensorAckAlarmParams: (state) => {
        state.sensorAckAlarmParams = {
            NotificationSeq: 0,
            NotificationID: 0
        }
    },
    setSensorAckAlarmParams: (state, paramObj) => {
        state.sensorAckAlarmParams = paramObj
    },

    /**
     * 센서 위치정보
     */
    initSensorLocation: (state) => {
        state.sensorLocation = {
            SensorID: 0,
            SensorNm: '',
            LocationInfo: '',
            PhotoFilePath: ''
        }
    },
    setSensorLocation: (state, paramObj) => {
        state.sensorLocationData = paramObj
    },

    /**
     * 센서 대응메뉴얼
     */
    initSensorManual: (state) => {
        state.sensorManualData = {
            ManualContent: ''
        }
    },
    setSensorManual: (state, paramObj) => {
        state.sensorManualData = paramObj
    },

    /**********************************************************************/
    /* 데이터메시지 */
    /**********************************************************************/

    /**
     * 센서 데이터메시지
     */
    setDataMessageList: (state, paramObj) => {
        state.dataMessageList = paramObj
    }
}
