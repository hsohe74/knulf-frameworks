export default {
    /**********************************************************************/
    /* 센서 */
    /**********************************************************************/

    /**
     * 센서알람 조치
     */
    sensorAckAlarmParams: (state) => {
        return state.sensorAckAlarmParams
    },

    /**
     * 센서 위치정보
     */
    sensorLocationData: (state) => {
        return state.sensorLocationData
    },

    /**
     * 센서 대응메뉴얼
     */
    sensorManualData: (state) => {
        return state.sensorManualData
    },

    /**********************************************************************/
    /* 데이터메시지 */
    /**********************************************************************/

    /**
     * 센서 데이터메시지
     */
    dataMessageList: (state) => {
        return state.dataMessageList
    }
}
