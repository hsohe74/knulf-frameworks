import axios from '@/plugins/axios'
import * as dataUtil from '@/utils/data'
export default {
    /**********************************************************************/
    /* 센서 */
    /**********************************************************************/

    /**
     * 센서알람 조치
     */
    initSensorAckAlarmParams({ commit }) {
        commit('initSensorAckAlarmParams')
    },
    ackSensorAlarm({ commit, state }, paramObj) {
        commit('setSensorAckAlarmParams', paramObj)
        return new Promise((resolve, reject) => {
            axios
                .post('/Sensor/SensorNotificationAck', state.sensorAckAlarmParams)
                .then((response) => {
                    if (response.data.Success) {
                        resolve(response)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },

    /**
     * 센서 위치정보
     */
    initSensorLocation({ commit }) {
        commit('initSensorLocation')
    },
    getSensorLocation({ commit }, sensorID) {
        return new Promise((resolve, reject) => {
            axios
                .post('/Sensor/SensorLocationDetail', { SensorID: sensorID })
                .then((response) => {
                    if (response.data.Success) {
                        commit('setSensorLocation', response.data.Data[0])
                        resolve(response)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    saveSensorLocation({ commit, state }, paramObj) {
        const formData = new FormData()
        formData.append('attachFile1', paramObj.attachFile1)
        formData.append('attachFile2', paramObj.attachFile2)
        formData.append('attachFile3', paramObj.attachFile3)
        formData.append('attachFile4', paramObj.attachFile4)
        formData.append('attachFile5', paramObj.attachFile5)
        formData.append('jsonData', paramObj.jsonData)
        return new Promise((resolve, reject) => {
            axios
                .post('/Sensor/SensorLocationSave', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then((response) => {
                    if (response.data.Success) {
                        resolve(response)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },

    /**
     * 센서 대응메뉴얼
     */
    initSensorManual({ commit }) {
        commit('initSensorManual')
    },
    getSensorManual({ commit }, paramObj) {
        return new Promise((resolve, reject) => {
            axios
                .post('/Sensor/SensorManualDetail', paramObj)
                .then((response) => {
                    if (response.data.Success) {
                        commit('setSensorManual', response.data.Data[0])
                        resolve(response)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },

    /**********************************************************************/
    /* 데이터메시지 */
    /**********************************************************************/

    /**
     * 센서 데이터메시지
     */
    initDataMessageList({ commit }) {
        commit('setDataMessageList', [])
    },
    getDataMessageList({ commit }, paramObj) {
        return new Promise((resolve, reject) => {
            axios
                .post('/Monnit/SensorDataMessages', paramObj)
                .then((response) => {
                    if (response.data.Success) {
                        commit('setDataMessageList', response.data.Data.Result)
                        resolve(response)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    }
}
