import * as loginUtil from '@/utils/login'
import jwtDecode from 'jwt-decode'

export default {
    /**
     * 로케일
     */
    setLocale(state, paramObj) {
        state.locale = paramObj
    },

    /**
     * 로그인 데이터 초기화
     */
    InitLoginData(state) {
        // Store 지우기
        state.loginData.UserName = ''
        state.loginData.UserId = ''
        state.loginData.UserType = ''
    },

    /**
     * 로그인 데이터 저장
     */
    SetLoginData(state, paramObj) {
        // state 저장
        const userData = jwtDecode(paramObj.AccessToken)
        state.loginData.UserName = userData.unique_name
        state.loginData.UserId = userData.nameid
        state.loginData.UserType = userData.role
        // 토큰 쿠키 저장
        loginUtil.setLoginData({
            AccessToken: paramObj.AccessToken,
            RefreshToken: paramObj.RefreshToken,
            UserName: userData.unique_name,
            UserId: userData.nameid
        })
    },

    /**
     * 자동 로그인 옵션
     */
    SetAutologin(state, Autologin) {
        state.loginData.Autologin = Autologin
    }
}
