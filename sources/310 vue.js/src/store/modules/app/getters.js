export default {
    /**
     * 로케일
     */
    locales: (state) => {
        return state.locales
    },
    locale: (state) => {
        return state.locale
    },

    /**
     * 로그인 데이터
     */
    loginData: (state) => {
        return state.loginData
    }
}
