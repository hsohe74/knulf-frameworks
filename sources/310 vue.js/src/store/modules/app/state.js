/**
 * persistedstate 로 브라우저의 저장소에 저장이 되는 스토어
 */
export default {
    /**
     * 로케일
     */
    locale: 'ko',
    locales: [
        { text: '한국어', value: 'ko' },
        { text: 'English', value: 'en' }
    ],

    /**
     * 로그인 데이터
     */
    loginData: {
        UserName: '',
        UserId: '',
        UserType: '',
        Autologin: false
    }
}
