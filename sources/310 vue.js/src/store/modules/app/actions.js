import axios from '@/plugins/axios'
import axiosPublic from '@/plugins/axiosPublic'
import vueCookie from '@/plugins/cookie'
import router from '@/router'
import * as loginUtil from '@/utils/login'
import * as dataUtil from '@/utils/data'
import fileDownload from 'js-file-download'

export default {
    /**
     * 로케일
     */
    setLocale({ commit }, paramObj) {
        // 다국어 설정
        commit('setLocale', paramObj)
    },

    /**
     * 로그인 데이터 초기화
     */
    InitLoginData({ commit }) {
        // 로그인 데이터 초기화
        loginUtil.initLoginData()
        // 메뉴 데이터 등 초기화
        commit('InitLoginData')
    },

    /**
     * 로그인 여부 체크
     */
    checkLogined({ state }) {
        return new Promise((resolve, reject) => {
            if (!vueCookie.get('AccessToken')) {
                resolve({ success: false })
            } else {
                resolve({ success: true })
            }
        })
    },

    /**
     * 로그인
     */
    LoginProc({ commit, dispatch }, loginFormData) {
        return new Promise((resolve, reject) => {
            axiosPublic
                .post('/User/CreateToken', loginFormData)
                .then((response) => {
                    if (response.data.Success) {
                        // 로그인 데이터 Store에 저장
                        commit('SetLoginData', response.data.Data)
                        // 자동 로그인 처리
                        commit('SetAutologin', loginFormData.Autologin)
                        // 결과반환
                        resolve(response)
                    } else {
                        // 자동 로그인 처리
                        commit('SetAutologin', false)
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },

    /**
     * 자동 로그인
     */
    AutoLoginProc({ commit, dispatch }) {
        return new Promise((resolve, reject) => {
            const refreshToken = vueCookie.get('RefreshToken')
            axiosPublic
                .post('/User/RefreshToken', null, { headers: { Authorization: 'Bearer ' + refreshToken } })
                .then((response) => {
                    if (response.data.Success) {
                        // 로그인 데이터 Store에 저장
                        commit('SetLoginData', response.data.Data)
                        // 결과반환
                        resolve(response)
                    } else {
                        // 자동 로그인 처리
                        commit('SetAutologin', false)
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },

    /**
     * 로그아웃
     */
    Logout({ dispatch }) {
        // 로그인 데이터 초기화
        dispatch('InitLoginData')
        // 로그인 화면 이동
        router.push({ name: 'Login' })
    },

    /**
     * 파일 다운로드
     * @param {*} paramObj filePath, fileName 을 담고 있다.
     */
    DownloadFile({ state }, paramObj) {
        // 파일 저장경로가 윈도즈인 경우를 한정해서 처리
        var reqObj = {
            FilePath: paramObj.filePath
        }
        return new Promise((resolve, reject) => {
            axios({
                url: '/Common/Download',
                method: 'POST',
                responseType: 'blob',
                data: reqObj
            })
                .then((response) => {
                    var filename = ''
                    if (!paramObj.filename) {
                        var disposition = response.headers['content-disposition']
                        if (disposition && disposition.indexOf('attachment') > -1) {
                            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/
                            var matches = filenameRegex.exec(disposition)
                            if (matches != null && matches[1]) {
                                filename = matches[1].replace(/['"]/g, '')
                            }
                        }
                    } else {
                        filename = paramObj.filename
                    }
                    fileDownload(response.data, decodeURI(filename))
                })
                .catch((error) => {
                    reject(dataUtil.getResponseData(error.response))
                })
        })
    },

    /**
     * 이미지 다운로드
     * 다운로드 데이터를 base64기반으로 변환. 큰 이미지는 지양.
     * 리턴받은 response를 data에 저장하고 image태그의 src에 이를 반영한다.
     * @param {*} paramObj filePath, fileName 을 담고 있다.
     */
    downloadImage({ state }, paramObj) {
        var reqObj = {
            FilePath: paramObj.filePath
        }
        return new Promise((resolve, reject) => {
            axios({
                url: '/Common/Download',
                method: 'POST',
                responseType: 'arraybuffer',
                data: reqObj
            })
                .then((response) => {
                    var bytes = new Uint8Array(response.data)
                    var binary = bytes.reduce((data, b) => (data += String.fromCharCode(b)), '')
                    resolve('data:image/jpeg;base64,' + btoa(binary))
                })
                .catch((error) => {
                    reject(dataUtil.getResponseData(error.response))
                })
        })
    }
}
