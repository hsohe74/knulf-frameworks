const requireModule = require.context('.', true, /\.js$/)
const modules = {}

requireModule.keys().forEach(fileName => {
    if (fileName === './index.js') return

    // Replace ./ and .js
    const path = fileName.replace(/(\.\/|\.js)/g, '')
    const moduleName = path.substring(0, path.lastIndexOf('/')).replace(/\//gi, '')
    const imported = path.substring(path.lastIndexOf('/') + 1)

    if (!modules[moduleName]) {
        modules[moduleName] = {
            namespaced: true
        }
    }

    if (imported === undefined) {
        modules[moduleName] = requireModule(fileName).default
    } else {
        modules[moduleName][imported] = requireModule(fileName).default
    }
})

export default modules
