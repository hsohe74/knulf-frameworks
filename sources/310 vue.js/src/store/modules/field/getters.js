export default {
    /**********************************************************************/
    /* 사업현장 */
    /**********************************************************************/

    /**
     * 사업현장 센서 상세
     */
    fieldSensorDetailParams: state => {
        return state.fieldSensorDetailParams
    },
    fieldSensorDetailData: state => {
        return state.fieldSensorDetailData
    }
}
