import axios from '@/plugins/axios'
import * as dataUtil from '@/utils/data'
export default {
    /**********************************************************************/
    /* 사업현장 */
    /**********************************************************************/

    /**
     * 사업현장 센서 상세
     */
    initFieldSensorDetailParams({ commit }) {
        commit('initFieldSensorDetailParams')
    },
    setFieldSensorDetailParams({ commit }, paramObj) {
        commit('setFieldSensorDetailParams', paramObj)
    },
    initFieldSensorDetailData({ commit }) {
        commit('initFieldSensorDetailData')
    },
    getFieldSensorDetailData({ commit, state }) {
        return new Promise((resolve, reject) => {
            axios
                .post('/Field/FieldSensorDetail', state.fieldSensorDetailParams)
                .then((response) => {
                    if (response.data.Success) {
                        commit('setFieldSensorDetailData', response.data.Data[0])
                        resolve(response)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    }
}
