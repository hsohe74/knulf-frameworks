import { setting } from '@/variables'
export default {
    /**********************************************************************/
    /* 사업현장 */
    /**********************************************************************/

    /**
     * 사업현장 센서 상세
     */
    fieldSensorDetailParams: {
        FieldSeq: 0,
        SensorID: 0
    },
    fieldSensorDetailData: {
        ParentSeq: 0,
        ParentNm: '',
        GroupSeq: 0,
        GroupNm: '',
        SensorID: 0,
        SensorNm: '',
        ApplicationID: 0,
        LastCommunicationDate: '',
        NextCommunicationDate: '',
        LastDataMessageGUID: '',
        Status: 0,
        CurrentReading: '',
        SignalStrength: 0,
        BatteryLevel: 0
    }
}
