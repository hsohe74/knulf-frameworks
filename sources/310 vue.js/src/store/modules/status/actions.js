import axios from '@/plugins/axios'
import * as dataUtil from '@/utils/data'
export default {
    /**********************************************************************/
    /* 사업현장 현황 */
    /**********************************************************************/

    /**
     * 사업현장 현황
     */
    initFieldStatusParams({ commit }) {
        commit('initFieldStatusParams')
    },
    setFieldStatusParams({ commit }, paramObj) {
        commit('setFieldStatusParams', paramObj)
    },
    initFieldStatusData({ commit }) {
        commit('initFieldStatusData')
    },
    getFieldStatusData({ commit, state }) {
        return new Promise((resolve, reject) => {
            axios
                .post('/Status/FieldStatus', state.fieldStatusParams)
                .then((response) => {
                    if (response.data.Success) {
                        commit('setFieldStatusData', response.data.Data)
                        resolve(response)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },

    /**
     * 센서 목록
     */
    setSensorListParams({ commit }, paramObj) {
        commit('setSensorListParams', paramObj)
    },
    initSensorListData({ commit }) {
        commit('setSensorListData', [])
    },
    getSensorListData({ commit, state }) {
        return new Promise((resolve, reject) => {
            axios
                .post('/Status/FieldStatusSensor', state.sensorListParams)
                .then((response) => {
                    if (response.data.Success) {
                        commit('setSensorListData', response.data.Data)
                        resolve(response)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },

    /**
     * 알람 목록
     */
    initAlarmListParams({ commit }) {
        commit('initAlarmListParams')
    },
    setAlarmListParams({ commit }, paramObj) {
        commit('setAlarmListParams', paramObj)
    },
    initAlarmListData({ commit }) {
        commit('setAlarmListData', [])
    },
    getAlarmListData({ commit, state }) {
        return new Promise((resolve, reject) => {
            axios
                .post('/Status/FieldStatusAlarm', state.alarmListParams)
                .then((response) => {
                    if (response.data.Success) {
                        commit('setAlarmListData', response.data.Data)
                        resolve(response)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },

    /**
     * 알람 요약 1 (화면용)
     */
    initAlarmSummary1Params({ commit }) {
        commit('initAlarmSummary1Params')
    },
    setAlarmSummary1Params({ commit }, paramObj) {
        commit('setAlarmSummary1Params', paramObj)
    },
    initAlarmSummary1Data({ commit }) {
        commit('setAlarmSummary1Data', [])
    },
    getAlarmSummary1Data({ commit, state }) {
        return new Promise((resolve, reject) => {
            axios
                .post('/Status/FieldStatusAlarmSummary', state.alarmSummary1Params)
                .then((response) => {
                    if (response.data.Success) {
                        commit('setAlarmSummary1Data', response.data.Data)
                        resolve(response)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },

    /**
     * 알람 요약 2 (화면용)
     */
    initAlarmSummary2Params({ commit }) {
        commit('initAlarmSummary2Params')
    },
    setAlarmSummary2Params({ commit }, paramObj) {
        commit('setAlarmSummary2Params', paramObj)
    },
    initAlarmSummary2Data({ commit }) {
        commit('setAlarmSummary2Data', [])
    },
    getAlarmSummary2Data({ commit, state }) {
        return new Promise((resolve, reject) => {
            axios
                .post('/Status/FieldStatusAlarmSummary', state.alarmSummary2Params)
                .then((response) => {
                    if (response.data.Success) {
                        commit('setAlarmSummary2Data', response.data.Data)
                        resolve(response)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },

    /**
     * 사업현장 센서그룹 경로
     */
    setFieldGroupPathParams({ commit }, paramObj) {
        commit('setFieldGroupPathParams', paramObj)
    },
    initFieldGroupPathData({ commit }) {
        commit('initFieldGroupPathData')
    },
    getFieldGroupPathData({ commit, state }) {
        return new Promise((resolve, reject) => {
            axios
                .post('/Status/FieldGroupPath', state.fieldGroupPathParams)
                .then((response) => {
                    if (response.data.Success) {
                        commit('setFieldGroupPathData', response.data.Data[0])
                        resolve(response)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    }
}
