import { setting } from '@/variables'
export default {
    /**********************************************************************/
    /* 사업현장 현황 */
    /**********************************************************************/

    /**
     * 사업현장 현황
     */
    fieldStatusParams: {
        FieldNm: ''
    },
    fieldListData: [],
    fieldGroupData: [],
    fieldStatusData: [],

    /**
     * 센서 목록
     */
    sensorListParams: {
        FieldSeq: 0,
        GroupSeq: 0
    },
    sensorListData: [],

    /**
     * 알람 목록
     */
    alarmListParams: {
        IsAcknowledgedYn: 'N',
        StartDate: '',
        EndDate: '',
        FieldSeq: 0,
        GroupSeq: 0,
        SensorID: 0,
        PageNumber: 1,
        PageSize: 0
    },
    alarmListData: [],

    /**
     * 알람 요약 1 (화면용)
     */
    alarmSummary1Params: {
        IsAcknowledgedYn: 'N',
        StartDate: '',
        EndDate: '',
        FieldSeq: 0,
        GroupSeq: 0,
        SensorID: 0,
        PageNumber: 1,
        PageSize: 0
    },
    alarmSummary1Data: [],

    /**
     * 알람 요약 2 (팝업용)
     */
    alarmSummary2Params: {
        IsAcknowledgedYn: 'N',
        StartDate: '',
        EndDate: '',
        FieldSeq: 0,
        GroupSeq: 0,
        SensorID: 0,
        PageNumber: 1,
        PageSize: 0
    },
    alarmSummary2Data: [],

    /**
     * 사업현장 센서그룹 경로
     */
    fieldGroupPathParams: {
        FieldSeq: 0,
        GroupSeq: 0
    },
    fieldGroupPathData: {
        GroupPath: ''
    }
}
