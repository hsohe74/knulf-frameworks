export default {
    /**********************************************************************/
    /* 사업현장 현황 */
    /**********************************************************************/

    /**
     * 사업현장 현황
     */
    fieldStatusParams: (state) => {
        return state.fieldStatusParams
    },
    fieldListData: (state) => {
        return state.fieldListData
    },
    fieldGroupData: (state) => {
        return state.fieldGroupData
    },
    fieldStatusData: (state) => {
        return state.fieldStatusData
    },

    /**
     * 센서 목록
     */
    sensorListParams: (state) => {
        return state.sensorListParams
    },
    sensorListData: (state) => {
        return state.sensorListData
    },

    /**
     * 알람 목록
     */
    alarmListParams: (state) => {
        return state.alarmListParams
    },
    alarmListData: (state) => {
        return state.alarmListData
    },

    /**
     * 알람 요약 1 (화면용)
     */
    alarmSummary1Params: (state) => {
        return state.alarmSummary1Params
    },
    alarmSummary1Data: (state) => {
        return state.alarmSummary1Data
    },

    /**
     * 알람 요약 2 (팝업용)
     */
    alarmSummary2Params: (state) => {
        return state.alarmSummary1Params
    },
    alarmSummary2Data: (state) => {
        return state.alarmSummary1Data
    },

    /**
     * 사업현장 센서그룹 경로
     */
    fieldGroupPathParams: (state) => {
        return state.fieldGroupPathParams
    },
    fieldGroupPathData: (state) => {
        return state.fieldGroupPathData
    }
}
