import Vue from 'vue'
import { setting } from '@/variables'

export default {
    /**********************************************************************/
    /* 사업현장 현황 */
    /**********************************************************************/

    /**
     * 사업현장 현황
     */
    initFieldStatusParams(state) {
        state.fieldStatusParams = {
            FieldNm: ''
        }
    },
    setFieldStatusParams: (state, paramObj) => {
        state.fieldStatusParams = paramObj
    },
    initFieldStatusData(state) {
        state.fieldStatusData = []
        state.fieldGroupData = []
        state.fieldListData = []
    },
    setFieldStatusData: (state, paramObj) => {
        // 사업현장 현황 데이터 (현장, 분류, 그룹)
        state.fieldStatusData = paramObj
        // 사업현장별 센서그룹 데이터 : FieldSeq, ParentSeq로 중복제거한 목록을 추출
        state.fieldGroupData = Vue._.uniqWith(paramObj, (A, B) => A.FieldSeq === B.FieldSeq && A.ParentSeq === B.ParentSeq)
        // 사업현장 데이터 : FieldSeq로 중복제거한 목록을 추출
        state.fieldListData = Vue._.uniqBy(paramObj, 'FieldSeq')
    },

    /**
     * 센서 목록
     */
    InitSensorListParams(state) {
        state.sensorListParams = {
            FieldSeq: 0,
            GroupSeq: 0
        }
    },
    setSensorListParams: (state, paramObj) => {
        state.sensorListParams = paramObj
    },
    setSensorListData: (state, paramObj) => {
        state.sensorListData = paramObj
    },

    /**
     * 알람 목록
     */
    initAlarmListParams(state) {
        state.alarmListParams = {
            IsAcknowledgedYn: 'N',
            StartDate: '',
            EndDate: '',
            FieldSeq: 0,
            GroupSeq: 0,
            SensorID: 0,
            PageNumber: 1,
            PageSize: 0
        }
    },
    setAlarmListParams: (state, paramObj) => {
        state.alarmListParams = paramObj
    },
    setAlarmListData: (state, paramObj) => {
        state.alarmListData = paramObj
    },

    /**
     * 알람 요약 1 (화면용)
     */
    initAlarmSummary1Params(state) {
        state.alarmSummary1Params = {
            IsAcknowledgedYn: 'N',
            StartDate: '',
            EndDate: '',
            FieldSeq: 0,
            GroupSeq: 0,
            SensorID: 0,
            PageNumber: 1,
            PageSize: 0
        }
    },
    setAlarmSummary1Params: (state, paramObj) => {
        state.alarmSummary1Params = paramObj
    },
    setAlarmSummary1Data: (state, paramObj) => {
        state.alarmSummary1Data = paramObj
    },

    /**
     * 알람 요약 2 (팝업용)
     */
    initAlarmSummary2Params(state) {
        state.alarmSummary2Params = {
            IsAcknowledgedYn: 'N',
            StartDate: '',
            EndDate: '',
            FieldSeq: 0,
            GroupSeq: 0,
            SensorID: 0,
            PageNumber: 1,
            PageSize: 0
        }
    },
    setAlarmSummary2Params: (state, paramObj) => {
        state.alarmSummary2Params = paramObj
    },
    setAlarmSummary2Data: (state, paramObj) => {
        state.alarmSummary2Data = paramObj
    },

    /**
     * 사업현장 센서그룹 경로
     */
    InitFieldGroupPathParams(state) {
        state.fieldGroupPathParams = {
            FieldSeq: 0,
            GroupSeq: 0
        }
    },
    setFieldGroupPathParams: (state, paramObj) => {
        state.fieldGroupPathParams = paramObj
    },
    initFieldGroupPathData: (state) => {
        state.fieldGroupPathData = {
            GroupPath: ''
        }
    },
    setFieldGroupPathData: (state, paramObj) => {
        state.fieldGroupPathData = paramObj
    }
}
