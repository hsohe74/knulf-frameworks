import axios from '@/plugins/axios'
export default {
    /**********************************************************************/
    /* 공통 */
    /**********************************************************************/

    /**
     * 공통코드
     */
    getComCodeCdList({ commit, state }, groupCd) {
        return new Promise((resolve, reject) => {
            axios
                .post('/Common/ComCodeCdList', { GrpCd: groupCd })
                .then((response) => {
                    if (response.data.Success) {
                        resolve(response.data.Data)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    /**
     * 전역설정
     * GrpCd: 전역설정 그룹코드
     * ConfCd: 설정코드 (없으면 그룹전체값 반환)
     */
    getGlobalConfList({ commit, state }, paramObj) {
        return new Promise((resolve, reject) => {
            axios
                .post('/Common/GlobalConfList', paramObj)
                .then((response) => {
                    if (response.data.Success) {
                        resolve(response.data.Data)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    saveGlobalConfValue({ commit, state }, paramObj) {
        return new Promise((resolve, reject) => {
            axios
                .post('/Common/GlobalConfSave', paramObj)
                .then((response) => {
                    if (response.data.Success) {
                        resolve(response.data)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },

    /**********************************************************************/
    /* 트리거 */
    /**********************************************************************/

    /**
     * 센서유형별 트리거 목록
     */
    getSensorTypeTrgList({ commit, state }, applicationID) {
        return new Promise((resolve, reject) => {
            axios
                .post('/SensorType/SensorTypeTrgList', { ApplicationID: applicationID })
                .then((response) => {
                    if (response.data.Success) {
                        resolve(response.data.Data)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },

    /**********************************************************************/
    /* 사용자 */
    /**********************************************************************/

    /**
     * 사용자
     */
    getUserList({ commit, state }, paramObj) {
        return new Promise((resolve, reject) => {
            axios
                .post('/User/UserList', paramObj)
                .then((response) => {
                    if (response.data.Success) {
                        resolve(response.data)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },

    /**********************************************************************/
    /* 모넷 */
    /**********************************************************************/

    /**
     * Datum
     */
    getDatumByTypeList({ commit, state }, applicationID) {
        return new Promise((resolve, reject) => {
            axios
                .post('/Monnit/DatumByTypeList', { ApplicationID: applicationID })
                .then((response) => {
                    if (response.data.Success) {
                        resolve(response.data.Data.Result)
                    } else {
                        reject(response)
                    }
                })
                .catch((error) => {
                    reject(error)
                })
        })
    }
}
