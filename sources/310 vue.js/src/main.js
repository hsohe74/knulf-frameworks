/**
 * NAME:    main.js
 * DESC:    main
 * DATE:    2021-11-18
 * CREATOR: hsohe74
 * HISTORY:
 *     2021-11-18 hsohe74 생성
 */
import Vue from 'vue'
import App from './App.vue'

// router & store
import { sync } from 'vuex-router-sync'
import router from './router'
import store from './store'

// i18n
import i18n from './i18n'

// buefy
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

// fontawesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// lodash
import VueLodash from 'vue-lodash'
import lodash from 'lodash'

// modal
import VModal from 'vue-js-modal'

// Form validate
import { ValidationProvider, ValidationObserver } from 'vee-validate'

// moment
import moment from 'moment'
import VueMomentJS from 'vue-momentjs'

// luxon (for vue-datetime)
import { Settings } from 'luxon'

// etc
import './components'
import './plugins'
import './filters'
import focus from '@/directives/focus'
import onlynumber from '@/directives/onlynumber'

// Sync store with router
sync(store, router)

// i18n
if (store.state.app.locale === undefined || store.state.app.locale == null || store.state.app.locale === '') {
    var userLang = navigator.language || navigator.userLanguage
    if (userLang.indexOf('ko') > -1) {
        store.state.app.locale = 'ko'
    } else {
        store.state.app.locale = 'en'
    }
}
i18n.locale = store.state.app.locale

// Buefy
Vue.use(Buefy, {
    defaultIconComponent: 'vue-fontawesome',
    defaultIconPack: 'fas',
    customIconPacks: {
        fas: {
            sizes: {
                default: 'lg',
                'is-small': '1x',
                'is-medium': '2x',
                'is-large': '3x'
            },
            iconPrefix: ''
        }
    }
})

// fortawesome
library.add(fas)
Vue.component('vue-fontawesome', FontAwesomeIcon)

// lodash
Vue.use(VueLodash, { name: 'custom', lodash: lodash })

// modal
Vue.use(VModal, { dynamic: true })

// Form validate
Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)

// moment
Vue.use(VueMomentJS, moment)

// luxon (for vue-datetime)
Settings.defaultLocale = 'ko'

// etc
Vue.directive('focus', focus)
Vue.directive('onlynumber', onlynumber)

// Vue
Vue.config.productionTip = false
new Vue({
    router,
    store,
    i18n,
    render: (h) => h(App)
}).$mount('#app')
