/**
 * NAME:    cookie.js
 * DESC:    vue-cookies 래핑
 * DATE:    2021-11-18
 * CREATOR: hsohe74
 * HISTORY:
 *     2021-11-18 hsohe74 생성
 */
import Vue from 'vue'
import VueCookie from 'vue-cookies'

Vue.use(VueCookie)
Vue.prototype.$cookies = VueCookie

export default VueCookie
