/**
 * NAME:    axios.js
 * DESC:    토큰 사용을 기반으로 한 axios를 어플리케이션에 맞게 래핑
 * DATE:    2021-11-18
 * CREATOR: hsohe74
 * HISTORY:
 *     2021-11-18 hsohe74 생성
 */
import Vue from 'vue'
import axios from 'axios'
import router from '@/router'
import store from '@/store'
import vueCookie from '@/plugins/cookie'
import { setting } from '@/variables.js'
import * as loginUtil from '@/utils/login'

let isRefreshing = false
let failedQueue = []

const processQueue = (error, token = null) => {
    failedQueue.forEach((prom) => {
        if (error) {
            prom.reject(error)
        } else {
            prom.resolve(token)
        }
    })

    failedQueue = []
}

/**
 * axios 인스턴스를 만들어 이를 사용.
 */
const baseApi = axios.create({
    baseURL: setting.apiBaseURL
})

/**
 * Request interceptor
 */
baseApi.interceptors.request.use(
    async function(config) {
        // 헤더 토큰을 발급받은 토큰으로 설정
        config.headers.Authorization = 'Bearer ' + vueCookie.get('AccessToken')
        // Locale
        if (config.data !== undefined) {
            // config.data.append('locale', store.state.app.locale);
        }
        // 반환
        return config
    },
    function(error) {
        return Promise.reject(error)
    }
)

/**
 * Response interceptor
 */
baseApi.interceptors.response.use(
    function(response) {
        return response
    },
    async function(error) {
        const originalRequest = error.config
        if (error.response.status === 401 && !originalRequest._retry) {
            if (isRefreshing) {
                return new Promise(function(resolve, reject) {
                    failedQueue.push({ resolve, reject })
                })
                    .then(() => {
                        originalRequest.headers.Authorization = 'Bearer ' + vueCookie.get('AccessToken')
                        return axios(originalRequest)
                    })
                    .catch((err) => {
                        return Promise.reject(err)
                    })
            }

            originalRequest._retry = true
            isRefreshing = true

            const refreshToken = vueCookie.get('RefreshToken')
            return axios
                .post(setting.apiBaseURL + '/User/RefreshToken', null, { headers: { Authorization: 'Bearer ' + refreshToken } })
                .then((response) => {
                    if (response.data.Success) {
                        // 리턴값
                        const returnData = response.data.Data

                        // 토큰 쿠키 저장
                        loginUtil.setTokenData(returnData)

                        // 헤더 토큰을 새로 발급받은 토큰으로 재설정
                        baseApi.defaults.headers.common.Authorization = 'Bearer ' + returnData.AccessToken
                        processQueue(null, returnData.AccessToken)

                        // 반환
                        return baseApi(originalRequest)
                    } else {
                        store.commit('app/InitLoginData')
                        router.push({ name: 'Login' })
                        processQueue(error, null)
                    }
                })
                .catch((err) => {
                    store.commit('app/InitLoginData')
                    router.push({ name: 'Login' })
                    processQueue(err, null)
                })
                .finally(() => {
                    isRefreshing = false
                })
        } else if (error.response.status === 601) {
            store.commit('app/InitLoginData')
            router.push({ name: 'Login' })
            processQueue(error, null)
            isRefreshing = false
            throw new Error()
        }
    }
)

// Set http
Vue.prototype.$http = baseApi

export default baseApi
