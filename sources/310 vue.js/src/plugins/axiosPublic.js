/**
 * NAME:    axiosPublic.js
 * DESC:    axios를 래핑 (토큰 미사용)
 * DATE:    2021-11-18
 * CREATOR: hsohe74
 * HISTORY:
 *     2021-11-18 hsohe74 생성
 */
// import Vue from 'vue'
import axios from 'axios'
import { setting } from '@/variables.js'

/**
 * axios 인스턴스를 만들어 이를 사용.
 */
const publicApi = axios.create({
    baseURL: setting.apiBaseURL
})

/**
 * Request interceptor
 */
publicApi.interceptors.request.use(
    async function(config) {
        // 반환
        return config
    },
    function(error) {
        return Promise.reject(error)
    }
)

export default publicApi
