import Vue from 'vue'
import _ from 'lodash'

/*
    ㅇ. 서브폴더를 만들경우
        -. 폴더명과 파일명을 조합해서 컴포넌트명이 생성된다.
        -. 생성된 컴포넌트명은 FolderFile 이런식이며 실제 태그로 사용시는 folder-file이다.
*/

const requireComponent = require.context('@/components', true, /\.vue$/)

requireComponent.keys().forEach(fileName => {
    const componentConfig = requireComponent(fileName)
    const componentName = _.upperFirst(_.camelCase(fileName.replace(/^\.\//, '').replace(/\.\w+$/, '')))
    Vue.component(componentName, componentConfig.default || componentConfig)
})
