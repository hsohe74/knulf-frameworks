/**
 * NAME:    data.js
 * DESC:    데이터 처리에 대한 유틸
 * DATE:    2021-11-18
 * CREATOR: hsohe74
 * HISTORY:
 *     2021-11-18 hsohe74 생성
 */
/**
 * 배열로 부터 Hierarchy 구조를 만드는 함수 (id, parent가 있어야 함)
 * @param {*} arry id, parent 구조로 되어 있는 배열 (논리 !으로 적용되는 모든것)
 */
 export const buildHierarchy = function(arry, idProp, parentProp) {
    let roots = [];
    let children = {};

    // find the top level nodes and hash the children based on parent
    for (let i = 0, len = arry.length; i < len; ++i) {
        let item = arry[i];
        let p = item[parentProp];

        if (!item[parentProp]) {
            roots.push(item);
        } else {
            if (!children[p]) {
                children[p] = [];
            }
            children[p].push(item);
        }
    }

    // function to recursively build the tree
    const findChildren = function(parent) {
        if (children[parent[idProp]]) {
            parent.children = children[parent[idProp]];
            for (let i = 0, len = parent.children.length; i < len; ++i) {
                findChildren(parent.children[i]);
            }
        }
    };

    // enumerate through to handle the case where there are multiple roots
    for (let i = 0, len = roots.length; i < len; ++i) {
        findChildren(roots[i]);
    }

    return roots;
}

/**
 * Reset list parameters
 * @param {*} listParam
 */
 export const resetListParams = function(listParam) {
    listParam.start = listParam.pageNumber - 1;
    listParam.limit = listParam.pageSize;
    listParam.startIndex = (listParam.pageNumber - 1) * listParam.pageSize + 1;
    listParam.endIndex = listParam.pageNumber * listParam.pageSize;
}

/**
 * 그리드 목록데이터와 조회 파라미터의 페이지 값을 읽어서 rn값을 자동 설정.
 * @param {*} listData 조회 데이터 (실제 list data array)
 * @param {*} listParam 조회 파라미터 (페이지 설정등이 들어간)
 */
export const resetListNumber = function(listData, listParam) {
    listData.forEach((element, index) => {
        element.rn = (index + 1) + ((listParam.pageNumber - 1) * listParam.pageSize);
    });
}

/**
 * FormData 를 가져옴
 */
export const getFormData = function(items) {
    var formData = new FormData()
    for (var key in items) {
        if (items[key] == null) {
            delete items[key]
        } else {
            formData.append(key, items[key])
        }
    }
    return formData
}

/**
 * Response 데이터를 가공처리
 */
export const getResponseData = function(response) {
    let returnData

    if (response !== undefined) {
        if (!response.data) {
            returnData = {}
        } else {
            returnData = response.data
        }

        if (response.status !== undefined && response.status === false) {
            returnData.status = response.status
            returnData.alertEnable = response.status !== 601
        }
    }

    return returnData
}

/**
 * 객체를 deep copy하여 반환
 * @param {*} object 복사할 대상
 */
export const copyObject = function(object) {
    return JSON.parse(JSON.stringify(object))
}

/**
 * 모바일 디바이스 여부를 체크하여 반환
 */
export const detectMobile = function() {
    /* eslint-disable */
    var check = false
    ;(function(a) {
        if (
            /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
                a
            ) ||
            /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
                a.substr(0, 4)
            )
        )
            check = true
    })(navigator.userAgent || navigator.vendor || window.opera)
    return check
}

/**
 * Remove HTML Tag
 * @param {*} html
 */
export const RemoveHtmlTag = function(html) {
    if (!html) {
        return ''
    } else {
        return html.replace(/(<([^>]+)>)/gi, '')
    }
}
