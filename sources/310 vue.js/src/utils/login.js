/**
 * NAME:    login.js
 * DESC:    로그인 처리에 대한 유틸
 * DATE:    2021-11-18
 * CREATOR: hsohe74
 * HISTORY:
 *     2021-11-18 hsohe74 생성
 */
import vueCookie from '@/plugins/cookie'

/**
 * 로그인 데이터 초기화
 */
export const initLoginData = function() {
    // 쿠키 지우기
    vueCookie.remove('AccessToken')
    vueCookie.remove('RefreshToken')
}

/**
 * 로그인 데이터 설정
 */
export const setLoginData = function(paramObj) {
    // 토큰 쿠키 저장
    vueCookie.set('AccessToken', paramObj.AccessToken)
    vueCookie.set('RefreshToken', paramObj.RefreshToken)
}

/**
 * 토큰 저장
 */
export const setTokenData = function(paramObj) {
    // 토큰 쿠키 저장
    vueCookie.set('AccessToken', paramObj.AccessToken)
    vueCookie.set('RefreshToken', paramObj.RefreshToken)
}
