/**
 * 라우터 생성/반환
 * @param {*} currentItem
 */
 export const makeRoute = function(currentItem) {
    // viewDirectory: view가 있는 상대경로로 directory가 설정되어 있으면 사용하고 없으면 path를 이용
    // viewPath: 실제 view가 있는 전체 경로
    let viewDirectory = (currentItem.directory === undefined) ? (currentItem.path.length > 0 ? currentItem.path : '') : currentItem.directory;
    let viewPath = (viewDirectory.length <= 1) ? 'views' : 'views' + viewDirectory;

    // 경로 끝에 /가 오면 그걸 제거
    if (viewPath.substring(viewPath.length - 1) == '/') {
        viewPath = viewPath.substring(0, viewPath.length - 1);
    }

    // 라우터 아이템을 생성
    let routerItem = {
        name: currentItem.name || currentItem.view,
        path: currentItem.path,
        props: true,
        viewPath: viewPath,
        meta: {
            title: !currentItem.title ? currentItem.name : currentItem.title,
            engTitle: !currentItem.engTitle ? currentItem.name : currentItem.engTitle
        },
        component: (resolve) => import(`@/${viewPath}/${currentItem.view}.vue`).then(resolve)
    }

    // children이 있을 경우 해당 하위 라우터를 구성함
    if (currentItem.children !== undefined && currentItem.children != null) {
        routerItem.children = [];
        currentItem.children.forEach(element => {
            element.noscan = true;
            // 경로
            if (viewDirectory.length > 0 && (element.directory === undefined || element.directory.length == null || element.directory.length == 0)) {
                element.directory = viewDirectory;
            }
            // 권한
            if (currentItem.requiredAuth !== undefined && element.requiredAuth === undefined) {
                element.requiredAuth = currentItem.requiredAuth;
            }
            if (currentItem.allowedAuth !== undefined && element.allowedAuth === undefined) {
                element.allowedAuth = true;
            }
            let childrenItem = makeRoute(element);
            routerItem.children.push(childrenItem);
        });
    }

    // 각 뷰의 하위 폴더에 정의된 라우터가 있을 경우 현재 라우터의 children으로 등록
    if (currentItem.directoryContent !== undefined && currentItem.directoryContent != null && currentItem.directoryContent == true) {
        try {
            if (require(`@/${viewPath}/index.js`)) {
                let childRoutes = require(`@/${viewPath}/index.js`).routes;
                if (childRoutes !== undefined && childRoutes.length > 0) {
                    routerItem.children = [];
                    childRoutes.forEach(element => {
                        element.noscan = true;
                        // 경로
                        if (viewDirectory.length > 0 && (element.directory === undefined || element.directory.length == null || element.directory.length == 0)) {
                            element.directory = viewDirectory;
                        }
                        // 권한
                        if (currentItem.requiredAuth !== undefined && element.requiredAuth === undefined) {
                            element.requiredAuth = currentItem.requiredAuth;
                        }
                        if (currentItem.allowedAuth !== undefined && element.allowedAuth === undefined) {
                            element.allowedAuth = true;
                        }
                        let childrenItem = makeRoute(element);
                        routerItem.children.push(childrenItem);
                    })
                }
            }
        } catch (e) {
            console.log(currentItem, ': ', e);
        }
    }

    // 라우터 아이템 반환
    return routerItem
}

/**
 * Home라우터의 하위라우터를 메뉴목록에서 가져와서 구성한다.
 * 메인 메뉴는 별도의 라우터 설정정보에서 가져와서 구성한다.
 * @param {*} router
 * @param {*} menuList
 */
 import mainRouterInfo from '@/router/mainRouter'
 export const addHomeSubRoute = function(router, menuList) {
     let rootPath = "/home";
     let homeRouter = router.options.routes.find(r => r.path === rootPath);

     // 홈라우터 초기화
     homeRouter.children = [];

     // 메인
     let mainRouterItem = makeRoute(mainRouterInfo);
     homeRouter.children.push(mainRouterItem);
     // 기타
     menuList.forEach(menuItem => {
         // 라우터 정보 설정
         if (menuItem.menuUrl != undefined && menuItem.menuUrl != null) {
             if (menuItem.menuUrl == rootPath + menuItem.menuDir) {
                 // 디텍터리 경로인 경우
                 let routerInfo = {
                     path: menuItem.menuUrl,
                     name: menuItem.menuCd,
                     title: menuItem.menuNm,
                     engTitle: menuItem.menuEngNm,
                     view: 'index',
                     directory: menuItem.menuDir,
                     directoryContent: true,
                     allowedAuth: true
                 }
                 let routerItem = makeRoute(routerInfo);
                 homeRouter.children.push(routerItem);
             } else {
                 // 파일 경로인 경우
                 let routerInfo = {
                     path: menuItem.menuUrl,
                     name: menuItem.menuCd,
                     title: menuItem.menuNm,
                     engTitle: menuItem.menuEngNm,
                     view: menuItem.menuUrl.substring(menuItem.menuUrl.lastIndexOf("/") + 1),
                     directory: menuItem.menuDir,
                     directoryContent: false,
                     allowedAuth: true
                 }
                 let routerItem = makeRoute(routerInfo);
                 homeRouter.children.push(routerItem);
             }
         }
     })
     router.addRoutes([homeRouter]);
 }
