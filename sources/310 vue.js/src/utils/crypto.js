/**
 * SHA256
 * @param {*} inputVal
 */
 export const sha256 = function(inputVal) {
    let sha256 = require("js-sha256").sha256
    return sha256(inputVal);
}

/**
 * MD5
 * @param {*} inputVal
 */
import crypto from 'crypto'
export const md5 = function(inputVal) {
    let md5 = crypto.createHash("md5");
    md5.update(inputVal);
    return md5.digest('hex');
}
