import 'chartjs-plugin-colorschemes'

/**
 * Line chart
 * @param {*} title
 */
export const getLineChartOptions = function(yMax, title, xStack, yStack, toolTipEnabled, xTickStep, yTickStep, callback) {
    return {
        responsive: true,
        maintainAspectRatio: false,
        title: {
            display: title !== undefined && title !== '',
            text: title,
            fontSize: 15,
            fontColor: '#ffff'
        },
        legend: {
            display: true,
            position: 'left',
            align: 'start',
            labels: {
                fontSize: 10,
                fontColor: '#333'
            }
        },
        tooltips: {
            enabled: toolTipEnabled === undefined ? true : toolTipEnabled,
            position: 'nearest',
            mode: 'index',
            intersect: false
        },
        scales: {
            xAxes: [
                {
                    stacked: xStack == null,
                    gridLines: {
                        display: true,
                        color: 'rgba(0, 0, 0, 0.2)',
                        drawBorder: true,
                        drawOnChartArea: true,
                        drawTicks: true
                    },
                    ticks: {
                        stepSize: xTickStep === undefined ? 10 : xTickStep,
                        fontSize: 10,
                        fontColor: '#333',
                        autoSkip: false,
                        maxRotation: 45,
                        minRotation: 45
                    },
                    type: 'time',
                    time: {
                        unit: 'minute',
                        unitStepSize: 60,
                        displayFormats: {
                            hour: 'MM-DD hh:mm',
                            minute: 'MM-DD hh:mm'
                        }
                    }
                }
            ],
            yAxes: [
                {
                    stacked: yStack == null,
                    gridLines: {
                        display: true,
                        color: 'rgba(0, 0, 0, 0.2)',
                        drawBorder: true,
                        drawOnChartArea: true,
                        drawTicks: true
                    },
                    ticks: {
                        max: yMax === 0 ? undefined : yMax,
                        beginAtZero: false,
                        stepSize: yTickStep === undefined ? 10 : yTickStep,
                        fontColor: '#333'
                    }
                }
            ]
        },
        elements: {
            line: {
                tension: 0.5,
                borderWidth: 0.8
            }
        },
        animation: {
            onComplete: !callback === false ? callback : null
        },
        plugins: {
            labels: {
                render: 'value',
                showZero: true,
                position: 'outside',
                outsidePadding: 10,
                textMargin: 5,
                fontColor: '#999'
            },
            colorschemes: {
                scheme: 'brewer.Paired12'
            }
        }
    }
}
