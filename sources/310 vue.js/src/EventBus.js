/**
 * 전역 이벤트 처리기로 사용할 EventBus
 */
import Vue from 'vue'

const EventBus = new Vue()

export default EventBus
