/**
 * directory : view 경로 (디렉토리 경로로 세분화할경우 지정 필요)
 */
 export default [
    {
        path: '/',
        name: 'Login',
        view: 'login',
        directory: '/'
    },
    {
        path: '/home',
        name: 'Home',
        view: 'home',
        directory: '/'
    },
    {
        path: '/help',
        name: 'helpViewer',
        view: 'helpViewer',
        directory: '/help'
    },
    {
        path: '/404',
        name: '404',
        view: '404',
        directory: '/'
    }
]
