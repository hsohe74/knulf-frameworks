// Lib imports
import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'
import paths from './paths'

import store from '@/store'
import { makeRoute, addHomeSubRoute } from '@/utils/router'

Vue.use(Router)
Vue.use(Meta)

let routers = paths.map(path => makeRoute(path));
const router = new Router({
    mode: 'history',
    routes: routers.concat([
        { path: '*', redirect: '/404', meta: { requiredAuth: false, allowedAuth: true } }
    ]),
    scrollBehavior (to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        }
        if (to.hash) {
            return { selector: to.hash }
        }
        return { x: 0, y: 0 }
    }
})
// router.beforeEach((to, from, next) => {
//     if (to.path != '/' && to.path != '/home/main') {
//         console.log('router to: ', to, ', from: ', from);
//     }
//     next();
// })

// menuList가 저장소에 있을 경우 Home라우터를 재구성한다.
let homeRouter = router.options.routes.find(r => r.path === "/home");
if (store.state.app.menuList.length > 0 && (homeRouter.children === undefined || homeRouter.children.length < 1)) {
    addHomeSubRoute(router, store.state.app.menuList);
}

export default router
