/**
 * directory : view 경로 (디렉토리 경로로 세분화할경우 지정 필요)
 */
 export default {
    path: '/home/main',
    name: 'Main',
    title: "메인",
    view: 'index',
    directory: '/main',
    directoryContent: true,
    allowedAuth: true
}
