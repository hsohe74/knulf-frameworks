/**
 * NAME:    filters.js
 * DESC:    필터 모음
 * DATE:    2021-11-18
 * CREATOR: hsohe74
 * HISTORY:
 *     2021-11-18 hsohe74 생성
 */
import Vue from 'vue'

Vue.filter('formatNumber', function(value) {
    // eslint-disable-next-line no-new-wrappers
    var num = new Number(value)
    return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, '$1,')
})
