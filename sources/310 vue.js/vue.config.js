module.exports = {
    pluginOptions: {
        i18n: {
            locale: process.env.VUE_APP_I18N_LOCALE,
            fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE,
            localeDir: 'locales',
            enableInSFC: false
        }
    },

    devServer: {
        disableHostCheck: true,
        port: 8085,
        proxy: {
            '/api': {
                target: 'http://localhost:9090',
                ws: true,
                changeOrigin: true
            }
        }
    },

    outputDir: 'dist/',
    assetsDir: 'resources/assets/'
}
