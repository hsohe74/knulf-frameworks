import { useRef, useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './styles/App.css'

function App() {
    const [count, setCount] = useState(0)
    const addNum = (val: number) => {
        setCount((count) => count + val)
    }

    const [inputs, setInputs] = useState({
        name: '',
        nickname: ''
    })
    const nameInput = useRef<HTMLInputElement>(null)

    const { name, nickname } = inputs
    const onChange = (e) => {
        const { value, name } = e.target
        setInputs({
            ...inputs,
            [name]: value
        })
    }

    const onReset = () => {
        setInputs({
            name: '',
            nickname: ''
        })
        nameInput.current.focus()
    }

    return (
        <>
            <div>
                <a href="https://vitejs.dev" target="_blank">
                    <img src={viteLogo} className="logo" alt="Vite logo" />
                </a>
                <a href="https://react.dev" target="_blank">
                    <img src={reactLogo} className="logo react" alt="React logo" />
                </a>
            </div>
            <h1>Vite + React</h1>
            <div className="card">
                <button onClick={() => addNum(1)}>count is {count}</button>
                &nbsp;
                <button onClick={() => addNum(-1)}>count is {count}</button>
                &nbsp;
                <button onClick={onReset}>초기화</button>
                <p>
                    Edit <code>src/App.tsx</code> and save to test HMR
                </p>
            </div>
            <div>
                <input type="text" name="name" value={name} ref={nameInput} onChange={onChange} />
                <input type="text" name="nickname" value={nickname} onChange={onChange} />
                <div>{name}</div>
                <div>{nickname}</div>
            </div>
            <p className="read-the-docs">Click on the Vite and React logos to learn more</p>
        </>
    )
}

export default App
