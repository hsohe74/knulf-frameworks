import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [react()],
    resolve: {
        extensions: ['.js', 'ts', '.tsx', '.json']
    },
    server: {
        port: 9194,
        proxy: {
            '/api': {
                target: 'http://localhost:9092',
                changeOrigin: true,
                secure: false
                //rewrite: (path) => path.replace('/api', '')
            }
        }
    },
    build: {
        assetsDir: 'resources/assets/'
    }
})
