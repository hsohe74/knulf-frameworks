// install event
self.addEventListener('install', (event) => {
    console.log('[Service Worker] installed', event)
})

// activate event

self.addEventListener('activate', (event) => {
    console.log('[Service Worker] actived', event)
})

// fetch event
self.addEventListener('fetch', (event) => {
    // console.log('[Service Worker] fetched resource ' + event.request.url)
})

// push
self.addEventListener('push', (event) => {
    const notiString = event.data.text()
    console.log('notiString: ', notiString)
    const notiData = JSON.parse(notiString)
    var options = {
        body: notiData.body,
        icon: 'favicon.ico',
        vibrate: [200, 100, 200, 100, 200, 100, 200],
        tag: 'vibration-sample'
    }

    event.waitUntil(self.registration.showNotification(notiData.title, options))
})

// notificationclick
// self.addEventListener('notificationclick', (event) => {
//     console.log('notificationclick > event: ', event)
//     var notification = event.notification
//     var action = event.action

//     if (action === 'close') {
//         notification.close()
//     } else {
//         notification.close()
//     }
// })
