/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

module.exports = {
    root: true,
    extends: ['plugin:vue/vue3-essential', 'eslint:recommended', '@vue/eslint-config-prettier/skip-formatting'],
    parserOptions: {
        ecmaVersion: 'latest'
    },
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        indent: ['error', 4],
        // 'max-len': ['error', { code: 140 }],
        'vue/html-indent': [
            'error',
            4,
            {
                attribute: 1,
                baseIndent: 1,
                closeBracket: 0,
                alignAttributesVertically: true,
                ignores: []
            }
        ],
        'space-before-function-paren': [
            'error',
            {
                anonymous: 'ignore',
                named: 'ignore',
                asyncArrow: 'ignore'
            }
        ],
        /* 사용하지 않는 변수 */
        'no-unused-vars': 'off',
        /* 컴포넌트 다중단어 */
        'vue/multi-word-component-names': 'off'
    }
}
