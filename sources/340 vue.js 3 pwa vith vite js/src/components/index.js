/**
 * Name : Component auto import
 * Desc : 폴더 하나와 파일명으로 컴포넌트를 등록하여 사용가능하게 함. 카멜케이스에서 첫자만 대문자로 처리.
 */
import _ from 'lodash'

export default {
    install(app) {
        // 컴포넌트 목록 (Vite의 globEager를 사용하여 여러 컴포는트를 import)
        const componentFiles = import.meta.glob('@/components/**/*.vue', { eager: true })
        Object.entries(componentFiles).forEach(([path, component]) => {
            const componentName = _.upperFirst(
                _.camelCase(
                    path
                        .replace('/src', '')
                        .replace('/components', '')
                        .replace(/^\.\//, '')
                        .replace(/\.\w+$/, '')
                )
            )
            app.component(`${componentName}`, component.default)
        })
    }
}
