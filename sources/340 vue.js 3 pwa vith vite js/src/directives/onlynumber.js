export const onlynumber = {
    mounted: (el) => {
        el.oninput = (event) => {
            const formattedValue = parseInt(event.target.value)
            el.value = isNaN(formattedValue) ? '' : formattedValue
        }
    }
}
