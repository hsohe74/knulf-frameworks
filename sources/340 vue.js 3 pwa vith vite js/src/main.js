import { createApp } from 'vue'

import App from '@/App.vue'
import pinia from '@/stores'
import router from '@/router'
import i18n from '@/locales'
import components from '@/components'
import directives from '@/directives'

/* fontawesome */
import { library } from '@fortawesome/fontawesome-svg-core' // fontawesome core
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome' // font awesome icon component
import { fas } from '@fortawesome/free-solid-svg-icons' // specific icons
library.add(fas) // add icons to the library

/* Create app */
const app = createApp(App)
app.use(pinia)
app.use(router)
app.use(i18n)
app.use(components)
app.use(directives)
app.component('font-awesome-icon', FontAwesomeIcon)
app.config.globalProperties.foo = 'bar'
app.mount('#app')

/* 설정된 로케일 불러들임 */
import { useAppStore } from '@/stores/app'
const appStore = useAppStore()
appStore.initLocale()
