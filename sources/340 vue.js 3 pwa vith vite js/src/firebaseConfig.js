import { initializeApp } from 'firebase/app'
import { getToken, getMessaging, isSupported } from 'firebase/messaging'

export const firebaseVapidKey =
    'BNDHd1yHx6SGEziVgM20idfhpbkkG8ZU7N9D1euHM0gn6at1gJJDcaqFxKw-B96DrhapE-fRqce0LyVGHeL7tWU'
export const firebaseConfig = {
    apiKey: 'AIzaSyBUQUFh5l1MGpw-WoN2lgvRba_lzRxyGXA',
    authDomain: 'livespot-2d7d0.firebaseapp.com',
    projectId: 'livespot-2d7d0',
    storageBucket: 'livespot-2d7d0.appspot.com',
    messagingSenderId: '757364179955',
    appId: '1:757364179955:web:504e95bad4b4ff10b60886'
}

export const firebaseApp = initializeApp(firebaseConfig)
export const messaging = getMessaging(firebaseApp)

// getOrRegisterServiceWorker function is used to try and get the service worker if it exists, otherwise it will register a new one.
export const getOrRegisterServiceWorker = () => {
    if ('serviceWorker' in navigator && typeof window.navigator.serviceWorker !== 'undefined') {
        return window.navigator.serviceWorker
            .getRegistration('/firebase-push-notification-scope')
            .then((serviceWorker) => {
                if (serviceWorker) return serviceWorker
                return window.navigator.serviceWorker.register('/firebase-messaging-sw.js', {
                    scope: '/firebase-push-notification-scope'
                })
            })
    }
    throw new Error('The browser doesn`t support service worker.')
}

// getFirebaseToken function generates the FCM token
export const getFirebaseToken = async () => {
    try {
        const messagingResolve = await messaging
        if (messagingResolve) {
            return getOrRegisterServiceWorker().then((serviceWorkerRegistration) => {
                return Promise.resolve(
                    getToken(messagingResolve, {
                        vapidKey: firebaseVapidKey,
                        serviceWorkerRegistration
                    })
                )
            })
        } else {
            return Promise.reject('messagingResolve false')
        }
    } catch (error) {
        alert('An error occurred while retrieving token. ' + error)
    }
}
