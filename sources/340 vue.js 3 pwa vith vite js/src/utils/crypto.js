import { sha256 } from 'js-sha256'

/**
 * SHA256
 * @param {*} inputVal
 * @returns
 */
export const SHA256 = function (inputVal) {
    return sha256(inputVal)
}
