/**
 * Common store
 * - 공통코드, 전역변수
 */
import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useAppStore } from '@/stores/app'
import axiosPublic from '@/plugins/axiosPublic'
import * as deviceUtil from '@/utils/device'

export const useCommonStore = defineStore('common', () => {
    /**
     * Reference
     */
    const appStore = useAppStore()

    /**********************************************************************/
    /* WebApp */
    /**********************************************************************/

    const isWebApp = ref(false)
    isWebApp.value = deviceUtil.isWebApp()

    const isNotificationGranted = ref(false)

    /**********************************************************************/
    /* 공통코드 */
    /**********************************************************************/

    /**
     * 공통코드
     */
    const getComCodeCdList = async function (groupCd) {
        try {
            const response = await axiosPublic.post('/Common/ComCodeCdList', { GrpCd: groupCd })
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**********************************************************************/
    /* 전역설정 */
    /**********************************************************************/

    /**
     * 전역설정
     * GrpCd: 전역설정 그룹코드
     * ConfCd: 설정코드 (없으면 그룹전체값 반환)
     */
    const getGlobalConfList = async (paramObj) => {
        try {
            const response = await axiosPublic.post('/Common/GlobalConfList', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**********************************************************************/
    /* Common */
    /**********************************************************************/

    /**
     * Return
     */
    return {
        /* WebApp */
        isWebApp,
        isNotificationGranted,
        /* 공통코드 */
        getComCodeCdList,
        /* 전역설정 */
        getGlobalConfList
    }
})
