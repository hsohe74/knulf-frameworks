/**
 * Site store
 * - 구독 사이트
 */
import { ref, reactive } from 'vue'
import { defineStore } from 'pinia'
import { useAppStore } from '@/stores/app'
import axios from '@/plugins/axios'
import * as dataUtil from '@/utils/data'

export const useSiteStore = defineStore('site', () => {
    /**
     * Reference
     */
    const appStore = useAppStore()

    /**********************************************************************/
    /* 사이트 */
    /**********************************************************************/

    /**
     * 사이트 목록
     */
    const siteParamInit = reactive({
        ...appStore.listParam,
        ...{
            SiteProduct: ''
        }
    })
    const siteParam = reactive({
        ...siteParamInit
    })
    const initSiteParam = () => {
        Object.assign(siteParam, siteParamInit)
    }
    const siteList = ref([])
    const initSiteList = () => {
        siteParam.TotalRecord = 0
        siteList.value = []
    }
    const setSiteList = (paramObj) => {
        siteParam.TotalRecord = paramObj.TotalRecord
        dataUtil.setPagingData(siteParam)
        siteList.value = paramObj.Data
    }
    const getSiteList = async () => {
        try {
            const response = await axios.post('/Site/SiteList', siteParam)
            if (response.data.Success) {
                setSiteList(response.data)
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }
    const getSiteListRaw = async (paramObj) => {
        try {
            const response = await axios.post('/Site/SiteList', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**
     * 사이트 상세
     */
    const siteDetail = ref({})
    const initSiteDetail = () => {
        siteDetail.value = {
            SubscriberSeq: 0,
            SiteSeq: 0,
            SiteProduct: 'LIVESPOT'
        }
    }
    const setSiteDetail = (paramObj) => {
        siteDetail.value = {
            ...paramObj
        }
    }
    const getSiteDetail = async (paramObj) => {
        try {
            const response = await axios.post('/Site/SiteDetail', paramObj)
            if (response.data.Success) {
                setSiteDetail(response.data.Data)
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }
    const saveSiteDetail = async (paramObj) => {
        try {
            const response = await axios.post('/Site/SiteSave', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }
    const deleteSiteDetail = async function (siteSeq) {
        const paramObj = {
            SiteSeq: siteSeq,
            StatusCd: '99'
        }
        try {
            const response = await axios.post('/Site/SiteSetStatus', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**********************************************************************/
    /* 사이트 사용자 */
    /**********************************************************************/

    /**
     * 사이트 사용자
     */
    const siteUserParamInit = reactive({
        ...appStore.listParam
    })
    const siteUserParam = reactive({
        ...siteUserParamInit
    })
    const initSiteUserParam = () => {
        Object.assign(siteUserParam, siteUserParamInit)
    }
    const siteUserList = ref([])
    const initSiteUserList = () => {
        siteUserList.value = []
    }
    const setSiteUserList = (paramObj) => {
        siteUserList.value = paramObj
    }
    const getSiteUserList = async () => {
        try {
            const response = await axios.post('/Site/SiteUserList', siteUserParam)
            if (response.data.Success) {
                setSiteUserList(response.data)
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }
    const getSiteUserListRaw = async (paramObj) => {
        try {
            const response = await axios.post('/Site/SiteUserList', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**
     * 사이트 사용자 상세
     */
    let siteUserDetail = ref({})
    const initSiteUserDetail = () => {
        siteUserDetail.value = {}
    }
    const setSiteUserDetail = (paramObj) => {
        siteUserDetail.value = {
            ...paramObj
        }
    }
    const getSiteUserDetail = async (paramObj) => {
        try {
            const response = await axios.post('/Site/SiteUserDetail', paramObj)
            if (response.data.Success) {
                setSiteUserDetail(response.data.Data)
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**
     * 사이트 사용자 추가/제거
     */
    const addSiteUser = async (paramObj) => {
        try {
            const response = await axios.post('/Site/SiteUserAdd', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }
    const removeSiteUser = async (paramObj) => {
        try {
            const response = await axios.post('/Site/SiteUserRemove', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**
     * 사이트에서 허용된 사용자인지 확인 (SiteHost만 인자로 전달)
     */

    const checkSiteAllowedUser = async (paramObj) => {
        try {
            const response = await axios.post('/Site/SiteAllowedUser', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**********************************************************************/
    /* Common */
    /**********************************************************************/

    /**
     * Return
     */
    return {
        /* 사이트 */
        siteParam,
        initSiteParam,
        siteList,
        initSiteList,
        setSiteList,
        getSiteList,
        getSiteListRaw,
        siteDetail,
        initSiteDetail,
        setSiteDetail,
        getSiteDetail,
        saveSiteDetail,
        deleteSiteDetail,
        /* 사이트 사용자 */
        siteUserParam,
        initSiteUserParam,
        siteUserList,
        initSiteUserList,
        setSiteUserList,
        getSiteUserList,
        getSiteUserListRaw,
        siteUserDetail,
        initSiteUserDetail,
        setSiteUserDetail,
        getSiteUserDetail,
        /* 사이트 사용자 추가/제거 */
        addSiteUser,
        removeSiteUser,
        /* 사이트에서 허용된 사용자인지 확인 (SiteHost만 인자로 전달) */
        checkSiteAllowedUser
    }
})
