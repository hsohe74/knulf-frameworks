/**
 * Vender store
 * - 제조사
 */
import { ref, reactive } from 'vue'
import { defineStore } from 'pinia'
import axiosPublic from '@/plugins/axiosPublic'

export const useVenderStore = defineStore('vender', () => {
    /**
     * Reference
     */

    /**********************************************************************/
    /* 제조사 */
    /**********************************************************************/

    /**
     * 제조사 목록
     */
    const vendorList = ref([])
    const initVendorList = () => {
        vendorList.value = []
    }
    const setVendorList = (paramObj) => {
        vendorList.value = paramObj
    }
    const getVendorList = async function () {
        try {
            const response = await axiosPublic.post('/Vendor/VendorList', null)
            if (response.data.Success) {
                setVendorList(response.data.Data)
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**********************************************************************/
    /* Common */
    /**********************************************************************/

    /**
     * Return
     */
    return {
        /* 제조사 */
        vendorList,
        initVendorList,
        setVendorList,
        getVendorList
    }
})
