/**
 * Subscriber store
 * - 구독자
 */
import { ref, reactive } from 'vue'
import { defineStore } from 'pinia'
import { useAppStore } from '@/stores/app'
import axios from '@/plugins/axios'
import * as dataUtil from '@/utils/data'

export const useSampleStore = defineStore('sample', () => {
    /**
     * Reference
     */
    const appStore = useAppStore()

    /**********************************************************************/
    /* Sample */
    /**********************************************************************/

    /**
     * Sample
     */
    const sampleListParamInit = reactive({
        ...appStore.listParam
    })
    const sampleListParam = reactive({
        ...sampleListParamInit
    })
    const initSampleListParam = () => {
        Object.assign(sampleListParam, sampleListParamInit)
    }

    /**********************************************************************/
    /* Common */
    /**********************************************************************/

    /**
     * Return
     */
    return {
        /* Sample */
        sampleListParam,
        initSampleListParam
    }
})
