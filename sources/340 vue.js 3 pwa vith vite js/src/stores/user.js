/**
 * User store
 * - LocalStorage에 저장
 * - 로케일 정보, 로그인정보, 공용 데이터 등
 */
import { ref, reactive } from 'vue'
import { defineStore } from 'pinia'
import { useAppStore } from '@/stores/app'
import { useRouter } from 'vue-router'
import axiosPublic from '@/plugins/axiosPublic'
import axios from '@/plugins/axios'
import i18n from '@/locales'
import jwtDecode from 'jwt-decode'
import * as dataUtil from '@/utils/data'

export const useUserStore = defineStore('user', () => {
    /**
     * Reference
     */
    const appStore = useAppStore()
    const router = useRouter()

    /**********************************************************************/
    /* 로그인 */
    /**********************************************************************/

    /**
     * 로그인 되어 있는지 확인
     * @returns
     */
    const checkLogin = () => {
        // 사용자 정보가 없고 토큰 정보만 있는 경우 토큰으로 사용자 정보 채움
        if (!appStore.loginData.UserId && appStore.loginToken.accessToken) {
            appStore.setLoginDataFromToken()
        }
        // 로그인 되어 있는지 확인
        if (appStore.loginData.UserId && appStore.loginToken.accessToken) return true
        else return false
    }

    /**
     * 로그인
     */
    const login = async function (loginFormData) {
        try {
            const response = await axiosPublic.post('/User/CreateToken', loginFormData)
            if (response.data.Success) {
                const userData = jwtDecode(response.data.Data.AccessToken)
                if (loginFormData.UserType > userData.role) {
                    // 지정된 권한보다 작으면 거부하도록 처리
                    response.data.Message = i18n.global.t('ui.message.noAuth')
                    // 결과반환
                    return Promise.reject(response)
                } else {
                    // 로그인 데이터 Store에 저장
                    appStore.setLoginData(response.data.Data)
                    // 결과반환
                    return response
                }
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**
     * 로그아웃
     */
    const logout = function (routePath) {
        // 로그인 데이터 초기화
        appStore.initLoginData().then(() => {
            // 화면 이동
            if (routePath) {
                router.push(routePath)
            } else {
                router.push('/')
            }
        })
    }

    /**********************************************************************/
    /* 사용자 */
    /**********************************************************************/

    /**
     * 사용자 목록
     */
    const userListParamInit = reactive({
        ...appStore.listParam,
        ...{
            SubscriberSeq: 0,
            DvsnSeq: 0,
            FieldSeq: 0,
            UserTypeCd: '',
            StatusCd: ''
        }
    })
    const userListParam = reactive({
        ...userListParamInit
    })
    const initUserListParam = () => {
        Object.assign(userListParam, userListParamInit)
    }
    const userList = ref([])
    const initUserList = () => {
        userListParam.TotalRecord = 0
        userList.value = []
    }
    const setUserList = (paramObj) => {
        userListParam.TotalRecord = paramObj.TotalRecord
        dataUtil.setPagingData(userListParam)
        userList.value = paramObj.Data
    }
    const getUserList = async () => {
        try {
            const response = await axios.post('/User/UserList', userListParam)
            if (response.data.Success) {
                setUserList(response.data)
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**
     * 사용자 상세
     */
    const userDetail = ref({})
    const initUserDetail = () => {
        userDetail.value = {
            UserSeq: 0,
            DvsnSeq: 0,
            FieldSeq: 0,
            UserTypeCd: '',
            StatusCd: '',
            UserNm: ''
        }
    }
    const setUserDetail = (paramObj) => {
        userDetail.value = {
            ...paramObj
        }
    }
    const getUserDetail = async (paramObj) => {
        try {
            const response = await axios.post('/User/UserDetail', paramObj)
            if (response.data.Success) {
                setUserDetail(response.data.Data)
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }
    const saveUserDetail = async (paramObj) => {
        try {
            const response = await axios.post('/User/UserSave', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }
    const deleteUserDetail = async function (userSeq) {
        const paramObj = {
            UserSeq: userSeq,
            StatusCd: '99'
        }
        try {
            const response = await axios.post('/User/UserSetStatus', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }
    const approveUserDetail = async function (userSeq) {
        const paramObj = {
            UserSeq: userSeq,
            StatusCd: '10'
        }
        try {
            const response = await axios.post('/User/UserSetStatus', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**
     * 사용자 개인
     */
    const getMyInfoDetail = async (paramObj) => {
        try {
            const response = await axios.post('/User/MyInfoDetail', paramObj)
            if (response.data.Success) {
                setUserDetail(response.data.Data)
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }
    const saveMyInfoDetail = async (paramObj) => {
        try {
            const response = await axios.post('/User/MyInfoSave', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }
    const saveMySettingNoti = async (paramObj) => {
        try {
            const response = await axios.post('/User/MySettingNotiSave', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }
    const saveMySettingSite = async (paramObj) => {
        try {
            const response = await axios.post('/User/MySettingSiteSave', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**
     * 사용자별 사이트
     */
    const userSiteList = ref([])
    const initUserSiteList = () => {
        userSiteList.value = []
    }
    const setUserSiteList = (paramObj) => {
        userSiteList.value = paramObj
    }
    const getUserSiteList = async (paramObj) => {
        try {
            const response = await axios.post('/User/UserSiteList', paramObj)
            if (response.data.Success) {
                setUserSiteList(response.data.Data)
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }
    const getUserSiteListRaw = async (paramObj) => {
        try {
            const response = await axios.post('/User/UserSiteList', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**********************************************************************/
    /* Common */
    /**********************************************************************/

    /**
     * Return
     */
    return {
        /* 로그인 */
        checkLogin,
        login,
        logout,
        /* 사용자 */
        userListParam,
        initUserListParam,
        userList,
        initUserList,
        setUserList,
        getUserList,
        /* 사용자 상세 */
        userDetail,
        initUserDetail,
        setUserDetail,
        getUserDetail,
        saveUserDetail,
        deleteUserDetail,
        approveUserDetail,
        /* 사용자 개인 */
        getMyInfoDetail,
        saveMyInfoDetail,
        saveMySettingNoti,
        saveMySettingSite,
        /* 사용자별 사이트 */
        userSiteList,
        initUserSiteList,
        getUserSiteList,
        getUserSiteListRaw
    }
})
