/**
 * Division store
 * - LocalStorage에 저장
 * - 로케일 정보, 로그인정보, 공용 데이터 등
 */
import { ref, reactive } from 'vue'
import { defineStore } from 'pinia'
import { useAppStore } from '@/stores/app'
import axios from '@/plugins/axios'
import * as dataUtil from '@/utils/data'

export const useDvsnStore = defineStore('dvsn', () => {
    /**
     * Reference
     */
    const appStore = useAppStore()

    /**********************************************************************/
    /* 사업부 */
    /**********************************************************************/

    /**
     * 사업부 목록
     */
    const dvsnListParamInit = reactive({
        ...appStore.listParam,
        ...{
            SiteSeq: 0,
            SiteHost: '',
            SearchColumn: '',
            SearchWord: '',
            PageNumber: 1,
            PageSize: 0
        }
    })
    const dvsnListParam = reactive({
        ...dvsnListParamInit
    })
    const initDvsnListParam = () => {
        Object.assign(dvsnListParam, dvsnListParamInit)
    }
    const dvsnListData = ref([])
    const initDvsnList = () => {
        dvsnListParam.TotalRecord = 0
        dvsnListData.value = []
    }
    const setDvsnList = (paramObj) => {
        dvsnListParam.TotalRecord = paramObj.TotalRecord
        dataUtil.setPagingData(dvsnListParam)
        dvsnListData.value = paramObj.Data
    }
    const getDvsnList = async () => {
        try {
            const response = await axios.post('/Dvsn/DvsnList', dvsnListParam)
            if (response.data.Success) {
                setDvsnList(response.data)
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }
    const getDvsnListRaw = async (paramObj) => {
        try {
            const response = await axios.post('/Dvsn/DvsnList', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**********************************************************************/
    /* Common */
    /**********************************************************************/

    /**
     * Return
     */
    return {
        /* 사업부 */
        dvsnListParam,
        initDvsnListParam,
        dvsnListData,
        initDvsnList,
        setDvsnList,
        getDvsnList,
        getDvsnListRaw
    }
})
