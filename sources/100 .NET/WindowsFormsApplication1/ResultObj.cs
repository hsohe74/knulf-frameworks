﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
	public class ResultObj
	{
		public int resultCd { get; set; }
		public string resultMsg { get; set; }
		public string resultMsgTyp { get; set; }
		public object data { get; set; }
	}
}
