﻿using knulf;
using knulf.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
	public partial class Form1 : Form
	{
		static string _APIKeyID = "jvcNieHRhTyH";
		static string _APISecretKey = "s3DDOpADgbxFZ7l08iz527Zyjf1kI7nl";
		string baseUrl = "http://localhost:8080";
		string targetUrl = "/api/bsns/cpon/user/selectListCponStdInfo";

		public Form1()
		{
			InitializeComponent();

			ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
		}

		private void btnExecute_Click(object sender, EventArgs e)
		{
			Dictionary<string, string> additionalHeaderData = new Dictionary<string, string>();
			//additionalHeaderData.Add("APIKeyID", _APIKeyID);
			//additionalHeaderData.Add("APISecretKey", _APISecretKey);

			Dictionary<string, object> formData = new Dictionary<string, object>();
			formData.Add("NetworkID", 66059);

			ParamObj paramObj = new ParamObj();
			paramObj.fmlyTypCd = "01";

			ReturnData returnData = RestClientUtil.ExecuteRequest(baseUrl, targetUrl, RestClientUtil.Method.POST, paramObj);

			this.WriteConsole(returnData.Data.ToString());
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Method : Console
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Method : Console

		public void WriteConsole(Exception ex)
		{
			this.WriteConsole(ex.ToString());
		}

		public void WriteConsole(string message)
		{
			if (this.txtConsole.InvokeRequired)
			{
				this.txtConsole.BeginInvoke(new Action(() =>
				{
					this.txtConsole.AppendText(message + "\r\n");
					this.txtConsole.ScrollToCaret();
				}));
			}
			else
			{
				this.txtConsole.AppendText(message + "\r\n");
				this.txtConsole.ScrollToCaret();
			}
		}

		public void ClearConsole()
		{
			if (this.txtConsole.InvokeRequired)
			{
				this.txtConsole.BeginInvoke(new Action(() =>
				{
					this.txtConsole.Clear();
				}));
			}
			else
			{
				this.txtConsole.Clear();
			}
		}

		#endregion
	}
}
