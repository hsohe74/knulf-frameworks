using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Models
{
	public abstract class UserBase : ModelBase
	{
		/// <summary>
		/// 사용자
		/// </summary>
		public long UserSeq { get; set; }
		public string UserNm { get; set; }

		/// <summary>
		/// 로그인 정보
		/// </summary>
		public string LoginId { get; set; }
		public string LoginPw { get; set; }

		/// <summary>
		/// 사용자 유형
		/// </summary>
		public string UserTypeCd { get; set; }
		public string UserTypeNm { get; set; }
		public string UserTypeEnNm { get; set; }

		/// <summary>
		/// 접속 IP
		/// </summary>
		public string ConnectIp { get; set; }
	}
}
