﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Models
{
	public class TokenInfo
	{
		public string Token { get; set; }
		public bool IsValid { get; set; }
		public DateTime ExpiryDate { get; set; }

		public string UserId { get; set; }
		public string UserNm { get; set; }
		public string UserTypeCd { get; set; }
	}
}
