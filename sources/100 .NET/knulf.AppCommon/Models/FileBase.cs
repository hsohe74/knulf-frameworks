using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Models
{
	public abstract class FileBase : ModelBase
	{
		/// <summary>
		/// 파일 Seq
		/// </summary>
		public long FileSeq { get; set; }

		/// <summary>
		/// 파일명
		/// </summary>
		public string FileNm { get; set; }

		/// <summary>
		/// 파일별칭
		/// </summary>
		public string FileAlias { get; set; }

		/// <summary>
		/// 파일 확장자
		/// </summary>
		public string FileExt { get; set; }

		/// <summary>
		/// File size
		/// </summary>
		public long FileSize { get; set; }

		/// <summary>
		/// File path
		/// </summary>
		public string FilePath { get; set; }
	}
}
