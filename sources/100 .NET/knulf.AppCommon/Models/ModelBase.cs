using knulf.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Models
{
	public abstract class ModelBase
	{
		/// <summary>
		/// 상태코드
		/// </summary>
		public string StatusCd { get; set; }
		public string StatusNm { get; set; }
		public string StatusEnNm { get; set; }

		/// <summary>
		/// 등록자
		/// </summary>
		public long RegUserSeq { get; set; }
		public string RegUserId { get; set; }
		public string RegUserNm { get; set; }
		public string RegDatetime { get; set; }

		/// <summary>
		/// 수정자
		/// </summary>
		public long ModUserSeq { get; set; }
		public string ModUserId { get; set; }
		public string ModUserNm { get; set; }
		public string ModDatetime { get; set; }

		/// <summary>
		/// 처리자 ID
		/// </summary>
		public long HandlerSeq { get; set; }
		public string HandlerId { get; set; }

		/// <summary>
		/// 순번
		/// </summary>
		public long Num { get; set; }
	}
}
