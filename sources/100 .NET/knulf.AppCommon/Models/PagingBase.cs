﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Models
{
	/// <summary>
	/// Base model for pageing request
	/// </summary>
	public abstract class PagingBase : ModelBase
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		public PagingBase()
		{
		}

		public PagingBase(int TotalRecord, int pageNumber)
		{
			this.TotalRecord = TotalRecord;
			this.PageNumber = pageNumber;
		}

		public PagingBase(int TotalRecord, int pageNumber, int pageSize, int blockSize)
		{
			this.TotalRecord = TotalRecord;
			this.PageNumber = pageNumber;
			this.PageSize = pageSize;
			this.BlockSize = blockSize;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Properties
		////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// 페이지 수
		/// </summary>
		public int PageNumber { get; set; }

		/// <summary>
		/// 페이지 당 레코드 수 
		/// </summary>
		public int PageSize { get; set; }

		/// <summary>
		/// 페이지 블럭 수
		/// </summary>
		public int BlockNumber { get; set; }

		/// <summary>
		/// 블럭 당 페이지 수
		/// </summary>
		public int BlockSize { get; set; }

		/// <summary>
		/// 전체 레코드 수
		/// </summary>
		public long TotalRecord { get; set; }

		/// <summary>
		/// 전체 페이지 수
		/// </summary>
		public long TotalPage { get; set; }

		/// <summary>
		/// 전체 블럭 수
		/// </summary>
		public long TotalBlock { get; set; }

		/// <summary>
		/// 시작 레코드
		/// </summary>
		public int StartRecord { get; set; }

		/// <summary>
		/// 시작 페이지
		/// </summary>
		public int StartPage { get; set; }

		/// <summary>
		/// 마지막 페이지
		/// </summary>
		public int EndPage { get; set; }

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Method
		////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Set all data
		/// </summary>
		public void SetAllData()
		{
			this.PageNumber = 1;
			this.PageSize = int.MaxValue;
		}

		/// <summary>
		/// Set paging data
		/// </summary>
		public void SetPaging()
		{
			// Row per page
			if (this.PageSize < 1)
				this.PageSize = int.MaxValue;

			// Start record
			this.StartRecord = (this.PageNumber - 1) * PageSize;

			// Total page
			if (this.TotalRecord == 0)
				this.TotalPage = 0;
			else
				this.TotalPage = (this.TotalRecord - 1) / this.PageSize + 1;

			if (this.PageNumber > this.TotalPage)
				this.PageNumber = Convert.ToInt32(this.TotalPage);

			// Total block
			if (this.TotalPage == 0)
				this.TotalBlock = 0;
			else
				this.TotalBlock = (this.TotalPage - 1) / this.BlockSize + 1;

			// Block
			if (this.TotalPage == 0)
				this.BlockNumber = 0;
			else
				this.BlockNumber = (this.PageNumber - 1) / this.BlockSize + 1;

			if (this.BlockNumber > this.TotalBlock)
				this.BlockNumber = Convert.ToInt32(this.TotalBlock);

			// Start page
			if (this.TotalPage > 0 && this.BlockNumber > 0)
				this.StartPage = (this.BlockNumber - 1) * this.BlockSize + 1;

			// End page
			this.EndPage = this.BlockNumber * this.BlockSize;
			if (this.EndPage > this.TotalPage)
				this.EndPage = Convert.ToInt32(this.TotalPage);
		}
	}
}
