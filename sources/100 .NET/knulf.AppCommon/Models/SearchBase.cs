﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Models
{
	public abstract class SearchBase : PagingBase
	{
		/// <summary>
		/// 검색컬럼
		/// </summary>
		public string SearchColumn { get; set; }

		/// <summary>
		/// 검색단어
		/// </summary>
		public string SearchWord { get; set; }

		/// <summary>
		/// 정렬컬럼
		/// </summary>
		public string SortColumn { get; set; }

		/// <summary>
		/// 정렬순서
		/// </summary>
		public string SortDirectioin { get; set; }
	}
}
