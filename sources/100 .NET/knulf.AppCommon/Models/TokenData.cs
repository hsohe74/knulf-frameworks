﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Models
{
	public class TokenData
	{
		/// <summary>
		/// Access token
		/// </summary>
		public string AccessToken { get; set; }

		/// <summary>
		/// Refresh token
		/// </summary>
		public string RefreshToken { get; set; }
	}
}
