﻿using knulf.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Dac
{
	public abstract class DacBase
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Properties
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Properties

		/// <summary>
		/// Get database object
		/// </summary>
		public static IDbObject DbObject { get; set; }

		/// <summary>
		/// Get or set connection string
		/// </summary>
		public static string ConnectionString
		{
			get { return DbObject.ConnectionString; }
			set { DbObject.ConnectionString = value; }
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Utilities
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Utilities

		/// <summary>
		/// Get result data
		/// </summary>
		public static Dictionary<string, object> GetResult(Exception ex)
		{
			Dictionary<string, object> result = new Dictionary<string, object>();
			result.Add(CodeLib._DB_OUT_ReturnCode, 0);
			result.Add(CodeLib._DB_OUT_ReturnMsg, ex.Message);
			return result;
		}

		/// <summary>
		/// Get result data
		/// </summary>
		public static Dictionary<string, object> GetResult(bool isSuccess, string message)
		{
			Dictionary<string, object> result = new Dictionary<string, object>();
			result.Add(CodeLib._DB_OUT_ReturnCode, (isSuccess ? 1 : 0));
			result.Add(CodeLib._DB_OUT_ReturnMsg, message);
			return result;
		}

		/// <summary>
		/// DateTime을 20자리 문자열로 반환
		/// </summary>
		/// <param name="date"></param>
		/// <returns></returns>
		protected static string GetDateTimeString4DB(DateTime date)
		{
			string result = null;

			if (date == DateTime.MinValue)
				result = null;
			else if (date == DateTime.MinValue.ToLocalTime())
				result = null;
			else
				result = date.ToString("yyyy-MM-dd HH:mm:ss");

			return result;
		}

		#endregion
	}
}
