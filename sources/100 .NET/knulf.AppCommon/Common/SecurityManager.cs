﻿using knulf.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Common
{
	public class SecurityManager
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Keys
		////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Common security key
		/// </summary>
		public static string SecurityKey { get; set; }

		/// <summary>
		/// JWT secret key
		/// </summary>
		public static string JwtSecretKey { get; set; }

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Encryption
		////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Get encrypt string by AES
		/// </summary>
		public static string Encrypt(string plainstring, string key = null)
		{
			if (string.IsNullOrEmpty(key) && string.IsNullOrEmpty(SecurityKey))
				key = GetSecurityKey("CommonKey");
			else if (string.IsNullOrEmpty(key) && string.IsNullOrEmpty(SecurityKey) == false)
				key = SecurityKey;

			return CryptoUtil.AESEncrypt(plainstring, key);
		}

		/// <summary>
		/// Get descrypt string by AES
		/// </summary>
		public static string Decrypt(string encryptstring, string key = null)
		{
			if (string.IsNullOrEmpty(key) && string.IsNullOrEmpty(SecurityKey))
				key = GetSecurityKey("CommonKey");
			else if (string.IsNullOrEmpty(key) && string.IsNullOrEmpty(SecurityKey) == false)
				key = SecurityKey;

			return CryptoUtil.AESDecrypt(encryptstring, key);
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// JWT
		////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Get JWT secret key string
		/// </summary>
		/// <returns></returns>
		public static string GetJwtSecretKey()
		{
			return GetSecurityKey("JwtSecretKey");
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Utilities
		////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Get security key string
		/// </summary>
		internal static string GetSecurityKey(string keyName)
		{
			string prvKey = null;
			System.Reflection.Assembly ExecAssmb = System.Reflection.Assembly.GetExecutingAssembly();

			try
			{
				System.IO.Stream st = ExecAssmb.GetManifestResourceStream("knulf.Common.Key." + keyName);
				System.IO.StreamReader sr = new System.IO.StreamReader(st);
				prvKey = sr.ReadToEnd();
			}
			catch { }

			return prvKey;
		}
	}
}
