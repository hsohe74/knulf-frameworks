﻿using knulf.Utility;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Web
{
	public static class JwtManager
	{
		public static readonly string _JWT_BEARER = "Bearer";
		public static readonly long _TOKEN_ACCESS_SECOND = 300; // 5분
		public static readonly long _TOKEN_REFRESH_SECOND = 2592000; // 30일

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Properties
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Properties

		/// <summary>
		/// Secret key for signing
		/// </summary>
		public static string SecretKey { get; set; }

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Method
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Method

		/// <summary>
		/// Generate token string
		/// </summary>
		public static string GenerateToken(
			long expireSeconds,
			string secretKey,
			string userId,
			string userNm,
			string userTypeCd)
		{
			return GenerateToken(
				expireSeconds,
				secretKey,
				new Claim(ClaimTypes.NameIdentifier, userId),
				new Claim(ClaimTypes.Name, userNm),
				new Claim(ClaimTypes.Role, userTypeCd)
			);
		}

		/// <summary>
		/// Generate token string
		/// </summary>
		public static string GenerateToken(
			long expireSeconds,
			string secretKey,
			params Claim[] claims)
		{
			return GenerateToken(expireSeconds, secretKey, true, claims);
		}

		/// <summary>
		/// Generate token string
		/// </summary>
		public static string GenerateToken(
			long expireSeconds,
			string secretKey,
			bool isUseBase64Key,
			params Claim[] claims)
		{
			if (claims.Length < 1)
				return null;

			var symmetricKey = (isUseBase64Key ? Convert.FromBase64String(secretKey) : Encoding.UTF8.GetBytes(secretKey));
			var tokenHandler = new JwtSecurityTokenHandler();

			var tokenDescriptor = new SecurityTokenDescriptor
			{
				NotBefore = DateTime.Now,
				Expires = DateTime.Now.AddSeconds(expireSeconds),
				IssuedAt = DateTime.Now,

				SigningCredentials = new SigningCredentials(
					new SymmetricSecurityKey(symmetricKey),
					SecurityAlgorithms.HmacSha256Signature)
			};
			tokenDescriptor.Subject = new ClaimsIdentity();
			foreach (var claim in claims)
			{
				tokenDescriptor.Subject.AddClaim(claim);
			}

			var securityToken = tokenHandler.CreateToken(tokenDescriptor);
			var token = tokenHandler.WriteToken(securityToken);

			return token;
		}

		/// <summary>
		/// Get Jwt Token with token string
		/// </summary>
		public static JwtSecurityToken GetJwtToken(string token)
		{
			JwtSecurityToken jwtToken = null;

			try
			{
				var tokenHandler = new JwtSecurityTokenHandler();
				jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;
			}
			catch
			{
				throw;
			}

			return jwtToken;
		}

		/// <summary>
		/// Get claims principal with token string
		/// </summary>
		public static ClaimsPrincipal GetPrincipal(string token, string secretKey, bool isUseBase64Key = true)
		{
			try
			{
				var symmetricKey = (isUseBase64Key ? Convert.FromBase64String(secretKey) : Encoding.UTF8.GetBytes(secretKey));
				var tokenHandler = new JwtSecurityTokenHandler();
				var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

				if (jwtToken == null)
					return null;

				var validationParameters = new TokenValidationParameters()
				{
					RequireExpirationTime = true,
					ValidateIssuer = false,
					ValidateAudience = false,
					IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
				};

				SecurityToken securityToken;
				var principal = tokenHandler.ValidateToken(token, validationParameters, out securityToken);

				return principal;
			}
			catch (SecurityTokenExpiredException)
			{
				return null;
			}
			catch (Exception)
			{
				return null;
			}
		}

		/// <summary>
		/// Get expired date
		/// </summary>
		public static DateTime GetExpiredDate(string token)
		{
			try
			{
				var jwtToken = GetJwtToken(token);
				var tokenExpiryDate = jwtToken.ValidTo;
				return tokenExpiryDate;
			}
			catch (Exception)
			{
				return DateTime.MinValue;
			}
		}

		/// <summary>
		/// Get whether token is expired
		/// </summary>
		/// <param name="token"></param>
		/// <returns></returns>
		public static bool IsExpired(string token)
		{
			bool result = false;

			try
			{
				var jwtToken = GetJwtToken(token);
				var tokenExpiryDate = jwtToken.ValidTo;
				var timeSpan = tokenExpiryDate - DateTime.UtcNow;

				if (timeSpan.TotalSeconds < 0)
					result = true;

				return result;
			}
			catch (Exception)
			{
				return true;
			}
		}

		#endregion

	}
}
