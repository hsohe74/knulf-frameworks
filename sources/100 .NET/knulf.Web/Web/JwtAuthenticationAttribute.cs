﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Filters;

namespace knulf.Web
{
	public class JwtAuthenticationAttribute : Attribute, IAuthenticationFilter
	{
		public string Token { get; set; }

		public string Realm { get; set; }

		public bool AllowMultiple => false;

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// IAuthenticationFilter
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region IAuthenticationFilter

		public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
		{
			var request = context.Request;
			var authorization = request.Headers.Authorization;

			if (authorization == null || authorization.Scheme != "Bearer")
				return;

			if (string.IsNullOrEmpty(authorization.Parameter))
			{
				context.ErrorResult = new AuthenticationFailureResult("Missing Jwt Token", request);
				return;
			}

			var token = authorization.Parameter;
			this.Token = token;
			var principal = await AuthenticateJwtToken(token);

			if (principal == null)
				context.ErrorResult = new AuthenticationFailureResult("Invalid token", request);
			else
				context.Principal = principal;
		}

		public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
		{
			Challenge(context);
			return Task.FromResult(0);
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Method
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Method

		protected Task<IPrincipal> AuthenticateJwtToken(string token)
		{
			string userId, userNm, userType;

			if (ValidateToken(token, out userId, out userNm, out userType))
			{
				var claims = new List<Claim>
				{
					new Claim(ClaimTypes.NameIdentifier, userId),
					new Claim(ClaimTypes.Name, userNm),
					new Claim(ClaimTypes.Role, userType)
				};

				var identity = new ClaimsIdentity(claims, "Jwt");
				IPrincipal user = new ClaimsPrincipal(identity);

				return Task.FromResult(user);
			}

			return Task.FromResult<IPrincipal>(null);
		}

		public static bool ValidateToken(string token, out string userId, out string userNm, out string userType)
		{
			userId = null;
			userNm = null;
			userType = null;

			try
			{
				var simplePrinciple = JwtManager.GetPrincipal(token, JwtManager.SecretKey);
				var identity = simplePrinciple.Identity as ClaimsIdentity;

				if (identity == null)
					return false;

				if (!identity.IsAuthenticated)
					return false;

				var userIdClaim = identity.FindFirst(ClaimTypes.NameIdentifier);
				userId = userIdClaim?.Value;
				var loginIdClaim = identity.FindFirst(ClaimTypes.Name);
				userNm = loginIdClaim?.Value;
				var userTypeClaim = identity.FindFirst(ClaimTypes.Role);
				userType = userTypeClaim?.Value;

				if (string.IsNullOrEmpty(userId))
					return false;
				if (string.IsNullOrEmpty(userNm))
					return false;
				if (string.IsNullOrEmpty(userType))
					return false;

				var expiredDatetime = JwtManager.GetExpiredDate(token);
			}
			catch
			{
				throw new HttpResponseException(HttpStatusCode.Unauthorized);
			}

			// More validate to check whether username exists in system
			if (JwtManager.IsExpired(token))
				return false;

			return true;
		}

		private void Challenge(HttpAuthenticationChallengeContext context)
		{
			string parameter = null;

			if (!string.IsNullOrEmpty(Realm))
				parameter = "realm=\"" + Realm + "\"";

			context.ChallengeWith("Bearer", parameter);
		}

		#endregion
	}
}
