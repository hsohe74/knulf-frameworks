﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace knulf.Utility
{
	/// <summary>
	/// Utilities of web api
	/// </summary>
	public class WebUtil4Api
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// File upload
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region File upload

		/// <summary>
		/// Is have file
		/// </summary>
		public static bool IsHaveFile(MultipartFileData file)
		{
			bool result = false;

			try
			{
				string fileName = file.Headers.ContentDisposition.FileName.Replace("\"", "");
				return !string.IsNullOrEmpty(fileName);
			}
			catch
			{
				result = false;
			}

			return result;
		}

		/// <summary>
		/// Save file
		/// </summary>
		public static List<FileUploadResult> SaveFile(MultipartFormDataStreamProvider provider, string targetDirectory)
		{
			return SaveFile(provider, null, targetDirectory);
		}

		/// <summary>
		/// Save file
		/// </summary>
		public static List<FileUploadResult> SaveFile(MultipartFormDataStreamProvider provider, string fileAlias, string targetDirectory)
		{
			List<FileUploadResult> result = new List<FileUploadResult>();
			foreach (MultipartFileData file in provider.FileData)
			{
				result.Add(SaveFile(file, fileAlias, targetDirectory));
			}

			return result;
		}

		/// <summary>
		/// Save file
		/// </summary>
		public static FileUploadResult SaveFile(MultipartFileData file, string targetDirectory)
		{
			return SaveFile(file, null, targetDirectory);
		}

		/// <summary>
		/// Save file
		/// </summary>
		public static FileUploadResult SaveFile(MultipartFileData file, string fileAlias, string targetDirectory)
		{
			FileUploadResult result = null;

			try
			{
				string originalFileName = file.Headers.ContentDisposition.FileName.Replace("\"", "");
				string savedFileName = file.LocalFileName;
				string targetPath = FileUtil.GetCombinePath(targetDirectory, originalFileName);
				targetPath = FileUtil.GetUniqueFilename(targetPath);

				// Move 
				FileUtil.MoveFile(file.LocalFileName, targetPath, true);

				// Get file information
				FileInfo targetFile = new FileInfo(targetPath);

				// Result object
				result = new FileUploadResult(fileAlias, targetPath, targetFile.Length);

				// Image
				if (ImageUtil.CheckIsWebImage(targetPath))
				{
					Size imageSize = ImageUtil.GetImageSize(targetPath);
					result.IsImage = true;
					result.ImageWidth = imageSize.Width;
					result.ImageHeight = imageSize.Height;
				}
				else
				{
					result.IsImage = false;
				}
			}
			catch (Exception ex)
			{
				result = new FileUploadResult(false, ex.Message);
			}

			return result;
		}

		#endregion
	}
}
