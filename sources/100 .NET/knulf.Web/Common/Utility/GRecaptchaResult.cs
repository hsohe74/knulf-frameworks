﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Utility
{
	public class GRecaptchaResult
	{
		public string success { get; set; }
		public string challenge_ts { get; set; }
		public string hostname { get; set; }
	}
}
