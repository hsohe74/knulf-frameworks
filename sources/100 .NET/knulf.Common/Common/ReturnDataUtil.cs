﻿using knulf.Language;
using knulf.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace knulf
{
	public class ReturnDataUtil
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Data conversion
		////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Convert to ReturnData data form DB output data
		/// </summary>
		public static ReturnData ConvertToReturnData(Dictionary<string, object> data)
		{
			ReturnData returnData = new ReturnData();

			if (data != null && data.Count > 0)
			{
				// OUT_ReturnCode
				if (data.Keys.Contains(CodeLib._DB_OUT_ReturnCode))
				{
					if (string.IsNullOrEmpty(data[CodeLib._DB_OUT_ReturnCode].ToString()))
						returnData.Success = true;
					else
						returnData.Success = Convert.ToInt32(data[CodeLib._DB_OUT_ReturnCode]) == 1 ? true : false;
				}
				else
				{
					returnData.Success = true;
				}

				// OUT_ReturnMsg
				if (data.Keys.Contains(CodeLib._DB_OUT_ReturnMsg))
					returnData.Message = data[CodeLib._DB_OUT_ReturnMsg].ToString();
				else
					returnData.Message = string.Empty;

				if (string.IsNullOrEmpty(returnData.Message) == false
					&& returnData.Message.Length >= 12
					&& returnData.Message.Substring(0, 12).Equals(CodeLib._DB_OUT_Successfully, StringComparison.OrdinalIgnoreCase))
					returnData.Message = Lang.Common_Success;

				// OUT_ReturnMsgCode
				if (data.Keys.Contains(CodeLib._DB_OUT_ReturnMsgCode))
					returnData.MessageCode = data[CodeLib._DB_OUT_ReturnMsgCode].ToString();
				else
					returnData.MessageCode = string.Empty;

				// Data
				data.Remove(CodeLib._DB_OUT_ReturnCode);
				data.Remove(CodeLib._DB_OUT_ReturnMsg);
				data.Remove(CodeLib._DB_OUT_ReturnMsgCode);

				if (data.Keys.Contains(CodeLib._DB_OUT_DataSet))
				{
					returnData.DbData = data[CodeLib._DB_OUT_DataSet] as DataSet;
					if (data.Keys.Contains(CodeLib._DB_OUT_TotalRecord))
					{
						returnData.TotalRecord = Convert.ToInt64(data[CodeLib._DB_OUT_TotalRecord]);
					}
					data.Remove(CodeLib._DB_OUT_DataSet);
				}

				returnData.Data = data;
			}
			else
			{
				returnData.Success = false;
				returnData.Message = Lang.Common_ErrorMessage_NoData;
			}

			return returnData;
		}

		/// <summary>
		/// Convert to ReturnData data form DB output data
		/// </summary>
		public static ReturnData ConvertToReturnData<T>(Dictionary<string, object> data) where T : class, new()
		{
			ReturnData returnData = new ReturnData();

			if (data != null && data.Count > 0)
			{
				// OUT_ReturnCode
				if (data.Keys.Contains(CodeLib._DB_OUT_ReturnCode))
				{
					if (string.IsNullOrEmpty(data[CodeLib._DB_OUT_ReturnCode].ToString()))
						returnData.Success = true;
					else
						returnData.Success = Convert.ToInt32(data[CodeLib._DB_OUT_ReturnCode]) == 1 ? true : false;
				}
				else
				{
					returnData.Success = true;
				}

				// OUT_ReturnMsg
				if (data.Keys.Contains(CodeLib._DB_OUT_ReturnMsg))
					returnData.Message = data[CodeLib._DB_OUT_ReturnMsg].ToString();
				else
					returnData.Message = string.Empty;

				if (string.IsNullOrEmpty(returnData.Message) == false
					&& returnData.Message.Length >= 12
					&& returnData.Message.Substring(0, 12).Equals(CodeLib._DB_OUT_Successfully, StringComparison.OrdinalIgnoreCase))
					returnData.Message = Lang.Common_Success;

				// OUT_ReturnMsgCode
				if (data.Keys.Contains(CodeLib._DB_OUT_ReturnMsgCode))
					returnData.MessageCode = data[CodeLib._DB_OUT_ReturnMsgCode].ToString();
				else
					returnData.MessageCode = string.Empty;

				// Data
				data.Remove(CodeLib._DB_OUT_ReturnCode);
				data.Remove(CodeLib._DB_OUT_ReturnMsg);
				data.Remove(CodeLib._DB_OUT_ReturnMsgCode);

				if (data.Keys.Contains(CodeLib._DB_OUT_DataSet))
				{
					List<T> itemListData = new List<T>();
					DataSet dsData = data[CodeLib._DB_OUT_DataSet] as DataSet;

					if (dsData == null || dsData.Tables.Count == 0 || dsData.Tables[0].Rows.Count == 0)
					{
						returnData.Data = itemListData;
						returnData.TotalRecord = 0;
					}
					else
					{
						foreach (DataRow row in dsData.Tables[0].Rows)
						{
							// T item = ReflectionUtil.GetObjectByDataRow<T>(row);
							T item = TypeUtil.ConvertToObject<T>(row);
							itemListData.Add(item);
						}
						returnData.Data = itemListData;

						if (data.Keys.Contains(CodeLib._DB_OUT_TotalRecord))
						{
							returnData.TotalRecord = Convert.ToInt64(data[CodeLib._DB_OUT_TotalRecord]);
						}
						else
						{
							returnData.TotalRecord = itemListData.Count;
						}
					}
					data.Remove(CodeLib._DB_OUT_DataSet);
				}
			}
			else
			{
				returnData.Success = false;
				returnData.Message = Lang.Common_ErrorMessage_NoData;
			}

			return returnData;
		}

		/// <summary>
		/// Convert to ReturnData data form DB output data
		/// </summary>
		public static ReturnData ConvertToReturnObject<T>(Dictionary<string, object> data) where T : class, new()
		{
			ReturnData returnData = new ReturnData();

			if (data != null)
			{
				// OUT_ReturnCode
				if (data.Keys.Contains(CodeLib._DB_OUT_ReturnCode))
				{
					if (string.IsNullOrEmpty(data[CodeLib._DB_OUT_ReturnCode].ToString()))
						returnData.Success = true;
					else
						returnData.Success = Convert.ToInt32(data[CodeLib._DB_OUT_ReturnCode]) == 1 ? true : false;
				}
				else
				{
					returnData.Success = true;
				}

				// OUT_ReturnMsg
				if (data.Keys.Contains(CodeLib._DB_OUT_ReturnMsg))
					returnData.Message = data[CodeLib._DB_OUT_ReturnMsg].ToString();
				else
					returnData.Message = string.Empty;

				if (string.IsNullOrEmpty(returnData.Message) == false
					&& returnData.Message.Length >= 12
					&& returnData.Message.Substring(0, 12).Equals(CodeLib._DB_OUT_Successfully, StringComparison.OrdinalIgnoreCase))
					returnData.Message = Lang.Common_Success;

				// OUT_ReturnMsgCode
				if (data.Keys.Contains(CodeLib._DB_OUT_ReturnMsgCode))
					returnData.MessageCode = data[CodeLib._DB_OUT_ReturnMsgCode].ToString();
				else
					returnData.MessageCode = string.Empty;

				// Data
				data.Remove(CodeLib._DB_OUT_ReturnCode);
				data.Remove(CodeLib._DB_OUT_ReturnMsg);
				data.Remove(CodeLib._DB_OUT_ReturnMsgCode);

				if (data.Keys.Contains(CodeLib._DB_OUT_DataSet))
				{
					DataSet dsData = data[CodeLib._DB_OUT_DataSet] as DataSet;

					if (dsData == null || dsData.Tables.Count == 0 || dsData.Tables[0].Rows.Count == 0)
					{
						returnData.Data = null;
						returnData.TotalRecord = 0;
					}
					else
					{
						foreach (DataRow row in dsData.Tables[0].Rows)
						{
							// T item = ReflectionUtil.GetObjectByDataRow<T>(row);
							T item = TypeUtil.ConvertToObject<T>(row);
							returnData.Data = item;
						}
					}
					data.Remove(CodeLib._DB_OUT_DataSet);
				}
			}
			else
			{
				returnData.Success = false;
				returnData.Message = Lang.Common_ErrorMessage_NoData;
			}

			return returnData;
		}

		/// <summary>
		/// Convert to ReturnData data from Object
		/// </summary>
		public static ReturnData ConvertToReturnData(object data)
		{
			if (data != null)
				return new ReturnData(true, string.Empty, data);
			else
				return new ReturnData(false, Lang.Common_ErrorMessage_NoData);
		}

		/// <summary>
		/// Convert to ReturnJson data from ReturnData
		/// </summary>
		public static ReturnJson ConvertToReturnJson(ReturnData returnData)
		{
			ReturnJson returnJson = new ReturnJson(returnData);
			if (returnData.DbData != null)
			{
				Dictionary<string, object> dbData = new Dictionary<string, object>();
				dbData.Add("listData", returnData.DbData.Tables[0]);
				dbData.Add("total", returnData.TotalRecord);
				returnJson.Data = dbData;
			}
			else
			{
				returnJson.Data = returnData.Data;
			}

			return returnJson;
		}
	}
}
