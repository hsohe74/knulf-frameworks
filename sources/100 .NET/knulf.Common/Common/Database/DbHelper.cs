﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace knulf.Database
{
	/// <summary>
	/// DB 객체에 대한 헬퍼 클래스
	/// </summary>
	public class DbHelper
	{
		public const string _DB_DataSet = "DataSet";
		public const string _DB_Table = "Table";

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// ENUM
		////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// DB type
		/// </summary>
		public enum DbmsType
		{
			MSSQL
		}

		public enum ExecuteType
		{
			Fill, FillEx, ExecuteNonQuery, ExecuteNonQueryEx, FillToList
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// DbObject
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region DbObject

		/// <summary>
		/// DB 객체를 반환
		/// </summary>
		public static IDbObject GetDbObject(DbmsType dbmsType)
		{
			return GetDbObject(dbmsType, null);
		}

		/// <summary>
		/// DB 객체를 반환
		/// </summary>
		public static IDbObject GetDbObject(DbmsType dbmsType, string connectionString)
		{
			IDbObject dbObject = null;
			
			// Make instance
			switch (dbmsType)
			{
				case DbmsType.MSSQL:
					dbObject = new DbObjectMSSQL();
					break;
			}

			// Set connection string
			if (dbObject != null && string.IsNullOrEmpty(connectionString) == false)
			{
				dbObject.ConnectionString = connectionString;
			}

			return dbObject;
		}

		#endregion

		#region DBParameters (MSSQL)

		/// <summary>
		/// DB 파라미터 반환
		/// </summary>
		public static IDataParameter GetParameter(string name, SqlDbType dbType, object value)
		{
			return GetParameter(name, dbType, 0, value, ParameterDirection.Input);
		}

		/// <summary>
		/// DB 파라미터 반환
		/// </summary>
		public static IDataParameter GetParameter(string name, SqlDbType dbType, int size, object value)
		{
			return GetParameter(name, dbType, size, value, ParameterDirection.Input);
		}

		/// <summary>
		/// DB 파라미터 반환
		/// </summary>
		public static IDataParameter GetParameter(string name, SqlDbType dbType, ParameterDirection direction)
		{
			return GetParameter(name, dbType, 0, null, direction);
		}

		/// <summary>
		/// DB 파라미터 반환
		/// </summary>
		public static IDataParameter GetParameter(string name, SqlDbType dbType, int size, ParameterDirection direction)
		{
			return GetParameter(name, dbType, size, null, direction);
		}

		/// <summary>
		/// DB 파라미터 반환
		/// </summary>
		public static IDataParameter GetParameter(string name, SqlDbType dbType, int size, object value, ParameterDirection direction)
		{
			SqlParameter parameter = new SqlParameter();
			parameter.ParameterName = name;
			parameter.SqlDbType = dbType;
			parameter.Direction = direction;

			if (size > 0)
				parameter.Size = size;

			if (value == null)
				parameter.Value = DBNull.Value;
			else
				parameter.Value = value;

			return parameter;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// DataType
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region DataType

		/// <summary>
		/// Get db type for type name string
		/// </summary>
		public static SqlDbType GetSqlDbType(string dbTypeString)
		{
			switch (dbTypeString)
			{
				case "bigint":return SqlDbType.BigInt;
				case "binary": return SqlDbType.Binary;
				case "char": return SqlDbType.Char;
				case "date": return SqlDbType.Date;
				case "datetime": return SqlDbType.DateTime;
				case "decimal": return SqlDbType.Decimal;
				case "float": return SqlDbType.Float;
				case "int": return SqlDbType.Int;
				case "nchar": return SqlDbType.NChar;
				case "ntext": return SqlDbType.NText;
				case "nvarchar": return SqlDbType.NVarChar;
				case "real": return SqlDbType.Real;
				case "smalldatetime": return SqlDbType.SmallDateTime;
				case "smallint": return SqlDbType.SmallInt;
				case "smallmoney": return SqlDbType.SmallMoney;
				case "text": return SqlDbType.Text;
				case "time": return SqlDbType.Time;
				case "timestamp": return SqlDbType.Timestamp;
				case "tinyint": return SqlDbType.TinyInt;
				case "uniqueidentifier": return SqlDbType.UniqueIdentifier;
				case "varbinary": return SqlDbType.VarBinary;
				case "varchar": return SqlDbType.VarChar;
				case "variant": return SqlDbType.Variant;
				default: return SqlDbType.VarChar;
			}
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Util
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Util

		/// <summary>
		/// 양식을 검증하고, 안전한 형태로 반환한다.
		/// </summary>
		public static string GetDbSafeString(string inputValue)
		{
			return GetDbSafeString(inputValue, null);
		}

		/// <summary>
		/// 양식을 검증하고, 안전한 형태로 반환한다.
		/// </summary>
		public static string GetDbSafeString(string inputValue, string defaultValue)
		{
			string resultValue = string.Empty;

			if (string.IsNullOrEmpty(inputValue))
				resultValue = defaultValue;
			else
				resultValue = inputValue;

			if (string.IsNullOrEmpty(resultValue) == false)
				resultValue = resultValue.Replace("'", "''");

			return resultValue;
		}

		#endregion
	}
}
