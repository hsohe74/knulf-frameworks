﻿using knulf.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace knulf.Database
{
	public class DbObjectMSSQL : IDbObject
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Constructer & Initialize

		public DbObjectMSSQL()
		{
			this.ConnectionString = string.Empty;
			this.SchemaSql = @"
select	PARAMETER_MODE, PARAMETER_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH
from	information_schema.parameters
where	specific_name = @ProcName
order by ORDINAL_POSITION asc";
		}

		public DbObjectMSSQL(string connectionString)
		{
			this.ConnectionString = connectionString;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Connection string & schema query
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Connection string & schema query

		/// <summary>
		/// DB 연결 문자열
		/// </summary>
		public string ConnectionString { get; set; }

		/// <summary>
		/// 스키마 쿼리
		/// </summary>
		public string SchemaSql { get; set; }

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Automatic procedure call
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Automatic procedure call

		/// <summary>
		/// Execute procedure with automatic setting parameters
		/// </summary>
		public object ExecuteProcedure(string procedureName, DbHelper.ExecuteType executeType, params object[] data)
		{
			if (string.IsNullOrEmpty(SchemaSql))
			{
				throw new Exception(Language.Lang.Database_Message_NoneSchemaData);
			}

			object result = null;

			try
			{
				// Get specification of procedure
				List<IDataParameter> schemaParameters = new List<IDataParameter>();
				schemaParameters.Add(DbHelper.GetParameter("@ProcName", SqlDbType.VarChar, 40, procedureName));
				DataSet schemaData = this.Fill(this.SchemaSql, CommandType.Text, schemaParameters);

				// Run Procedure
				if (schemaData == null || schemaData.Tables.Count == 0)
				{
					throw new Exception(Language.Lang.Database_Message_NoneSchemaData);
				}
				else
				{
					List<IDataParameter> parameters = this.GetDataParameter(procedureName, schemaData, data);

					switch (executeType)
					{
						case DbHelper.ExecuteType.Fill:
							result = this.Fill(procedureName, CommandType.StoredProcedure, parameters);
							break;
						case DbHelper.ExecuteType.FillEx:
							result = this.FillEx(procedureName, parameters);
							break;
						case DbHelper.ExecuteType.ExecuteNonQuery:
							result = this.ExecuteNonQuery(procedureName, CommandType.StoredProcedure, parameters);
							break;
						case DbHelper.ExecuteType.ExecuteNonQueryEx:
							result = this.ExecuteNonQueryEx(procedureName, parameters);
							break;
					}
				}

			}
			catch
			{
				throw;
			}

			return result;
		}

		/// <summary>
		/// Execute procedure with automatic setting parameters
		/// </summary>
		public object ExecuteProcedure<T>(string procedureName, DbHelper.ExecuteType executeType, params object[] data) where T : class, new()
		{
			if (string.IsNullOrEmpty(SchemaSql))
			{
				throw new Exception(Language.Lang.Database_Message_NoneSchemaData);
			}

			object result = null;

			try
			{
				// Get specification of procedure
				List<IDataParameter> schemaParameters = new List<IDataParameter>();
				schemaParameters.Add(DbHelper.GetParameter("@ProcName", SqlDbType.VarChar, 40, procedureName));
				DataSet schemaData = this.Fill(this.SchemaSql, CommandType.Text, schemaParameters);

				// Run Procedure
				if (schemaData == null || schemaData.Tables.Count == 0 || schemaData.Tables[0].Rows.Count == 0)
				{
					throw new Exception(Language.Lang.Database_Message_NoneSchemaData);
				}
				else
				{
					List<IDataParameter> parameters = this.GetDataParameter(procedureName, schemaData, data);

					switch (executeType)
					{
						case DbHelper.ExecuteType.FillToList:
							result = this.FillToList<T>(procedureName, parameters);
							break;
					}
				}

			}
			catch
			{
				throw;
			}

			return result;
		}

		private List<IDataParameter> GetDataParameter(string procedureName, DataSet schemaData, params object[] data)
		{
			List<IDataParameter> parameters = new List<IDataParameter>();
			if (schemaData.Tables[0].Rows.Count > 0)
			{
				for (int i = 0; i < schemaData.Tables[0].Rows.Count; i++)
				{
					if (schemaData.Tables[0].Rows[i]["CHARACTER_MAXIMUM_LENGTH"] == DBNull.Value)
					{
						if (schemaData.Tables[0].Rows[i]["PARAMETER_MODE"].ToString().Equals("INOUT"))
							parameters.Add(DbHelper.GetParameter(
								schemaData.Tables[0].Rows[i]["PARAMETER_NAME"].ToString(),
								DbHelper.GetSqlDbType(schemaData.Tables[0].Rows[i]["DATA_TYPE"].ToString()),
								ParameterDirection.Output));
						else
							parameters.Add(DbHelper.GetParameter(
								schemaData.Tables[0].Rows[i]["PARAMETER_NAME"].ToString(),
								DbHelper.GetSqlDbType(schemaData.Tables[0].Rows[i]["DATA_TYPE"].ToString()),
								data[i]));
					}
					else
					{
						if (schemaData.Tables[0].Rows[i]["PARAMETER_MODE"].ToString().Equals("INOUT"))
							parameters.Add(DbHelper.GetParameter(
								schemaData.Tables[0].Rows[i]["PARAMETER_NAME"].ToString(),
								DbHelper.GetSqlDbType(schemaData.Tables[0].Rows[i]["DATA_TYPE"].ToString()),
								Convert.ToInt32(schemaData.Tables[0].Rows[i]["CHARACTER_MAXIMUM_LENGTH"]),
								ParameterDirection.Output));
						else
							parameters.Add(DbHelper.GetParameter(
								schemaData.Tables[0].Rows[i]["PARAMETER_NAME"].ToString(),
								DbHelper.GetSqlDbType(schemaData.Tables[0].Rows[i]["DATA_TYPE"].ToString()),
								Convert.ToInt32(schemaData.Tables[0].Rows[i]["CHARACTER_MAXIMUM_LENGTH"]),
								data[i]));
					}
				}
			}

			return parameters;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Fill
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Fill

		/// <summary>
		/// DB명령 수행 후 결과를 DataSet으로 반환
		/// </summary>
		public DataSet Fill(string sql)
		{
			return Fill(sql, CommandType.Text, null);
		}

		/// <summary>
		/// DB명령 수행 후 결과를 DataSet으로 반환
		/// </summary>
		public DataSet Fill(string sql, CommandType commandType)
		{
			return Fill(sql, commandType, null);
		}

		/// <summary>
		/// DB명령 수행 후 결과를 DataSet으로 반환
		/// </summary>
		public DataSet Fill(string sql, CommandType commandType, List<IDataParameter> parameters)
		{
			DataSet result = null;
			int affectRow = 0;

			try
			{
				if (string.IsNullOrEmpty(sql) == false)
				{
					result = new DataSet();

					using (SqlConnection connection = new SqlConnection(this.ConnectionString))
					{
						using (SqlDataAdapter adapter = new SqlDataAdapter(sql, connection))
						{
							// Set command
							adapter.SelectCommand.CommandType = commandType;

							// Set parameter
							if (parameters != null && parameters.Count > 0)
							{
								foreach (IDataParameter parameter in parameters)
								{
									adapter.SelectCommand.Parameters.Add(parameter);
								}
							}

							// Fill data
							affectRow = adapter.Fill(result);
							if (result != null && result.Tables.Count > 0)
							{
								for (int i = 0; i < result.Tables.Count; i++)
								{
									result.Tables[i].TableName = DbHelper._DB_Table + i.ToString();
								}
							}
						}
					}
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		/// <summary>
		/// DB명령 수행 후 결과를 DataSet으로 반환
		/// </summary>
		public DataSet Fill(List<string> sqlList)
		{
			return Fill(sqlList, CommandType.Text);
		}

		/// <summary>
		/// DB명령 수행 후 결과를 DataSet으로 반환
		/// </summary>
		public DataSet Fill(List<string> sqlList, CommandType commandType)
		{
			DataSet result = null;

			try
			{
				if (this.ConnectionString != null && sqlList != null && sqlList.Count > 0)
				{
					result = new DataSet();

					using (SqlConnection connection = new SqlConnection(this.ConnectionString))
					{
						for (int i = 0; i < sqlList.Count; i++)
						{
							// Make new table
							DataTable table = new DataTable(DbHelper._DB_Table + i.ToString());

							// Get result table
							if (string.IsNullOrEmpty(sqlList[i]) == false)
							{
								using (SqlDataAdapter adapter = new SqlDataAdapter(sqlList[i], connection))
								{
									// Set command
									adapter.SelectCommand.CommandType = commandType;

									// Fill data
									adapter.Fill(table);
									if (table != null)
										result.Tables.Add(table.Copy());
									else
										result.Tables.Add(new DataTable(DbHelper._DB_Table + i.ToString()).Copy());
								}
							}
							else
							{
								result.Tables.Add(table.Copy());
							}
						}
					}
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		/// <summary>
		/// DB명령 수행 후 결과를 DataSet으로 반환
		/// </summary>
		public DataSet Fill(List<DbTransactionObject> dbTransactionObjectList)
		{
			DataSet result = null;

			try
			{
				if (this.ConnectionString != null && dbTransactionObjectList != null && dbTransactionObjectList.Count > 0)
				{
					result = new DataSet();

					using (SqlConnection connection = new SqlConnection(this.ConnectionString))
					{
						for (int i = 0; i < dbTransactionObjectList.Count; i++)
						{
							using (SqlDataAdapter adapter = new SqlDataAdapter(dbTransactionObjectList[i].Sql, connection))
							{
								// Set command
								adapter.SelectCommand.CommandType = dbTransactionObjectList[i].CommandType;

								// Set parameter
								if (dbTransactionObjectList[i].Parameters != null && dbTransactionObjectList[i].Parameters.Count > 0)
								{
									foreach (IDataParameter parameter in dbTransactionObjectList[i].Parameters)
									{
										adapter.SelectCommand.Parameters.Add(parameter);
									}
								}

								// Fill data
								DataTable table = new DataTable(DbHelper._DB_Table + i.ToString());
								adapter.Fill(table);
								if (table != null)
								{
									result.Tables.Add(table.Copy());
								}
							}
						}
					}
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Advanced Fill
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Advanced Fill

		/// <summary>
		/// 프로시저를 수행 후 결과를 OUTPUT 파라미터로 전달 (CURSOR를 DataTable로 반환)
		/// </summary>
		public Dictionary<string, object> FillEx(string procedureName)
		{
			return FillEx(procedureName, null);
		}

		/// <summary>
		/// 프로시저를 수행 후 결과를 OUTPUT 파라미터로 전달 (CURSOR를 DataTable로 반환)
		/// </summary>
		public Dictionary<string, object> FillEx(string procedureName, List<IDataParameter> parameters)
		{
			Dictionary<string, object> result = null;
			DataSet dsResult = null;
			int affectRow = 0;

			try
			{
				if (string.IsNullOrEmpty(procedureName) == false)
				{
					result = new Dictionary<string, object>();
					dsResult = new DataSet();
					dsResult.DataSetName = DbHelper._DB_DataSet;

					using (SqlConnection connection = new SqlConnection(this.ConnectionString))
					{
						using (SqlDataAdapter adapter = new SqlDataAdapter(procedureName, connection))
						{
							// Set command
							adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

							// Set parameter
							if (parameters != null && parameters.Count > 0)
							{
								foreach (IDataParameter parameter in parameters)
								{
									adapter.SelectCommand.Parameters.Add(parameter);
								}
							}

							// Fill data
							affectRow = adapter.Fill(dsResult);

							// Out parameters
							if (parameters != null && parameters.Count > 0)
							{
								for (int i = 0; i < parameters.Count; i++)
								{
									if (parameters[i].Direction == ParameterDirection.Output)
									{
										result.Add(TextUtil.RemoveFirstMatch(parameters[i].ParameterName, "@"), parameters[i].Value);
									}
								}
							}

							// Add data if DataSet is not null
							if (dsResult.Tables.Count > 0)
							{
								result.Add(dsResult.DataSetName, dsResult);
							}
						}
					}
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Get object list from DB return data
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Get object list from DB return data

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="procedureName"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		public List<T> FillToList<T>(string procedureName, List<IDataParameter> parameters) where T : class, new()
		{
			List<T> result = new List<T>();

			try
			{
				if (string.IsNullOrEmpty(procedureName) == false)
				{
					using (SqlConnection connection = new SqlConnection(this.ConnectionString))
					{
						connection.Open();

						// Fill data
						using (SqlCommand command = new SqlCommand(procedureName, connection))
						{
							// Set command
							command.CommandType = CommandType.StoredProcedure;

							// Set parameter
							if (parameters != null && parameters.Count > 0)
							{
								foreach (IDataParameter parameter in parameters)
								{
									command.Parameters.Add(parameter);
								}
							}

							// Get reader
							using (SqlDataReader reader = command.ExecuteReader())
							{
								while (reader.Read())
								{
									result.Add(TypeUtil.ConvertToObject<T>(reader));
								}
							}
						}

						connection.Close();
					}
				}

				if (result.Count == 0)
				{
					result = null;
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// ExecuteNonQuery
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region ExecuteNonQuery

		/// <summary>
		/// 쿼리를 수행하고 반환값을 반환
		/// </summary>
		public int ExecuteNonQuery(string sql)
		{
			return ExecuteNonQuery(sql, CommandType.Text, null);
		}

		/// <summary>
		/// 쿼리를 수행하고 반환값을 반환
		/// </summary>
		public int ExecuteNonQuery(string sql, CommandType commandType)
		{
			return ExecuteNonQuery(sql, commandType, null);
		}

		/// <summary>
		/// 쿼리를 수행하고 반환값을 반환
		/// </summary>
		public int ExecuteNonQuery(string sql, CommandType commandType, List<IDataParameter> parameters)
		{
			int result = 0;

			try
			{
				if (string.IsNullOrEmpty(sql) == false)
				{
					using (SqlConnection connection = new SqlConnection(this.ConnectionString))
					{
						// Make command
						using (SqlCommand command = new SqlCommand(sql, connection))
						{
							// Set command
							command.CommandType = commandType;

							// Set SqlParameter
							if (parameters != null && parameters.Count > 0)
							{
								foreach (IDataParameter parameter in parameters)
								{
									command.Parameters.Add(parameter);
								}
							}

							// Excute
							command.Connection.Open();
							result = command.ExecuteNonQuery();
							command.Connection.Close();
						}
					}
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Advanced ExecuteNonQuery
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Advanced ExecuteNonQuery

		/// <summary>
		/// 프로시저를 수행 후 결과를 OUTPUT 파라미터로 전달
		/// </summary>
		public Dictionary<string, object> ExecuteNonQueryEx(string procedureName)
		{
			return ExecuteNonQueryEx(procedureName, null);
		}

		/// <summary>
		/// 프로시저를 수행 후 결과를 OUTPUT 파라미터로 전달
		/// </summary>
		public Dictionary<string, object> ExecuteNonQueryEx(string procedureName, List<IDataParameter> parameters)
		{
			Dictionary<string, object> result = null;
			int affectRow = 0;

			try
			{
				if (string.IsNullOrEmpty(procedureName) == false)
				{
					result = new Dictionary<string, object>();

					using (SqlConnection connection = new SqlConnection(this.ConnectionString))
					{
						// Make command
						using (SqlCommand command = new SqlCommand(procedureName, connection))
						{
							// Set command
							command.CommandType = CommandType.StoredProcedure;

							// Set SqlParameter
							if (parameters != null && parameters.Count > 0)
							{
								foreach (IDataParameter parameter in parameters)
								{
									command.Parameters.Add(parameter);
								}
							}

							// Excute
							command.Connection.Open();
							affectRow = command.ExecuteNonQuery();
							command.Connection.Close();

							// Out parameters
							if (parameters != null && parameters.Count > 0)
							{
								foreach (IDataParameter paramater in parameters)
								{
									if (paramater.Direction == ParameterDirection.Output)
									{
										result.Add(TextUtil.RemoveFirstMatch(paramater.ParameterName, "@"), paramater.Value);
									}
								}
							}
						}
					}
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// ExecuteTransactionNonQuery
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region ExecuteTransactionNonQuery

		/// <summary>
		/// 트랜잭션용 쿼리를 수행하고 반환값을 반환
		/// </summary>
		public bool ExecuteTransactionNonQuery(List<string> sqlList)
		{
			return ExecuteTransactionNonQuery(sqlList, CommandType.Text, false);
		}

		/// <summary>
		/// 트랜잭션용 쿼리를 수행하고 반환값을 반환
		/// </summary>
		public bool ExecuteTransactionNonQuery(List<string> sqlList, bool isCheckAffectRow)
		{
			return ExecuteTransactionNonQuery(sqlList, CommandType.Text, isCheckAffectRow);
		}

		/// <summary>
		/// 트랜잭션용 쿼리를 수행하고 반환값을 반환
		/// </summary>
		public bool ExecuteTransactionNonQuery(List<string> sqlList, CommandType commandType, bool isCheckAffectRow)
		{
			bool result = true;
			int affectRows = 0;

			SqlConnection connection = null;
			SqlCommand command = null;
			SqlTransaction transaction = null;

			try
			{
				if (sqlList != null && sqlList.Count > 0)
				{
					// Open connection
					connection = new SqlConnection(this.ConnectionString);
					connection.Open();

					// Set command
					command = connection.CreateCommand();

					// Begin transaction
					transaction = connection.BeginTransaction();

					// Execute sql
					for (int i = 0; i < sqlList.Count; i++)
					{
						command.CommandType = commandType;
						command.CommandText = sqlList[i];

						// Execute sql
						affectRows = command.ExecuteNonQuery();
						if (isCheckAffectRow && affectRows == 0)
						{
							result = false;
							break;
						}
					}

					// Commit or rollback transaction
					if (result)
						transaction.Commit();
					else
					{
						transaction.Rollback();
					}
				}
			}
			catch
			{
				if (transaction != null)
				{
					transaction.Rollback();
				}
				throw;
			}
			finally
			{
				if (transaction != null)
				{
					transaction.Dispose();
					transaction = null;
				}
				if (command != null)
				{
					command.Dispose();
					command = null;
				}
				if (connection != null)
				{
					if (connection.State != ConnectionState.Closed)
					{
						connection.Close();
					}
					connection.Dispose();
					connection = null;
				}
			}

			return result;
		}

		/// <summary>
		/// 트랜잭션용 쿼리를 수행하고 반환값을 반환
		/// </summary>
		public bool ExecuteTransactionNonQuery(List<DbTransactionObject> dbTransactionObjectList)
		{
			return ExecuteTransactionNonQuery(dbTransactionObjectList, false);
		}

		/// <summary>
		/// 트랜잭션용 쿼리를 수행하고 반환값을 반환
		/// </summary>
		public bool ExecuteTransactionNonQuery(List<DbTransactionObject> dbTransactionObjectList, bool isCheckAffectRow)
		{
			bool result = true;
			int affectRows = 0;

			SqlConnection connection = null;
			SqlCommand command = null;
			SqlTransaction transaction = null;

			try
			{
				if (dbTransactionObjectList != null && dbTransactionObjectList.Count > 0)
				{
					// Open connection
					connection = new SqlConnection(this.ConnectionString);
					connection.Open();

					// Set command
					command = connection.CreateCommand();

					// Begin transaction
					transaction = connection.BeginTransaction();

					// Execute sql
					for (int i = 0; i < dbTransactionObjectList.Count; i++)
					{
						command.CommandType = dbTransactionObjectList[i].CommandType;
						command.CommandText = dbTransactionObjectList[i].Sql;
						command.Parameters.Clear();
						if (dbTransactionObjectList[i].Parameters != null && dbTransactionObjectList[i].Parameters.Count > 0)
						{
							foreach (IDataParameter parameter in dbTransactionObjectList[i].Parameters)
							{
								command.Parameters.Add(parameter);
							}
						}

						// Execute sql
						affectRows = command.ExecuteNonQuery();
						if (isCheckAffectRow && affectRows == 0)
						{
							result = false;
							break;
						}
					}

					// Commit or rollback transaction
					if (result)
						transaction.Commit();
					else
					{
						transaction.Rollback();
					}
				}
			}
			catch
			{
				if (transaction != null)
				{
					transaction.Rollback();
				}
				throw;
			}
			finally
			{
				if (transaction != null)
				{
					transaction.Dispose();
					transaction = null;
				}
				if (command != null)
				{
					command.Dispose();
					command = null;
				}
				if (connection != null)
				{
					if (connection.State != ConnectionState.Closed)
					{
						connection.Close();
					}
					connection.Dispose();
					connection = null;
				}
			}

			return result;
		}

		#endregion
	}
}
