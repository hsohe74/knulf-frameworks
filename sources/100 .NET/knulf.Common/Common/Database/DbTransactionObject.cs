﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace knulf.Database
{
	/// <summary>
	/// 트랜잭션을 수행하기 위해 각 단위별로 파라미터를 전달하기 위한 객체
	/// </summary>
	public class DbTransactionObject
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Constructer & Initialize

		public DbTransactionObject()
		{
		}

		public DbTransactionObject(string sql, List<IDataParameter> parameters)
			: this(sql, parameters, CommandType.Text)
		{
		}

		public DbTransactionObject(string sql, List<IDataParameter> parameters, CommandType commandType)
		{
			this.Sql = sql;
			this.Parameters = parameters;
			this.CommandType = commandType;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Properties
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Properties

		/// <summary>
		/// 쿼리 문자열
		/// </summary>
		public string Sql { get; set; }

		/// <summary>
		/// 전달할 파라미터
		/// </summary>
		public List<IDataParameter> Parameters { get; set; }

		/// <summary>
		/// 명령 유형
		/// </summary>
		public CommandType CommandType { get; set; }

		#endregion
	}
}
