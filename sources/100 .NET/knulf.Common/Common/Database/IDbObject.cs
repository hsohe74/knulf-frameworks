﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace knulf.Database
{
	public interface IDbObject
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// DB connection string
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region DB connection string

		/// <summary>
		/// DB 연결 문자열
		/// </summary>
		string ConnectionString { get; set; }

		/// <summary>
		/// 스키마 쿼리
		/// </summary>
		string SchemaSql { get; set; }

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Automatic procedure call
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Automatic procedure call

		/// <summary>
		/// Execute procedure with automatic setting parameters
		/// </summary>
		object ExecuteProcedure(string procedureName, DbHelper.ExecuteType executeType, params object[] data);

		/// <summary>
		/// Execute procedure with automatic setting parameters
		/// </summary>
		object ExecuteProcedure<T>(string procedureName, DbHelper.ExecuteType executeType, params object[] data) where T : class, new();

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Fill
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Fill

		/// <summary>
		/// DB명령 수행 후 결과를 DataSet으로 반환
		/// </summary>
		DataSet Fill(string sql);

		/// <summary>
		/// DB명령 수행 후 결과를 DataSet으로 반환
		/// </summary>
		DataSet Fill(string sql, CommandType commandType);

		/// <summary>
		/// DB명령 수행 후 결과를 DataSet으로 반환
		/// </summary>
		DataSet Fill(string sql, CommandType commandType, List<IDataParameter> parameters);

		/// <summary>
		/// DB명령 수행 후 결과를 DataSet으로 반환
		/// </summary>
		DataSet Fill(List<string> sqlList);

		/// <summary>
		/// DB명령 수행 후 결과를 DataSet으로 반환
		/// </summary>
		DataSet Fill(List<string> sqlList, CommandType commandType);

		/// <summary>
		/// DB명령 수행 후 결과를 DataSet으로 반환
		/// </summary>
		DataSet Fill(List<DbTransactionObject> dbTransactionObjectList);

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Advanced Fill
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Advanced Fill

		/// <summary>
		/// 프로시저를 수행 후 결과를 OUTPUT 파라미터로 전달
		/// </summary>
		Dictionary<string, object> FillEx(string procedureName);

		/// <summary>
		/// 프로시저를 수행 후 결과를 OUTPUT 파라미터로 전달
		/// </summary>
		Dictionary<string, object> FillEx(string procedureName, List<IDataParameter> parameters);

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// FillToList
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region FillToList


		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="procedureName"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		List<T> FillToList<T>(string procedureName, List<IDataParameter> parameters) where T : class, new();

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// ExecuteNonQuery
		////////////////////////////////////////////////////////////////////////////////////////////////////

			#region ExecuteNonQuery

		/// <summary>
		/// 쿼리를 수행하고 반환값을 반환
		/// </summary>
		int ExecuteNonQuery(string sql);

		/// <summary>
		/// 쿼리를 수행하고 반환값을 반환
		/// </summary>
		int ExecuteNonQuery(string sql, CommandType commandType);

		/// <summary>
		/// 쿼리를 수행하고 반환값을 반환
		/// </summary>
		int ExecuteNonQuery(string sql, CommandType commandType, List<IDataParameter> parameters);

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Advanced ExecuteNonQuery
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Advanced ExecuteNonQuery

		/// <summary>
		/// 프로시저를 수행 후 결과를 OUTPUT 파라미터로 전달
		/// </summary>
		Dictionary<string, object> ExecuteNonQueryEx(string procedureName);

		/// <summary>
		/// 프로시저를 수행 후 결과를 OUTPUT 파라미터로 전달
		/// </summary>
		Dictionary<string, object> ExecuteNonQueryEx(string procedureName, List<IDataParameter> parameters);

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// ExecuteTransactionNonQuery
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region ExecuteTransactionNonQuery

		/// <summary>
		/// 트랜잭션용 쿼리를 수행하고 반환값을 반환
		/// </summary>
		bool ExecuteTransactionNonQuery(List<string> sqlList);

		/// <summary>
		/// 트랜잭션용 쿼리를 수행하고 반환값을 반환
		/// </summary>
		bool ExecuteTransactionNonQuery(List<string> sqlList, bool isCheckAffectRow);

		/// <summary>
		/// 트랜잭션용 쿼리를 수행하고 반환값을 반환
		/// </summary>
		bool ExecuteTransactionNonQuery(List<string> sqlList, CommandType commandType, bool isCheckAffectRow);

		/// <summary>
		/// 트랜잭션용 쿼리를 수행하고 반환값을 반환
		/// </summary>
		bool ExecuteTransactionNonQuery(List<DbTransactionObject> dbTransactionObjectList);

		/// <summary>
		/// 트랜잭션용 쿼리를 수행하고 반환값을 반환
		/// </summary>
		bool ExecuteTransactionNonQuery(List<DbTransactionObject> dbTransactionObjectList, bool isCheckAffectRow);

		#endregion
	}
}
