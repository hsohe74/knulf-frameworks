﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Pattern
{
	/// <summary>
	/// Initialize
	/// </summary>
	public interface IInitialize
	{
		/// <summary>
		/// Initialize
		/// </summary>
		void Initialize();
	}
}
