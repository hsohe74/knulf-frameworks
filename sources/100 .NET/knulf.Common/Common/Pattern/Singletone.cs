﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Pattern
{
	/// <summary>
	/// Base class of singleton pattern
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public abstract class Singletone<T> where T : new()
	{
		// Instance
		private static Lazy<object> instance;

		/// <summary>
		/// Instance of singleton
		/// </summary>
		public static T Current
		{
			get
			{
				if (instance == null)
				{
					instance = new Lazy<object>(() => new T());
				}
				return (T)instance.Value;
			}
		}
	}
}
