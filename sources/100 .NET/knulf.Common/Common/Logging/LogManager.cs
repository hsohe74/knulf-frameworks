﻿using knulf.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Logging
{
	/// <summary>
	/// Main class of log management
	/// </summary>
	public class LogManager : Pattern.Singletone<LogManager>
	{
		// LogWriter default interal
		private const int LOG_WRITER_INTERVAL = 1000 * 2;
		private const int LOG_CLEANER_INTERVAL = 1000 * 2;
		public const string _LOG_DIRECTORY_NAME = "logs";

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region CONSTRUCTER

		public LogManager()
		{
			// Properties initialize
			LogEnable = true;
			LogPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _LOG_DIRECTORY_NAME);
			LogName = "Log";
			LogLevel = LOG_LEVEL.ERROR;
			LogMethod = LOG_WRITING_METHOD.File;
			LogWritingTerm = LOG_WRITING_TERM.DAY;
			LogStoredCount = 0;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// ENUM
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region ENUM

		/// <summary>
		/// Log level
		/// </summary>
		public enum LOG_LEVEL
		{
			ERROR = 10,
			WARNING = 20,
			INFORMATION = 30,
			DEBUG = 80,
			CONSOLE = 90
		}

		/// <summary>
		/// Log write method
		/// </summary>
		public enum LOG_WRITING_METHOD
		{
			File = 0,
			Console = 1,
			Event = 2,
			DB = 3
		}

		/// <summary>
		/// Log writing period term
		/// </summary>
		public enum LOG_WRITING_TERM
		{
			DAY,
			MONTH,
			YEAR
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Properties
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Properties

		/// <summary>
		/// Log enable
		/// </summary>
		public bool LogEnable { get; set; }

		/// <summary>
		/// Log file path
		/// </summary>
		public string LogPath { get; set; }

		/// <summary>
		/// Log name
		/// </summary>
		public string LogName { get; set; }

		/// <summary>
		/// Log level
		/// </summary>
		public LOG_LEVEL LogLevel { get; set; }

		/// <summary>
		/// Log writing method
		/// </summary>
		public LOG_WRITING_METHOD LogMethod { get; set; }

		/// <summary>
		/// Log writing period term
		/// </summary>
		public LOG_WRITING_TERM LogWritingTerm { get; set; }

		/// <summary>
		/// Log stored item count. Log files beyond the period will be deleted.
		/// 0 : Will be stored forever.
		/// </summary>
		public int LogStoredCount { get; set; }

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Load setting
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Load setting

		/// <summary>
		/// Load setting from app.config
		/// </summary>
		public void LoadSetting()
		{
			if (string.IsNullOrEmpty(ConfigurationUtil.GetAppSetting("LOG_ENABLE")) == false)
				LogEnable = Convert.ToBoolean(ConfigurationUtil.GetAppSetting("LOG_ENABLE"));

			if (string.IsNullOrEmpty(ConfigurationUtil.GetAppSetting("LOG_PATH")) == false)
				LogPath = ConfigurationUtil.GetAppSetting("LOG_PATH");

			if (string.IsNullOrEmpty(ConfigurationUtil.GetAppSetting("LOG_NAME")) == false)
				LogName = ConfigurationUtil.GetAppSetting("LOG_NAME");

			if (string.IsNullOrEmpty(ConfigurationUtil.GetAppSetting("LOG_LEVEL")) == false)
				LogLevel = (LOG_LEVEL)Enum.Parse(typeof(LOG_LEVEL), ConfigurationUtil.GetAppSetting("LOG_LEVEL"));

			if (string.IsNullOrEmpty(ConfigurationUtil.GetAppSetting("LOG_WRITING_METHOD")) == false)
				LogMethod = (LOG_WRITING_METHOD)Enum.Parse(typeof(LOG_WRITING_METHOD), ConfigurationUtil.GetAppSetting("LOG_WRITING_METHOD"));

			if (string.IsNullOrEmpty(ConfigurationUtil.GetAppSetting("LOG_WRITING_TERM")) == false)
				LogWritingTerm = (LOG_WRITING_TERM)Enum.Parse(typeof(LOG_WRITING_TERM), ConfigurationUtil.GetAppSetting("LOG_WRITING_TERM"));

			if (string.IsNullOrEmpty(ConfigurationUtil.GetAppSetting("LOG_STORED_COUNT")) == false)
				LogStoredCount = Convert.ToInt32(ConfigurationUtil.GetAppSetting("LOG_STORED_COUNT"));
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Start logging
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Start logging

		/// <summary>
		/// Start log writer with property value
		/// </summary>
		public void StartLogging()
		{
			// LogWriter setting
			LogWriter.Current.Interval = LOG_WRITER_INTERVAL;
			LogWriter.Current.IsBackground = true;
			LogWriter.Current.IsActive = true;
			LogWriter.Current.Start();

			// LogCleaner setting
			LogCleaner.Current.Interval = LOG_CLEANER_INTERVAL;
			LogCleaner.Current.IsBackground = true;
			LogCleaner.Current.IsActive = true;
			LogCleaner.Current.Start();
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Write Log
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Write Log

		public void WriteLog(string msg)
		{
			LogWriter.Current.WriteLogToQueue(LogLevel, msg);
		}

		public void WriteLog(LOG_LEVEL level, string msg)
		{
			LogWriter.Current.WriteLogToQueue(level, msg);
		}

		public void WriteException(Exception ex)
		{
			string msg = "[" + ex.Source + "] " + ex.ToString();
			LogWriter.Current.WriteLogToQueue(LOG_LEVEL.ERROR, msg);
		}

		public void WriteError(string msg)
		{
			LogWriter.Current.WriteLogToQueue(LOG_LEVEL.ERROR, msg);
		}

		public void WriteWarning(string msg)
		{
			LogWriter.Current.WriteLogToQueue(LOG_LEVEL.WARNING, msg);
		}

		public void WriteInfo(string msg)
		{
			LogWriter.Current.WriteLogToQueue(LOG_LEVEL.INFORMATION, msg);
		}

		public void WriteDebug(string msg)
		{
			LogWriter.Current.WriteLogToQueue(LOG_LEVEL.DEBUG, msg);
		}

		public void WriteConsole(string msg)
		{
			LogWriter.Current.WriteLogToQueue(LOG_LEVEL.CONSOLE, msg);
		}

		#endregion
	}
}
