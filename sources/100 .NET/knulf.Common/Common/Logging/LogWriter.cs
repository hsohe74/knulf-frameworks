﻿using knulf.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Logging
{
	/// <summary>
	/// Write log with message in queue. Manage log file with setting term.
	/// </summary>
	public class LogWriter : Worker.ThreadWorkerBase
	{
		// Singleton instance
		private static LogWriter thisInstance = new LogWriter();

		// Log filename & filepath
		private string logTermPattern = string.Empty;
		private string logCurrentTermName = string.Empty;
		private string logFilePath = string.Empty;

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region CONSTRUCTER

		public LogWriter()
		{
			this.IsActive = false;

			// Queue
			this.LogQueue = new System.Collections.Queue();
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Properties
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Properties

		/// <summary>
		/// Get singleton instance
		/// </summary>
		internal static LogWriter Current
		{
			get
			{
				if (thisInstance == null)
					thisInstance = new LogWriter();
				return thisInstance;
			}
		}

		/// <summary>
		/// Whether the current instance is active.
		/// </summary>
		internal bool IsActive { get; set; }

		/// <summary>
		/// Log message queue
		/// </summary>
		internal System.Collections.Queue LogQueue { get; private set; }

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Method : Write log to queue
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Write log to queue

		/// <summary>
		/// Write log message to queue of LogWriter or debug console.
		/// </summary>
		/// <param name="logLevel"></param>
		/// <param name="logMessage"></param>
		internal void WriteLogToQueue(LogManager.LOG_LEVEL logLevel, string logMessage)
		{
			StackFrame frame;
			string logHeaderMessage = string.Empty;
			string logFullMessage = string.Empty;

			// 작성할 로그 등급이 설정된 로그 등급보다 작거나 같아야 작성
			if (LogManager.Current.LogEnable && (Convert.ToInt32(logLevel) <= Convert.ToInt32(LogManager.Current.LogLevel)))
			{
				try
				{
					// 2단계 건너띈 StackFrame을 가져옴
					frame = new StackFrame(2);
					if (frame != null)
					{

						// 로그 메시지 작성
						logHeaderMessage = string.Format("[{0}] [{1}] ({2}.{3})",
							(LogManager.Current.LogWritingTerm.Equals(LogManager.LOG_WRITING_TERM.DAY) ? DateTime.Now.ToString("HH:mm:ss") : DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")),
							logLevel.ToString(),
							frame.GetMethod().DeclaringType.FullName,
							frame.GetMethod().Name);
						logFullMessage = logHeaderMessage + "\n" + logMessage;

						// 로그 출력
						if (logLevel == LogManager.LOG_LEVEL.CONSOLE)
						{
							WriteConsoleLog(logFullMessage);
							return;
						}
					}
				}
				catch (Exception ex)
				{
					// 로그 메시지 작성
					logHeaderMessage = string.Format("[{0}] [ERROR] (LogManager)",
						(LogManager.Current.LogWritingTerm.Equals(LogManager.LOG_WRITING_TERM.DAY) ? DateTime.Now.ToString("HH:mm:ss") : DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
					logFullMessage = logHeaderMessage + "\n" + logMessage + "\n" + ex.Message + "\n" + ex.ToString() + "\n";
				}

				// Write log
				if (this.IsActive)
				{
					Current.LogQueue.Enqueue(logFullMessage + "\n\n");
				}
				else
				{
					this.WriteLog(logFullMessage + "\n\n");
				}
			}
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Method : Write log thread from Queue
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Write log thread from Queue

		/// <summary>
		/// Run writer
		/// </summary>
		protected override void Run()
		{
			if (this.IsActive)
			{
				while (this.LogQueue.Count > 0)
				{
					var logMessage = (string)LogQueue.Dequeue();
					this.WriteLog(logMessage);
				}
			}
		}

		/// <summary>
		/// Write log
		/// </summary>
		private void WriteLog(string logMessage)
		{
			// Check log setting
			switch (LogManager.Current.LogMethod)
			{
				case LogManager.LOG_WRITING_METHOD.File:
					CheckLogDirectory(LogManager.Current.LogPath);
					CheckLogFile(
						LogManager.Current.LogPath,
						LogManager.Current.LogName,
						LogManager.Current.LogWritingTerm,
						ref this.logFilePath,
						ref this.logTermPattern,
						ref this.logCurrentTermName);
					break;
			}

			// Write log
			switch (LogManager.Current.LogMethod)
			{
				case LogManager.LOG_WRITING_METHOD.File:
					if (this.logFilePath != null)
					{
						WriteFileLog(this.logFilePath, logMessage);
					}
					break;
				case LogManager.LOG_WRITING_METHOD.Console:
					WriteConsoleLog(logMessage);
					break;
				case LogManager.LOG_WRITING_METHOD.Event:
					break;
				case LogManager.LOG_WRITING_METHOD.DB:
					break;
			}
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Write log message (File)
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Write log message (File)

		/// <summary>
		/// Write log to specified file path
		/// </summary>
		/// <param name="logFilePath">Log file path</param>
		/// <param name="logMessage">Log message</param>
		public static void WriteFileLog(string logFilePath, string logMessage)
		{
			WriteFileLog(logFilePath, logMessage, Encoding.UTF8);
		}

		/// <summary>
		/// Write log to specified file path
		/// </summary>
		/// <param name="logFilePath">Log file path</param>
		/// <param name="logMessage">Log message</param>
		/// <param name="encoding">Log message encoding</param>
		public static void WriteFileLog(string logFilePath, string logMessage, Encoding encoding)
		{
			try
			{
				FileUtil.WriteTextFile(logFilePath, logMessage, true, encoding, true);
			}
			catch { }
		}

		/// <summary>
		/// 로그파일의 경로를 확인하고 없는 경로면 생성한다.
		/// </summary>
		public static void CheckLogDirectory(string logPath)
		{
			// Check log directory
			FileUtil.CreateDirectory(logPath);
		}

		/// <summary>
		/// 지난 로그파일을 별도로 저장하고 신규 로그파일을 생성한다.
		/// </summary>
		public static void CheckLogFile(
			string logPath,
			string logName,
			LogManager.LOG_WRITING_TERM logWritingTerm,
			ref string logFilePath,
			ref string logTermPattern,
			ref string logCurrentTermName)
		{
			// Make Log file name
			if (string.IsNullOrEmpty(logPath))
			{
				logFilePath = null;
				return;
			}
			if (logName == null) logName = string.Empty;
			logFilePath = Path.Combine(logPath, logName + ".log");

			// Get log term name
			string logNowTermName = string.Empty;
			switch (logWritingTerm)
			{
				case LogManager.LOG_WRITING_TERM.DAY:
					logTermPattern = "yyyyMMdd";
					break;
				case LogManager.LOG_WRITING_TERM.MONTH:
					logTermPattern = "yyyyMM";
					break;
				case LogManager.LOG_WRITING_TERM.YEAR:
					logTermPattern = "yyyy";
					break;
			}
			logNowTermName = DateTime.Now.ToString(logTermPattern);

			// Check log file term & rename log file
			if (string.IsNullOrEmpty(logCurrentTermName))
			{
				logCurrentTermName = logNowTermName;
			}

			// Move old log file with rename
			if (string.IsNullOrEmpty(logCurrentTermName) == false && logCurrentTermName.Equals(logNowTermName) == true)
			{
				// Old log file rename with term name
				FileInfo oldFile = new FileInfo(logFilePath);

				if (oldFile.Exists && oldFile.Length > 0 && oldFile.LastWriteTime.ToString(logTermPattern).Equals(logNowTermName) == false)
				{
					String targetFile = Path.Combine(logPath, logName + oldFile.LastWriteTime.ToString(logTermPattern) + ".log");
					if (File.Exists(targetFile))
					{
						File.Delete(targetFile);
					}
					oldFile.MoveTo(targetFile);
				}
			}
			else if (string.IsNullOrEmpty(logCurrentTermName) == false && logCurrentTermName.Equals(logNowTermName) == false)
			{
				// Old log file rename with term name
				FileInfo oldFile = new FileInfo(logFilePath);

				if (oldFile.Exists&& oldFile.Length > 0)
				{
					string targetFile = Path.Combine(logPath, logName + logCurrentTermName + ".log");
					if (File.Exists(targetFile))
					{
						File.Delete(targetFile);
					}
					oldFile.MoveTo(targetFile);
				}

				// Save logCurrentTerm
				logCurrentTermName = logNowTermName;
			}

			// if not exist log file, make that.
			FileInfo file = new FileInfo(logFilePath);
			if (!file.Exists)
			{
				using (StreamWriter sw = file.CreateText())
				{
				}
			}
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Write log message (Console)
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Write log message (Console)

		/// <summary>
		/// Write log to console
		/// </summary>
		/// <param name="logMessage">Log message</param>
		public static void WriteConsoleLog(string logMessage)
		{
			try
			{
				System.Diagnostics.Debug.WriteLine(logMessage);
			}
			catch { }
		}

		#endregion
	}
}
