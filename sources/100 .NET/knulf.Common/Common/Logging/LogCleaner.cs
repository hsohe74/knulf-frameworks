﻿using System.IO;
using System.Linq;

namespace knulf.Logging
{
	public class LogCleaner : Worker.ThreadWorkerBase
	{
		// Singleton instance
		private static LogCleaner thisInstance = new LogCleaner();

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region CONSTRUCTER

		public LogCleaner()
		{
			this.IsActive = false;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Properties
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Properties

		/// <summary>
		/// Get singleton instance
		/// </summary>
		internal static LogCleaner Current
		{
			get
			{
				if (thisInstance == null)
					thisInstance = new LogCleaner();
				return thisInstance;
			}
		}

		/// <summary>
		/// Whether the current instance is active.
		/// </summary>
		internal bool IsActive { get; set; }

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Method : Cleaning log
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Cleaning log

		protected override void Run()
		{
			if (this.IsActive && LogManager.Current.LogMethod == LogManager.LOG_WRITING_METHOD.File)
			{
				CleaningLog(LogManager.Current.LogStoredCount, LogManager.Current.LogPath, 1);
			}
		}

		/// <summary>
		/// Cleaning log files
		/// </summary>
		public static void CleaningLog(int logStoredCount, string logPath, int activeLogCount)
		{
			if (logStoredCount > 0 && string.IsNullOrEmpty(logPath) == false)
			{
				string[] logFiles = Utility.FileUtil.GetFileListInDirectory(logPath);
				if (logFiles.Length > 0 && logFiles.Length <= (logStoredCount + activeLogCount))
				{
					return;
				}

				var sortedLogFiles = logFiles.OrderByDescending(path => File.GetLastWriteTime(path)).ToArray();
				for (int i = 0; i < sortedLogFiles.Length; i++)
				{
					if (i > (logStoredCount + activeLogCount - 1))
					{
						Utility.FileUtil.DeleteFile(sortedLogFiles[i]);
					}
				}
			}
		}

		#endregion
	}
}
