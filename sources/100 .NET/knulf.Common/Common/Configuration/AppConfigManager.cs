﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace knulf.Configuration
{
	/// <summary>
	/// Local configuration file manager
	/// </summary>
	public class AppConfigManager : Pattern.Singletone<AppConfigManager>
	{
		// string constants
		public const string _DEFAULT_CONFIG_FILE = "AppSettings.xml";
		public const string _XML_ROOT = "AppSetting";
		public const string _XML_SUB_ROOT = "Settings";
		public const string _XML_PROPERTY_ITEM_NAME = "Name";
		public const string _XML_PROPERTY_ITEM_VALUE = "Value";

		public const string _PATH_DELIMITER = ".";

		// Event
		public delegate void ConfigChangedEventHandler();
		public event ConfigChangedEventHandler ConfigChanged;

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		public AppConfigManager()
		{
			this.IsSettingLoaded = false;

			try
			{
				Settings = new SettingCollection();
				SettingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _DEFAULT_CONFIG_FILE);
				Load();
			}
			catch { }
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Properties
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Properties

		/// <summary>
		/// Whether setting file is loaded
		/// </summary>
		public bool IsSettingLoaded { get; private set; }

		/// <summary>
		/// Setting file
		/// </summary>
		public string SettingFile { get; set; }

		/// <summary>
		/// Settinig item collection
		/// </summary>
		public SettingCollection Settings { get; set; }

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Method : PUBLIC
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Load & Save

		/// <summary>
		/// Load configuration file
		/// </summary>
		public void Load()
		{
			if (File.Exists(SettingFile))
			{
				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(Utility.FileUtil.ReadTextFile(SettingFile, Encoding.UTF8));

				if (Settings != null) Settings.Clear();
				Settings = GetCollectionObject(xmlDoc.DocumentElement.SelectSingleNode(_XML_SUB_ROOT));

				// Flag is setting loaded
				IsSettingLoaded = true;

				// Occur event
				if (ConfigChanged != null) {
					ConfigChanged.Invoke();
				}
			}
		}

		/// <summary>
		/// Load configuration from xml string
		/// </summary>
		public void Load(string configXml)
		{
			if (string.IsNullOrEmpty(configXml) == false)
			{
				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(configXml);

				if (Settings != null) Settings.Clear();
				Settings = GetCollectionObject(xmlDoc.DocumentElement.SelectSingleNode(_XML_SUB_ROOT));

				// Flag is setting loaded
				IsSettingLoaded = true;

				// Occur event
				if (ConfigChanged != null) {
					ConfigChanged.Invoke();
				}
			}
			else
			{
				IsSettingLoaded = false;
			}
		}

		/// <summary>
		/// Save configuration file
		/// </summary>
		public void Save()
		{
			XmlDocument xmlDoc = new XmlDocument();

			XmlNode rootNode = Utility.XmlUtil.GetXmlNode(xmlDoc, _XML_ROOT);
			xmlDoc.AppendChild(rootNode);
			rootNode.AppendChild(GetCollectionNode(xmlDoc, _XML_SUB_ROOT, Settings));

			Utility.FileUtil.WriteTextFile(SettingFile, Utility.XmlUtil.GetXmlString(xmlDoc));
		}

		#endregion

		#region Get

		/// <summary>
		/// Get setting value by path by seperator dot
		/// </summary>
		public object GetValue(string settingKeyPath)
		{
			if (GetSettingObject(settingKeyPath, false) == null)
				return null;
			else
				return GetSettingObject(settingKeyPath, false).Value;
		}

		/// <summary>
		/// Set setting value by path by seperator dot
		/// </summary>
		public void SetValue(string settingKeyPath, string value)
		{
			GetSettingObject(settingKeyPath, true).Value = value;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Method : PRIVATE (XML)
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Get config item

		/// <summary>
		/// Get setting object
		/// </summary>
		private Setting GetSettingObject(string settingKeyPath, bool isEanbleCreate)
		{
			Setting rObject = null;
			SettingCollection rSC = this.Settings;

			string[] settingKeys = settingKeyPath.Split(_PATH_DELIMITER.ToCharArray());

			try
			{
				if (settingKeys.Length == 1)
				{
					if (rSC.ContainsKey(settingKeys[0]))
					{
						rObject = (Setting)rSC[settingKeys[0]];
					}
					else
					{
						rObject = new Setting(settingKeys[0]);

						if (isEanbleCreate)
						{
							rSC.Add(settingKeys[0], rObject);
						}
					}
				}
				else
				{
					for (int i = 0; i < settingKeys.Length - 1; i++)
					{
						if (rSC.ContainsKey(settingKeys[i]))
						{
							rSC = (SettingCollection)((Setting)rSC[settingKeys[i]]).Value;
						}
						else
						{
							rObject = new Setting(settingKeys[i], new SettingCollection());
							rSC.Add(settingKeys[i], rObject);
							rSC = (SettingCollection)rObject.Value;

							if (i == settingKeys.Length - 2)
							{
								rSC.Add(settingKeys[settingKeys.Length - 1], new Setting(settingKeys[i + 1]));
							}
						}

					}

					if (isEanbleCreate && rSC.ContainsKey(settingKeys[settingKeys.Length - 1]) == false)
					{
						rSC.Add(settingKeys[settingKeys.Length - 1], new Setting(settingKeys[settingKeys.Length - 1]));
					}
					rObject = (Setting)rSC[settingKeys[settingKeys.Length - 1]];
				}
			}
			catch
			{
				rObject = null;
			}

			return rObject;
		}

		#endregion

		#region Deserialize

		/// <summary>
		/// Get collection object 
		/// </summary>
		private SettingCollection GetCollectionObject(XmlNode collectionNode)
		{
			var collectionObject = (SettingCollection)Activator.CreateInstance(typeof(SettingCollection));

			string keyName = string.Empty;
			for (int i = 0; i < collectionNode.ChildNodes.Count; i++)
			{
				keyName = collectionNode.ChildNodes[i].SelectSingleNode(_XML_PROPERTY_ITEM_NAME).InnerText;
				collectionObject.Add(keyName, GetItemObject(collectionNode.ChildNodes[i]));
			}

			return collectionObject;
		}

		/// <summary>
		/// Get item object
		/// </summary>
		private object GetItemObject(XmlNode itemNode)
		{
			var objItem = Activator.CreateInstance(typeof(Setting));

			foreach (System.Reflection.PropertyInfo property in objItem.GetType().GetProperties())
			{
				if (itemNode.SelectSingleNode(property.Name) != null)
				{
					if (property.PropertyType.Equals(typeof(SettingCollection)))
					{
						property.SetValue(objItem, GetCollectionObject(itemNode.SelectSingleNode(property.Name)), null);
					}
					else
					{
						if (itemNode.SelectSingleNode(property.Name).InnerXml != "" && itemNode.SelectSingleNode(property.Name).ChildNodes[0].ChildNodes.Count > 0)
							property.SetValue(objItem, GetCollectionObject(itemNode.SelectSingleNode(property.Name)), null);
						else
							property.SetValue(objItem, Utility.XmlUtil.GetXmlDecode(itemNode.SelectSingleNode(property.Name).InnerText), null);
					}
				}
			}

			return objItem;
		}

		#endregion

		#region Serialize

		/// <summary>
		/// Get collection node
		/// </summary>
		private XmlNode GetCollectionNode(XmlDocument xmlDoc, string nodeName, SettingCollection collectionObject)
		{
			XmlNode collectionNode = Utility.XmlUtil.GetXmlNode(xmlDoc, nodeName);

			foreach (string key in collectionObject.Keys)
			{
				collectionNode.AppendChild(GetItemNode(xmlDoc, collectionObject[key]));
			}

			return collectionNode;
		}

		/// <summary>
		/// Get item node
		/// </summary>
		private XmlNode GetItemNode(XmlDocument xmlDoc, object itemObject)
		{
			XmlNode itemNode = Utility.XmlUtil.GetXmlNode(xmlDoc, itemObject.GetType().Name);

			foreach (System.Reflection.PropertyInfo property in itemObject.GetType().GetProperties())
			{
				if (property.GetValue(itemObject, null) != null)
				{
					if (property.GetValue(itemObject, null).GetType().Equals(typeof(SettingCollection)))
					{
						itemNode.AppendChild(GetCollectionNode(xmlDoc, property.Name, (SettingCollection)property.GetValue(itemObject, null)));
					}
					else
					{
						itemNode.AppendChild(Utility.XmlUtil.GetXmlNode(xmlDoc, property.Name, property.GetValue(itemObject, null).ToString()));
					}
				}
			}

			return itemNode;
		}

		#endregion
	}
}
