﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace knulf.Configuration
{
	/// <summary>
	/// Setting item collection class. Inherit from Dictionary
	/// </summary>
	public class SettingCollection : Dictionary<string, object>
	{
	}
}
