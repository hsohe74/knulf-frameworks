﻿using knulf.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf
{
	/// <summary>
	/// Specific code library.
	/// </summary>
	public class CodeLib
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Configuration
		////////////////////////////////////////////////////////////////////////////////////////////////////

		// Configuration
		public const string _CONFIG_LOG_ENABLE = "LOG_ENABLE";
		public const string _CONFIG_LOG_PATH = "LOG_PATH";
		public const string _CONFIG_LOG_LEVEL = "LOG_LEVEL";
		public const string _CONFIG_LOG_METHOD = "LOG_METHOD";
		public const string _CONFIG_LOG_WRITING_TERM = "LOG_WRITING_TERM";
		public const string _CONFIG_LOG_STORED_COUNT = "LOG_STORED_COUNT";

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Data
		////////////////////////////////////////////////////////////////////////////////////////////////////

		public const string _DB_OUT_ReturnCode = "OUT_ReturnCode";
		public const string _DB_OUT_ReturnMsg = "OUT_ReturnMsg";
		public const string _DB_OUT_ReturnMsgCode = "OUT_ReturnMsgCode";
		public const string _DB_OUT_Data = "OUT_Data";
		public const string _DB_OUT_Successfully = "Successfully";
		public static readonly string _DB_OUT_TotalRecord = "OUT_TotalRow";
		public static readonly string _DB_OUT_DataSet = DbHelper._DB_DataSet;
		public static readonly string _DB_OUT_Table = DbHelper._DB_Table;

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Site common
		////////////////////////////////////////////////////////////////////////////////////////////////////

		public const string _LANGUAGE_CODE_KR = "KR";
		public const string _LANGUAGE_CODE_EN = "EN";

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Site upload
		////////////////////////////////////////////////////////////////////////////////////////////////////

		public const string _CONFIG_UPLOAD_PATH = "UPLOAD_PATH";

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Image
		////////////////////////////////////////////////////////////////////////////////////////////////////
		
		public const string _IMAGE_THUMBNAIL_SUBFIX = "_thumb";
		public const string _IMAGE_RESIZED_SUBFIX = "_resized";

		public const string _IMAGE_RESIZED_WIDTH = "IMAGE_RESIZED_WIDTH";
		public const string _IMAGE_RESIZED_HEIGHT = "IMAGE_RESIZED_HEIGHT";
	}
}
