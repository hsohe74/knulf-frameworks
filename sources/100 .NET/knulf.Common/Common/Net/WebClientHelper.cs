﻿
using knulf.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace knulf.Net
{
	public class WebClientHelper
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// enum
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region enum

		/// <summary>
		/// Web mthod kind
		/// </summary>
		public enum WebMethod
		{
			GET, POST
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// RequestWeb
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region RequestWeb

		/// <summary>
		/// Reqeust specific url with GET method
		/// </summary>
		/// <param name="url"></param>
		/// <param name="webClient"></param>
		/// <returns></returns>
		public static string RequestGET(string url, WebClientEx webClient = null)
		{
			// Request web
			return RequestGET(url, Encoding.UTF8, webClient);
		}

		/// <summary>
		/// Reqeust specific url with GET method
		/// </summary>
		/// <param name="url"></param>
		/// <param name="encoding"></param>
		/// <param name="webClient"></param>
		/// <returns></returns>
		public static string RequestGET(string url, Encoding encoding, WebClientEx webClient = null)
		{
			// Request web
			return RequestWeb(url, WebMethod.GET, null, encoding, webClient);
		}

		/// <summary>
		/// Reqeust specific url with POST method
		/// </summary>
		/// <param name="url"></param>
		/// <param name="sendData"></param>
		/// <param name="encoding"></param>
		/// <param name="webClient"></param>
		/// <returns></returns>
		public static string RequestPOST(
			string url,
			System.Collections.Specialized.NameValueCollection sendData,
			Encoding encoding = null,
			WebClientEx webClient = null)
		{
			// Encoding
			if (encoding == null)
				encoding = Encoding.UTF8;

			// Request web
			return RequestWeb(url, WebMethod.POST, sendData, encoding, webClient);
		}

		/// <summary>
		/// Reqeust specific url with POST method
		/// </summary>
		/// <param name="url"></param>
		/// <param name="sendDataString">A string with delimiter &</param>
		/// <param name="encoding"></param>
		/// <param name="webClient"></param>
		/// <returns></returns>
		public static string RequestPOST(
			string url,
			string sendDataString,
			Encoding encoding = null,
			WebClientEx webClient = null)
		{
			try
			{
				// Send data
				System.Collections.Specialized.NameValueCollection sendData
					= new System.Collections.Specialized.NameValueCollection();
				string[] items = sendDataString.Split('&');
				foreach (string item in items)
				{
					string[] keyValue = item.Split('=');
					sendData.Add(keyValue[0], keyValue[1]);
				}

				// Encoding
				if (encoding == null)
					encoding = Encoding.UTF8;

				// Request web
				return RequestWeb(url, WebMethod.POST, sendData, encoding, webClient);
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Reqeust specific url
		/// </summary>
		/// <param name="url"></param>
		/// <param name="webMethod"></param>
		/// <param name="sendData"></param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static string RequestWeb(
			string url,
			WebMethod webMethod,
			System.Collections.Specialized.NameValueCollection sendData)
		{
			string strResponse = null;

			try
			{
				// Request web
				strResponse = RequestWeb(url, webMethod, sendData, Encoding.UTF8);
			}
			catch
			{
				throw;
			}

			return strResponse;
		}

		/// <summary>
		///  Reqeust specific url
		/// </summary>
		/// <param name="url"></param>
		/// <param name="webMethod"></param>
		/// <param name="sendData"></param>
		/// <param name="encoding"></param>
		/// <param name="webClient"></param>
		/// <returns></returns>
		public static string RequestWeb(
			string url,
			WebMethod webMethod,
			System.Collections.Specialized.NameValueCollection sendData,
			Encoding encoding = null,
			WebClientEx webClient = null)
		{
			string strResponse = null;
			byte[] responseArray;

			try
			{
				// Encoding
				if (encoding == null)
					encoding = Encoding.UTF8;

				// WebClient
				if (webClient == null)
					webClient = new WebClientEx();

				// Encoding
				webClient.Encoding = encoding;

				// Check HTTPS protocol
				SetHttpsByUrl(url);

				// Send data
				if (sendData == null)
				{
					strResponse = webClient.DownloadString(url);
				}
				else
				{
					if (webMethod.Equals(WebMethod.GET.ToString()))
					{
						StringBuilder sbSendData = new StringBuilder();
						foreach (string key in sendData.Keys)
							sbSendData.Append(key + "=" + sendData[key]);

						responseArray = webClient.UploadData(url, webMethod.ToString(), webClient.Encoding.GetBytes(sbSendData.ToString()));
						if (responseArray.Length > 0) strResponse = webClient.Encoding.GetString(responseArray);
					}
					else
					{
						responseArray = webClient.UploadValues(url, sendData);
						if (responseArray.Length > 0) strResponse = webClient.Encoding.GetString(responseArray);
					}
				}
			}
			catch
			{
				throw;
			}
			finally
			{
				if (webClient != null)
				{
					webClient.Dispose();
					webClient = null;
				}
				responseArray = null;
			}

			return strResponse;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Download and upload
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Download and upload

		/// <summary>
		/// Download file
		/// </summary>
		/// <param name="fileUrl"></param>
		/// <param name="filePath"></param>
		/// <param name="isAsync"></param>
		public static void Download(string fileUrl, string filePath, bool isAsync = true, AsyncCompletedEventHandler compltedEventHandler = null)
		{
			try
			{
				// Create directory
				string directoryPath = FileUtil.GetDirectoryPath(filePath);
				FileUtil.CreateDirectory(directoryPath);

				// Download
				using (var webClient = new WebClient())
				{
					// Check HTTPS protocol
					SetHttpsByUrl(fileUrl);

					if (!FileUtil.IsLockedFile(filePath))
					{
						// Download file
						if (isAsync)
						{
							webClient.DownloadFileAsync(new Uri(fileUrl), filePath);
							webClient.DownloadFileCompleted += compltedEventHandler;
						}
						else
						{
							webClient.DownloadFile(new Uri(fileUrl), filePath);
						}
					}
				}
			}
			catch
			{
				throw;
			}
		}

		private static void WebClient_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Upload file
		/// </summary>
		/// <param name="fileUrl"></param>
		/// <param name="filePath"></param>
		/// <param name="isAsync"></param>
		public static void Upload(string fileUrl, string filePath, bool isAsync = true)
		{
			try
			{
				using (var webClient = new WebClient())
				{
					// Check HTTPS protocol
					SetHttpsByUrl(fileUrl);

					// Upload file
					if (isAsync)
						webClient.UploadFileAsync(new Uri(fileUrl), filePath);
					else
						webClient.UploadFile(new Uri(fileUrl), filePath);
				}
			}
			catch
			{
				throw;
			}
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Request Image
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Request Image

		/// <summary>
		/// Get request stream
		/// </summary>
		public static Stream GetRequestStream(string url)
		{
			Stream result = null;

			try
			{
				WebRequest request = WebRequest.Create(url);
				WebResponse response = request.GetResponse();
				result = response.GetResponseStream();
			}
			catch
			{
				throw;
			}

			return result;
		}

		/// <summary>
		/// Get request stream data
		/// </summary>
		public static byte[] GetRequestStreamData(string url)
		{
			byte[] result = null;

			try
			{
				WebRequest request = WebRequest.Create(url);
				WebResponse response = request.GetResponse();
				using (Stream stream = response.GetResponseStream())
				{
					MemoryStream ms = new MemoryStream();
					stream.CopyTo(ms);
					result = ms.ToArray();
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Utilities
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Utilities

		/// <summary>
		/// Get file name without path
		/// </summary>
		public static string GetFileNameByUrl(string url)
		{
			return url.Substring(url.LastIndexOf('/') + 1);
		}

		/// <summary>
		/// Check whether is https connection
		/// </summary>
		/// <param name="url"></param>
		public static void SetHttpsByUrl(string url)
		{
			// Check HTTPS protocol
			string patternString = @"^\s*https";
			MatchCollection matches = Regex.Matches(url, patternString, RegexOptions.IgnoreCase);
			if (matches.Count > 0)
			{
				SetHttpsByUrl();
			}
		}

		/// <summary>
		/// Check whether is https connection
		/// </summary>
		public static void SetHttpsByUrl()
		{
			ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
		}

		#endregion
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// WebClientEx
	////////////////////////////////////////////////////////////////////////////////////////////////////

	/// <summary>
	/// Web client extension class
	/// </summary>
	public class WebClientEx : WebClient
	{
		/// <summary>
		/// Get this instance
		/// </summary>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static WebClient GetInstance(Encoding encoding)
		{
			WebClient webClient = new WebClient();
			webClient.Encoding = encoding;

			return webClient;
		}

		protected override WebRequest GetWebRequest(Uri address)
		{
			WebRequest request = base.GetWebRequest(address);
			if (request is HttpWebRequest)
			{
				//(request as HttpWebRequest).KeepAlive = false;
				//(request as HttpWebRequest).Timeout = 1000 * 60 * 60;
				(request as HttpWebRequest).ServicePoint.MaxIdleTime = 1000;
				(request as HttpWebRequest).ServicePoint.CloseConnectionGroup((request as HttpWebRequest).ServicePoint.ConnectionName);
			}
			return request;
		}
	}
}
