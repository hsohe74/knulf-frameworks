﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Common.Common.Net
{
	public class EmailHelper : Pattern.Singletone<EmailHelper>
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region CONSTRUCTER

		public EmailHelper()
		{
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Send email
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Send email

		/// <summary>
		/// Send email with STMP
		/// </summary>
		public void SendEmail(
			string toEmail,
			string sendEmail, string sendName,
			string subject, string bodyContent, bool isBodyHtml,
			string serverHost, int serverPort, bool isServerSsl, string loginId, string loginPasswd)
		{
			SendEmail(
				toEmail,
				sendEmail, sendName,
				subject, bodyContent, isBodyHtml,
				Encoding.UTF8,
				null,
				MailPriority.High,
				serverHost, serverPort, isServerSsl, loginId, loginPasswd);
		}

		/// <summary>
		/// Send email with STMP
		/// </summary>
		public void SendEmail(
			string toEmail,
			string sendEmail, string sendName,
			string subject, string bodyContent, bool isBodyHtml,
			string attachFilePath,
			string serverHost, int serverPort, bool isServerSsl, string loginId, string loginPasswd)
		{
			SendEmail(
				toEmail,
				sendEmail, sendName,
				subject, bodyContent, isBodyHtml,
				Encoding.UTF8,
				attachFilePath,
				MailPriority.High,
				serverHost, serverPort, isServerSsl, loginId, loginPasswd);
		}

		/// <summary>
		/// Send email with STMP
		/// </summary>
		public void SendEmail(
			string toEmail,
			string sendEmail, string sendName,
			string subject, string bodyContent, bool isBodyHtml,
			Encoding encoding,
			string attachFilePath,
			MailPriority priority,
			string serverHost, int serverPort, bool isServerSsl, string loginId, string loginPasswd)
		{
			try
			{
				using (MailMessage message = new MailMessage())
				{
					// Receiver
					message.To.Add(toEmail);

					// Sender
					if (string.IsNullOrEmpty(sendName))
						message.From = new MailAddress(sendEmail);
					else
						message.From = new MailAddress(sendEmail, sendName, encoding);

					message.Subject = subject;
					message.SubjectEncoding = encoding;
					message.Body = bodyContent;
					message.BodyEncoding = encoding;
					message.IsBodyHtml = isBodyHtml;
					message.Priority = priority;
					message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

					// Attach file
					if (string.IsNullOrEmpty(attachFilePath) == false)
					{
						Attachment attFile = new Attachment(attachFilePath);
					}

					// SMTP client
					SmtpClient client = new SmtpClient();
					client.Host = serverHost;
					client.Port = serverPort;
					client.EnableSsl = isServerSsl;
					client.Timeout = 10000;
					client.DeliveryMethod = SmtpDeliveryMethod.Network;
					client.UseDefaultCredentials = false;
					if (string.IsNullOrEmpty(loginId) == false)
					{
						client.Credentials = new System.Net.NetworkCredential(loginId, loginPasswd);
					}

					// Send
					client.Send(message);
				}
			}
			catch
			{
				throw;
			}
		}

		#endregion
	}
}
