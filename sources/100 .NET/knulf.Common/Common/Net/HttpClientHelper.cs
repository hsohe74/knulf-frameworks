﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Net
{
	/// <summary>
	/// HTTP request using HttpClient
	/// </summary>
	public class HttpClientHelper : Pattern.Singletone<HttpClientHelper>
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region CONSTRUCTER

		public HttpClientHelper()
		{
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// RequestWeb
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region RequestWeb

		public static async Task<string> GetAsync(string url)
		{
			string result = string.Empty;

			using (HttpClient client = new HttpClient())
			using (HttpResponseMessage response = await client.GetAsync(url))
			using (HttpContent content = response.Content)
			{
				result = await content.ReadAsStringAsync();
			}

			return result;
		}

		#endregion
	}
}
