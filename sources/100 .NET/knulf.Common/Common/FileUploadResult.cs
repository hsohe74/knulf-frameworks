﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf
{
	public class FileUploadResult
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////
		
		public FileUploadResult(bool isSuccess)
			: this(isSuccess, null, null, null, null, null, false, null, null) { }

		public FileUploadResult(bool isSuccess, string message)
			: this(isSuccess, message, null, null, null, null, false, null, null) { }

		public FileUploadResult(string fileAlias, string filePath, long fileSize)
			: this(true, null, fileAlias, filePath, fileSize, null, false, null, null) { }

		public FileUploadResult(string fileAlias, string filePath, long fileSize, string contentType)
			: this(true, null, fileAlias, filePath, fileSize, contentType, false, null, null) { }

		public FileUploadResult(string fileAlias, string filePath, long fileSize, int imageWidth, int imageHeight)
			: this(true, null, fileAlias, filePath, fileSize, null, true, imageWidth, imageHeight) { }

		public FileUploadResult(string fileAlias, string filePath, long fileSize, bool isImageYN, int imageWidth, int imageHeight)
			: this(true, null, fileAlias, filePath, fileSize, null, isImageYN, imageWidth, imageHeight) { }

		public FileUploadResult(bool isSuccess, string fileAlias, string filePath, long fileSize)
			: this(isSuccess, null, fileAlias, filePath, fileSize, null, false, null, null) { }

		public FileUploadResult(bool isSuccess, string fileAlias, string filePath, long fileSize, string contentType)
			: this(isSuccess, null, fileAlias, filePath, fileSize, contentType, false, null, null) { }

		public FileUploadResult(bool isSuccess, string fileAlias, string filePath, long fileSize,int imageWidth, int imageHeight)
			: this(isSuccess, null, fileAlias, filePath, fileSize, null, true, imageWidth, imageHeight) { }

		public FileUploadResult(bool isSuccess, string fileAlias, string filePath, long fileSize, bool isImageYN, int imageWidth, int imageHeight)
			: this(isSuccess, null, fileAlias, filePath, fileSize, null, isImageYN, imageWidth, imageHeight) { }

		public FileUploadResult(bool isSuccess, string message, string fileAlias, string filePath, long? fileSize, string contentType, bool isImage, int? imageWidth, int? imageHeight)
		{
			this.IsSuccess = isSuccess;
			this.Message = message;
			this.FileName = Utility.FileUtil.GetFileName(filePath);
			this.FileAlias = (string.IsNullOrEmpty(fileAlias) ? this.FileName : fileAlias);
			this.FilePath = filePath;
			this.FileSize = fileSize;
			this.ContentType = contentType;
			this.IsImage = isImage;
			this.ImageWidth = imageWidth;
			this.ImageHeight = imageHeight;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Properties
		////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Whether process is successfully done 
		/// </summary>
		public bool IsSuccess { get; set; }

		/// <summary>
		/// Message
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// File name with extension
		/// </summary>
		public string FileName { get; set; }

		/// <summary>
		/// File alias name that is not physical name
		/// </summary>
		public string FileAlias { get; set; }

		/// <summary>
		/// File full path
		/// </summary>
		public string FilePath { get; set; }

		/// <summary>
		/// File size
		/// </summary>
		public long? FileSize { get; set; }

		/// <summary>
		/// MIME Content type
		/// </summary>
		public string ContentType { get; set; }

		/// <summary>
		/// Image
		/// </summary>
		public bool IsImage { get; set; }

		/// <summary>
		/// Image width
		/// </summary>
		public int? ImageWidth { get; set; }

		/// <summary>
		/// Image height
		/// </summary>
		public int? ImageHeight { get; set; }
	}
}
