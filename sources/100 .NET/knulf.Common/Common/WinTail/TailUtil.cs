﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.WinTail
{
	/// <summary>
	/// Monitor changed file and raise event
	/// </summary>
	public class TailUtil
	{
		private FileInfo targetFileInfo = null;
		private FileSystemWatcher fileSystemWatcher = null;
		private long prevSeekPos = 0;

		public delegate void MoreDataHandler(object sender, string newData);
		public event MoreDataHandler MoreData;

		public delegate void ResetDataHandler();
		public event ResetDataHandler ResetData;

		// Lock object
		private object lockObject = new object();

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Constructer & Initialize

		public TailUtil()
			: this(null)
		{
		}

		public TailUtil(String filename)
		{
			this.Filename = filename;
			this.MaxBytes = 1024 * 16;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Properties
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Properties

		/// <summary>
		/// Monitored filename
		/// </summary>
		public string Filename { get; set; }

		/// <summary>
		/// Maximum bytes
		/// </summary>
		public int MaxBytes { get; set; }

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Method : PUBLIC
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region PUBLIC

		public void Start()
		{
			if (this.Filename != null)
			{
				// Target file
				this.targetFileInfo = new FileInfo(this.Filename);

				// Initialize previous seek positin
				this.prevSeekPos = 0;

				// Make FileSystemWatcher instance
				this.fileSystemWatcher = new FileSystemWatcher();
				this.fileSystemWatcher.IncludeSubdirectories = false;
				this.fileSystemWatcher.Path = targetFileInfo.DirectoryName;
				this.fileSystemWatcher.Filter = targetFileInfo.Name;

				if (!this.targetFileInfo.Exists)
				{
					this.fileSystemWatcher.Created += delegate { StartMonitoring(); };
					this.fileSystemWatcher.EnableRaisingEvents = true;
				}
				else
				{
					this.TargetFile_Changed(null, null);
					this.StartMonitoring();
				}
			}
		}

		public void Stop()
		{
			if (this.fileSystemWatcher != null)
			{
				this.fileSystemWatcher.EnableRaisingEvents = false;
				this.fileSystemWatcher.Dispose();
			}
			this.fileSystemWatcher = null;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Method : Monitoring file
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Monitoring file

		private void StartMonitoring()
		{
			this.fileSystemWatcher.Changed += new FileSystemEventHandler(TargetFile_Changed);
			this.fileSystemWatcher.EnableRaisingEvents = true;
		}

		private void TargetFile_Changed(object sender, FileSystemEventArgs e)
		{
			lock (lockObject)
			{
				//read from current seek position to end of file
				byte[] bytesRead = new byte[this.MaxBytes];
				FileStream fs = new FileStream(this.Filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

				// If saved previous position is larger than file length, initial and raise ResetData event
				if (fs.Length > 0 && fs.Length < this.prevSeekPos)
				{
					this.prevSeekPos = 0;
					if (this.ResetData != null) ResetData();
				}

				// If file length is larger than MaxBytes, previous position subtract MaxBytes from file length
				if (this.prevSeekPos == 0 && fs.Length > this.MaxBytes)
				{
					this.prevSeekPos = fs.Length - this.MaxBytes;
				}

				// Read file with offset with previous position
				this.prevSeekPos = (int)fs.Seek(this.prevSeekPos, SeekOrigin.Begin);
				int numBytes = fs.Read(bytesRead, 0, this.MaxBytes);
				fs.Close();

				this.prevSeekPos += numBytes;

				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < numBytes; i++)
				{
					sb.Append((char)bytesRead[i]);
				}

				//call delegates with the string
				this.MoreData(this, sb.ToString());
			}
		}

		#endregion
	}
}
