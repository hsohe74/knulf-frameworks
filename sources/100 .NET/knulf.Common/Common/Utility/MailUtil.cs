﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Common.Common.Utility
{
	/// <summary>
	/// Utilities of Mail
	/// </summary>
	public class MailUtil
	{
		/// <summary>
		/// Send Email Using SMTP
		/// </summary>
		/// <param name="smtpClient"></param>
		/// <param name="senderEmail"></param>
		/// <param name="receiverEmail"></param>
		/// <param name="subject"></param>
		/// <param name="subjectEncoding"></param>
		/// <param name="body"></param>
		/// <param name="bodyEncoding"></param>
		/// <param name="isBodyHtml"></param>
		/// <returns></returns>
		public static bool SendMail(
			SmtpClient smtpClient,
			string senderEmail,
			string receiverEmail,
			string subject, Encoding subjectEncoding,
			string body, Encoding bodyEncoding, bool isBodyHtml)
		{
			List<string> receiverEmailList = new List<string>();
			receiverEmailList.Add(receiverEmail);

			return SendMail(
				smtpClient,
				null, senderEmail,
				receiverEmailList,
				null,
				null,
				subject, subjectEncoding,
				body, bodyEncoding, isBodyHtml);
		}

		/// <summary>
		/// Send Email Using SMTP
		/// </summary>
		/// <param name="smtpClient"></param>
		/// <param name="senderName"></param>
		/// <param name="senderEmail"></param>
		/// <param name="receiverEmailList"></param>
		/// <param name="subject"></param>
		/// <param name="subjectEncoding"></param>
		/// <param name="body"></param>
		/// <param name="bodyEncoding"></param>
		/// <param name="isBodyHtml"></param>
		/// <returns></returns>
		public static bool SendMail(
			SmtpClient smtpClient,
			string senderName, string senderEmail,
			List<string> receiverEmailList,
			string subject, Encoding subjectEncoding,
			string body, Encoding bodyEncoding, bool isBodyHtml)
		{
			return SendMail(
				smtpClient,
				senderName, senderEmail,
				receiverEmailList,
				null,
				null,
				subject, subjectEncoding,
				body, bodyEncoding, isBodyHtml);
		}

		/// <summary>
		/// Send Email Using SMTP
		/// </summary>
		/// <param name="smtpClient"></param>
		/// <param name="senderName"></param>
		/// <param name="senderEmail"></param>
		/// <param name="receiverEmailList"></param>
		/// <param name="ccList"></param>
		/// <param name="bccList"></param>
		/// <param name="subject"></param>
		/// <param name="subjectEncoding"></param>
		/// <param name="body"></param>
		/// <param name="bodyEncoding"></param>
		/// <param name="isBodyHtml"></param>
		/// <returns></returns>
		public static bool SendMail(
			SmtpClient smtpClient,
			string senderName, string senderEmail,
			List<string> receiverEmailList,
			List<string> ccList,
			List<string> bccList,
			string subject, Encoding subjectEncoding, 
			string body, Encoding bodyEncoding, bool isBodyHtml)
		{
			bool result = true;

			MailMessage mailMessage = new MailMessage();

			// 보내는 사람
			if (string.IsNullOrEmpty(senderName))
				mailMessage.From = new MailAddress(senderEmail);
			else
				mailMessage.From = new MailAddress(senderEmail, senderName, Encoding.UTF8);
			// 받는 사람
			foreach (string email in receiverEmailList)
			{
				mailMessage.To.Add(email);
			}
			// 참조
			//mailMessage.CC.Add();
			if (ccList != null && ccList.Count > 0)
			{
				foreach (string email in ccList)
				{
					mailMessage.CC.Add(email);
				}
			}
			// 숨은 참조
			if (bccList != null && bccList.Count > 0)
			{
				foreach (string email in bccList)
				{
					mailMessage.Bcc.Add(email);
				}
			}
			// 제목
			mailMessage.Subject = subject;
			mailMessage.SubjectEncoding = subjectEncoding;
			// 본문
			mailMessage.Body = body;
			mailMessage.BodyEncoding = bodyEncoding;
			mailMessage.IsBodyHtml = isBodyHtml;

			// 메일발송
			smtpClient.Send(mailMessage);

			return result;
		}

		/// <summary>
		/// Get instance of SMTP client
		/// </summary>
		/// <param name="host"></param>
		/// <param name="port"></param>
		/// <param name="userId"></param>
		/// <param name="userPw"></param>
		/// <param name="isSslEnable"></param>
		/// <returns></returns>
		public static SmtpClient GetSmtpClient(string host, int port, string userId, string userPw, bool isSslEnable)
		{
			SmtpClient smtpClient = null;

			try
			{
				smtpClient = new SmtpClient();
				smtpClient.Host = host;
				smtpClient.Port = port;
				smtpClient.Credentials = new System.Net.NetworkCredential(userId, userPw);
				smtpClient.EnableSsl = isSslEnable;
			}
			catch
			{
				smtpClient = null;
			}

			return smtpClient;
		}

		public static SmtpClient GetSmtpClient(string host, int port, bool isSslEnable)
		{
			SmtpClient smtpClient = null;

			try
			{
				smtpClient = new SmtpClient();
				smtpClient.Host = host;
				smtpClient.Port = port;
				smtpClient.EnableSsl = isSslEnable;
			}
			catch
			{
				smtpClient = null;
			}

			return smtpClient;
		}
	}
}
