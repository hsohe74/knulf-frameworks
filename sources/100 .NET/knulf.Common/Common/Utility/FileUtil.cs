﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace knulf.Utility
{
	/// <summary>
	/// Utilities of file handling
	/// </summary>
	public class FileUtil
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Binary file handing
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Binary file handing

		/// <summary>
		/// Read binary file
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public static byte[] ReadBinaryFile(string filePath)
		{
			byte[] returnBuffer = null;

			try
			{
				if (!File.Exists(filePath))
				{
					throw new FileNotFoundException("File not found.", filePath);
				}

				using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
				{
					using (BinaryReader br = new BinaryReader(fs))
					{
						returnBuffer = br.ReadBytes(Convert.ToInt32(fs.Length));
					}
				}
			}
			catch
			{
				throw;
			}

			return returnBuffer;
		}

		/// <summary>
		/// Write file from byte array
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="binaryData"></param>
		public static void WriteBinaryFile(string filePath, byte[] binaryData)
		{
			try
			{
				// Directory check
				string directoryPath = Path.GetDirectoryName(filePath);
				if (!Directory.Exists(directoryPath))
					Directory.CreateDirectory(directoryPath);

				File.WriteAllBytes(filePath, binaryData);
			}
			catch
			{
				throw;
			}
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Text file handing
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Text file handing

		/// <summary>
		/// Read text file
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public static string ReadTextFile(string filePath)
		{
			return ReadTextFile(filePath, Encoding.UTF8, true);
		}

		/// <summary>
		/// Read text file
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static string ReadTextFile(string filePath, Encoding encoding)
		{
			return ReadTextFile(filePath, encoding, true);
		}

		/// <summary>
		/// Read text file
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="encoding"></param>
		/// <param name="isWithoutBom"></param>
		/// <returns></returns>
		public static string ReadTextFile(string filePath, Encoding encoding, bool isWithoutBom)
		{
			string returnstring = string.Empty;

			try
			{
				using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
				{
					if (encoding == Encoding.UTF8 && isWithoutBom == true)
					{
						using (StreamReader sr = new StreamReader(fs, Encoding.UTF8, true))
						{
							returnstring = sr.ReadToEnd();
						}
					}
					else
					{
						using (StreamReader sr = new StreamReader(fs, encoding))
						{
							returnstring = sr.ReadToEnd();
						}
					}
				}
			}
			catch
			{
				throw;
			}

			return returnstring;
		}

		/// <summary>
		/// Write text to file
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="text"></param>
		/// <returns></returns>
		public static bool WriteTextFile(string filePath, string text)
		{
			return WriteTextFile(filePath, text, Encoding.UTF8, true);
		}

		/// <summary>
		/// Write text to file
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="text"></param>
		/// <param name="isAppend"></param>
		/// <returns></returns>
		public static bool WriteTextFile(string filePath, string text, bool isAppend)
		{
			return WriteTextFile(filePath, text, isAppend, Encoding.UTF8, true);
		}

		/// <summary>
		/// Write text to file
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="text"></param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static bool WriteTextFile(string filePath, string text, Encoding encoding)
		{
			return WriteTextFile(filePath, text, false, encoding, true);
		}

		/// <summary>
		/// Write text to file
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="text"></param>
		/// <param name="encoding"></param>
		/// <param name="isWithoutBom"></param>
		/// <returns></returns>
		public static bool WriteTextFile(string filePath, string text, Encoding encoding, bool isWithoutBom)
		{
			return WriteTextFile(filePath, text, false, encoding, isWithoutBom);
		}

		/// <summary>
		/// Write text to file
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="text"></param>
		/// <param name="isAppend"></param>
		/// <param name="encoding"></param>
		/// <param name="isWithoutBom"></param>
		/// <returns></returns>
		public static bool WriteTextFile(string filePath, string text, bool isAppend, Encoding encoding, bool isWithoutBom)
		{
			bool isSuccess = true;
			string directory = null;

			try
			{
				// Directory check
				directory = Path.GetDirectoryName(filePath);
				if (!Directory.Exists(directory))
					Directory.CreateDirectory(directory);

				// Write file
				using (FileStream file = new FileStream(filePath, (isAppend) ? FileMode.Append : FileMode.Create, FileAccess.Write, FileShare.None))
				{
					if (encoding == Encoding.UTF8 && isWithoutBom == true)
					{
						using (StreamWriter writer = new StreamWriter(file, new UTF8Encoding(false)))
						{
							writer.Write(text);
						}
					}
					else
					{
						using (StreamWriter writer = new StreamWriter(file, encoding))
						{
							writer.Write(text);
						}
					}
				}
			}
			catch
			{
				throw;
			}

			return isSuccess;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// File system
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region File system

		/// <summary>
		/// Get combined path
		/// </summary>
		public static string GetCombinePath(string[] path)
		{
			return Path.Combine(path);
		}

		/// <summary>
		/// Get combined path
		/// </summary>
		public static string GetCombinePath(string path1, string path2)
		{
			return Path.Combine(path1, path2);
		}

		/// <summary>
		/// Get combined path
		/// </summary>
		public static string GetCombinePath(string path1, string path2, string path3)
		{
			return Path.Combine(path1, path2, path3);
		}

		/// <summary>
		/// Get combined path
		/// </summary>
		public static string GetCombinePath(string path1, string path2, string path3, string path4)
		{
			return Path.Combine(path1, path2, path3, path4);
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// File handling
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region File handling

		/// <summary>
		/// Get indicated value whether file locked.
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public static bool IsLockedFile(string filePath)
		{
			try
			{
				FileStream fs = File.OpenWrite(filePath);
				fs.Close();
				return false;
			}
			catch (Exception) { return true; }
		}

		/// <summary>
		/// Get file name from path string
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public static string GetFileName(string filePath)
		{
			string fileName = null;

			try
			{
				fileName = Path.GetFileName(filePath);
			}
			catch
			{
				return null; ;
			}

			return fileName;
		}

		/// <summary>
		/// Get file name with out extension of path string
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public static string GetFileNameWithoutExtension(string filePath)
		{
			string fileName = null;

			try
			{
				if (filePath.IndexOf(Path.DirectorySeparatorChar) < 0)
					filePath = AppDomain.CurrentDomain.BaseDirectory + Path.DirectorySeparatorChar + filePath;
				fileName = Path.GetFileNameWithoutExtension(filePath);
			}
			catch
			{
				return null; ;
			}

			return fileName;
		}

		/// <summary>
		/// Get file extension of path string
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public static string GetFileExtension(string filePath)
		{
			string fileName = null;

			try
			{
				fileName = Path.GetExtension(filePath).Substring(1).ToLower();
			}
			catch
			{
				return null;
			}

			return fileName;
		}

		/// <summary>
		/// Get unique file name
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public static string GetUniqueFilename(string filePath)
		{
			string uniqueFilepath = null;
			int renameIndex = 1;

			try
			{
				string directory = GetDirectoryPath(filePath);
				string filename = GetFileNameWithoutExtension(filePath);
				string fileextension = GetFileExtension(filePath);
				uniqueFilepath = filePath;

				Match regex = Regex.Match(filePath, @"(.+) \((\d+)\)\.\w+");
				if (regex.Success)
				{
					filename = regex.Groups[1].Value;
					renameIndex = int.Parse(regex.Groups[2].Value);
				}

				while (File.Exists(uniqueFilepath))
				{
					uniqueFilepath = Path.Combine(directory, string.Format("{0} ({1}).{2}", filename, renameIndex++, fileextension));
				}
			}
			catch
			{
				uniqueFilepath = null;
			}

			return uniqueFilepath;
		}

		/// <summary>
		/// Delete file
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public static bool DeleteFile(string filePath)
		{
			bool isSuccess = true;

			try
			{
				if (File.Exists(filePath)) File.Delete(filePath);
			}
			catch
			{
				return false;
			}

			return isSuccess;
		}

		/// <summary>
		/// Copy file
		/// </summary>
		/// <param name="sourceFilePath"></param>
		/// <param name="targetFilePath"></param>
		/// <param name="isOverwrite"></param>
		/// <returns></returns>
		public static bool CopyFile(string sourceFilePath, string targetFilePath, bool isOverwrite)
		{
			bool isSuccess = true;
			string directory = null;

			try
			{
				if (!File.Exists(sourceFilePath))
				{
					throw new FileNotFoundException("File not found.", sourceFilePath);
				}

				directory = GetDirectoryPath(targetFilePath);
				if (CreateDirectory(directory))
				{
					File.Copy(sourceFilePath, targetFilePath, isOverwrite);
				}
			}
			catch
			{
				throw;
			}

			return isSuccess;
		}

		/// <summary>
		/// Move file
		/// </summary>
		/// <param name="sourceFilePath"></param>
		/// <param name="targetFilePath"></param>
		/// <returns></returns>
		public static bool MoveFile(string sourceFilePath, string targetFilePath)
		{
			return MoveFile(sourceFilePath, targetFilePath, true);
		}

		/// <summary>
		/// Move file
		/// </summary>
		/// <param name="sourceFilePath"></param>
		/// <param name="targetFilePath"></param>
		/// <param name="isOverwrite"></param>
		/// <returns></returns>
		public static bool MoveFile(string sourceFilePath, string targetFilePath, bool isOverwrite)
		{
			bool isSuccess = true;
			string directory = null;

			try
			{
				if (!File.Exists(sourceFilePath))
				{
					throw new FileNotFoundException("File not found.", sourceFilePath);
				}

				directory = GetDirectoryPath(targetFilePath);
				if (CreateDirectory(directory))
				{
					if (File.Exists(targetFilePath))
					{
						if (isOverwrite == true)
						{
							File.Delete(targetFilePath);
							File.Move(sourceFilePath, targetFilePath);
						}
					}
					else
					{
						File.Move(sourceFilePath, targetFilePath);
					}
				}
			}
			catch
			{
				throw;
			}

			return isSuccess;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Dirve handling
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Dirve handling

		/// <summary>
		/// Get logical dirve list
		/// </summary>
		/// <returns></returns>
		public static string[] GetLogicalDrives()
		{
			return Environment.GetLogicalDrives();
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Directory handling
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Directory handling

		/// <summary>
		/// Get directory name from directory path
		/// </summary>
		/// <param name="directoryPath"></param>
		/// <returns></returns>
		public static string GetDirectoryNameByDirectory(string directoryPath)
		{
			string directory = null;

			try
			{
				directory = new DirectoryInfo(directoryPath).Name;
			}
			catch
			{
				return null; ;
			}

			return directory;
		}

		/// <summary>
		/// Get directory name from file path
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public static string GetDirectoryName(string filePath)
		{
			string directory = null;

			try
			{
				directory = new FileInfo(filePath).Directory.Name;
			}
			catch
			{
				return null; ;
			}

			return directory;
		}

		/// <summary>
		/// Get directory full path by file path
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public static string GetDirectoryPath(string filePath)
		{
			string directoryPath = null;

			try
			{
				directoryPath = new FileInfo(filePath).Directory.FullName;
			}
			catch
			{
				return null;
			}

			return directoryPath;
		}

		/// <summary>
		/// Get directory info object
		/// </summary>
		/// <param name="directoryPath"></param>
		/// <returns></returns>
		public static DirectoryInfo GetDirectoryInfo(string directoryPath)
		{
			DirectoryInfo directoryInfo = null;

			try
			{
				directoryInfo = new DirectoryInfo(directoryPath);
			}
			catch
			{
				return null;
			}

			return directoryInfo;
		}

		/// <summary>
		/// Create directory
		/// </summary>
		/// <param name="directoryPath"></param>
		/// <returns></returns>
		public static bool CreateDirectory(string directoryPath)
		{
			bool isSuccess = true;

			try
			{
				if (!Directory.Exists(directoryPath))
					Directory.CreateDirectory(directoryPath);
			}
			catch
			{
				return false;
			}

			return isSuccess;
		}

		/// <summary>
		/// Delete directory
		/// </summary>
		/// <param name="directoryPath"></param>
		/// <param name="isRecursive"></param>
		/// <returns></returns>
		public static bool DeleteDirectory(string directoryPath, bool isRecursive)
		{
			bool isSuccess = true;

			try
			{
				if (Directory.Exists(directoryPath))
					Directory.Delete(directoryPath, isRecursive);
			}
			catch
			{
				return false;
			}

			return isSuccess;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="directoryPath"></param>
		/// <returns></returns>
		public static string[] GetSubDirectories(string directoryPath)
		{
			return Directory.GetDirectories(directoryPath, "*", SearchOption.TopDirectoryOnly);
		}

		/// <summary>
		/// Get sub directory list
		/// </summary>
		/// <param name="directoryPath"></param>
		/// <param name="searchOption"></param>
		/// <returns></returns>
		public static string[] GetSubDirectories(string directoryPath, SearchOption searchOption)
		{
			return Directory.GetDirectories(directoryPath, "*", searchOption);
		}

		/// <summary>
		/// Get sub directory list
		/// </summary>
		/// <param name="directoryPath"></param>
		/// <param name="searchPattern"></param>
		/// <returns></returns>
		public static string[] GetSubDirectories(string directoryPath, string searchPattern)
		{
			return Directory.GetDirectories(directoryPath, searchPattern, SearchOption.TopDirectoryOnly);
		}

		/// <summary>
		/// Get sub directory list
		/// </summary>
		/// <param name="directoryPath"></param>
		/// <param name="searchPattern"></param>
		/// <param name="searchOption"></param>
		/// <returns></returns>
		public static string[] GetSubDirectories(string directoryPath, string searchPattern, SearchOption searchOption)
		{
			return Directory.GetDirectories(directoryPath, searchPattern, searchOption);
		}

		/// <summary>
		/// Get directory's file list
		/// </summary>
		/// <param name="directoryPath"></param>
		/// <returns></returns>
		public static string[] GetFileListInDirectory(string directoryPath)
		{
			return GetFileListInDirectory(directoryPath, "*", SearchOption.TopDirectoryOnly);
		}

		/// <summary>
		/// Get directory's file list
		/// </summary>
		/// <param name="directoryPath"></param>
		/// <param name="searchOption"></param>
		/// <returns></returns>
		public static string[] GetFileListInDirectory(string directoryPath, SearchOption searchOption)
		{
			return GetFileListInDirectory(directoryPath, "*", searchOption);
		}

		/// <summary>
		/// Get directory's file list
		/// </summary>
		/// <param name="directoryPath"></param>
		/// <param name="searchPattern"></param>
		/// <returns></returns>
		public static string[] GetFileListInDirectory(string directoryPath, string searchPattern)
		{
			return GetFileListInDirectory(directoryPath, searchPattern, SearchOption.TopDirectoryOnly);
		}

		/// <summary>
		/// Get directory's file list
		/// </summary>
		/// <param name="directoryPath"></param>
		/// <param name="searchPattern"></param>
		/// <param name="searchOption"></param>
		/// <returns></returns>
		public static string[] GetFileListInDirectory(string directoryPath, string searchPattern, SearchOption searchOption)
		{
			string[] filePathes = null;

			try
			{
				filePathes = Directory.GetFiles(directoryPath, searchPattern, searchOption);
			}
			catch
			{
				throw;
			}

			return filePathes;
		}

		/// <summary>
		/// Get directory's file information list
		/// </summary>
		/// <param name="directoryPath"></param>
		/// <returns></returns>
		public static FileInfo[] GetFileInfoListInDirectory(string directoryPath)
		{
			return GetFileInfoListInDirectory(directoryPath, "*", SearchOption.TopDirectoryOnly);
		}

		/// <summary>
		/// Get directory's file information list
		/// </summary>
		/// <param name="directoryPath"></param>
		/// <param name="searchOption"></param>
		/// <returns></returns>
		public static FileInfo[] GetFileInfoListInDirectory(string directoryPath, SearchOption searchOption)
		{
			return GetFileInfoListInDirectory(directoryPath, "*", searchOption);
		}

		/// <summary>
		/// Get directory's file information list
		/// </summary>
		/// <param name="directoryPath"></param>
		/// <param name="searchPattern"></param>
		/// <returns></returns>
		public static FileInfo[] GetFileInfoListInDirectory(string directoryPath, string searchPattern)
		{
			return GetFileInfoListInDirectory(directoryPath, searchPattern, SearchOption.TopDirectoryOnly);
		}

		/// <summary>
		/// Get directory's file information list
		/// </summary>
		/// <param name="directoryPath"></param>
		/// <param name="searchPattern"></param>
		/// <param name="searchOption"></param>
		/// <returns></returns>
		public static FileInfo[] GetFileInfoListInDirectory(string directoryPath, string searchPattern, SearchOption searchOption)
		{
			FileInfo[] fileInfoList = null;

			try
			{
				DirectoryInfo directoryInfo = GetDirectoryInfo(directoryPath);
				if (directoryInfo != null)
				{
					fileInfoList = directoryInfo.GetFiles(searchPattern, searchOption);
				}
			}
			catch
			{
				throw;
			}

			return fileInfoList;
		}

		/// <summary>
		/// Remove last separator
		/// </summary>
		/// <param name="directoryPath"></param>
		/// <returns></returns>
		public static string RemoveDirectoryLastSeparator(string directoryPath)
		{
			if (directoryPath.LastIndexOf(Path.DirectorySeparatorChar) == directoryPath.Length - 1)
				return directoryPath = directoryPath.Substring(0, directoryPath.Length - 1);
			else
				return directoryPath;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Utilities
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Utilities

		/// <summary>
		/// Size suffixes
		/// </summary>
		public static string[] SizeSuffixesBinary = { "bytes", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB" };
		public static string[] SizeSuffixesCommon = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

		/// <summary>
		/// Get Size string with unit
		/// </summary>
		public static string GetSizeWithSuffix(long fileSize, int decimalPlaces = 1, bool isBinarySize = true)
		{
			// Size suffixes
			string[] sizeSuffixes;
			if (isBinarySize)
				sizeSuffixes = SizeSuffixesBinary;
			else
				sizeSuffixes = SizeSuffixesCommon;

			// If value is minus than...
			if (fileSize < 0)
			{
				return "-" + GetSizeWithSuffix(-fileSize);
			}

			// If value is zero than...
			if (fileSize == 0)
			{
				return string.Format("{0:n" + decimalPlaces + "} {1}", 0.0M, sizeSuffixes[0]);
			}

			// mag is 0 for bytes, 1 for KiB, 2, for MiB, etc.
			int logValue = (int)Math.Log(fileSize, 1024);

			// 1L << (mag * 10) == 2 ^ (10 * mag) 
			decimal adjustedSize = (decimal)fileSize / (1L << (logValue * 10));

			// make adjustment when the value is large enough that
			// it would round up to 1000 or more
			if (Math.Round(adjustedSize, decimalPlaces) >= 1024)
			{
				logValue += 1;
				adjustedSize /= 1024;
			}

			// Return
			return string.Format("{0:n" + decimalPlaces + "} {1}", adjustedSize, sizeSuffixes[logValue]);
		}

		#endregion

	}
}
