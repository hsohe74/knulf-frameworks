﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace knulf.Utility
{
	public class AsyncUtil
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Run asynchronous method
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Run asynchronous method

		/// <summary>
		/// Run BackgroundWorker
		/// </summary>
		public static void RunBackgroundWorker(Action doWorkAction, Action workerCompletedAction = null, Action progressChangedAction = null)
		{
			using (var worker = new BackgroundWorker())
			{
				worker.DoWork += delegate
				{
					doWorkAction.Invoke();
				};
				worker.RunWorkerCompleted += delegate {
					if (workerCompletedAction != null)
					{
						workerCompletedAction.Invoke();
					}
				};
				worker.ProgressChanged += delegate {
					if (progressChangedAction != null)
					{
						progressChangedAction.Invoke();
					}
				};
				worker.RunWorkerAsync();
			}
		}

		/// <summary>
		/// Run Task
		/// </summary>
		public static void RunTask(Action startAction, Action continueAction = null)
		{
			try
			{
				var task = Task.Factory.StartNew(() =>
				{
					startAction.Invoke();
				});

				if (continueAction != null)
				{
					task.ContinueWith(delegate
					{
						continueAction.Invoke();
					}, TaskScheduler.FromCurrentSynchronizationContext());
				}
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Run STA thread
		/// </summary>
		public static Thread RunSTAThread(Action action)
		{
			var thread = new Thread(() => {
				action.Invoke();
			});
			thread.SetApartmentState(ApartmentState.STA);
			thread.Start();

			return thread;
		}

		#endregion
	}
}
