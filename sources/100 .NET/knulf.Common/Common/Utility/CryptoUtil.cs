﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace knulf.Utility
{
	/// <summary>
	/// Utilities of security
	/// </summary>
	public class CryptoUtil
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// AES
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region AES

		/// <summary>
		/// Get encrypt string by AES256
		/// </summary>
		public static string AESEncrypt(string plainstring, string key)
		{
			Encoding encoding = Encoding.UTF8;
			string encryptstring = string.Empty;
			byte[] encryptData = null;
			byte[] keyBytes = new byte[32];
			byte[] iv = new byte[16];

			if (string.IsNullOrEmpty(plainstring))
			{
				throw new SecurityArgumentException("Plain string is null or empty string.");
			}
			if (string.IsNullOrEmpty(key))
			{
				throw new SecurityArgumentException("Key is null or empty string.");
			}
			if ((encoding.GetByteCount(key) < 32))
			{
				throw new SecurityArgumentException("Key's byte length is not 32.");
			}
			else
			{
				if (encoding.GetByteCount(key) == 32)
					keyBytes = encoding.GetBytes(key);
				else
					Array.Copy(encoding.GetBytes(key), keyBytes, 32);
			}
			
			if (iv == null || iv.Length != 16)
				throw new SecurityArgumentException("Initialization vector is null or length is not 16.");
			else
				Array.Copy(encoding.GetBytes(key), iv, 16);

			try {
				RijndaelManaged rijndaelCipher = new RijndaelManaged();
				rijndaelCipher.Mode = CipherMode.CBC;
				rijndaelCipher.Padding = PaddingMode.PKCS7;
				rijndaelCipher.KeySize = 32 * 8; // 256
				rijndaelCipher.BlockSize = 128;
				rijndaelCipher.Key = keyBytes;
				rijndaelCipher.IV = iv;

				ICryptoTransform transform = rijndaelCipher.CreateEncryptor(rijndaelCipher.Key, rijndaelCipher.IV);
				using (var ms = new MemoryStream())
				{
					using (var cs = new CryptoStream(ms, transform, CryptoStreamMode.Write))
					{
						byte[] plainData = Encoding.UTF8.GetBytes(plainstring);
						cs.Write(plainData, 0, plainData.Length);
					}
					encryptData = ms.ToArray();
				}

				encryptstring = System.Convert.ToBase64String(encryptData);
			}
			catch {
				throw;
			}

			return encryptstring;
		}

		/// <summary>
		/// Get descrypt string by AES256
		/// </summary>
		public static string AESDecrypt(string encryptstring, string key)
		{
			Encoding encoding = Encoding.UTF8;
			string plainstring = string.Empty;
			byte[] decryptData = null;
			byte[] keyBytes = new byte[32];
			byte[] iv = new byte[16];

			if (string.IsNullOrEmpty(encryptstring))
				throw new SecurityArgumentException("Encrypt string is null or empty string.");
			if (string.IsNullOrEmpty(key))
				throw new SecurityArgumentException("Key is null or empty string.");
			if ((encoding.GetByteCount(key) < 32)) {
				throw new SecurityArgumentException("Key's byte length is not 32.");
			}
			else
			{
				if (encoding.GetByteCount(key) == 32)
					keyBytes = encoding.GetBytes(key);
				else
					Array.Copy(encoding.GetBytes(key), keyBytes, 32);
			}
			
			if (iv == null || iv.Length != 16)
				throw new SecurityArgumentException("Initialization vector is null or length is not 16.");
			else
				Array.Copy(encoding.GetBytes(key), iv, 16);

			try
			{
				RijndaelManaged rijndaelCipher = new RijndaelManaged();
				rijndaelCipher.Mode = CipherMode.CBC;
				rijndaelCipher.Padding = PaddingMode.PKCS7;
				rijndaelCipher.KeySize = 32 * 8; // 256
				rijndaelCipher.BlockSize = 128;
				rijndaelCipher.Key = keyBytes;
				rijndaelCipher.IV = iv;

				ICryptoTransform transform = rijndaelCipher.CreateDecryptor();
				using (var ms = new MemoryStream())
				{
					using (var cs = new CryptoStream(ms, transform, CryptoStreamMode.Write))
					{
						byte[] encryptData = Convert.FromBase64String(encryptstring);
						cs.Write(encryptData, 0, encryptData.Length);
					}

					decryptData = ms.ToArray();
				}

				plainstring = encoding.GetString(decryptData);
			}
			catch
			{
				throw;
			}

			return plainstring;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Hash
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Hash

		/// <summary>
		/// Get hash data with SHA256 algorithm
		/// </summary>
		public static string SHA256Hash(string data)
		{
			if (data == null) return null;

			byte[] dataBytes = Encoding.UTF8.GetBytes(data);

			StringBuilder StringBuilder = new StringBuilder();
			using (SHA256 sha = new SHA256Managed())
			{
				byte[] hash = sha.ComputeHash(dataBytes);
				foreach (byte b in hash)
				{
					StringBuilder.AppendFormat("{0:x2}", b);
				}
			}

			return StringBuilder.ToString();
		}

		/// <summary>
		/// Get hash data with SHA256 algorithm
		/// </summary>
		public static string SHA256HashWithSalt(string data, string salt)
		{
			if (data == null) return null;

			byte[] dataBytes = Encoding.UTF8.GetBytes(data);
			byte[] saltBytes = Encoding.UTF8.GetBytes(salt);

			List<byte> listByte = new List<byte>();
			listByte.AddRange(dataBytes);
			listByte.AddRange(saltBytes);

			StringBuilder StringBuilder = new StringBuilder();
			using (SHA256 sha = new SHA256Managed())
			{
				byte[] hash = sha.ComputeHash(listByte.ToArray());
				foreach (byte b in hash)
				{
					StringBuilder.AppendFormat("{0:x2}", b);
				}
			}

			return StringBuilder.ToString();
		}

		/// <summary>
		/// Get hash data with SHA512 algorithm
		/// </summary>
		public static string SHA512Hash(string data)
		{
			if (data == null) return null;

			byte[] dataBytes = Encoding.UTF8.GetBytes(data);

			StringBuilder StringBuilder = new StringBuilder();
			using (SHA512 sha = new SHA512Managed())
			{
				byte[] hash = sha.ComputeHash(dataBytes);
				foreach (byte b in hash)
				{
					StringBuilder.AppendFormat("{0:x2}", b);
				}
			}

			return StringBuilder.ToString();
		}

		/// <summary>
		/// Get hash data with SHA512 algorithm
		/// </summary>
		public static string SHA512HashWithSalt(string data, string salt)
		{
			if (data == null) return null;

			byte[] dataBytes = Encoding.UTF8.GetBytes(data);
			byte[] saltBytes = Encoding.UTF8.GetBytes(salt);

			List<byte> listByte = new List<byte>();
			listByte.AddRange(dataBytes);
			listByte.AddRange(saltBytes);

			StringBuilder StringBuilder = new StringBuilder();
			using (SHA512 sha = new SHA512Managed())
			{
				byte[] hash = sha.ComputeHash(listByte.ToArray());
				foreach (byte b in hash)
				{
					StringBuilder.AppendFormat("{0:x2}", b);
				}
			}

			return StringBuilder.ToString();
		}

		/// <summary>
		/// Get hash data with MD5 algorithm
		/// </summary>
		public static string Md5Hash(string data)
		{
			if (data == null) return null;

			StringBuilder StringBuilder = new StringBuilder();
			using (MD5 md5Hash = MD5.Create())
			{
				byte[] hash = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(data));
				for (int i = 0; i < data.Length; i++)
				{
					StringBuilder.Append(hash[i].ToString("x2"));
				}
			}

			return StringBuilder.ToString();
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// HMAC-SHA256
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region HMAC-SHA256

		public static string HMACSHA256(string message, string key, bool isReplaceEqualSign)
		{
			key = key ?? "";
			byte[] keyByte = Encoding.UTF8.GetBytes(key);
			byte[] messageBytes = Encoding.UTF8.GetBytes(message);
			using (var hmacsha256 = new HMACSHA256(keyByte))
			{
				byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
				var result = Convert.ToBase64String(hashmessage);
				if (isReplaceEqualSign)
					result = result.Replace("=", "");
				return result;
			}
		}

		#endregion
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Exception
	////////////////////////////////////////////////////////////////////////////////////////////////////

	public class SecurityArgumentException : System.Exception
	{
		public SecurityArgumentException(string message) : base(message) { }
	}
}
