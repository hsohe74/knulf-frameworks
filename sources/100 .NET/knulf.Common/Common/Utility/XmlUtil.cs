﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;
using System.Reflection;

namespace knulf.Utility
{
	/// <summary>
	/// XML common library
	/// </summary>
	public class XmlUtil
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Xml Node
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Xml Node

		/// <summary>
		/// Get xml node by element name
		/// </summary>
		/// <param name="xmlDoc"></param>
		/// <param name="elementName"></param>
		/// <returns></returns>
		public static XmlNode GetXmlNode(XmlDocument xmlDoc, string nodeName)
		{
			return GetXmlNode(xmlDoc, nodeName, null);
		}

		/// <summary>
		/// Get xml node by element name and value
		/// </summary>
		/// <param name="xmlDoc"></param>
		/// <param name="elementName"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public static XmlNode GetXmlNode(XmlDocument xmlDoc, string nodeName, string value)
		{
			XmlNode node = xmlDoc.CreateElement(string.Empty, nodeName, string.Empty);
			if (value != null && value != string.Empty) node.InnerXml = GetXmlEncode(value);

			return node;
		}

		/// <summary>
		/// Get xml node by element name and value with sub xml
		/// </summary>
		/// <param name="xmlDoc"></param>
		/// <param name="nodeName"></param>
		/// <param name="xml"></param>
		/// <returns></returns>
		public static XmlNode GetXmlNodeWithSub(XmlDocument xmlDoc, string nodeName, string xml)
		{
			XmlNode node = xmlDoc.CreateElement(string.Empty, nodeName, string.Empty);
			node.InnerXml = xml;

			return node;
		}

		/// <summary>
		/// Get xml node's inner text
		/// </summary>
		/// <param name="node"></param>
		/// <returns></returns>
		public static string GetXmlNodeInnterText(XmlNode node)
		{
			return GetXmlDecode(node.InnerText);
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Xml Util
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Xml Util

		/// <summary>
		/// Load DataSet with xml
		/// </summary>
		public static void LoadDataSetByXml(DataSet dataSet, string xml)
		{
			try
			{
				if (dataSet == null)
				{
					dataSet = new DataSet();
				}

				using (System.IO.StringReader sr = new System.IO.StringReader(xml))
				{
					dataSet.ReadXml(sr);
				}
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Get DataSet with xml
		/// </summary>
		public static DataSet GetDataSetFromXml(string xml)
		{
			DataSet dataSet = new DataSet();

			try
			{
				using (System.IO.StringReader sr = new System.IO.StringReader(xml))
				{
					dataSet.ReadXml(sr);
				}
			}
			catch
			{
				throw;
			}

			return dataSet;
		}

		/// <summary>
		/// Get Xml from DataSet
		/// </summary>
		public static string GetXmlFromDataSet(DataSet dataSet)
		{
			try
			{
				System.IO.StringWriter stringWriter = new System.IO.StringWriter();
				dataSet.WriteXml(stringWriter, XmlWriteMode.IgnoreSchema);
				return stringWriter.ToString();
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Get xml document string
		/// </summary>
		/// <param name="xmlDoc"></param>
		/// <returns></returns>
		public static string GetXmlString(XmlDocument xmlDoc)
		{
			string strXml = string.Empty;

			// Temporary indented xml string stream
			using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
			{
				// Make XmlWriter for indented xml
				XmlWriterSettings xmlSet = new XmlWriterSettings();
				xmlSet.OmitXmlDeclaration = true;
				xmlSet.Indent = true;
				xmlSet.IndentChars = "  ";
				xmlSet.Encoding = new UTF8Encoding(false);

				XmlWriter xmlWriter = XmlWriter.Create(ms, xmlSet);

				// Save
				xmlDoc.Save(xmlWriter);
				xmlWriter.Close();

				// Get string
				strXml = Encoding.UTF8.GetString(ms.ToArray());
			}

			return strXml;
		}

		/// <summary>
		/// XML escape문자열을 위한 인코딩
		/// </summary>
		/// <param name="val"></param>
		/// <returns></returns>
		public static string GetXmlEncode(string val)
		{
			if (string.IsNullOrEmpty(val)) return val;

			//return !System.Security.SecurityElement.IsValidText(val) ? System.Security.SecurityElement.Escape(val) : val;

			StringBuilder sb = null;
			string xml = null;

			try
			{
				sb = new StringBuilder();
				sb.Append(val);
				sb.Replace("&", "&amp;");
				sb.Replace(">", "&gt;");
				sb.Replace("<", "&lt;");
				sb.Replace("\"", "&quot;");
				sb.Replace("\'", "&apos;");
				sb.Replace(";amp;", ";"); // prevent & repeat conversion

				xml = sb.ToString();
			}
			catch
			{
				xml = val;
			}
			finally
			{
				sb = null;
			}

			return xml;
		}

		/// <summary>
		/// XML 인코딩된 문자열을 디코딩
		/// </summary>
		/// <param name="val"></param>
		/// <returns></returns>
		public static string GetXmlDecode(string val)
		{
			if (string.IsNullOrEmpty(val)) return val;

			StringBuilder sb = null;
			string xml = null;

			try
			{
				sb = new StringBuilder();
				sb.Append(val);
				sb.Replace("&amp;", "&");
				sb.Replace("&quot;", "\"");
				sb.Replace("&apos;", "\'");
				sb.Replace("&gt;", ">");
				sb.Replace("&lt;", "<");

				xml = sb.ToString();
			}
			catch
			{
				xml = val;
			}
			finally
			{
				sb = null;
			}

			return xml;
		}

		/// <summary>
		/// XML 인코딩된 문자열을 디코딩 (&amp;)
		/// </summary>
		/// <param name="val"></param>
		/// <returns></returns>
		public static string GetXmlDecodeAmp(string val)
		{
			if (string.IsNullOrEmpty(val)) return val;

			StringBuilder sb = null;
			string xml = null;

			try
			{
				sb = new StringBuilder();
				sb.Append(val);
				sb.Replace("&amp;", "&");

				xml = sb.ToString();
			}
			catch
			{
				xml = val;
			}
			finally
			{
				sb = null;
			}

			return xml;
		}

		#endregion
	}
}
