﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;

namespace knulf.Utility
{
	/// <summary>
	/// Utilities of Type
	/// </summary>
	public class TypeUtil
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Convert
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Convert

		/// <summary>
		/// Convert DataTable to object list
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="table"></param>
		/// <returns></returns>
		public static List<T> ConvertToList<T>(DataTable table) where T : class, new()
		{
			try
			{
				if (table.Rows.Count > 0)
				{
					List<T> list = new List<T>();
					foreach (var row in table.AsEnumerable())
					{
						T obj = new T();
						foreach (var property in obj.GetType().GetProperties())
						{
							try
							{
								if (row.Table.Columns.Contains(property.Name) && row[property.Name] != DBNull.Value)
								{
									PropertyInfo propertyInfo = obj.GetType().GetProperty(property.Name);
									if (row[property.Name].GetType() == typeof(System.Guid))
										propertyInfo.SetValue(obj, row[property.Name].ToString(), null);
									else
										propertyInfo.SetValue(obj, Convert.ChangeType(row[property.Name], propertyInfo.PropertyType), null);
								}
							}
							catch
							{
								continue;
							}
						}
						list.Add(obj);
					}
					return list;
				}
				else
				{
					return null;
				}
			}
			catch
			{
				return null;
			}
		}

		/// <summary>
		/// Convert DataRow to object
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="row"></param>
		/// <returns></returns>
		public static T ConvertToObject<T>(DataRow row) where T : class, new()
		{
			try
			{
				if (row != null)
				{
					T obj = new T();
					foreach (var property in obj.GetType().GetProperties())
					{
						try
						{
							if (row.Table.Columns.Contains(property.Name) && row[property.Name] != DBNull.Value)
							{
								PropertyInfo propertyInfo = obj.GetType().GetProperty(property.Name);
								if (row[property.Name].GetType() == typeof(System.Guid))
									propertyInfo.SetValue(obj, row[property.Name].ToString(), null);
								else
									propertyInfo.SetValue(obj, Convert.ChangeType(row[property.Name], propertyInfo.PropertyType), null);
							}
						}
						catch
						{
							continue;
						}
					}
					return obj;
				}
				else
				{
					return null;
				}
			}
			catch
			{
				return null;
			}
		}

		/// <summary>
		/// Convert DataRow to object
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="data"></param>
		/// <returns></returns>
		public static T ConvertToObject<T>(DbDataReader data) where T : class, new()
		{
			try
			{
				if (data != null)
				{
					T obj = new T();
					foreach (var property in obj.GetType().GetProperties())
					{
						try
						{
							if (HasComlumn(data, property.Name) && data[property.Name] != DBNull.Value)
							{
								PropertyInfo propertyInfo = obj.GetType().GetProperty(property.Name);
								if (data[property.Name].GetType() == typeof(System.Guid))
									propertyInfo.SetValue(obj, data[property.Name].ToString(), null);
								else
									propertyInfo.SetValue(obj, Convert.ChangeType(data[property.Name], propertyInfo.PropertyType), null);
							}
						}
						catch
						{
							continue;
						}
					}
					return obj;
				}
				else
				{
					return null;
				}
			}
			catch
			{
				return null;
			}
		}

		public static bool HasComlumn(DbDataReader data, string columnName)
		{
			bool result = false;

			for (int i = 0; i < data.FieldCount; i++)
			{
				if (data.GetName(i).Equals(columnName))
				{
					return true;
				}
			}

			return result;
		}

		/// <summary>
		/// Get version object by string
		/// </summary>
		/// <param name="versionstring"></param>
		/// <returns></returns>
		public static Version ConvertToVersion(string versionstring)
		{
			Version version = null;

			try
			{
				if (versionstring == null)
				{
					throw new ArgumentNullException("versionstring");
				}
				else
				{
					version = new Version(versionstring);
				}
			}
			catch
			{
				version = null;
				throw;
			}

			return version;
		}

		/// <summary>
		/// Convert string to integer
		/// </summary>
		public static int ConvertToInt32(string value)
		{
			int result = 0;

			if (string.IsNullOrEmpty(value) == false)
			{
				int.TryParse(value, out result);
			}

			return result;
		}

		/// <summary>
		/// Convert string to integer
		/// </summary>
		public static bool ConvertToInt32(string value, out int result)
		{
			return int.TryParse(value, out result);
		}

		/// <summary>
		/// Test convert string to integer
		/// </summary>
		public static bool ConvertToInt32Test(string value)
		{
			int result = 0;
			return int.TryParse(value, out result);
		}
		
		/// <summary>
		/// Convert to Nullable type
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="s"></param>
		/// <returns></returns>
		public static Nullable<T> ConvertToNullable<T>(string s) where T : struct
		{
			Nullable<T> result = new Nullable<T>();
			try
			{
				if (!string.IsNullOrEmpty(s) && s.Trim().Length > 0)
				{
					TypeConverter conv = TypeDescriptor.GetConverter(typeof(T));
					result = (T)conv.ConvertFrom(s);
				}
			}
			catch { }
			return result;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Enum
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Enum

		/// <summary>
		/// Enum type by name
		/// </summary>
		public static T GetEnumByName<T>(string itemName)
		{
			return (T)Enum.Parse(typeof(T), itemName);
		}

		public static T GetEnumByValue<T>(int itemValue)
		{
			return (T)Enum.GetValues(typeof(T)).GetValue(itemValue);
		}

		public static int GetEnumLength<T>()
		{
			return System.Enum.GetValues(typeof(T)).Length;
		}

		#endregion
	}
}
