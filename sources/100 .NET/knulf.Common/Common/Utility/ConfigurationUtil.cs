﻿using knulf.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace knulf.Utility
{
	/// <summary>
	/// Wrapping class of System.Configuration.ConfigurationManager
	/// </summary>
	public class ConfigurationUtil
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Built-in ConfigurationManager
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Built-in ConfigurationManager

		/// <summary>
		/// Return value of AppSettings.
		/// </summary>
		public static string GetAppSetting(string key)
		{
			string result = string.Empty;

			if (ConfigurationManager.AppSettings[key] != null)
				result = ConfigurationManager.AppSettings[key].ToString();

			return result;
		}

		/// <summary>
		/// Return value of Connectionstrings.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public static string GetConnectionString(string key)
		{
			string result = string.Empty;

			if (ConfigurationManager.ConnectionStrings[key] != null)
				result = ConfigurationManager.ConnectionStrings[key].ToString();

			return result;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// User-defined ConfigurationManager
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region User-defined ConfigurationManager

		/// <summary>
		/// User-defined configuration
		/// </summary>
		public static System.Configuration.Configuration UserConfig { get; set; }

		/// <summary>
		/// Set User-defined configuration
		/// </summary>
		public static void SetUserConfig(string filePath)
		{
			ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
			fileMap.ExeConfigFilename = filePath;

			if (File.Exists(filePath) == false)
			{
				FileUtil.WriteTextFile(filePath, "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<configuration>\n\t<appSettings />\n</configuration>");
			}

			UserConfig = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
		}

		/// <summary>
		/// Save User-defined configuration
		/// </summary>
		public static void SaveUserConfig()
		{
			try
			{
				if (UserConfig != null) UserConfig.Save(ConfigurationSaveMode.Full);
			}
			catch { }
		}

		/// <summary>
		/// Get value from User-defined configuration
		/// </summary>
		public static string GetValue(string key)
		{
			try
			{
				if (UserConfig != null) return UserConfig.AppSettings.Settings[key].Value;
				else return null;
			}
			catch
			{
				return null;
			}
		}

		/// <summary>
		/// Set value to User-defined configuration
		/// </summary>
		public static void SetValue(string key, string value)
		{
			try
			{
				if (UserConfig != null)
				{
					if (UserConfig.AppSettings.Settings.AllKeys.Contains(key))
						UserConfig.AppSettings.Settings[key].Value = value;
					else
						UserConfig.AppSettings.Settings.Add(key, value);
				}
			}
			catch { }
		}

		/// <summary>
		/// Remove value from User-defined configuration
		/// </summary>
		public static void RemoveValue(string key)
		{
			try
			{
				if (UserConfig != null) UserConfig.AppSettings.Settings.Remove(key);
			}
			catch { }
		}

		#endregion
	}
}
