﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace knulf.Utility
{
	/// <summary>
	/// Utilities of text handling
	/// </summary>
	public class TextUtil
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// General text
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region General text

		/// <summary>
		/// Get string that first character is capital.
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public static string GetCapitalizedFirstString(string data)
		{
			if (string.IsNullOrEmpty(data))
				return data;
			else
				return data.Substring(0, 1).ToUpper() + data.Substring(1);
		}

		/// <summary>
		/// If the input object is null, returns the empty string.
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public static string Nvl(string data)
		{
			return Nvl(data, "");
		}

		/// <summary>
		/// If the input object is null, returns default string.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="defaultString"></param>
		/// <returns></returns>
		public static string Nvl(string data, string defaultString)
		{
			return string.IsNullOrEmpty(data.ToString()) ? defaultString : data.ToString();
		}

		/// <summary>
		/// Remove target string in first position and return result string
		/// </summary>
		/// <param name="data"></param>
		/// <param name="target"></param>
		/// <returns></returns>
		public static string RemoveFirstMatch(string data, string target)
		{
			if (data.Length > target.Length)
			{
				if (data.Substring(0, target.Length).Equals(target))
					return data.Substring(target.Length);
				else
					return data;
			}
			else
			{
				return data;
			}
		}

		/// <summary>
		/// Get nul if data is empty string
		/// </summary>
		public static string GetNullString4Empty(string data)
		{
			if (string.IsNullOrEmpty(data))
				return null;
			else
				return data;
		}

		/// <summary>
		/// Get empty string if data is null
		/// </summary>
		public static string GetEmptyString4Null(string data)
		{
			if (string.IsNullOrEmpty(data))
				return string.Empty;
			else
				return data;
		}

		/// <summary>
		/// Get number string that removed non-number string.
		/// </summary>
		public static string GetOnlyNumberString(string data)
		{
			string result = null;

			if (string.IsNullOrEmpty(data) == false)
				result = System.Text.RegularExpressions.Regex.Replace(data, @"[^\d]", string.Empty);

			return result;
		}

		/// <summary>
		/// Remove html tag
		/// </summary>
		public static string RemoveHtml(string source, bool isBrTag = false)
		{
			if (string.IsNullOrEmpty(source) == false)
			{
				source = Regex.Replace(source, "<.*?>|&.*?;", string.Empty);
				StringBuilder sb = new StringBuilder(source);
				if (isBrTag)
				{
					sb.Replace("\n", "<br />\n");
				}

				return sb.ToString();
			}
			else
			{
				return source;
			}
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Base64
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Base64

		/// <summary>
		/// Get Base64 string from string
		/// </summary>
		/// <param name="source"></param>
		/// <returns></returns>
		public static string GetBase64Encode(string source)
		{
			return GetBase64Encode(source, Encoding.UTF8);
		}

		/// <summary>
		/// Get Base64 string from string
		/// </summary>
		/// <param name="source"></param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static string GetBase64Encode(string source, Encoding encoding)
		{
			string returnstring;

			try
			{
				byte[] bytes = encoding.GetBytes(source);
				returnstring = Convert.ToBase64String(bytes);
			}
			catch
			{
				throw;
			}

			return returnstring;
		}

		/// <summary>
		/// Get Base64 string from byte array
		/// </summary>
		/// <param name="bytes"></param>
		/// <returns></returns>
		public static string GetBase64Encode(byte[] bytes)
		{
			string returnstring;

			try
			{
				returnstring = Convert.ToBase64String(bytes);
			}
			catch
			{
				throw;
			}

			return returnstring;
		}

		/// <summary>
		/// Get string from Base64 string
		/// </summary>
		/// <param name="base64string"></param>
		/// <returns></returns>
		public static string GetBase64Decode(string base64string)
		{
			return GetBase64Decode(base64string, Encoding.UTF8);
		}

		/// <summary>
		/// Get string from Base64 string
		/// </summary>
		/// <param name="base64string"></param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static string GetBase64Decode(string base64string, Encoding encoding)
		{
			string returnstring = null;
			byte[] bytes;

			try
			{
				bytes = Convert.FromBase64String(base64string);
				returnstring = encoding.GetString(bytes);
			}
			catch
			{
				throw;
			}

			return returnstring;
		}

		/// <summary>
		/// Get byte array from Base64 string
		/// </summary>
		/// <param name="base64string"></param>
		/// <returns></returns>
		public static byte[] GetBase64DecodeBytes(string base64string)
		{
			byte[] returnBytes;

			try
			{
				returnBytes = Convert.FromBase64String(base64string);
			}
			catch
			{
				throw;
			}

			return returnBytes;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Alphabet number
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Alphabet number

		public static readonly string AlphabetData = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		public static readonly int AlphabetBase = AlphabetData.Length;

		/// <summary>
		/// Encode integer to alphabet string
		/// </summary>
		public static string AlphabetEncode(int i)
		{
			if (i == 0) return AlphabetData[0].ToString();

			var s = string.Empty;

			while (i > 0)
			{
				s += AlphabetData[i % AlphabetBase];
				i = i / AlphabetBase;
			}

			return string.Join(string.Empty, s.Reverse());
		}

		/// <summary>
		/// Decode alphabet string to integer
		/// </summary>
		public static int AlphabetDecode(string s)
		{
			var i = 0;

			foreach (var c in s)
			{
				i = (i * AlphabetBase) + AlphabetData.IndexOf(c);
			}

			return i;
		}

		#endregion
	}
}
