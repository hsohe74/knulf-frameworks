﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Utility
{
	/// <summary>
	/// Utilities of compression
	/// </summary>
	public class CompressionUtil
	{

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// GZip
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region GZip

		/// <summary>
		/// Get compressed string by GZip with Base64 format
		/// </summary>
		/// <param name="plainString"></param>
		/// <returns></returns>
		public static string CompressGZipString(string plainString)
		{
			return CompressGZipString(plainString, Encoding.UTF8);
		}

		/// <summary>
		/// Get compressed string by GZip with Base64 format
		/// </summary>
		/// <param name="plainString"></param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static string CompressGZipString(string plainString, Encoding encoding)
		{
			string compressedString = null;
			byte[] sourceData = null;
			byte[] compressedData = null;

			try
			{
				sourceData = encoding.GetBytes(plainString);
				compressedData = CompressGZipData(sourceData);
				if (compressedData != null)
				{
					compressedString = TextUtil.GetBase64Encode(compressedData);
				}
			}
			catch
			{
				throw;
			}

			return compressedString;
		}

		/// <summary>
		/// Get compress GZip
		/// </summary>
		/// <param name="sourceData"></param>
		/// <returns></returns>
		public static byte[] CompressGZipData(byte[] sourceData)
		{
			byte[] compressedData = null;

			try
			{
				using (System.IO.MemoryStream compressStream = new System.IO.MemoryStream())
				{
					if (sourceData.Length > 0)
					{
						using (System.IO.Compression.GZipStream gZips
							= new System.IO.Compression.GZipStream(compressStream, System.IO.Compression.CompressionMode.Compress))
						{
							gZips.Write(sourceData, 0, sourceData.Length);
						}
						compressedData = compressStream.ToArray();
					}
				}
			}
			catch
			{
				throw;
			}

			return compressedData;
		}

		/// <summary>
		/// Get decompressed string by GZip data with Base64 format
		/// </summary>
		/// <param name="compressedString"></param>
		/// <returns></returns>
		public static string DecompressGZipString(string compressedString)
		{
			return DecompressGZipString(compressedString, Encoding.UTF8);
		}

		/// <summary>
		/// Get decompressed string by GZip data with Base64 format
		/// </summary>
		/// <param name="compressedString"></param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static string DecompressGZipString(string compressedString, Encoding encoding)
		{
			string plainString = null;
			byte[] compressedData = null;
			byte[] decompressedData = null;

			try
			{
				compressedData = TextUtil.GetBase64DecodeBytes(compressedString);
				decompressedData = DecompressGZipData(compressedData);
				if (decompressedData != null)
				{
					plainString = encoding.GetString(DecompressGZipData(compressedData));
				}
			}
			catch
			{
				throw;
			}

			return plainString;
		}

		/// <summary>
		/// Get data from compressed GZip data
		/// </summary>
		/// <param name="compressedString"></param>
		/// <returns></returns>
		public static byte[] DecompressGZipData(byte[] compressedData)
		{
			byte[] decompressedData = null;

			try
			{
				using (System.IO.MemoryStream compressedStream = new System.IO.MemoryStream(compressedData))
				{
					using (System.IO.Compression.GZipStream gZips
						= new System.IO.Compression.GZipStream(compressedStream, System.IO.Compression.CompressionMode.Decompress))
					{
						using (System.IO.MemoryStream decompressedStream = new System.IO.MemoryStream())
						{
							gZips.CopyTo(decompressedStream);
							decompressedData = new byte[decompressedStream.Length];
							decompressedData = decompressedStream.ToArray();
						}
					}
				}
			}
			catch
			{
				throw;
			}

			return decompressedData;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Zip
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Zip

		/// <summary>
		/// Create zip file from source file
		/// </summary>
		/// <param name="sourceFileName"></param>
		/// <param name="zipFilePath"></param>
		public static void CreateZipFromFile(
			string sourceFilePath,
			string zipFilePath
			)
		{
			try
			{
				CreateZipFromFile(new string[] { sourceFilePath }, zipFilePath);
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Create zip file from source files
		/// </summary>
		/// <param name="sourceFileNames"></param>
		/// <param name="zipFilePath"></param>
		public static void CreateZipFromFile(
			string[] sourceFilePaths,
			string zipFilePath
			)
		{
			try
			{
				using (FileStream fileStream = new FileStream(zipFilePath, FileMode.Create))
				{
					using (ZipArchive zipArchive = new ZipArchive(fileStream, ZipArchiveMode.Create))
					{
						foreach (string sourceFilePath in sourceFilePaths)
						{
							zipArchive.CreateEntryFromFile(
								sourceFilePath,
								FileUtil.GetFileName(sourceFilePath));
						}
					}
				}
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Create zip file from source directory
		/// </summary>
		/// <param name="sourceDirectoryPath"></param>
		/// <param name="zipFilePath"></param>
		public static void CreateZipFromDirectory(
			string sourceDirectoryPath,
			string zipFilePath)
		{
			try
			{
				CreateZipFromDirectory(
					sourceDirectoryPath,
					zipFilePath,
					CompressionLevel.Optimal,
					true,
					Encoding.UTF8);
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Create zip file from source directory
		/// </summary>
		/// <param name="sourceDirectoryPath"></param>
		/// <param name="zipFilePath"></param>
		/// <param name="compressionLevel"></param>
		/// <param name="isIncludeBaseDirectory"></param>
		public static void CreateZipFromDirectory(
			string sourceDirectoryPath,
			string zipFilePath,
			CompressionLevel compressionLevel,
			bool isIncludeBaseDirectory)
		{
			try
			{
				CreateZipFromDirectory(
					sourceDirectoryPath,
					zipFilePath,
					compressionLevel,
					isIncludeBaseDirectory,
					Encoding.UTF8);
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Create zip file from source directory
		/// </summary>
		/// <param name="sourceDirectoryPath"></param>
		/// <param name="zipFilePath"></param>
		/// <param name="compressionLevel"></param>
		/// <param name="isIncludeBaseDirectory"></param>
		/// <param name="entryNameEncoding"></param>
		public static void CreateZipFromDirectory(
			string sourceDirectoryPath,
			string zipFilePath,
			CompressionLevel compressionLevel,
			bool isIncludeBaseDirectory,
			Encoding entryNameEncoding)
		{
			try
			{
				ZipFile.CreateFromDirectory(
					sourceDirectoryPath,
					zipFilePath,
					compressionLevel,
					isIncludeBaseDirectory,
					entryNameEncoding);
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Extract zip file to destination directory
		/// </summary>
		/// <param name="zipFilePath"></param>
		public static void ExtractZipToDirectory(
			string zipFilePath)
		{
			try
			{
				ExtractZipToDirectory(
					zipFilePath,
					FileUtil.GetDirectoryPath(zipFilePath),
					Encoding.UTF8);
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Extract zip file to destination directory
		/// </summary>
		/// <param name="zipFilePath"></param>
		/// <param name="destinationDirectoryPath"></param>
		public static void ExtractZipToDirectory(
			string zipFilePath,
			string destinationDirectoryPath)
		{
			try
			{
				ExtractZipToDirectory(
					zipFilePath,
					destinationDirectoryPath,
					Encoding.UTF8);
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Extract zip file to destination directory
		/// </summary>
		/// <param name="zipFilePath"></param>
		/// <param name="destinationDirectoryPath"></param>
		/// <param name="entryNameEncoding"></param>
		public static void ExtractZipToDirectory(
			string zipFilePath,
			string destinationDirectoryPath,
			Encoding entryNameEncoding)
		{
			try
			{
				if (Directory.Exists(destinationDirectoryPath) == false)
					Directory.CreateDirectory(destinationDirectoryPath);

				using (var zipArchive = ZipFile.OpenRead(zipFilePath))
				{
					foreach (var entry in zipArchive.Entries)
					{
						string fullPath = Path.Combine(destinationDirectoryPath, entry.FullName);
						if (string.IsNullOrEmpty(entry.Name))
						{
							Directory.CreateDirectory(fullPath);
						}
						else
						{
							entry.ExtractToFile(Path.Combine(destinationDirectoryPath, entry.FullName), true);
						}
					}
				}
			}
			catch
			{
				throw;
			}
		}

		#endregion
	}
}
