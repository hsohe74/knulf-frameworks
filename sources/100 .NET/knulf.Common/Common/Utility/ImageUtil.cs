﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Utility
{
	/// <summary>
	/// Utilities of image handling
	/// </summary>
	public class ImageUtil
	{
		public static string[] _WEB_IMAGE_EXTENSION = new string[] { "BMP", "GIF", "PNG", "JPEG", "JPG", "JPE" };

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Image information
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Image information

		/// <summary>
		/// Get image size
		/// </summary>
		public static Size GetImageSize(string filePath)
		{
			Size size = new Size(0, 0);

			using (var bmp = new Bitmap(filePath))
			{
				size.Width = bmp.Width;
				size.Height = bmp.Height;
			}

			return size;
		}

		/// <summary>
		/// Get image width
		/// </summary>
		public static int GetImageWidth(string filePath)
		{
			Size size = new Size(0, 0);

			using (var bmp = new Bitmap(filePath))
			{
				size.Width = bmp.Width;
				size.Height = bmp.Height;
			}

			return size.Width;
		}

		/// <summary>
		/// Get image height
		/// </summary>
		public static int GetImageHeight(string filePath)
		{
			Size size = new Size(0, 0);

			using (var bmp = new Bitmap(filePath))
			{
				size.Width = bmp.Width;
				size.Height = bmp.Height;
			}

			return size.Height;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Get Image
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Get Image

		/// <summary>
		/// Get image from local
		/// </summary>
		public static Image GetImageFromLocal(string filePath)
		{
			Image image = new Bitmap(filePath);
			return image;
		}

		/// <summary>
		/// Get image from web
		/// </summary>
		public static Image GetImageFromWeb(string imageUri)
		{
			try
			{
				var imageData = Net.WebClientHelper.GetRequestStream(imageUri);
				var image = Image.FromStream(imageData);
				return image;
			}
			catch
			{
				return null;
			}
		}

		/// <summary>
		/// Whether file extension is web image.
		/// </summary>
		public static bool CheckIsWebImage(string filePath)
		{
			string fileExt = FileUtil.GetFileExtension(filePath).ToUpper();

			if (string.IsNullOrEmpty(fileExt) == false && Array.IndexOf(_WEB_IMAGE_EXTENSION, fileExt) > -1)
				return true;
			else
				return false;
		}

		/// <summary>
		/// Whether file extension is web image.
		/// </summary>
		public static bool CheckIsWebImageUrl(string imageUrl)
		{
			string fileExt = imageUrl.Substring(imageUrl.LastIndexOf('.') + 1).ToUpper();

			if (string.IsNullOrEmpty(fileExt) == false && Array.IndexOf(_WEB_IMAGE_EXTENSION, fileExt) > -1)
				return true;
			else
				return false;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Image handling
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Image handling

		/// <summary>
		/// Convert TIFF to JPG
		/// </summary>
		public static string[] ConvertTiffToJpeg(string fileName)
		{
			using (Image imageFile = Image.FromFile(fileName))
			{
				FrameDimension frameDimensions = new FrameDimension(imageFile.FrameDimensionsList[0]);

				// Gets the number of pages from the tiff image (if multipage) 
				int frameNum = imageFile.GetFrameCount(frameDimensions);
				string[] jpegPaths = new string[frameNum];

				for (int frame = 0; frame < frameNum; frame++)
				{
					// Selects one frame at a time and save as jpeg. 
					imageFile.SelectActiveFrame(frameDimensions, frame);
					using (Bitmap bmp = new Bitmap(imageFile))
					{
						jpegPaths[frame] = String.Format("{0}\\{1}-{2}.jpg",
							Path.GetDirectoryName(fileName),
							Path.GetFileNameWithoutExtension(fileName),
							frame);
						bmp.Save(jpegPaths[frame], ImageFormat.Jpeg);
					}
				}

				return jpegPaths;
			}
		}

		/// <summary>
		/// Convert TIFF to JPG
		/// </summary>
		public static string ConvertTiffToJpegSingle(string filePath)
		{
			Image imageFile = Image.FromFile(filePath);

			string jpegPath = Path.Combine(
				Path.GetDirectoryName(filePath),
				Path.GetFileNameWithoutExtension(filePath) + ".jpg");
			imageFile.Save(jpegPath, ImageFormat.Jpeg);

			return jpegPath;
		}

		/// <summary>
		/// Create thumnamil
		/// </summary>
		public static void CreateThumbnail(string filePath, string thumbSuffix)
		{
			CreateThumbnail(filePath, thumbSuffix, null);
		}

		/// <summary>
		/// Create thumnamil
		/// </summary>
		public static void CreateThumbnail(string filePath, string thumbSuffix, int thumbWidth, int thumbHeight)
		{
			CreateThumbnail(filePath, thumbSuffix, null, true, thumbWidth, thumbHeight);
		}

		/// <summary>
		/// Create thumnamil
		/// </summary>
		/// <param name="filePath">원본 파일</param>
		/// <param name="thumbSuffix">썸네일 꼬리 이름 (확장자 바로 앞에 붙임)</param>
		/// <param name="destinationPath">파일 저장위치 (null일 경우 같은 위치)</param>
		/// <param name="isCover">좌우를 짤라내고 표시하는 Cover 형태일 경우</param>
		/// <param name="thumbWidth">원하는 썸내일 사이즈 폭</param>
		/// <param name="thumbHeight">원하는 썸내일 사이즈 높이</param>
		public static void CreateThumbnail(
			string filePath, string thumbSuffix,
			string destinationPath,
			bool isCover = true,
			int thumbWidth = 240, int thumbHeight = 240)
		{
			bool isUseOriginal = false;
			Image sourceImage;
			Image thumbImage;
			double wantWidth = (double)thumbWidth;
			double wantHeight = (double)thumbHeight;

			if (string.IsNullOrEmpty(filePath))
			{
				throw new Exception("image file path was not allow empty.");
			}

			try
			{
				if (string.IsNullOrEmpty(destinationPath))
				{
					destinationPath = FileUtil.GetDirectoryPath(filePath);
				}

				string thumbPath = Path.Combine(
					destinationPath,
					FileUtil.GetFileNameWithoutExtension(filePath) + thumbSuffix + "." + FileUtil.GetFileExtension(filePath));

				sourceImage = Image.FromFile(filePath);
				double imageWidth = sourceImage.Width;
				double imageHeight = sourceImage.Height;
				double resetWidth = wantWidth;
				double resetHeight = wantHeight;

				if (isCover)
				{
					if (wantWidth > imageWidth || wantHeight > imageHeight)
					{
						isUseOriginal = true;
					}
					else
					{
						if ((imageWidth / wantWidth) > (imageHeight / wantHeight))
						{
							resetWidth = wantHeight * (imageWidth / imageHeight);
							resetHeight = wantHeight;
						}
						else
						{
							resetWidth = wantWidth;
							resetHeight = wantWidth * (imageHeight / imageWidth);
						}
					}
				}
				else
				{
					if (wantWidth > imageWidth && wantHeight > imageHeight)
					{
						isUseOriginal = true;
					}
					else if((imageWidth / wantWidth) > (imageHeight / wantHeight))
					{
						resetWidth = wantWidth;
						resetHeight = wantWidth * (imageHeight / imageWidth);
					}
					else
					{
						resetWidth = wantHeight * (imageWidth / imageHeight);
						resetHeight = wantHeight;
					}
				}

				if (isUseOriginal)
				{
					FileUtil.CopyFile(filePath, thumbPath, true);
				}
				else
				{
					thumbImage = GetResizedImage(sourceImage, (int)resetWidth, (int)resetHeight);
					thumbImage.Save(thumbPath);
				}

				sourceImage.Dispose();
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Resize image
		/// </summary>
		public static void ResizeImage(string filePath, string resizeSuffix)
		{
			ResizeImage(filePath, resizeSuffix, null);
		}

		/// <summary>
		/// Resize image
		/// </summary>
		public static void ResizeImage(string filePath, string resizeSuffix, int resizeWidth, int resizeHeight)
		{
			ResizeImage(filePath, resizeSuffix, null, resizeWidth, resizeHeight);
		}

		/// <summary>
		/// Resize image
		/// </summary>
		/// <param name="filePath">원본 파일</param>
		/// <param name="resizeSuffix">썸네일 꼬리 이름 (확장자 바로 앞에 붙임)</param>
		/// <param name="destinationPath">파일 저장위치 (null일 경우 같은 위치)</param>
		/// <param name="resizeWidth">원하는 사이즈 폭</param>
		/// <param name="resizeHeight">원하는 사이즈 높이</param>
		public static void ResizeImage(
			string filePath, string resizeSuffix,
			string destinationPath,
			int resizeWidth = 240, int resizeHeight = 240)
		{
			bool isUseOriginal = false;
			Image sourceImage;
			Image resizeImage;
			double wantWidth = (double)resizeWidth;
			double wantHeight = (double)resizeHeight;

			if (string.IsNullOrEmpty(filePath))
			{
				throw new Exception("image file path was not allow empty.");
			}

			try
			{
				if (string.IsNullOrEmpty(destinationPath))
				{
					destinationPath = FileUtil.GetDirectoryPath(filePath);
				}

				string resizedPath = Path.Combine(
					destinationPath,
					FileUtil.GetFileNameWithoutExtension(filePath) + resizeSuffix + "." + FileUtil.GetFileExtension(filePath));

				sourceImage = Image.FromFile(filePath);
				double imageWidth = sourceImage.Width;
				double imageHeight = sourceImage.Height;
				double resetWidth = wantWidth;
				double resetHeight = wantHeight;

				if (wantWidth > imageWidth && wantHeight > imageHeight)
				{
					isUseOriginal = true;
				}
				else if ((imageWidth / wantWidth) > (imageHeight / wantHeight))
				{
					resetWidth = wantWidth;
					resetHeight = wantWidth * (imageHeight / imageWidth);
				}
				else
				{
					resetWidth = wantHeight * (imageWidth / imageHeight);
					resetHeight = wantHeight;
				}

				if (isUseOriginal)
				{
					FileUtil.CopyFile(filePath, resizedPath, true);
				}
				else
				{
					resizeImage = GetResizedImage(sourceImage, (int)resetWidth, (int)resetHeight);
					resizeImage.Save(resizedPath);
				}

				sourceImage.Dispose();

				// 원본 삭제후 Resize 이미지 교체
				FileUtil.DeleteFile(filePath);
				FileUtil.MoveFile(resizedPath, filePath);
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Get resized image
		/// </summary>
		public static Image GetResizedImage(Image source, int width)
		{
			int height = width * source.Height / source.Width;
			return GetResizedImage(source, width, height);
		}

		/// <summary>
		/// Get resized image
		/// </summary>
		public static Image GetResizedImage(Image source, int width, int height)
		{
			var resizedImage = new Bitmap(source, new Size(width, height));
			return resizedImage;
		}

		#endregion
	}
}
