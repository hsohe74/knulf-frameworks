﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace knulf.Utility
{
	/// <summary>
	/// Utilities of process
	/// </summary>
	public class ProcessUtil
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Handling process
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Handling process

		/// <summary>
		/// Kill process by name
		/// </summary>
		/// <param name="processName"></param>
		/// <returns></returns>
		public static bool KillProcess(string processName)
		{
			bool result = true;

			try
			{
				// Name check
				if (processName.Length > 4 && processName.Substring(processName.Length - 4, 4).Equals(".exe"))
				{
					processName = processName.Substring(0, processName.Length - 4);
				}

				// Kill process
				Process[] process = Process.GetProcessesByName(processName);
				if (process.Length > 0)
				{
					for (int i = 0; i < process.Length; i++)
					{
						process[i].Kill();
					}
				}
			}
			catch
			{
				result = false;
			}

			return result;
		}

		/// <summary>
		/// Check whether process running
		/// </summary>
		/// <param name="processName"></param>
		/// <returns></returns>
		public static bool IsProcessRunning(string processName)
		{
			bool result = false;

			try
			{
				// Name check
				if (processName.Length > 4 && processName.Substring(processName.Length - 4, 4).Equals(".exe"))
				{
					processName = processName.Substring(0, processName.Length - 4);
				}

				// Check process
				Process[] process = Process.GetProcessesByName(processName);
				if (process.Length > 0)
					return true;
			}
			catch
			{
				result = false;
			}

			return result;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Execute command line
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Execute command line

		/// <summary>
		/// Execute command line
		/// </summary>
		/// <param name="commandLine"></param>
		public static void ExecuteProcessStart(string commandLine)
		{
			Process.Start(commandLine);
		}

		/// <summary>
		/// Execute command line
		/// </summary>
		/// <param name="commandLine"></param>
		/// <returns></returns>
		public static string ExecuteCommand(string commandLine, int sleepTime = 1000)
		{
			string reuslt = string.Empty;

			Process cmdProcess = new Process();

			try
			{
				var processStartInfo = new ProcessStartInfo();
				processStartInfo.FileName = "cmd";
				processStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
				processStartInfo.CreateNoWindow = true;
				processStartInfo.RedirectStandardInput = true;
				processStartInfo.RedirectStandardOutput = true;
				processStartInfo.UseShellExecute = false;

				cmdProcess.StartInfo = processStartInfo;
				cmdProcess.Start();

				cmdProcess.StandardInput.WriteLine(commandLine);
				cmdProcess.StandardInput.Flush();
				cmdProcess.StandardInput.Close();

				reuslt = cmdProcess.StandardOutput.ReadToEnd();

				System.Threading.Thread.Sleep(sleepTime);

			}
			catch
			{
				throw;
			}
			finally
			{
				if (cmdProcess != null) cmdProcess.Close();
			}

			return reuslt;
		}

		#endregion
	}
}
