﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Utility
{
	/// <summary>
	/// Utilities of assembliy information and product path
	/// </summary>
	public class ProductUtil
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Assembly information
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Assembly information

		/// <summary>
		/// Assembly
		/// </summary>
		public static Assembly MainAssembly
		{
			get { return Assembly.GetEntryAssembly(); }
		}

		/// <summary>
		/// AssemblyName
		/// </summary>
		public static AssemblyName MainAssemblyName
		{
			get { return MainAssembly.GetName(); }
		}

		/// <summary>
		/// Product name by assembly
		/// </summary>
		public static string Title
		{
			get
			{
				Attribute[] attributes = (Attribute[])MainAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
				if (attributes != null && attributes.Length > 0)
					return ((AssemblyTitleAttribute)attributes[0]).Title;
				else
					return null;
			}
		}

		/// <summary>
		/// Product name by assembly
		/// </summary>
		public static string Description
		{
			get
			{
				Attribute[] attributes = (Attribute[])MainAssembly.GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
				if (attributes != null && attributes.Length > 0)
					return ((AssemblyDescriptionAttribute)attributes[0]).Description;
				else
					return null;
			}
		}

		/// <summary>
		/// Company
		/// </summary>
		public static string Company
		{
			get
			{
				Attribute[] attributes = (Attribute[])MainAssembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
				if (attributes != null && attributes.Length > 0)
					return ((AssemblyCompanyAttribute)attributes[0]).Company;
				else
					return null;
			}
		}

		/// <summary>
		/// Product
		/// </summary>
		public static string Product
		{
			get
			{
				Attribute[] attributes = (Attribute[])MainAssembly.GetCustomAttributes(typeof(AssemblyProductAttribute), false);
				if (attributes != null && attributes.Length > 0)
					return ((AssemblyProductAttribute)attributes[0]).Product;
				else
					return null;
			}
		}

		/// <summary>
		/// Copyright
		/// </summary>
		public static string Copyright
		{
			get
			{
				Attribute[] attributes = (Attribute[])MainAssembly.GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
				if (attributes != null && attributes.Length > 0)
					return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
				else
					return null;
			}
		}

		/// <summary>
		/// Product versioin by assembly
		/// </summary>
		public static Version Version
		{
			get
			{
				return MainAssembly.GetName().Version;
			}
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Product path
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Product path

		/// <summary>
		/// Product versioin by assembly
		/// </summary>
		public static string ProductVersion
		{
			get
			{
				return MainAssemblyName.Version.Major.ToString()
					+ "." + MainAssemblyName.Version.Minor.ToString()
					+ "." + MainAssemblyName.Version.Build.ToString()
					+ "." + MainAssemblyName.Version.Revision.ToString();
			}
		}

		/// <summary>
		/// Product code (English)
		/// </summary>
		public static string ProductCode { get; set; }

		/// <summary>
		/// Get product name with title, version
		/// </summary>
		public static string ProductTitle
		{
			get { return Title + " " + ProductVersion; }
		}

		/// <summary>
		/// Get exucting assembly path
		/// </summary>
		public static string ExecutingAssemblyPath
		{
			get { return Assembly.GetExecutingAssembly().Location; }
		}

		/// <summary>
		/// Application main path
		/// </summary>
		public static string ExecutingPath
		{
			get { return FileUtil.RemoveDirectoryLastSeparator(AppDomain.CurrentDomain.BaseDirectory); }
		}

		/// <summary>
		/// Application data path for user
		/// </summary>
		public static string ApplicationDataPath
		{
			get
			{
				if (string.IsNullOrEmpty(CompanyCode) == false && string.IsNullOrEmpty(ProductCode) == false)
					return Path.Combine(
						Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
						CompanyCode,
						ProductCode);
				else
					return Path.Combine(
						Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
						Company,
						Product);
			}
		}

		/// <summary>
		/// Company code (English)
		/// </summary>
		public static string CompanyCode { get; set; }

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Deployment
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Deployment

		/// <summary>
		/// Get whether deployed throw network
		/// </summary>
		public static bool IsNetworkDeployed
		{
			get { return System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed; }
		}

		#endregion
	}
}
