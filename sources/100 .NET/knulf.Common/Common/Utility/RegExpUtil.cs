﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace knulf.Utility
{
	/// <summary>
	/// Utilities of Regular expression
	/// </summary>
	public class RegExpUtil
	{
		private static Dictionary<string, string> patternList;

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Constructer & Initialize

		static RegExpUtil()
		{
			patternList = new Dictionary<string, string>();
			patternList.Add("Guid", @"^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$");
			patternList.Add("Numeric", @"^\d+$");
			patternList.Add("NumericReal", @"^(\d+|\d{1,3}(,\d{3})*)(\.\d+)?$");
			patternList.Add("Email", @"^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$");
			patternList.Add("Alphabet", @"^[a-zA-Z]+$");
			patternList.Add("Korean", @"^[가-힣]+$");
			patternList.Add("Special", @"^[~`!@#$%^&*()_\-+={}[\]|\\;:'""<>,.?/]+$");
			patternList.Add("Text", @"^[0-9a-zA-Z]+$");
			patternList.Add("TextKorean", @"^[0-9a-zA-Z가-힣]+$");
			patternList.Add("FullText", @"^[0-9a-zA-Z~`!@#$%^&*()_\-+={}[\]|\\;:'""<>,.?/]+$");
			patternList.Add("FullTextKorean", @"^[0-9a-zA-Z가-힣~`!@#$%^&*()_\-+={}[\]|\\;:'""<>,.?/]+$");
			patternList.Add("LoginId", @"^[a-zA-Z0-9_-]+$");
			patternList.Add("EnglishName", @"^[A-Za-z0-9\s&_\.\-\\]+$");
			patternList.Add("KoreanName", @"^[ㄱ-ㅎ|ㅏ-ㅣ|가-힣|0-9\s&_\.\-\\]+$");
			patternList.Add("ShortDate", @"^\d{4}-?(0[1-9]|1[012])-?(0[1-9]|[12][0-9]|3[01])$");
			patternList.Add("DateTime", @"^\d{4}-?(0[1-9]|1[012])-?(0[1-9]|[12][0-9]|3[01])\s?(0[1-9]|1[0-9]|2[0-3]):?(0[0-9]|[1-5][0-9])(:?(0[0-9]|[1-5][0-9]))?$");
			patternList.Add("Telephone", @"^((0[1-9]\d?)?-?(\d{3,4})-?(\d{4}))$");
			patternList.Add("TelephoneInternational", @"^((\d{1,5})-(\d{1,3})-(\d{1,4})-(\d{1,4}))$");
			patternList.Add("Cellphone", @"^((01\d)-?(\d{3,4})-?(\d{4}))$");
			patternList.Add("CellphoneWithoutHyphen", @"^((01\d)(\d{3,4})(\d{4}))$");
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Properties
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Properties

		public static Dictionary<string, string> PatternList
		{
			get { return patternList; }
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Regular expression
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Regular expression

		/// <summary>
		/// Check pattern match
		/// </summary>
		public static bool IsMatch(string data, string pattern)
		{
			if (Regex.IsMatch(data, pattern))
				return true;
			else
				return false;
		}

		/// <summary>
		/// Get replaced text by pattern
		/// </summary>
		/// <param name="source"></param>
		/// <param name="pattern"></param>
		/// <param name="replace"></param>
		/// <returns></returns>
		public static string Replace(string source, string pattern, string replace)
		{
			string result = null;

			try
			{
				result = Regex.Replace(source, pattern, replace);
			}
			catch
			{
				result = null;
			}

			return result;
		}

		/// <summary>
		/// Get replaced text by pattern
		/// </summary>
		/// <param name="source"></param>
		/// <param name="pattern"></param>
		/// <param name="replace"></param>
		/// <param name="regexOption"></param>
		/// <returns></returns>
		public static string Replace(string source, string pattern, string replace, RegexOptions regexOption)
		{
			string result = null;

			try
			{
				result = Regex.Replace(source, pattern, replace, regexOption);
			}
			catch
			{
				result = null;
			}

			return result;
		}

		/// <summary>
		/// Get matched list by pattern
		/// </summary>
		/// <param name="source"></param>
		/// <param name="pattern"></param>
		/// <param name="regexOption"></param>
		/// <returns></returns>
		public static List<string> GetMatchList(string source, string pattern, RegexOptions regexOption)
		{
			List<string> result = new List<string>();

			try
			{
				foreach (Match match in Regex.Matches(source, pattern, regexOption))
				{
					result.Add(match.Value);
				}
			}
			catch
			{
				result = null;
			}

			return result;
		}

		public static UrlInfomation GetUrlInfomation(string url)
		{
			UrlInfomation result = new UrlInfomation();
			string pattern = "^(.*:)//([A-Za-z0-9\\-\\.]+)(:[0-9]+)?(.*)$";

			try
			{
				foreach (Match match in Regex.Matches(url, pattern, RegexOptions.IgnoreCase))
				{
					if (match.Groups.Count > 0)
					{
						result.Url = match.Groups[0].ToString();
						result.Protocol = (match.Groups.Count < 2 || string.IsNullOrEmpty(match.Groups[1].ToString()) ? "" : match.Groups[1].ToString().Replace(":", ""));
						result.Host = (match.Groups.Count < 3 || string.IsNullOrEmpty(match.Groups[2].ToString()) ? "" : match.Groups[2].ToString());
						result.Port = (match.Groups.Count < 4 || string.IsNullOrEmpty(match.Groups[3].ToString()) ? "" : match.Groups[3].ToString().Replace(":", ""));
						result.Path = (match.Groups.Count < 5 || string.IsNullOrEmpty(match.Groups[4].ToString()) ? "" : match.Groups[4].ToString());
						result.IsSecureConnection = result.Protocol.Equals("https", StringComparison.CurrentCultureIgnoreCase);
					}
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		public static List<string> GetMatchGroups(string source, string pattern)
		{
			List<string> result = new List<string>();

			try
			{
				foreach (Match match in Regex.Matches(source, pattern, RegexOptions.IgnoreCase))
				{
					if (match.Groups.Count > 0)
					{
						for (int i = 0; i < match.Groups.Count; i++)
						{
							result.Add(match.Groups[i].ToString());
						}
					}
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		#endregion
	}

	public struct UrlInfomation
	{
		public string Url { get; set; }
		public string Protocol { get; set; }
		public bool IsSecureConnection { get; set; }
		public string Host { get; set; }
		public string Port { get; set; }
		public string Path { get; set; }
	}
}
