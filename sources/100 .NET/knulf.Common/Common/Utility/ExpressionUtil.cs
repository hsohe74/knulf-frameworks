﻿using knulf.Logging;
using knulf.Pattern;
using NCalc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Common.Common.Utility
{
	/// <summary>
	/// NCalc 사용한 수식 문자열 처리
	/// https://github.com/ncalc/ncalc
	/// Logical: or, ||, and, &&
	/// Relational: =, ==, !=, <>, <, <=, >, >=
	/// Arithmetic: +, -, *, /, %
	/// Primary: (, )
	/// Functions: Abs, Acos, Asin, Atan, Ceiling, Cos, Exp, Floor, Log, Log10, Max, Min, Pow, Round, Sign, Sin, Sqrt, Tan, Truncate
	/// </summary>
	public class ExpressionUtil
	{
		public static ExpressionResult Eval(string expressionString)
		{
			ExpressionResult result = new ExpressionResult(expressionString);

			Expression expression = new Expression(expressionString, EvaluateOptions.IgnoreCase);
			result.HasError = expression.HasErrors();
			if (expression.HasErrors() == false)
			{
				result.ResultObject = expression.Evaluate();
			}
			else
			{
				result.Error = expression.Error;
				result.ResultObject = null;
				LogManager.Current.WriteDebug(string.Format("Expression has error: {0} => {1}", expressionString, expression.Error));
			}

			return result;
		}
		public static ExpressionResult Judge(string expressionString)
		{
			ExpressionResult result = new ExpressionResult(expressionString);

			Expression expression = new Expression(expressionString, EvaluateOptions.IgnoreCase);
			result.HasError = expression.HasErrors();
			if (expression.HasErrors() == false)
			{
				object evalResult = expression.Evaluate();
				if (evalResult.GetType().Name != "Boolean")
				{
					result.ResultBool = true;
				}
				else
				{
					result.ResultBool = (bool)evalResult;
				}
			}
			else
			{
				result.Error = expression.Error;
				result.ResultBool = false;
				LogManager.Current.WriteDebug(string.Format("Expression has error: {0} => {1}", expressionString, expression.Error));
			}

			return result;
		}
	}

	public class ExpressionResult
	{
		public ExpressionResult(string expressionString)
		{
			this.Expression = expressionString;
		}

		public string Expression { get; set; }

		public bool HasError { get; set; }
		public string Error { get; set; }

		public object ResultObject { get; set; }
		public bool ResultBool { get; set; }
	}
}
