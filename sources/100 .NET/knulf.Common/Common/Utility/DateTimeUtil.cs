﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Utility
{
	/// <summary>
	/// Utilities of datetime
	/// </summary>
	public class DateTimeUtil
	{
		public static readonly DateTime Jan1st1970Utc = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// ENUM
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Enum

		public enum DatePart
		{
			Year, Month, Week, Day, Hour, Minute, Second, Millisecond
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// UTC
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region UTC

		/// <summary>
		/// Get total value with UTC from Jan1st1970
		/// </summary>
		/// <returns></returns>
		public static long GetUtcTotalValue(DateTime jan1st1970, DatePart datePart)
		{
			long result = 0L;

			switch(datePart)
			{
				case DatePart.Day:
					result = (long)(DateTime.UtcNow - jan1st1970).TotalDays;
					break;
				case DatePart.Hour:
					result = (long)(DateTime.UtcNow - jan1st1970).TotalHours;
					break;
				case DatePart.Minute:
					result = (long)(DateTime.UtcNow - jan1st1970).TotalMinutes;
					break;
				case DatePart.Second:
					result = (long)(DateTime.UtcNow - jan1st1970).TotalSeconds;
					break;
				case DatePart.Millisecond:
					result = (long)(DateTime.UtcNow - jan1st1970).TotalMilliseconds;
					break;
				default:
					result = (long)(DateTime.UtcNow - jan1st1970).TotalMilliseconds;
					break;
			}

			return result;
		}

		/// <summary>
		/// Get datetime from time total value from Jan1st1970
		/// </summary>
		/// <param name="value"></param>
		/// <param name="datePart"></param>
		/// <returns></returns>
		public static DateTime GetUtcDateFromTotalValue(DateTime jan1st1970, long value, DatePart datePart)
		{
			DateTime result = jan1st1970;

			switch (datePart)
			{
				case DatePart.Day:
					result = jan1st1970.Add(TimeSpan.FromDays(value));
					break;
				case DatePart.Hour:
					result = jan1st1970.Add(TimeSpan.FromHours(value));
					break;
				case DatePart.Minute:
					result = jan1st1970.Add(TimeSpan.FromMinutes(value));
					break;
				case DatePart.Second:
					result = jan1st1970.Add(TimeSpan.FromSeconds(value));
					break;
				case DatePart.Millisecond:
					result = jan1st1970.Add(TimeSpan.FromMilliseconds(value));
					break;
				default:
					result = jan1st1970.Add(TimeSpan.FromMilliseconds(value));
					break;
			}

			return result;
		}

		/// <summary>
		/// Get datetime from time total value from Jan1st1970
		/// </summary>
		/// <param name="value"></param>
		/// <param name="datePart"></param>
		/// <returns>UTC Date</returns>
		public static DateTime GetUtcDateFromTotalValue(long value, DatePart datePart)
		{
			DateTime result = Jan1st1970Utc;

			switch (datePart)
			{
				case DatePart.Day:
					result = Jan1st1970Utc.Add(TimeSpan.FromDays(value));
					break;
				case DatePart.Hour:
					result = Jan1st1970Utc.Add(TimeSpan.FromHours(value));
					break;
				case DatePart.Minute:
					result = Jan1st1970Utc.Add(TimeSpan.FromMinutes(value));
					break;
				case DatePart.Second:
					result = Jan1st1970Utc.Add(TimeSpan.FromSeconds(value));
					break;
				case DatePart.Millisecond:
					result = Jan1st1970Utc.Add(TimeSpan.FromMilliseconds(value));
					break;
				default:
					result = Jan1st1970Utc.Add(TimeSpan.FromMilliseconds(value));
					break;
			}

			return result;
		}

		/// <summary>
		/// Get datetime from time total value from Jan1st1970
		/// </summary>
		/// <param name="value">Total milliseconds</param>
		/// <returns>UTC Date</returns>
		public static DateTime GetUtcDateFromTotalValue(long value)
		{
			return Jan1st1970Utc.Add(TimeSpan.FromMilliseconds(value));
		}

		/// <summary>
		/// Get local date from UTC date
		/// </summary>
		/// <param name="utcDate">UTC Date</param>
		/// <returns>Local Date</returns>
		public static DateTime GetLocalDateFromUtc(DateTime utcDate)
		{
			return utcDate.ToLocalTime();
		}

		/// <summary>
		/// Get UTC Date from Local date
		/// </summary>
		/// <param name="localDate">Local Date</param>
		/// <returns>UTC Date</returns>
		public static DateTime GetUtcDateFromLocal(DateTime localDate)
		{
			return localDate.ToUniversalTime();
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// DateTime
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region DateTime

		/// <summary>
		/// Get datetime
		/// </summary>
		/// <param name="datetime"></param>
		/// <param name="format"></param>
		/// <returns></returns>
		public static DateTime GetDateTime(string datetime, string format)
		{
			return DateTime.ParseExact(datetime, format, null);
		}

		/// <summary>
		/// Get datetime string
		/// </summary>
		/// <param name="datetime"></param>
		/// <param name="format"></param>
		/// <returns></returns>
		public static string GetDateTimeString(DateTime datetime, string format)
		{
			return datetime.ToString(format);
		}

		#endregion
	}
}
