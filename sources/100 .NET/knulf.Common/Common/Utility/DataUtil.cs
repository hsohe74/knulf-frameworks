﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Utility
{
	/// <summary>
	/// Utilities of data (DataTable, DataSet)
	/// </summary>
	public class DataUtil
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// CSV
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region CSV

		/// <summary>
		/// Get DataTable from CSV file
		/// </summary>
		/// <param name="csvFilePath">CSV file path</param>
		/// <param name="delimiter">Data delimiter</param>
		/// <param name="isFirstRowHeader">Wheter first row is header</param>
		/// <returns></returns>
		public static DataTable GetDataTableFromCsv(string csvFilePath, string delimiter, bool isFirstRowHeader)
		{
			return GetDataTableFromCsv(csvFilePath, delimiter, isFirstRowHeader, Encoding.UTF8);
		}

		/// <summary>
		/// Get DataTable from CSV file
		/// </summary>
		/// <param name="csvFilePath">CSV file path</param>
		/// <param name="delimiter">Data delimiter</param>
		/// <param name="isFirstRowHeader">Wheter first row is header</param>
		/// <param name="encoding">Encoding</param>
		/// <returns></returns>
		public static DataTable GetDataTableFromCsv(string csvFilePath, string delimiter, bool isFirstRowHeader, Encoding encoding)
		{
			DataTable dataTable = null;

			string[] lines = null;
			string[] headers = null;
			string[] datas = null;

			try
			{
				lines = File.ReadAllLines(csvFilePath, encoding);
				headers = lines[0].Split(delimiter.ToCharArray());
				datas = null;

				// Make DataTable instance
				dataTable = new DataTable();

				// Make column
				if (isFirstRowHeader)
				{
					for (int i = 0; i < headers.Length; i++)
					{
						dataTable.Columns.Add(headers[i], typeof(string));
					}
				}
				else
				{
					for (int i = 0; i < headers.Length; i++)
					{
						dataTable.Columns.Add("Column" + (i + 1).ToString(), typeof(string));
					}
				}

				// Add rows
				for (int i = (isFirstRowHeader ? 1 : 0); i < lines.Length; i++)
				{
					datas = lines[i].Split(delimiter.ToCharArray());

					DataRow row = dataTable.NewRow();
					for (int j = 0; j < headers.Length; j++)
					{
						row[j] = datas[j];
					}
					dataTable.Rows.Add(row);
				}
			}
			catch
			{
				throw;
			}
			finally
			{
				lines = null;
				headers = null;
				datas = null;
			}

			return dataTable;
		}

		/// <summary>
		/// Save source DataTable to CSV file
		/// </summary>
		/// <param name="dataTable">Source DataTable</param>
		/// <param name="csvFilePath">CSV file path</param>
		/// <param name="delimiter">Data delimiter</param>
		/// <param name="isFirstRowHeader">Wheter first row is header</param>
		/// <returns></returns>
		public static bool SaveCsvFromDataTable(DataTable dataTable, string csvFilePath, string delimiter, bool isFirstRowHeader)
		{
			return SaveCsvFromDataTable(dataTable, csvFilePath, delimiter, isFirstRowHeader, Encoding.UTF8);
		}

		/// <summary>
		/// Save source DataTable to CSV file
		/// </summary>
		/// <param name="dataTable">Source DataTable</param>
		/// <param name="csvFilePath">CSV file path</param>
		/// <param name="delimiter">Data delimiter</param>
		/// <param name="isFirstRowHeader">Wheter first row is header</param>
		/// <param name="encoding">Encoding</param>
		/// <returns></returns>
		public static bool SaveCsvFromDataTable(DataTable dataTable, string csvFilePath, string delimiter, bool isFirstRowHeader, Encoding encoding)
		{
			bool isSuccess = true;

			StringBuilder sbCsvContent = null;

			try
			{
				sbCsvContent = new StringBuilder();

				// Header
				if (isFirstRowHeader)
				{
					for (int i = 0; i < dataTable.Columns.Count; i++)
					{
						if (i > 0) sbCsvContent.Append(delimiter);
						sbCsvContent.Append(dataTable.Columns[i].ToString());
					}
					sbCsvContent.Append("\n");
				}

				// Data
				for (int i = 0; i < dataTable.Rows.Count; i++)
				{
					for (int j = 0; j < dataTable.Columns.Count; j++)
					{
						if (j > 0) sbCsvContent.Append(delimiter);
						sbCsvContent.Append(dataTable.Rows[i][j].ToString());
					}
					sbCsvContent.Append("\n");
				}

				// Save file
				FileUtil.WriteTextFile(csvFilePath, sbCsvContent.ToString(), encoding);
			}
			catch
			{
				throw;
			}
			finally
			{
				sbCsvContent = null;
			}

			return isSuccess;
		}

		#endregion
	}
}
