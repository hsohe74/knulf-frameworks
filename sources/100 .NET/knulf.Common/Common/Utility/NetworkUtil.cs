﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;

namespace knulf.Utility
{
	/// <summary>
	/// Utilities of network
	/// </summary>
	public class NetworkUtil
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Network information
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Network information

		/// <summary>
		/// Get host name
		/// </summary>
		/// <returns></returns>
		public static string GetHostname()
		{
			return Dns.GetHostName();
		}

		/// <summary>
		/// Get IP address
		/// </summary>
		/// <returns></returns>
		public static IPAddress GetIpAddress()
		{
			IPAddress ipAddress = null;

			try
			{
				IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
				IPAddress[] ipAddrs = ipHost.AddressList;
				for (int i = 0; i < ipAddrs.Length; i++)
				{
					if (ipAddrs[i].AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
					{
						ipAddress = ipAddrs[i];
						break;
					}
				}
			}
			catch
			{
				ipAddress = null;
			}

			return ipAddress;
		}

		/// <summary>
		/// Get mac address (first selected)
		/// </summary>
		/// <returns></returns>
		public static string GetMacAddress()
		{
			string macAddress = null;

			try
			{
				foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
				{
					if ((nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet
						|| nic.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
						&& nic.OperationalStatus == OperationalStatus.Up)
					{
						macAddress = nic.GetPhysicalAddress().ToString();
						break;
					}
				}

				if (macAddress == null)
				{
					var nic = NetworkInterface.GetAllNetworkInterfaces().Where(p => p.OperationalStatus == OperationalStatus.Up).FirstOrDefault();
					if (nic != null)
					{
						macAddress = nic.GetPhysicalAddress().ToString();
					}
				}
			}
			catch
			{
				macAddress = null;
			}

			return macAddress;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Ping
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Ping

		/// <summary>
		/// Ping test
		/// </summary>
		public static bool PingTestWithCommand(out string resultMessage, string host, int timeout = 120)
		{
			bool result = false;
			StringBuilder sbResultMessage = new StringBuilder();

			try
			{
				PingReply pingReply = PingTest(host, timeout);
				if (pingReply != null)
				{
					if (pingReply.Status == IPStatus.Success)
					{
						var processResult = ProcessUtil.ExecuteCommand(string.Format("ping {0} -w {1}", host, timeout));
						sbResultMessage.Append(processResult);
						result = true;
					}
					else
					{
						sbResultMessage.Append(string.Format("Address: {0}\n", pingReply.Address.ToString()));
						sbResultMessage.Append(string.Format("Status: {0}\n", pingReply.Status.ToString()));
					}
				}
				else
				{
					sbResultMessage.Append("Ping reply is null");
				}
				resultMessage = sbResultMessage.ToString();
			}
			catch (Exception ex)
			{
				resultMessage = ex.ToString();
				result = false;
			}

			return result;
		}

		/// <summary>
		/// Ping test
		/// </summary>
		public static bool PingTest(out string resultMessage, string host, int timeout = 120)
		{
			bool result = false;
			StringBuilder sbResultMessage = new StringBuilder();

			try
			{
				PingReply pingReply = PingTest(host, timeout);
				if (pingReply != null)
				{
					if (pingReply.Status == IPStatus.Success)
					{
						sbResultMessage.Append(string.Format("Address: {0}\n", pingReply.Address.ToString()));
						sbResultMessage.Append(string.Format("RoundTrip time: {0}\n", pingReply.RoundtripTime));
						sbResultMessage.Append(string.Format("Time to live: {0}\n", pingReply.Options.Ttl));
						sbResultMessage.Append(string.Format("Don't fragment: {0}\n", pingReply.Options.DontFragment));
						sbResultMessage.Append(string.Format("Buffer size: {0}\n", pingReply.Buffer.Length));
						sbResultMessage.Append(string.Format("Status: {0}\n", pingReply.Status.ToString()));
						result = true;
					}
					else
					{
						sbResultMessage.Append(string.Format("Address: {0}\n", pingReply.Address.ToString()));
						sbResultMessage.Append(string.Format("Status: {0}\n", pingReply.Status.ToString()));
					}
				}
				else
				{
					sbResultMessage.Append("Ping reply is null");
				}
				resultMessage = sbResultMessage.ToString();
			}
			catch (Exception ex)
			{
				resultMessage = ex.ToString();
				result = false;
			}

			return result;
		}

		/// <summary>
		/// Ping test
		/// </summary>
		public static PingReply PingTest(string host, int timeout = 120)
		{
			PingReply pingReply = null;

			Ping ping = new Ping();
			PingOptions pingOptions = new PingOptions();
			pingOptions.DontFragment = true;

			try
			{
				pingReply = ping.Send(host, timeout);
			}
			catch
			{
				throw;
			}

			return pingReply;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Utilities
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Utilities

		/// <summary>
		/// Get whether network available
		/// </summary>
		/// <returns></returns>
		public static bool IsNetworkAvailable()
		{
			try
			{
				return NetworkInterface.GetIsNetworkAvailable();
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// Get whether internet connected
		/// </summary>
		public static bool IsInternetConnected()
		{
			const string NCSI_TEST_URL = "http://www.msftncsi.com/ncsi.txt";
			const string NCSI_TEST_RESULT = "Microsoft NCSI";
			const string NCSI_DNS = "dns.msftncsi.com";
			const string NCSI_DNS_IP_ADDRESS = "131.107.255.255";

			try
			{
				// Check NCSI test link
				var webClient = new WebClient();
				string result = webClient.DownloadString(NCSI_TEST_URL);
				if (result != NCSI_TEST_RESULT)
				{
					return false;
				}

				// Check NCSI DNS IP
				var dnsHost = Dns.GetHostEntry(NCSI_DNS);
				if (dnsHost.AddressList.Count() < 0 || dnsHost.AddressList[0].ToString() != NCSI_DNS_IP_ADDRESS)
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex);
				return false;
			}

			return true;
		}

		#endregion
	}
}
