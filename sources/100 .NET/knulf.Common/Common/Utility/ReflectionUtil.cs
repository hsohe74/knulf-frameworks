﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Utility
{
	/// <summary>
	/// Utilities of Refrection
	/// </summary>
	public class ReflectionUtil
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Create instance
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Create instance

		/// <summary>
		/// Create instance by object type
		/// </summary>
		/// <param name="objectType"></param>
		/// <returns></returns>
		public static object CreateInstance(Type objectType)
		{
			object itemObject = null;

			try
			{
				itemObject = Activator.CreateInstance(objectType);
			}
			catch
			{
				itemObject = null;
			}

			return itemObject;
		}

		/// <summary>
		/// Create instance by AssemblyQualifiedName and type name
		/// </summary>
		/// <param name="assemblyQualifiedName"></param>
		/// <param name="fullTypeName"></param>
		/// <returns></returns>
		public static object CreateInstance(string assemblyQualifiedName, string fullTypeName)
		{
			object itemObject = null;
			System.Reflection.Assembly assembly = null;

			try
			{
				if (assemblyQualifiedName == null || fullTypeName == null)
					return null;

				assembly = Type.GetType(assemblyQualifiedName).Assembly;
				itemObject = CreateInstance(assembly, fullTypeName);
			}
			catch
			{
				itemObject = null;
			}

			return itemObject;
		}

		/// <summary>
		/// Create instance by Assembly and type name
		/// </summary>
		/// <param name="assembly"></param>
		/// <param name="fullTypeName"></param>
		/// <returns></returns>
		public static object CreateInstance(Assembly assembly, string fullTypeName)
		{
			Type itemType = null;
			object itemObject = null;

			try
			{
				if (assembly == null || fullTypeName == null)
					return null;

				itemType = assembly.GetType(fullTypeName);
				itemObject = Activator.CreateInstance(itemType);
			}
			catch
			{
				itemObject = null;
			}

			return itemObject;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Type
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Type

		public static Type GetType(Assembly assembly, string fullTypeName)
		{
			Type itemType = null;

			try
			{
				if (assembly == null || fullTypeName == null)
					return null;

				itemType = assembly.GetType(fullTypeName);
			}
			catch
			{
				itemType = null;
			}

			return itemType;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Field
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Field

		/// <summary>
		/// Get FieldInfo list
		/// </summary>
		public static List<FieldInfo> GetFields(Type type, BindingFlags bindingAttr)
		{
			List<FieldInfo> fields = type.GetFields(bindingAttr).ToList();
			return fields;
		}

		/// <summary>
		/// Set field value
		/// </summary>
		public static void SetFieldValue(object targetObject, string fieldName, dynamic value)
		{
			try
			{
				var field = targetObject.GetType().GetField(fieldName);
				if (field != null)
					field.SetValue(targetObject, value);
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Set field value
		/// </summary>
		public static dynamic GetFieldValue(object targetObject, string fieldName)
		{
			try
			{
				var field = targetObject.GetType().GetField(fieldName);
				if (field != null)
					return field.GetValue(targetObject);
				else
					return null;
			}
			catch
			{
				throw;
			}
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Property
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Property

		/// <summary>
		/// Get PropertyInfo list
		/// </summary>
		public static List<PropertyInfo> GetProperties(Type type, BindingFlags bindingAttr)
		{
			List<PropertyInfo> properties = type.GetProperties(bindingAttr).ToList();
			return properties;
		}

		/// <summary>
		/// Set property value
		/// </summary>
		public static void SetPropertyValue(object targetObject, string propertyName, dynamic value)
		{
			try
			{
				var property = targetObject.GetType().GetProperty(propertyName);
				if (property != null && property.CanWrite)
					property.SetValue(targetObject, value, null);
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Set property value
		/// </summary>
		public static dynamic GetPropertyValue(object targetObject, string propertyName)
		{
			try
			{
				var property = targetObject.GetType().GetProperty(propertyName);
				if (property != null && property.CanRead)
					return property.GetValue(targetObject, null);
				else
					return null;
			}
			catch
			{
				throw;
			}
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Attribute
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Attribute

		/// <summary>
		/// Get description attribute's value
		/// </summary>
		public static string GetDescriptionAttributeString<T>(T source)
		{
			FieldInfo fieldInfo = source.GetType().GetField(source.ToString());

			DescriptionAttribute[] attributes
				= (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

			if (attributes != null && attributes.Length > 0) return attributes[0].Description;
			else return null;
		}

		/// <summary>
		/// Get attribute by FieldInfo
		/// </summary>
		public static T GetAttribute<T>(FieldInfo field)
		{
			var attribute = field.GetCustomAttributes(false).Where(p => p.GetType() == typeof(T)).FirstOrDefault();
			if (attribute == null) return default(T);

			return (T)attribute;
		}

		/// <summary>
		/// Get attribute by PropertyInfo
		/// </summary>
		public static T GetAttribute<T>(PropertyInfo property)
		{
			var attribute = property.GetCustomAttributes(false).Where(p => p.GetType() == typeof(T)).FirstOrDefault();
			if (attribute == null) return default(T);

			return (T)attribute;
		}

		#endregion
	}
}
