﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf
{
	public class ReturnJson
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		public ReturnJson()
			: this(true, null, null, null) { }

		public ReturnJson(bool success)
			: this(success, null, null, null) { }

		public ReturnJson(bool success, string message)
			: this(success, null, message, null) { }

		public ReturnJson(bool success, string message, object data)
			: this(success, null, message, data) { }

		public ReturnJson(bool success, string code, string message, object data)
		{
			this.Success = success;
			this.Code = code;
			this.Message = message;
			this.Data = data;
		}

		public ReturnJson(Exception ex)
		{
			var w32ex = ex as System.ComponentModel.Win32Exception;
			this.Success = false;
			this.Code = (w32ex == null ? null : w32ex.ErrorCode.ToString());
			this.Message = ex.Message;
			this.Data = ex;
		}

		public ReturnJson(ReturnData returnData)
		{
			this.Success = returnData.Success;
			this.Code = returnData.Code;
			this.Message = returnData.Message;
			this.Data = returnData.Data;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Properties
		////////////////////////////////////////////////////////////////////////////////////////////////////

		public bool Success { get; set; }
		public string Code { get; set; }
		public string Message { get; set; }
		public object Data { get; set; }
	}
}
