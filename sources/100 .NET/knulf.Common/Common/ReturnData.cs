﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace knulf
{
	/// <summary>
	/// Returned data for common situation
	/// </summary>
	public class ReturnData
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		public ReturnData()
			: this(true, null, null, null) { }

		public ReturnData(bool success)
			: this(success, null, null, null) { }

		public ReturnData(bool success, string message)
			: this(success, null, message, null) { }

		public ReturnData(bool success, string message, object data)
			: this(success, null, message, data) { }

		public ReturnData(bool success, string code, string message, object data)
		{
			Success = success;
			Code = code;
			Message = message;
			Data = data;
		}

		public ReturnData(Exception ex)
		{
			var w32ex = ex as System.ComponentModel.Win32Exception;
			Success = false;
			Code = (w32ex == null ? null : w32ex.ErrorCode.ToString());
			Message = ex.Message;
			Data = ex;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Properties
		////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Gets or sets the value that indicates whether success of process
		/// </summary>
		public bool Success { get; set; }

		/// <summary>
		/// Gets or sets the return code of process
		/// </summary>
		public string Code { get; set; }

		/// <summary>
		/// Gets or sets the return message of process
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets the return message code of process
		/// </summary>
		public string MessageCode { get; set; }

		/// <summary>
		/// Gets or sets the return value object of process
		/// </summary>
		public object Data { get; set; }

		/// <summary>
		/// Gets or sets DB data's total records
		/// </summary>
		public long TotalRecord { get; set; }

		/// <summary>
		/// Gets or sets the DB data by DataSet format
		/// </summary>
		public System.Data.DataSet DbData { get; set; }

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Method
		////////////////////////////////////////////////////////////////////////////////////////////////////

		public void SetData(bool success)
		{
			this.SetData(success, null, null, null);
		}

		public void SetData(bool success, string message)
		{
			this.SetData(success, null, message, null);
		}

		public void SetData(bool success, string message, object data)
		{
			this.SetData(success, null, message, data);
		}

		public void SetData(bool success, string code, string message, object data)
		{
			Success = success;
			Code = code;
			Message = message;
			Data = data;
		}

		public void SetData(Exception ex)
		{
			var w32ex = ex as System.ComponentModel.Win32Exception;

			Success = false;
			Code = (w32ex == null ? null : w32ex.ErrorCode.ToString());
			Message = ex.Message;
			Data = ex;
		}
	}
}
