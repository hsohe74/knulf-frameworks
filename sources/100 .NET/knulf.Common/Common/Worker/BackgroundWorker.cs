﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Worker
{
	/// <summary>
	/// BackgroundWorker를 래핑한 클래스
	/// </summary>
	public class BackgroundWorker
	{
		private System.ComponentModel.BackgroundWorker worker = null;

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Constructer & Initialize

		public BackgroundWorker() : this(true, true)
		{
		}

		public BackgroundWorker(bool isReportsProgress, bool isSupportsCancellation)
		{
			this.worker = new System.ComponentModel.BackgroundWorker();
			this.worker.WorkerReportsProgress = isReportsProgress;
			this.worker.WorkerSupportsCancellation = isSupportsCancellation;
			this.worker.DoWork += Worker_DoWork;
			this.worker.ProgressChanged += Worker_ProgressChanged;
			this.worker.RunWorkerCompleted += Worker_RunWorkerCompleted;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Properties
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Properties

		/// <summary>
		/// DoWork action
		/// </summary>
		public Action<System.ComponentModel.DoWorkEventArgs> DoWorkAction { get; set; }

		/// <summary>
		/// ProgressChanged action
		/// </summary>
		public Action<System.ComponentModel.ProgressChangedEventArgs> ProgressChangedAction { get; set; }

		/// <summary>
		/// RunWorkerCompleted action
		/// </summary>
		public Action<System.ComponentModel.RunWorkerCompletedEventArgs> WorkerCompletedAction { get; set; }

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Event handler
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Event handler

		/// <summary>
		/// Worker_DoWork
		/// </summary>
		private void Worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
		{
			if (DoWorkAction != null)
			{
				DoWorkAction.Invoke(e);
			}
		}

		/// <summary>
		/// Worker_ProgressChanged
		/// </summary>
		private void Worker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
		{
			if (ProgressChangedAction != null)
			{
				ProgressChangedAction.Invoke(e);
			}
		}

		/// <summary>
		/// Worker_RunWorkerCompleted
		/// </summary>
		private void Worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
		{
			if (WorkerCompletedAction != null)
			{
				WorkerCompletedAction.Invoke(e);
			}
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Method
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Method

		/// <summary>
		/// Run worker
		/// </summary>
		public void RunWorker()
		{
			this.worker.RunWorkerAsync();
		}

		#endregion
	}
}
