﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace knulf.Worker
{
	/// <summary>
	/// Thread를 이용해서 주기적으로 처리하는 추상클래스
	/// </summary>
	public abstract class ThreadWorkerBase
	{
		// Thread
		protected Thread worker = null;
		protected bool threadAliveFlag = true;
		protected int threadInterval = 1000 * 5;

		// Lock object
		private object lockObject = new object();

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		public ThreadWorkerBase()
		{
			this.IsBackground = true;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Properties
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Properties

		/// <summary>
		/// Worker Interval (Millisecond)
		/// </summary>
		public int Interval
		{
			get { return this.threadInterval; }
			set { this.threadInterval = value; }
		}

		/// <summary>
		/// A value indicating whether or not a worker is a background thread.
		/// </summary>
		public bool IsBackground { get; set; }

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Method : Worker
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Worker

		/// <summary>
		/// Start worker
		/// </summary>
		public void Start()
		{
			// Active thread flag
			this.threadAliveFlag = true;

			// Make new thread and start
			if (this.worker == null || this.worker.ThreadState == ThreadState.Stopped)
			{
				this.worker = new Thread(new ThreadStart(LoopWorker));
				this.worker.IsBackground = this.IsBackground;
				this.worker.Start();
			}
		}

		/// <summary>
		/// Stop worker
		/// </summary>
		public void Stop()
		{
			// Deactive thread flag
			this.threadAliveFlag = false;

			// Stop thread
			if (this.worker != null) this.worker.Join(100);
		}

		/// <summary>
		/// Run task method
		/// </summary>
		protected abstract void Run();
		
		/// <summary>
		/// Loop worker
		/// </summary>
		private void LoopWorker()
		{
			while (this.threadAliveFlag)
			{
				try
				{
					lock (lockObject)
					{
						if (this.threadAliveFlag) this.Run();
					}
					Thread.Sleep(this.threadInterval);
				}
				catch { }
			}
		}

		#endregion
	}
}
