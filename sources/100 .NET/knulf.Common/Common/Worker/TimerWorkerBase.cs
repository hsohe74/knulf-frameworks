﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Worker
{
	/// <summary>
	/// Timer를 이용해서 주기적으로 처리하는 추상클래스
	/// </summary>
	public abstract class TimerWorkerBase
	{
		// Thread
		private System.Timers.Timer worker = null;
		private bool threadAliveFlag = true;
		protected int threadInterval = 1000 * 20;

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		public TimerWorkerBase()
		{
			this.worker = new System.Timers.Timer();
			this.worker.Elapsed += new System.Timers.ElapsedEventHandler(worker_Elapsed);
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Properties
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Properties

		/// <summary>
		/// Worker Interval (Millisecond)
		/// </summary>
		public int Interval
		{
			get { return this.threadInterval; }
			set { this.threadInterval = value; }
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Method : Worker
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Worker

		/// <summary>
		/// Start worker
		/// </summary>
		public void Start()
		{
			// Active thread flag
			this.threadAliveFlag = true;

			// First Start
			this.Run();

			// Make new thread and start
			this.worker.Interval = this.threadInterval;
			this.worker.Enabled = true;
			this.worker.Start();
		}

		/// <summary>
		/// Stop worker
		/// </summary>
		public void Stop()
		{
			// Deactive thread flag
			this.threadAliveFlag = false;

			// Stop thread
			this.worker.Stop();
		}

		/// <summary>
		/// Run task method
		/// </summary>
		protected abstract void Run();

		/// <summary>
		/// Event handler for elapsed
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void worker_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			if (this.threadAliveFlag) this.Run();
		}

		#endregion
	}
}
