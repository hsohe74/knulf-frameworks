﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Worker
{
	/// <summary>
	/// Define common method and property of thread
	/// </summary>
	public interface IThreadRunner
	{
		/// <summary>
		/// Start method
		/// </summary>
		void Start();

		/// <summary>
		/// Stop method
		/// </summary>
		void Stop();

		/// <summary>
		/// Run method
		/// </summary>
		void Run();
	}
}
