﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace knulf.Worker
{
	/// <summary>
	/// Thread를 이용해서 주기적으로 처리하는 클래스
	/// </summary>
	public class ThreadRunner
	{
		// Thread
		protected Thread worker = null;
		protected bool threadAliveFlag = true;
		protected int threadInterval = 1000 * 5;

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		public ThreadRunner()
		{
			this.IsBackground = true;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Properties
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region

		/// <summary>
		/// Thread Interval (Millisecond)
		/// </summary>
		public int Interval
		{
			get { return this.threadInterval; }
			set { this.threadInterval = value; }
		}

		/// <summary>
		/// A value indicating whether or not a thread is a background thread.
		/// </summary>
		public bool IsBackground { get; set; }

		/// <summary>
		/// Action method for thread
		/// </summary>
		public Action RunAction { get; set; }

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Method : Worker
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Worker

		/// <summary>
		/// Start thread
		/// </summary>
		public void Start()
		{
			// Active thread flag
			this.threadAliveFlag = true;

			// Make new thread and start
			if (this.worker == null || this.worker.ThreadState == ThreadState.Stopped)
			{
				this.worker = new Thread(new ThreadStart(LoopWorker));
				this.worker.IsBackground = this.IsBackground;
				this.worker.Start();
			}
		}

		/// <summary>
		/// Stop thread
		/// </summary>
		public void Stop()
		{
			// Deactive thread flag
			this.threadAliveFlag = false;

			// Stop thread
			if (this.worker != null) this.worker.Join(100);
		}

		/// <summary>
		/// Loop worker
		/// </summary>
		private void LoopWorker()
		{
			while (this.threadAliveFlag)
			{
				try
				{
					if (this.threadAliveFlag)
					{
						RunAction();
					}
					Thread.Sleep(this.threadInterval);
				}
				catch { }
			}
		}

		#endregion
	}
}
