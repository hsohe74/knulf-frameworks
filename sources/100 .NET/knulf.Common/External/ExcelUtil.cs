﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace knulf.Utility
{
	/// <summary>
	/// Utilities of excel with EPPlus library
	/// </summary>
	public class ExcelUtil
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Get data from excel file
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Get data from excel file

		/// <summary>
		/// Get data set from excel file with first workbook
		/// </summary>
		public static DataSet GetDataSet(string filePath, bool isHasColumnHeaderRow, bool isUseColumnHeaderRow)
		{
			DataSet dsResult = null;

			try
			{
				dsResult = new DataSet();

				using (var excelPackage = new ExcelPackage())
				{
					// Load from file
					using (var stream = File.OpenRead(filePath))
					{
						excelPackage.Load(stream);
					}

					// Get first workbook
					foreach (ExcelWorksheet ws in excelPackage.Workbook.Worksheets)
					{
						try
						{
							dsResult.Tables.Add(GetDataTable(ws, isHasColumnHeaderRow, isUseColumnHeaderRow).Copy());
						}
						catch (DuplicateNameException)
						{
							DataTable dtTemp = GetDataTable(ws, isHasColumnHeaderRow, isUseColumnHeaderRow).Copy();
							dtTemp.TableName = Guid.NewGuid().ToString();
							dsResult.Tables.Add(dtTemp);
						}
						catch { }
					}
				}
			}
			catch
			{
				throw;
			}

			return dsResult;
		}

		/// <summary>
		/// Get data table from excel file with first workbook
		/// </summary>
		public static DataTable GetDataTable(string filePath, bool isHasColumnHeaderRow, bool isUseColumnHeaderRow)
		{
			DataTable dtResult = null;

			try
			{
				dtResult = new DataTable();

				using (var excelPackage = new ExcelPackage())
				{
					// Load from file
					using (var stream = File.OpenRead(filePath))
					{
						excelPackage.Load(stream);
					}

					// Get first workbook
					if (excelPackage.Workbook.Worksheets.Count < 1)
					{
						throw new Exception("There is no data in the Excel file, or Excel file was made by html.");
					}
					else
					{
						var ws = excelPackage.Workbook.Worksheets.First();
						dtResult = GetDataTable(ws, isHasColumnHeaderRow, isUseColumnHeaderRow).Copy();
					}
				}
			}
			catch
			{
				throw;
			}

			return dtResult;
		}

		/// <summary>
		/// Get data table from worksheet
		/// </summary>
		private static DataTable GetDataTable(ExcelWorksheet ws, bool isHasColumnHeaderRow, bool isUseColumnHeaderRow)
		{
			DataTable dtResult = null;

			try
			{
				dtResult = new DataTable();
				dtResult.TableName = ws.Name;

				// Set column
				for (var i = 0; i < ws.Dimension.End.Column; i++)
				{
					var headerCell = ws.Cells[1, i + 1, 1, i + 1];
					var headerString = string.Format("Column {0}", i + 1);
					if (isHasColumnHeaderRow && isUseColumnHeaderRow && string.IsNullOrEmpty(headerCell.Text) == false)
					{
						headerString = headerCell.Text;
					}
					dtResult.Columns.Add(headerString);
				}

				// Data
				var startRow = isHasColumnHeaderRow ? 1 : 0;
				for (var rowNum = startRow; rowNum < ws.Dimension.End.Row; rowNum++)
				{
					var wsRow = ws.Cells[rowNum + 1, 1, rowNum + 1, ws.Dimension.End.Column];
					var row = dtResult.NewRow();
					foreach (var cell in wsRow)
					{
						row[cell.Start.Column - 1] = cell.Text;
					}
					dtResult.Rows.Add(row);
				}
			}
			catch
			{
				throw;
			}

			return dtResult;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Get excel file from data
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Get excel file from data

		/// <summary>
		/// Save Excel file
		/// </summary>
		public static bool SaveExcelFile(
			DataSet dataSource, string targetFilePath, 
			bool isHasColumnHeaderRow,
			bool isHasHeaderRow, string header, 
			params int[] hiddenColumnIndexs)
		{
			bool isSuccess = true;

			try
			{
				if (dataSource != null && dataSource.Tables.Count > 0)
				{
					FileInfo fileInfo = new FileInfo(targetFilePath);
					using (var excelPackage = new ExcelPackage())
					{
						foreach (DataTable dt in dataSource.Tables)
						{
							AddWorksheet(excelPackage, dt, isHasColumnHeaderRow, isHasHeaderRow, header, hiddenColumnIndexs);
						}
						excelPackage.SaveAs(fileInfo);
					}
				}
			}
			catch
			{
				throw;
			}

			return isSuccess;
		}

		/// <summary>
		/// Save Excel file
		/// </summary>
		public static bool SaveExcelFile(
			DataTable dataSource, string targetFilePath,
			bool isHasColumnHeaderRow,
			bool isHasHeaderRow, string header,
			params int[] hiddenColumnIndexs)
		{
			bool isSuccess = true;

			try
			{
				FileInfo fileInfo = new FileInfo(targetFilePath);
				using (var excelPackage = new ExcelPackage())
				{
					AddWorksheet(excelPackage, dataSource, isHasColumnHeaderRow, isHasHeaderRow, header, hiddenColumnIndexs);
					excelPackage.SaveAs(fileInfo);
				}
			}
			catch
			{
				throw;
			}

			return isSuccess;
		}

		/// <summary>
		/// Save Excel file
		/// </summary>
		public static bool SaveExcelFile(
			DataTable dataSource, string targetFilePath,
			bool isHasColumnHeaderRow, Dictionary<string, string> columnNames,
			bool isHasHeaderRow, string header)
		{
			bool isSuccess = true;

			try
			{
				for (int i = dataSource.Columns.Count - 1; i >= 0; i--)
				{
					if (columnNames.Keys.Contains(dataSource.Columns[i].ColumnName) == false)
					{
						dataSource.Columns.Remove(dataSource.Columns[i].ColumnName);
					}
				}

				foreach (string key in columnNames.Keys)
				{
					dataSource.Columns[key].ColumnName = columnNames[key];
				}

				FileInfo fileInfo = new FileInfo(targetFilePath);
				using (var excelPackage = new ExcelPackage())
				{
					AddWorksheet(excelPackage, dataSource, isHasColumnHeaderRow, isHasHeaderRow, header);
					excelPackage.SaveAs(fileInfo);
				}
			}
			catch
			{
				throw;
			}

			return isSuccess;
		}

		/// <summary>
		/// Add excel sheet from data table
		/// </summary>
		private static ExcelWorksheet AddWorksheet(
			ExcelPackage excelPackage,
			DataTable dataSource, bool isHasColumnHeaderRow,
			bool isHasHeaderRow, string header,
			params int[] hiddenColumnIndexs)
		{
			ExcelWorksheet ws = null;

			try
			{
				if (dataSource != null && dataSource.Rows.Count > 0)
				{
					ws = excelPackage.Workbook.Worksheets.Add(dataSource.TableName);
					ws.Cells["A1"].LoadFromDataTable(dataSource, isHasColumnHeaderRow);
					ws.View.ShowGridLines = false;
					ws.Name = dataSource.TableName;

					// Get data range
					var dataRange = ws.Cells[1, 1, dataSource.Rows.Count + (isHasColumnHeaderRow ? 1 : 0) + (isHasHeaderRow ? 2 : 0), ws.Dimension.End.Column];

					// Set header
					if (isHasHeaderRow)
					{
						ws.InsertRow(1, 2);
						ws.Cells[1, 1].Value = header;
						ws.Cells[1, 1, 2, ws.Dimension.End.Column].Merge = true;

						//System.Drawing.Color bgColor = System.Drawing.ColorTranslator.FromHtml("#4D4D4D");
						//ws.Cells[1, 1, 2, ws.Dimension.End.Column].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
						//ws.Cells[1, 1, 2, ws.Dimension.End.Column].Style.Fill.BackgroundColor.SetColor(bgColor);
						//ws.Cells[1, 1, 2, ws.Dimension.End.Column].Style.Font.Color.SetColor(System.Drawing.Color.White);
						ws.Cells[1, 1, 2, ws.Dimension.End.Column].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
						ws.Cells[1, 1, 2, ws.Dimension.End.Column].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
					}

					// Auto fit
					dataRange.AutoFitColumns();

					// Set header background
					if (isHasColumnHeaderRow)
					{
						//System.Drawing.Color bgColor = System.Drawing.ColorTranslator.FromHtml("#4D4D4D");
						//ws.Cells[(isHasHeaderRow ? 3 : 1), 1, (isHasHeaderRow ? 3 : 1), ws.Dimension.End.Column].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
						//ws.Cells[(isHasHeaderRow ? 3 : 1), 1, (isHasHeaderRow ? 3 : 1), ws.Dimension.End.Column].Style.Fill.BackgroundColor.SetColor(bgColor);
						//ws.Cells[(isHasHeaderRow ? 3 : 1), 1, (isHasHeaderRow ? 3 : 1), ws.Dimension.End.Column].Style.Font.Color.SetColor(System.Drawing.Color.White);
						ws.Cells[(isHasHeaderRow ? 3 : 1), 1, (isHasHeaderRow ? 3 : 1), ws.Dimension.End.Column].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
						ws.Cells[(isHasHeaderRow ? 3 : 1), 1, (isHasHeaderRow ? 3 : 1), ws.Dimension.End.Column].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
					}

					// Set border
					var border
						= dataRange.Style.Border.Top.Style
						= dataRange.Style.Border.Left.Style
						= dataRange.Style.Border.Right.Style
						= dataRange.Style.Border.Bottom.Style
						= OfficeOpenXml.Style.ExcelBorderStyle.Thin;

					// Hidden column
					if (hiddenColumnIndexs != null && hiddenColumnIndexs.Count() > 0)
					{
						foreach (int columnIndex in hiddenColumnIndexs)
						{
							ws.Column(columnIndex).Hidden = true;
						}
					}
				}
			}
			catch
			{
				throw;
			}

			return ws;
		}

		#endregion
	}
}
