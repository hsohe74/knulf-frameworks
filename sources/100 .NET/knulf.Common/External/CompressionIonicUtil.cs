﻿using Ionic.Zip;
using knulf.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Utility
{
	/// <summary>
	/// Utilities of compression by Ionic library
	/// </summary>
	public class CompressionIonicUtil
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Zip
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Zip

		/// <summary>
		/// Create zip file from source files
		/// </summary>
		/// <param name="sourceFilePath"></param>
		/// <param name="zipFilePath"></param>
		public static void CreateZipFromFile(
			string sourceFilePath,
			string zipFilePath)
		{
			CreateZipFromFile(new string[] { sourceFilePath }, zipFilePath);
		}

		/// <summary>
		/// Create zip file from source files
		/// </summary>
		/// <param name="sourceFilePaths"></param>
		/// <param name="zipFilePath"></param>
		public static void CreateZipFromFile(
			string[] sourceFilePaths,
			string zipFilePath)
		{
			CreateZip(null, sourceFilePaths, zipFilePath, Encoding.UTF8);
		}

		/// <summary>
		/// Create zip file from source directory
		/// </summary>
		/// <param name="sourceFilePath"></param>
		/// <param name="zipFilePath"></param>
		public static void CreateZipFromDirectory(
			string sourceDirectoryPath,
			string zipFilePath)
		{
			CreateZipFromDirectory(new string[] { sourceDirectoryPath }, zipFilePath);
		}

		/// <summary>
		/// Create zip file from source directory
		/// </summary>
		/// <param name="sourceFilePaths"></param>
		/// <param name="zipFilePath"></param>
		public static void CreateZipFromDirectory(
			string[] sourceDirectoryPaths,
			string zipFilePath)
		{
			CreateZip(sourceDirectoryPaths, null, zipFilePath, Encoding.UTF8);
		}

		/// <summary>
		/// Create zip file
		/// </summary>
		/// <param name="sourceDirectoryPaths"></param>
		/// <param name="sourceFilePaths"></param>
		/// <param name="zipFilePath"></param>
		public static void CreateZip(
			string[] sourceDirectoryPaths,
			string[] sourceFilePaths,
			string zipFilePath)
		{
			CreateZip(sourceDirectoryPaths, sourceFilePaths, zipFilePath, Encoding.UTF8);
		}

		/// <summary>
		/// Create zip file
		/// </summary>
		/// <param name="sourceDirectoryPaths"></param>
		/// <param name="sourceFilePaths"></param>
		/// <param name="zipFilePath"></param>
		/// <param name="entryNameEncoding"></param>
		public static void CreateZip(
			string[] sourceDirectoryPaths,
			string[] sourceFilePaths,
			string zipFilePath,
			Encoding entryNameEncoding)
		{
			try
			{
				using (ZipFile zipFile = new ZipFile())
				{
					zipFile.AlternateEncoding = entryNameEncoding;
					zipFile.AlternateEncodingUsage = ZipOption.AsNecessary;

					// Directory
					if (sourceDirectoryPaths != null && sourceDirectoryPaths.Length > 0)
					{
						foreach (string sourceDirectoryPath in sourceDirectoryPaths)
						{
							zipFile.AddDirectory(sourceDirectoryPath, FileUtil.GetDirectoryNameByDirectory(sourceDirectoryPath));
						}
					}

					// File
					if (sourceFilePaths != null && sourceFilePaths.Length > 0)
					{
						foreach (string sourceFilePath in sourceFilePaths)
						{
							ZipEntry zipEntry = zipFile.AddFile(sourceFilePath);
							zipEntry.FileName = FileUtil.GetFileName(sourceFilePath);
						}
					}

					// Save zip file
					zipFile.Save(zipFilePath);
				}
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Extract zip file with target directory
		/// </summary>
		/// <param name="zipFilePath"></param>
		public static void ExtractZip(
			string zipFilePath)
		{
			ExtractZip(zipFilePath, FileUtil.GetDirectoryPath(zipFilePath), Encoding.UTF8);
		}

		/// <summary>
		/// Extract zip file with target directory
		/// </summary>
		/// <param name="zipFilePath"></param>
		/// <param name="destinationDirectoryPath"></param>
		public static void ExtractZip(
			string zipFilePath,
			string destinationDirectoryPath)
		{
			ExtractZip(zipFilePath, destinationDirectoryPath, Encoding.UTF8);
		}

		/// <summary>
		/// Extract zip file with target directory
		/// </summary>
		/// <param name="zipFilePath"></param>
		/// <param name="destinationDirectoryPath"></param>
		/// <param name="entryNameEncoding"></param>
		public static void ExtractZip(
			string zipFilePath,
			string destinationDirectoryPath,
			Encoding entryNameEncoding)
		{
			using (ZipFile zipFile = ZipFile.Read(zipFilePath))
			{
				zipFile.AlternateEncoding = entryNameEncoding;
				zipFile.AlternateEncodingUsage = ZipOption.AsNecessary;

				foreach (ZipEntry entry in zipFile)
				{
					entry.Extract(destinationDirectoryPath);
				}
			}
		}

		#endregion
	}
}
