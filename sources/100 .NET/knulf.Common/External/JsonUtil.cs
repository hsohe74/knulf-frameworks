﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace knulf.Utility
{
	/// <summary>
	/// Utilities of Json handling with Newtonsoft.Json library
	/// </summary>
	/// <history>
	/// 2017-02-27 (hsohe74) first generate and modify
	/// </history>
	public class JsonUtil
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Common
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Common

		/// <summary>
		/// Get json escape value 
		/// </summary>
		public static string JsonEscapeValue(string source)
		{
			string result = string.Empty;

			if (string.IsNullOrEmpty(source) == false)
			{
				result = source;
				result = result.Replace("\"", "\\\"");
				result = result.Replace("\\", "\\\\");
				result = result.Replace("/", "\\/");
				result = result.Replace(Environment.NewLine, "\\n");
			}

			return result;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Parse
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Parse
		
		/// <summary>
		/// Get json object for linq
		/// </summary>
		/// <param name="jsonString"></param>
		/// <returns></returns>
		public static JObject GetJsonObject(string jsonString)
		{
			JObject jsonObject = null;

			try
			{
				jsonObject = JObject.Parse(jsonString);
			}
			catch
			{
				jsonObject = null;
			}

			return jsonObject;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Serialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Serialize

		/// <summary>
		/// Serialize json from object
		/// </summary>
		public static string Serialize(object value)
		{
			return Serialize(value, false);
		}

		/// <summary>
		/// Serialize json from object
		/// </summary>
		public static string Serialize(object value, bool isIndented)
		{
			JsonSerializerSettings setting = new JsonSerializerSettings()
			{
				NullValueHandling = NullValueHandling.Ignore
			};

			return Serialize(value, false, setting);
		}

		/// <summary>
		/// Serialize json from object
		/// </summary>
		public static string Serialize(object value, bool isIndented, JsonSerializerSettings setting)
		{
			string result = string.Empty;

			try
			{
				if (value != null)
				{
					if (isIndented)
						result = JsonConvert.SerializeObject(value, Formatting.Indented, setting);
					else
						result = JsonConvert.SerializeObject(value, Formatting.None, setting);
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Deserialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Deserialize

		/// <summary>
		/// Deserialize object from json string
		/// </summary>
		public static T Deserialize<T>(string jsonString)
		{
			T result = default(T);

			try
			{
				if (string.IsNullOrEmpty(jsonString) == false)
				{
					result = JsonConvert.DeserializeObject<T>(jsonString);
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		/// <summary>
		/// Deserialize object from json string
		/// </summary>
		public static object Deserialize(string jsonString, Type type)
		{
			object result = null;

			try
			{
				if (string.IsNullOrEmpty(jsonString) == false)
				{
					result = JsonConvert.DeserializeObject(jsonString, type);
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		#endregion
	}
}
