﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Utility
{
	/// <summary>
	/// Utilities of Rest client handling with RestSharp library
	/// </summary>
	public class RestClientUtil
	{
		private static readonly int _REQUEST_TIMEOUT = 1000 * 60 * 60;

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// ENUM
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region ENUM

		/// <summary>
		/// RestSharp's Method
		/// </summary>
		public enum Method
		{
			GET = 0,
			POST = 1,
			PUT = 2,
			DELETE = 3,
			HEAD = 4,
			OPTIONS = 5,
			PATCH = 6,
			MERGE = 7
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Execute request
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Execute request

		/// <summary>
		/// Execute request
		/// </summary>
		public static ReturnData ExecuteRequest<T>(
			string baseUrl,
			string targetUrl,
			Method method,
			Dictionary<string, object> formData = null,
			Dictionary<string, object> fileData = null,
			Dictionary<string, string> additionalHeaderData = null) where T : new()
		{
			ReturnData returnData = new ReturnData();

			// Make client
			var client = new RestClient();
			client.BaseUrl = new Uri(baseUrl);

			// Make request
			var request = new RestRequest(TypeUtil.GetEnumByName<RestSharp.Method>(method.ToString()));
			request.Timeout = _REQUEST_TIMEOUT;
			request.Resource = targetUrl;

			// Additional header data
			if (additionalHeaderData != null && additionalHeaderData.Count > 0)
			{
				foreach (var key in additionalHeaderData.Keys)
				{
					request.AddHeader(key, additionalHeaderData[key]);
				}
			}

			// AddFile
			if (fileData != null && fileData.Count > 0)
			{
				foreach(string key in fileData.Keys)
				{
					request.AddFile(key, Convert.ToString(fileData[key]));
				}
			}

			// AddParameter
			if (formData != null && formData.Count > 0)
			{
				foreach (string key in formData.Keys)
				{
					request.AddParameter(key, formData[key]);
				}
			}

			// Result of request
			try
			{
				var response = client.Execute(request);
				if (response.ErrorException != null)
				{
					returnData.SetData(response.ErrorException);
				}
				else if (response.StatusCode != HttpStatusCode.OK)
				{
					returnData.SetData(false, response.StatusCode.ToString(), string.Format("{0} {1}", response.StatusDescription));
				}
				else
				{
					returnData = new ReturnData();
					returnData.Success = true;
					returnData.Data = JsonUtil.Deserialize<T>(response.Content);
				}
			}
			catch (Exception ex)
			{
				returnData.SetData(ex);
			}

			return returnData;
		}

		/// <summary>
		/// Execute request
		/// </summary>
		public static ReturnData ExecuteRequest(
			string baseUrl,
			string targetUrl,
			Method method,
			Dictionary<string, object> formData = null,
			Dictionary<string, object> fileData = null,
			Dictionary<string, string> additionalHeaderData = null)
		{
			ReturnData returnData = new ReturnData();

			// Make client
			var client = new RestClient();
			client.BaseUrl = new Uri(baseUrl);

			// Make request
			var request = new RestRequest(TypeUtil.GetEnumByName<RestSharp.Method>(method.ToString()));
			request.Timeout = _REQUEST_TIMEOUT;
			request.Resource = targetUrl;

			// Additional header data
			if (additionalHeaderData != null && additionalHeaderData.Count > 0)
			{
				foreach (var key in additionalHeaderData.Keys)
				{
					request.AddHeader(key, additionalHeaderData[key]);
				}
			}

			// AddFile
			if (fileData != null && fileData.Count > 0)
			{
				foreach (string key in fileData.Keys)
				{
					request.AddFile(key, Convert.ToString(fileData[key]));
				}
			}

			// AddParameter
			if (formData != null && formData.Count > 0)
			{
				foreach (string key in formData.Keys)
				{
					request.AddParameter(key, formData[key]);
				}
			}

			// Result of request
			try
			{
				var response = client.Execute(request);
				if (response.ErrorException != null)
				{
					returnData.SetData(response.ErrorException);
				}
				else if (response.StatusCode != HttpStatusCode.OK)
				{
					returnData.SetData(false, response.StatusCode.ToString(), string.Format("{0} {1}", response.StatusDescription));
				}
				else
				{
					returnData = new ReturnData();
					returnData.Success = true;
					returnData.Data = response.Content;
				}
			}
			catch (Exception ex)
			{
				returnData.SetData(ex);
			}

			return returnData;
		}

		/// <summary>
		/// Execute request with RequestBody
		/// </summary>
		public static ReturnData ExecuteRequest<T>(
			string baseUrl,
			string targetUrl,
			Method method,
			object sendData,
			Dictionary<string, string> additionalHeaderData = null) where T : new()
		{
			ReturnData returnData = new ReturnData();

			// Make client
			var client = new RestClient();
			client.BaseUrl = new Uri(baseUrl);

			// Make request
			var request = new RestRequest(TypeUtil.GetEnumByName<RestSharp.Method>(method.ToString()));
			request.Timeout = _REQUEST_TIMEOUT;
			request.Resource = targetUrl;

			// Additional header data
			if (additionalHeaderData != null && additionalHeaderData.Count > 0)
			{
				foreach (var key in additionalHeaderData.Keys)
				{
					request.AddHeader(key, additionalHeaderData[key]);
				}
			}

			// AddParameter
			request.AddParameter("application/json", JsonUtil.Serialize(sendData), ParameterType.RequestBody);
			request.AddHeader("Content-Type", "application/json");

			// Result of request
			try
			{
				var response = client.Execute(request);
				if (response.ErrorException != null)
				{
					returnData.SetData(response.ErrorException);
				}
				else if (response.StatusCode != HttpStatusCode.OK)
				{
					returnData.SetData(false, response.StatusCode.ToString(), string.Format("{0} {1}", response.StatusDescription));
				}
				else
				{
					returnData = new ReturnData();
					returnData.Success = true;
					try
					{
						returnData.Data = JsonUtil.Deserialize<T>(response.Content);
					}
					catch
					{
						returnData.Success = false;
						returnData.Data = response.Content;
					}
				}
			}
			catch (Exception ex)
			{
				returnData.SetData(ex);
			}

			return returnData;
		}

		/// <summary>
		/// Execute request with RequestBody
		/// </summary>
		public static ReturnData ExecuteRequest(
			string baseUrl,
			string targetUrl,
			Method method,
			object sendData,
			Dictionary<string, string> additionalHeaderData = null)
		{
			ReturnData returnData = new ReturnData();

			// Make client
			var client = new RestClient();
			client.BaseUrl = new Uri(baseUrl);

			// Make request
			var request = new RestRequest(TypeUtil.GetEnumByName<RestSharp.Method>(method.ToString()));
			request.Timeout = _REQUEST_TIMEOUT;
			request.Resource = targetUrl;

			// Additional header data
			if (additionalHeaderData != null && additionalHeaderData.Count > 0)
			{
				foreach (var key in additionalHeaderData.Keys)
				{
					request.AddHeader(key, additionalHeaderData[key]);
				}
			}

			// AddParameter
			request.AddParameter("application/json", JsonUtil.Serialize(sendData), ParameterType.RequestBody);
			request.AddHeader("Content-Type", "application/json");

			// Result of request
			try
			{
				var response = client.Execute(request);
				if (response.ErrorException != null)
				{
					returnData.SetData(response.ErrorException);
				}
				else if (response.StatusCode != HttpStatusCode.OK)
				{
					returnData.SetData(false, response.StatusCode.ToString(), string.Format("{0} {1}", response.StatusDescription));
				}
				else
				{
					returnData = new ReturnData();
					returnData.Success = true;
					returnData.Data = response.Content;
				}
			}
			catch (Exception ex)
			{
				returnData.SetData(ex);
			}

			return returnData;
		}

		/// <summary>
		/// Execute request
		/// </summary>
		public static ReturnData ExecuteRequestXml(
			string baseUrl,
			string targetUrl,
			Method method,
			Dictionary<string, string> additionalHeaderData = null)
		{
			ReturnData returnData = new ReturnData();

			// Make client
			var client = new RestClient();
			client.ClearHandlers();
			client.BaseUrl = new Uri(baseUrl);

			// Make request
			var request = new RestRequest(TypeUtil.GetEnumByName<RestSharp.Method>(method.ToString()));
			request.Timeout = _REQUEST_TIMEOUT;
			request.AddHeader("Accept", "application/xml");
			request.RequestFormat = DataFormat.Xml;
			request.Parameters.Clear();
			request.AddParameter("application/xml", string.Empty, ParameterType.RequestBody);
			request.Resource = targetUrl;

			// Additional header data
			if (additionalHeaderData != null && additionalHeaderData.Count > 0)
			{
				foreach (var key in additionalHeaderData.Keys)
				{
					request.AddHeader(key, additionalHeaderData[key]);
				}
			}

			// Result of request
			try
			{
				var response = client.Execute(request);
				if (response.ErrorException != null)
				{
					returnData.SetData(response.ErrorException);
				}
				else if (response.StatusCode != HttpStatusCode.OK)
				{
					returnData.SetData(false, response.StatusCode.ToString(), string.Format("{0} {1}", response.StatusDescription));
				}
				else
				{
					returnData.SetData(true, null, response.Content);
				}
			}
			catch (Exception ex)
			{
				returnData.SetData(ex);
			}

			return returnData;
		}

		#endregion
	}
}
