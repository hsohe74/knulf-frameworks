﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knulf.Language
{
	public class LanManager
	{
		/// <summary>
		/// Set language by culture name
		/// </summary>
		/// <param name="cultureName"></param>        
		public static void SetLanguage(string cultureName)
		{
			System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.GetCultureInfo(cultureName);
			System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo(cultureName);
		}

		/// <summary>
		/// Set language by culture name
		/// </summary>
		/// <param name="thread"></param>
		/// <param name="cultureName"></param>  
		public static void SetLanguage(System.Threading.Thread thread, string cultureName)
		{
			thread.CurrentUICulture = new System.Globalization.CultureInfo(cultureName);
		}
	}
}
