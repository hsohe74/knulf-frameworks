﻿namespace DirectoryFileRename
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtConsole = new System.Windows.Forms.RichTextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnMove = new System.Windows.Forms.Button();
			this.btnSourceBrowse = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.txtSourceDirectory = new System.Windows.Forms.TextBox();
			this.btnRename = new System.Windows.Forms.Button();
			this.btnTargetBrowse = new System.Windows.Forms.Button();
			this.txtTargetDirectory = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.cbMoveToParent = new System.Windows.Forms.CheckBox();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// txtConsole
			// 
			this.txtConsole.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtConsole.Location = new System.Drawing.Point(10, 10);
			this.txtConsole.Name = "txtConsole";
			this.txtConsole.Size = new System.Drawing.Size(633, 238);
			this.txtConsole.TabIndex = 0;
			this.txtConsole.Text = "";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.cbMoveToParent);
			this.panel1.Controls.Add(this.btnMove);
			this.panel1.Controls.Add(this.btnSourceBrowse);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.txtSourceDirectory);
			this.panel1.Controls.Add(this.btnRename);
			this.panel1.Controls.Add(this.btnTargetBrowse);
			this.panel1.Controls.Add(this.txtTargetDirectory);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(653, 176);
			this.panel1.TabIndex = 1;
			// 
			// btnMove
			// 
			this.btnMove.Location = new System.Drawing.Point(535, 40);
			this.btnMove.Name = "btnMove";
			this.btnMove.Size = new System.Drawing.Size(100, 40);
			this.btnMove.TabIndex = 3;
			this.btnMove.Text = "Move";
			this.btnMove.UseVisualStyleBackColor = true;
			this.btnMove.Click += new System.EventHandler(this.btnMove_Click);
			// 
			// btnSourceBrowse
			// 
			this.btnSourceBrowse.Location = new System.Drawing.Point(535, 11);
			this.btnSourceBrowse.Name = "btnSourceBrowse";
			this.btnSourceBrowse.Size = new System.Drawing.Size(100, 23);
			this.btnSourceBrowse.TabIndex = 2;
			this.btnSourceBrowse.Text = "Browse";
			this.btnSourceBrowse.UseVisualStyleBackColor = true;
			this.btnSourceBrowse.Click += new System.EventHandler(this.btnSourceBrowse_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(24, 17);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(106, 12);
			this.label2.TabIndex = 5;
			this.label2.Text = "Source directory: ";
			// 
			// txtSourceDirectory
			// 
			this.txtSourceDirectory.Location = new System.Drawing.Point(132, 12);
			this.txtSourceDirectory.Name = "txtSourceDirectory";
			this.txtSourceDirectory.Size = new System.Drawing.Size(397, 21);
			this.txtSourceDirectory.TabIndex = 1;
			// 
			// btnRename
			// 
			this.btnRename.Location = new System.Drawing.Point(535, 115);
			this.btnRename.Name = "btnRename";
			this.btnRename.Size = new System.Drawing.Size(100, 40);
			this.btnRename.TabIndex = 13;
			this.btnRename.Text = "Rename";
			this.btnRename.UseVisualStyleBackColor = true;
			this.btnRename.Click += new System.EventHandler(this.btnRename_Click);
			// 
			// btnTargetBrowse
			// 
			this.btnTargetBrowse.Location = new System.Drawing.Point(535, 86);
			this.btnTargetBrowse.Name = "btnTargetBrowse";
			this.btnTargetBrowse.Size = new System.Drawing.Size(100, 23);
			this.btnTargetBrowse.TabIndex = 12;
			this.btnTargetBrowse.Text = "Browse";
			this.btnTargetBrowse.UseVisualStyleBackColor = true;
			this.btnTargetBrowse.Click += new System.EventHandler(this.btnTargetBrowse_Click);
			// 
			// txtTargetDirectory
			// 
			this.txtTargetDirectory.Location = new System.Drawing.Point(132, 87);
			this.txtTargetDirectory.Name = "txtTargetDirectory";
			this.txtTargetDirectory.Size = new System.Drawing.Size(397, 21);
			this.txtTargetDirectory.TabIndex = 11;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(24, 92);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(102, 12);
			this.label1.TabIndex = 0;
			this.label1.Text = "Target directory: ";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.txtConsole);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 176);
			this.panel2.Name = "panel2";
			this.panel2.Padding = new System.Windows.Forms.Padding(10);
			this.panel2.Size = new System.Drawing.Size(653, 258);
			this.panel2.TabIndex = 2;
			// 
			// cbMoveToParent
			// 
			this.cbMoveToParent.AutoSize = true;
			this.cbMoveToParent.Location = new System.Drawing.Point(132, 115);
			this.cbMoveToParent.Name = "cbMoveToParent";
			this.cbMoveToParent.Size = new System.Drawing.Size(161, 16);
			this.cbMoveToParent.TabIndex = 14;
			this.cbMoveToParent.Text = "Move to parent directory";
			this.cbMoveToParent.UseVisualStyleBackColor = true;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(653, 434);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
			this.Load += new System.EventHandler(this.Form1_Load);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.RichTextBox txtConsole;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnRename;
		private System.Windows.Forms.Button btnTargetBrowse;
		private System.Windows.Forms.TextBox txtTargetDirectory;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtSourceDirectory;
		private System.Windows.Forms.Button btnMove;
		private System.Windows.Forms.Button btnSourceBrowse;
		private System.Windows.Forms.CheckBox cbMoveToParent;
	}
}

