﻿using knulf.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DirectoryFileRename
{
	public partial class Form1 : Form
	{

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constructer & Initialize
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Constructer & Initialize

		public Form1()
		{
			InitializeComponent();
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Event handler
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Event handler

		private void Form1_Load(object sender, EventArgs e)
		{
			// SOURCE_DIRECTORY
			var sourceDirectoryPath = ConfigurationManager.AppSettings["SOURCE_DIRECTORY"];
			if (string.IsNullOrEmpty(sourceDirectoryPath) == false)
			{
				this.txtSourceDirectory.Text = sourceDirectoryPath;
			}

			// TARGET_DIRECTORY
			var targetDirectoryPath = ConfigurationManager.AppSettings["TARGET_DIRECTORY"];
			if (string.IsNullOrEmpty(targetDirectoryPath) == false)
			{
				this.txtTargetDirectory.Text = targetDirectoryPath;
			}
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			// SOURCE_DIRECTORY
			if (string.IsNullOrEmpty(this.txtSourceDirectory.Text) == false)
			{
				Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
				config.AppSettings.Settings.Remove("SOURCE_DIRECTORY");
				config.AppSettings.Settings.Add("SOURCE_DIRECTORY", this.txtSourceDirectory.Text);
				config.Save(ConfigurationSaveMode.Minimal);
			}

			// TARGET_DIRECTORY
			if (string.IsNullOrEmpty(this.txtTargetDirectory.Text) == false)
			{
				Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
				config.AppSettings.Settings.Remove("TARGET_DIRECTORY");
				config.AppSettings.Settings.Add("TARGET_DIRECTORY", this.txtTargetDirectory.Text);
				config.Save(ConfigurationSaveMode.Minimal);
			}
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// 파일 이동
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region 파일 이동

		private void btnSourceBrowse_Click(object sender, EventArgs e)
		{
			FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
			folderBrowserDialog.SelectedPath = this.txtSourceDirectory.Text;
			if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
			{
				this.txtSourceDirectory.Text = folderBrowserDialog.SelectedPath;
			}
		}

		private void btnMove_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(this.txtSourceDirectory.Text) == false
				&& string.IsNullOrEmpty(this.txtTargetDirectory.Text) == false)
			{
				var dailyDirectoryPaths = FileUtil.GetSubDirectories(this.txtSourceDirectory.Text, System.IO.SearchOption.TopDirectoryOnly);
				if (dailyDirectoryPaths != null && dailyDirectoryPaths.Length > 0)
				{
					foreach (string dailyDirectoryPath in dailyDirectoryPaths)
					{
						var dailyDirectoryName = FileUtil.GetDirectoryNameByDirectory(dailyDirectoryPath);
						var picDirectoryPaths = FileUtil.GetSubDirectories(dailyDirectoryPath, System.IO.SearchOption.TopDirectoryOnly);
						if (picDirectoryPaths != null && picDirectoryPaths.Length > 0)
						{
							foreach (string picDirectoryPath in picDirectoryPaths)
							{
								var picDirectoryNameOriginal = FileUtil.GetDirectoryNameByDirectory(picDirectoryPath);
								var picDirectoryName = FileUtil.GetDirectoryNameByDirectory(picDirectoryPath);
								var targetDirectoryPath = Path.Combine(this.txtTargetDirectory.Text, picDirectoryName);
								if (System.IO.Directory.Exists(targetDirectoryPath))
								{
									picDirectoryName = picDirectoryName + "_" + dailyDirectoryName;
									Directory.Move(
										Path.Combine(dailyDirectoryPath, picDirectoryNameOriginal),
										Path.Combine(dailyDirectoryPath, picDirectoryName)
									);

									// 로그
									this.WriteConsole("\t중복이름 변경 {0} => {1}", picDirectoryNameOriginal, picDirectoryName);
								}

								// ZIP 파일을 상위로 올림
								var zipFilepaths = FileUtil.GetFileListInDirectory(
									Path.Combine(dailyDirectoryPath, picDirectoryName),
									"*.zip",
									SearchOption.TopDirectoryOnly
								);
								if (zipFilepaths != null && zipFilepaths.Length > 0)
								{
									foreach(var zipFilepath in zipFilepaths)
									{
										var zipFilename = FileUtil.GetFileName(zipFilepath);
										FileUtil.MoveFile(
											Path.Combine(dailyDirectoryPath, picDirectoryName, zipFilename),
											Path.Combine(dailyDirectoryName, zipFilename)
										);
									}
								}

								// 대상 폴더로 이동
								Directory.Move(
									Path.Combine(dailyDirectoryPath, picDirectoryName),
									Path.Combine(this.txtTargetDirectory.Text, picDirectoryName)
								);

								// 로그
								this.WriteConsole(
									"\t이동 {0} => {1}", 
									Path.Combine(dailyDirectoryPath, picDirectoryName), 
									Path.Combine(this.txtTargetDirectory.Text, picDirectoryName)
								);
							}
						}
					}
				}
			}
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// 파일명 변경
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region 파일명 변경

		private void btnTargetBrowse_Click(object sender, EventArgs e)
		{
			FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
			folderBrowserDialog.SelectedPath = this.txtTargetDirectory.Text;
			if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
			{
				this.txtTargetDirectory.Text = folderBrowserDialog.SelectedPath;
			}
		}

		private void btnRename_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(this.txtTargetDirectory.Text) == false)
			{
				var subDirectoryPaths = FileUtil.GetSubDirectories(this.txtTargetDirectory.Text, System.IO.SearchOption.TopDirectoryOnly);
				if (subDirectoryPaths != null && subDirectoryPaths.Length > 0)
				{
					foreach (string directoryPath in subDirectoryPaths)
					{
						var directoryName = FileUtil.GetDirectoryNameByDirectory(directoryPath);
						this.WriteConsole("폴더 : {0}", directoryName);

						var filepaths = FileUtil.GetFileListInDirectory(directoryPath, System.IO.SearchOption.TopDirectoryOnly);
						for (int fileIdx = 1; fileIdx <= filepaths.Length; fileIdx++)
						{
							var filepath = filepaths[fileIdx - 1];
							var fileExtension = FileUtil.GetFileExtension(filepath);
							var oldFilename = FileUtil.GetFileNameWithoutExtension(filepath) + "." + fileExtension;
							var newFilename = directoryName + "_" + fileIdx.ToString().PadLeft(3, '0') + "." + fileExtension;

							// Rename
							FileUtil.MoveFile(
								Path.Combine(directoryPath, oldFilename),
								Path.Combine(directoryPath, newFilename)
							);
							// Move to parent directory
							if (this.cbMoveToParent.Checked)
							{
								FileUtil.MoveFile(
									Path.Combine(directoryPath, newFilename),
									Path.Combine(this.txtTargetDirectory.Text, newFilename)
								);
							}

							// 로그
							this.WriteConsole("\t {0} => {1}", oldFilename, newFilename);
						}
					}
				}
			}
		}

		#endregion

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// Method : Console
		////////////////////////////////////////////////////////////////////////////////////////////////////

		#region Method : Console

		private void Delay(int ms)
		{
			DateTime dateTimeNow = DateTime.Now;
			TimeSpan duration = new TimeSpan(0, 0, 0, 0, ms);
			DateTime dateTimeAdd = dateTimeNow.Add(duration);
			while (dateTimeAdd >= dateTimeNow)
			{
				System.Windows.Forms.Application.DoEvents();
				dateTimeNow = DateTime.Now;
			}
			return;
		}

		private void WriteConsole(Exception ex)
		{
			this.WriteConsole(ex.ToString());
		}

		private void WriteConsole(string format, params string[] args)
		{
			this.WriteConsole(string.Format(format, args));
		}

		private void WriteConsole(string message)
		{
			if (this.txtConsole.InvokeRequired)
			{
				this.txtConsole.BeginInvoke(new Action(() =>
				{
					this.txtConsole.AppendText("[" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] " + message + "\r\n");
					this.txtConsole.ScrollToCaret();
				}));
			}
			else
			{
				this.txtConsole.AppendText("[" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] " + message + "\r\n");
				this.txtConsole.ScrollToCaret();
			}
		}

		private void ClearConsole()
		{
			if (this.txtConsole.InvokeRequired)
			{
				this.txtConsole.BeginInvoke(new Action(() =>
				{
					this.txtConsole.Clear();
				}));
			}
			else
			{
				this.txtConsole.Clear();
			}
		}

		#endregion
	}
}
