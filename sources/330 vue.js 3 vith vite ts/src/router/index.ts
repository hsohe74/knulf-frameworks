import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/:pathMatch(.*)*',
            name: 'NotFound404',
            meta: { requiredAuth: false, requiredAuthMsg: false },
            component: () => import('@/views/404.vue')
        },
        {
            path: '/',
            name: 'Index',
            component: () => import('@/views/index.vue')
        }
    ]
})

export default router
