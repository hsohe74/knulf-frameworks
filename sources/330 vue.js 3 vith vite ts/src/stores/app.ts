/**
 * App store
 * - LocalStorage에 저장
 * - 로케일 정보, 로그인정보, 공용 데이터 등
 */
import { ref, reactive } from 'vue'
import type { Ref } from 'vue'
import { defineStore } from 'pinia'
import { useRouter } from 'vue-router'
import axios from '@/plugins/axios'
import i18n from '@/locales'
import jwtDecode from 'jwt-decode'
import fileDownload from 'js-file-download'
import { setting } from '@/variables'
import * as dataUtil from '@/utils/data'

export const useAppStore = defineStore(
    'app',
    () => {
        /**
         * Reference
         */
        const router = useRouter()

        /**********************************************************************/
        /* 로케일 */
        /**********************************************************************/

        /**
         * 로케일
         */

        const locale: Ref<string> = ref(i18n.global.locale.value)
        const locales = reactive([
            { label: 'Korean', value: 'ko' },
            { label: 'English', value: 'en' }
        ])
        const initLocale = (): void => {
            i18n.global.locale.value = locale.value
        }
        const changeLocale = (val: string): void => {
            locale.value = val
            i18n.global.locale.value = val
            router.go(0)
        }

        /**********************************************************************/
        /* 섹션 */
        /**********************************************************************/

        /**
         * 섹션
         */
        const sectionName: Ref<string> = ref('')
        const setSectionName = (name: string): void => {
            sectionName.value = name
        }

        /**********************************************************************/
        /* 로그인 데이터 */
        /**********************************************************************/

        /**
         * 로그인 데이터
         */
        const loginToken = reactive({
            accessToken: '',
            refreshToken: ''
        })
        const loginData = reactive({
            UserName: '',
            UserId: '',
            UserType: ''
        })
        const loginFormData = reactive({
            SubscriberId: '',
            SiteHost: '',
            LoginId: '',
            LoginPw: '',
            UserTypeCd: ''
        })
        const initLoginData = (userType: string) => {
            return new Promise((resolve) => {
                loginData.UserName = ''
                loginData.UserId = ''
                loginData.UserType = userType ? userType : loginData.UserType
                loginToken.accessToken = ''
                loginToken.refreshToken = ''
                // 결과반환
                resolve({})
            })
        }
        const setLoginToken = (paramObj: any): void => {
            loginToken.accessToken = paramObj.AccessToken
            loginToken.refreshToken = paramObj.RefreshToken
        }
        const setLoginData = (paramObj: any): void => {
            setLoginToken(paramObj)
            setLoginDataFromToken()
        }
        const setLoginDataFromToken = (): void => {
            if (loginToken.accessToken) {
                const userData: any = jwtDecode(loginToken.accessToken)
                loginData.UserName = userData.unique_name
                loginData.UserId = userData.nameid
                loginData.UserType = userData.role
            }
        }

        /**********************************************************************/
        /* 다운로드 */
        /**********************************************************************/

        /**
         * 파일 다운로드
         * @param {*} paramObj filePath, fileName
         */
        const downloadFile = (paramObj: any) => {
            // 파일 저장경로가 윈도즈인 경우를 한정해서 처리
            const reqObj = {
                FilePath: paramObj.filePath
            }
            return new Promise((reject) => {
                axios({
                    url: '/Common/Download',
                    method: 'POST',
                    responseType: 'blob',
                    data: reqObj
                })
                    .then((response) => {
                        let filename = ''
                        if (!paramObj.filename) {
                            const disposition = response.headers['content-disposition']
                            if (disposition && disposition.indexOf('attachment') > -1) {
                                const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/
                                const matches = filenameRegex.exec(disposition)
                                if (matches != null && matches[1]) {
                                    filename = matches[1].replace(/['"]/g, '')
                                }
                            }
                        } else {
                            filename = paramObj.filename
                        }
                        fileDownload(response.data, decodeURI(filename))
                    })
                    .catch((error) => {
                        reject(dataUtil.getResponseData(error.response))
                    })
            })
        }

        /**
         * 이미지 다운로드
         * 다운로드 데이터를 base64기반으로 변환. 큰 이미지는 지양.
         * 리턴받은 response를 data에 저장하고 image태그의 src에 이를 반영한다.
         * @param {*} paramObj filePath
         */
        const downloadImage = (paramObj: any) => {
            const reqObj = {
                FilePath: paramObj.filePath
            }
            return new Promise((resolve, reject) => {
                axios({
                    url: '/Common/Download',
                    method: 'POST',
                    responseType: 'arraybuffer',
                    data: reqObj
                })
                    .then((response) => {
                        const bytes = new Uint8Array(response.data)
                        const binary = bytes.reduce((data, b) => (data += String.fromCharCode(b)), '')
                        resolve('data:image/jpeg;base64,' + btoa(binary))
                    })
                    .catch((error) => {
                        reject(dataUtil.getResponseData(error.response))
                    })
            })
        }

        /**
         * Base64 인코딩된 이미지 다운로드
         * @param {*} paramObj base64Image, filename
         */
        const downloadBase64Image = (paramObj: any) => {
            const a = document.createElement('a')
            const url = paramObj.base64Image
            a.href = url
            a.download = paramObj.filename
            document.body.appendChild(a)
            a.click()
            setTimeout(() => {
                document.body.removeChild(a)
                window.URL.revokeObjectURL(url)
            }, 0)
        }

        /**********************************************************************/
        /* Common */
        /**********************************************************************/

        /**
         * Common
         */
        const listParam = reactive({
            SearchColumn: '',
            SearchWord: '',
            TotalRecord: 0,
            TotalPage: 0,
            PageSize: setting.defaultPageSize,
            PageNumber: 1,
            PageNumbers: [],
            TotalBlock: 0,
            BlockSize: setting.defaultBlockSize,
            BlockNumber: 0
        })

        /**
         * Return
         */
        return {
            /* 로케일 */
            locale,
            locales,
            initLocale,
            changeLocale,
            /* 섹션명 */
            sectionName,
            setSectionName,
            /* 로그인 데이터 */
            loginToken,
            loginData,
            loginFormData,
            initLoginData,
            setLoginToken,
            setLoginData,
            setLoginDataFromToken,
            /* Download */
            downloadFile,
            downloadImage,
            downloadBase64Image,
            /* Common */
            listParam
        }
    },
    {
        /**
         * Local storage
         */
        persist: {
            storage: localStorage
        }
    }
)
