export const onlynumber = {
    mounted: (el: any) => {
        el.oninput = (event: any) => {
            const formattedValue = parseInt(event.target.value)
            el.value = isNaN(formattedValue) ? '' : formattedValue
        }
    }
}
