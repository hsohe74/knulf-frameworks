import { focus } from './focus'
import { onlynumber } from './onlynumber'

export default {
    install(app: any) {
        app.directive('focus', focus)
        app.directive('onlynumber', onlynumber)
    }
}
