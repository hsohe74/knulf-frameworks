/* Popup */
export const showPopup = Symbol('ShowPopup')

/* Tab */
export const tabsProvider = Symbol('TabsProvider')
export const addTab = Symbol('AddTab')
export const removeTab = Symbol('RemoveTab')
