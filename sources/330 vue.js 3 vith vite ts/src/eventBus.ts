import { TinyEmitter } from 'tiny-emitter'

const emitter = new TinyEmitter()

export default {
    $on: (event: string, callback: Function, ctx?: any) => emitter.on(event, callback, ctx),
    $once: (event: string, callback: Function, ctx?: any) => emitter.once(event, callback, ctx),
    $off: (event: string, ...args: any[]) => emitter.off(event, ...args),
    $emit: (event: string, callback?: Function) => emitter.emit(event, callback)
}
