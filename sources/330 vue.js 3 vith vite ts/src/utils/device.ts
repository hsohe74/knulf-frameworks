/**
 * 모바일 체크인지 확인
 */
export const isMobile = () => {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        return true
    } else {
        return false
    }
}

/**
 * Livespot 안드로이드앱 인지 여부
 * @returns 안드로이드 앱 여부
 */
export const isAndroidApp = () => {
    if (/APP_LIVESPOT_Android/i.test(navigator.userAgent)) {
        return true
    } else {
        return false
    }
}
