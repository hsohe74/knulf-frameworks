/**
 * 엘리먼트 이동 시장
 * @param {*} evt
 */
export const startMoving = function (evt: any) {
    const elem = evt.target.parentElement

    const posX: number = evt.clientX
    const posY: number = evt.clientY

    const offsets: any = elem.getBoundingClientRect()
    const elemLeft: number = parseInt(offsets.left)
    const elemTop: number = parseInt(offsets.top)

    const diffX = posX - elemLeft
    const diffY = posY - elemTop

    elem.style.cursor = 'move'
    document.onmousemove = function (evt) {
        const posX: number = evt.clientX
        const posY: number = evt.clientY
        let aX: number = posX - diffX
        let aY: number = posY - diffY

        if (aX < 0) aX = 0
        if (aY < 0) aY = 0
        doMoving(elem, aX, aY)
    }
}

/**
 * 엘리먼트 이동
 * @param {*} elem
 * @param {*} aX
 * @param {*} aY
 */
export const doMoving = function (elem: any, aX: number, aY: number) {
    elem.style.left = aX.toString() + 'px'
    elem.style.top = aY.toString() + 'px'
}

/**
 * 엘리먼트 이동 중지
 * @param {*} evt
 */
export const stopMoving = function (evt: any) {
    const elem = evt.target.parentElement
    elem.style.cursor = 'default'
    document.onmousemove = function () {}
}
