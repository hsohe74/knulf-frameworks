import axios from 'axios'
import { useRouter } from 'vue-router'
import { useAppStore } from '@/stores/app'
import { setting } from '@/variables'
import * as netUtil from '@/utils/net'

// 새 토큰을 받고 재요청인지 여부
let isRefreshing = false

/**
 * 실패 프로세스 Queue (401이 되었는데도 다른곳에 요청하는 것을 방지)
 */
let failedQueue: any[] = []
const processQueue = (error: any, token: string | null = null) => {
    failedQueue.forEach((prom) => {
        if (error) {
            prom.reject(error)
        } else {
            prom.resolve(token)
        }
    })

    failedQueue = []
}

/**
 * axios 인스턴스를 만들어 이를 사용.
 */
const axiosInstance = axios.create({
    baseURL: setting.apiBaseURL
})

/**
 * 요청 (Request) interceptor
 */
axiosInstance.interceptors.request.use(
    async (config) => {
        // Reference
        const appStore = useAppStore()

        // 헤더 토큰을 발급받은 토큰으로 설정
        config.headers.Authorization = 'Bearer ' + appStore.loginToken.accessToken
        // Locale
        if (config.data !== undefined) {
            // config.data.append('locale', store.state.app.locale);
        }
        // 반환
        return config
    },
    (error) => {
        return Promise.reject(error)
    }
)

/**
 * 응답 (Response) interceptor
 */
axiosInstance.interceptors.response.use(
    (response) => {
        return response
    },
    async (error) => {
        // Reference
        const appStore = useAppStore()
        const router = useRouter()

        // 원본 요청
        const originalRequest = error.config

        // 401일 경우
        if (error.response.status === 401 && !originalRequest._retry) {
            // 첫 401 발생한 이후 요청인 경우 failedQueue에 추가
            if (isRefreshing) {
                return new Promise((resolve, reject) => {
                    failedQueue.push({ resolve, reject })
                })
                    .then(() => {
                        originalRequest.headers.Authorization = 'Bearer ' + appStore.loginToken.accessToken
                        return axios(originalRequest)
                    })
                    .catch((err) => {
                        return Promise.reject(err)
                    })
            }

            // 재요청상태로 변경
            originalRequest._retry = true
            isRefreshing = true

            // RefreshToken를 사용하여 토큰갱신 요청 (이요청하는 동안 오는 모든 Request는 큐에 넣음)
            const refreshToken = appStore.loginToken.refreshToken
            return axios
                .post(setting.apiBaseURL + '/User/RefreshToken', null, {
                    headers: { Authorization: 'Bearer ' + refreshToken }
                })
                .then((response) => {
                    if (response.data.Success) {
                        // 리턴값
                        const returnData = response.data.Data

                        // 토큰 쿠키 저장
                        appStore.setLoginToken(returnData)

                        // 헤더 토큰을 새로 발급받은 토큰으로 재설정
                        axiosInstance.defaults.headers.common.Authorization = 'Bearer ' + returnData.AccessToken
                        originalRequest.headers.Authorization = 'Bearer ' + returnData.AccessToken
                        processQueue(null, returnData.AccessToken)

                        // 반환
                        return axiosInstance(originalRequest)
                    } else {
                        // 로그인 데이터 초기화
                        appStore.initLoginData('')
                        // 로그인 화면 이동 (첫경로를 읽어서 경로 생성)
                        router.push('/' + netUtil.getFirstPath() + '/login').catch(() => {})
                        // Queue
                        processQueue(error, null)
                    }
                })
                .catch((err) => {
                    // UserType
                    const userType = appStore.loginData.UserType
                    // 로그인 데이터 초기화
                    appStore.initLoginData(userType)
                    // 로그인 화면 이동 (첫경로를 읽어서 경로 생성)
                    router.push('/' + netUtil.getFirstPath() + '/login').catch(() => {})
                    // Queue
                    processQueue(err, null)
                })
                .finally(() => {
                    isRefreshing = false
                })
        } else if (error.response.status === 601) {
            // UserType
            const userType = appStore.loginData.UserType
            // 로그인 데이터 초기화
            appStore.initLoginData(userType)
            // 로그인 화면 이동 (첫경로를 읽어서 경로 생성)
            router.push('/' + netUtil.getFirstPath() + '/login').catch(() => {})
            // Queue
            processQueue(error, null)
            isRefreshing = false
            throw new Error()
        }
    }
)

export default axiosInstance
