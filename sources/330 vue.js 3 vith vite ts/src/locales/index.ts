import { createI18n } from 'vue-i18n'
import ko from './ko.json'
import en from './en.json'

type MessageSchema = typeof ko
const i18n = createI18n<[MessageSchema], 'ko' | 'en'>({
    locale: 'ko',
    fallbackLocale: 'en',
    legacy: false,
    messages: {
        ko: ko,
        en: en
    },
    warnHtmlInMessage: 'off'
})

export default i18n
