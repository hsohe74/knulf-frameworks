import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [vue()],
    resolve: {
        extensions: ['.js', '.ts', '.vue', '.json'],
        alias: {
            '@': fileURLToPath(new URL('./src', import.meta.url))
        }
    },
    server: {
        port: 9193,
        proxy: {
            '/api': {
                target: 'http://localhost:9092',
                changeOrigin: true,
                secure: false
                //rewrite: (path) => path.replace('/api', '')
            }
        }
    },
    build: {
        assetsDir: 'resources/assets/'
    }
})
