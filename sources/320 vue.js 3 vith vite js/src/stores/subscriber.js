/**
 * Subscriber store
 * - 구독자
 */
import { ref, reactive } from 'vue'
import { defineStore } from 'pinia'
import { useAppStore } from '@/stores/app'
import axios from '@/plugins/axios'
import * as dataUtil from '@/utils/data'

export const useSubscriberStore = defineStore('subscriber', () => {
    /**
     * Reference
     */
    const appStore = useAppStore()

    /**********************************************************************/
    /* 구독자 */
    /**********************************************************************/

    /**
     * 구독자 목록
     */
    const subscriberParamInit = reactive({
        ...appStore.listParam
    })
    const subscriberParam = reactive({
        ...subscriberParamInit
    })
    const initSubscriberParam = () => {
        Object.assign(subscriberParam, subscriberParamInit)
    }
    const subscriberList = ref([])
    const initSubscriberList = () => {
        subscriberParam.TotalRecord = 0
        subscriberList.value = []
    }
    const setSubscriberList = (paramObj) => {
        subscriberParam.TotalRecord = paramObj.TotalRecord
        dataUtil.setPagingData(subscriberParam)
        subscriberList.value = paramObj.Data
    }
    const getSubscriberList = async () => {
        try {
            const response = await axios.post('/Subscriber/SubscriberList', subscriberParam)
            if (response.data.Success) {
                setSubscriberList(response.data)
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }
    const getSubscriberListRaw = async (paramObj) => {
        try {
            const response = await axios.post('/Subscriber/SubscriberList', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**
     * 구독자 상세
     */
    const subscriberDetail = ref({})
    const initSubscriberDetail = () => {
        subscriberDetail.value = {
            SubscriberSeq: 0
        }
    }
    const setSubscriberDetail = (paramObj) => {
        subscriberDetail.value = {
            ...paramObj
        }
    }
    const getSubscriberDetail = async (paramObj) => {
        try {
            const response = await axios.post('/Subscriber/SubscriberDetail', paramObj)
            if (response.data.Success) {
                setSubscriberDetail(response.data.Data)
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }
    const saveSubscriberDetail = async (paramObj) => {
        try {
            const response = await axios.post('/Subscriber/SubscriberSave', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }
    const deleteSubscriberDetail = async function (subscriberSeq) {
        const paramObj = {
            SubscriberSeq: subscriberSeq,
            StatusCd: '99'
        }
        try {
            const response = await axios.post('/Subscriber/SubscriberSetStatus', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**********************************************************************/
    /* 구독자 만료일 로그 */
    /**********************************************************************/

    /**
     * 구독자 만료일 로그 목록
     */
    let subscriptionLogParamInit = reactive({
        ...appStore.listParam,
        ...{
            SubscriberSeq: 0
        }
    })
    let subscriptionLogParam = reactive({
        ...subscriptionLogParamInit
    })
    const initSubscriptionLogParam = () => {
        Object.assign(subscriptionLogParam, subscriptionLogParamInit)
    }
    const subscriptionLogList = ref([])
    const initSubscriptionLogList = () => {
        subscriptionLogParam.TotalRecord = 0
        subscriptionLogList.value = []
    }
    const setSubscriptionLogList = (paramObj) => {
        subscriptionLogParam.TotalRecord = paramObj.TotalRecord
        dataUtil.setPagingData(subscriptionLogParam)
        subscriptionLogList.value = paramObj.Data
    }
    const getSubscriptionLogList = async () => {
        try {
            const response = await axios.post('/Subscriber/SubscriptionLogList', subscriptionLogParam)
            if (response.data.Success) {
                setSubscriptionLogList(response.data)
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    const renewSubscription = async (paramObj) => {
        try {
            const response = await axios.post('/Subscriber/SubscriptionRenew', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**********************************************************************/
    /* Common */
    /**********************************************************************/

    /**
     * Return
     */
    return {
        /* 구독자 */
        subscriberParam,
        initSubscriberParam,
        subscriberList,
        initSubscriberList,
        setSubscriberList,
        getSubscriberList,
        getSubscriberListRaw,
        subscriberDetail,
        initSubscriberDetail,
        setSubscriberDetail,
        getSubscriberDetail,
        saveSubscriberDetail,
        deleteSubscriberDetail,
        /* 구독자 만료일 로그 */
        subscriptionLogParam,
        initSubscriptionLogParam,
        subscriptionLogList,
        initSubscriptionLogList,
        setSubscriptionLogList,
        getSubscriptionLogList,
        renewSubscription
    }
})
