/**
 * Field store
 * - LocalStorage에 저장
 * - 로케일 정보, 로그인정보, 공용 데이터 등
 */
import { ref, reactive } from 'vue'
import { defineStore } from 'pinia'
import { useAppStore } from '@/stores/app'
import axios from '@/plugins/axios'
import * as dataUtil from '@/utils/data'

export const useFieldStore = defineStore('field', () => {
    /**
     * Reference
     */
    const appStore = useAppStore()

    /**********************************************************************/
    /* 사업현장 */
    /**********************************************************************/

    /**
     * 사업현장 목록
     */
    const fieldListParamInit = reactive({
        ...appStore.listParam,
        ...{
            SiteSeq: 0,
            SiteHost: '',
            SearchColumn: '',
            SearchWord: '',
            PageNumber: 1,
            PageSize: 0
        }
    })
    const fieldListParam = reactive({
        ...fieldListParamInit
    })
    const initFieldListParam = () => {
        Object.assign(fieldListParam, fieldListParamInit)
    }
    const fieldListData = ref([])
    const initFieldList = () => {
        fieldListParam.TotalRecord = 0
        fieldListData.value = []
    }
    const setFieldList = (paramObj) => {
        fieldListParam.TotalRecord = paramObj.TotalRecord
        dataUtil.setPagingData(fieldListParam)
        fieldListData.value = paramObj.Data
    }
    const getFieldList = async () => {
        try {
            const response = await axios.post('/Field/FieldList', fieldListParam)
            if (response.data.Success) {
                setFieldList(response.data)
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }
    const getFieldListRaw = async (paramObj) => {
        try {
            const response = await axios.post('/Field/FieldList', paramObj)
            if (response.data.Success) {
                return response
            } else {
                return Promise.reject(response)
            }
        } catch (error) {
            return Promise.reject(error.response)
        }
    }

    /**********************************************************************/
    /* Common */
    /**********************************************************************/

    /**
     * Return
     */
    return {
        /* 사업현장 */
        fieldListParam,
        initFieldListParam,
        fieldListData,
        initFieldList,
        setFieldList,
        getFieldList,
        getFieldListRaw
    }
})
