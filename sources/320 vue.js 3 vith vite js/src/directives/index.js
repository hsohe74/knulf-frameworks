import { focus } from './focus'
import { onlynumber } from './onlynumber'

export default {
    install(app) {
        app.directive('focus', focus)
        app.directive('onlynumber', onlynumber)
    }
}
