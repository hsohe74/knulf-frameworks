import { createRouter, createWebHistory } from 'vue-router'
import { useUserStore } from '@/stores/user'

import i18n from '@/locales'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/:pathMatch(.*)*',
            name: 'NotFound404',
            meta: { requiredAuth: false, requiredAuthMsg: false },
            component: () => import('@/views/404.vue')
        },
        {
            path: '/',
            name: 'Index',
            component: () => import('@/views/index.vue')
        },
        {
            path: '/sysAdmin/login',
            name: 'sysAdminlogin',
            meta: { requiredAuth: false, requiredAuthMsg: false },
            component: () => import('@/views/sysAdmin/login.vue')
        },
        {
            path: '/sysAdmin',
            name: 'sysAdmin',
            meta: { requiredAuth: true, requiredAuthMsg: false },
            component: () => import('@/views/sysAdmin/index.vue'),
            children: [
                {
                    path: '/sysAdmin/subscriber',
                    name: 'sysAdminsubscriber',
                    meta: { requiredAuth: true, requiredAuthMsg: true },
                    component: () => import('@/views/sysAdmin/subscriber/index.vue')
                },
                {
                    path: '/sysAdmin/site',
                    name: 'sysAdminsite',
                    meta: { requiredAuth: true, requiredAuthMsg: true },
                    component: () => import('@/views/sysAdmin/site/index.vue')
                },
                {
                    path: '/sysAdmin/user',
                    name: 'sysAdminuser',
                    meta: { requiredAuth: true, requiredAuthMsg: true },
                    component: () => import('@/views/sysAdmin/user/index.vue')
                }
            ]
        },
        {
            path: '/sample',
            name: 'sample',
            meta: { requiredAuth: false },
            component: () => import('@/views/sample/index.vue'),
            children: [
                {
                    path: '/sample/Global',
                    name: 'sampleGlobal',
                    meta: { requiredAuth: false },
                    component: () => import('@/views/sample/Global.vue')
                },
                {
                    path: '/sample/CommonAxios',
                    name: 'sampleCommonAxios',
                    meta: { requiredAuth: false },
                    component: () => import('@/views/sample/CommonAxios.vue')
                },
                {
                    path: '/sample/FormButton',
                    name: 'sampleFormButton',
                    meta: { requiredAuth: false },
                    component: () => import('@/views/sample/FormButton.vue')
                },
                {
                    path: '/sample/FormControls',
                    name: 'sampleFormControls',
                    meta: { requiredAuth: false },
                    component: () => import('@/views/sample/FormControls.vue')
                },
                {
                    path: '/sample/FormUpDownload',
                    name: 'sampleFormUpDownload',
                    meta: { requiredAuth: false },
                    component: () => import('@/views/sample/FormUpDownload.vue')
                },
                {
                    path: '/sample/LayoutDataList',
                    name: 'sampleLayoutDataList',
                    meta: { requiredAuth: false },
                    component: () => import('@/views/sample/LayoutDataList.vue')
                },
                {
                    path: '/sample/LayoutDataForm',
                    name: 'sampleLayoutDataForm',
                    meta: { requiredAuth: false },
                    component: () => import('@/views/sample/LayoutDataForm.vue')
                },
                {
                    path: '/sample/LayoutSingleSection',
                    name: 'sampleLayoutSingleSection',
                    meta: { requiredAuth: false },
                    component: () => import('@/views/sample/LayoutSingleSection.vue')
                },
                {
                    path: '/sample/LayoutMutlSection',
                    name: 'sampleLayoutMutlSection',
                    meta: { requiredAuth: false },
                    component: () => import('@/views/sample/LayoutMutlSection.vue')
                },
                {
                    path: '/sample/Tab',
                    name: 'sampleTab',
                    meta: { requiredAuth: false },
                    component: () => import('@/views/sample/Tab.vue')
                },
                {
                    path: '/sample/Popup',
                    name: 'samplePopup',
                    meta: { requiredAuth: false },
                    component: () => import('@/views/sample/Popup.vue')
                },
                {
                    path: '/sample/Loading',
                    name: 'sampleLoading',
                    meta: { requiredAuth: false },
                    component: () => import('@/views/sample/Loading.vue')
                }
            ]
        }
    ]
})

router.beforeEach((to, from) => {
    const userStore = useUserStore()
    if (to.meta.requiredAuth && userStore.checkLogin() === false) {
        if (to.path.split('/')[1] === 'sysAdmin') {
            if (to.meta.requiredAuthMsg) {
                alert(i18n.global.t('ui.message.noAuth'))
            }
            router.push('/sysAdmin/login')
        }
    }
})

export default router
