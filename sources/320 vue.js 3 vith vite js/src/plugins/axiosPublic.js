import axios from 'axios'
import { setting } from '@/variables'

/**
 * axios 인스턴스를 만들어 이를 사용.
 */
const axiosInstance = axios.create({
    baseURL: setting.apiBaseURL
})

/**
 * Request interceptor
 */
axiosInstance.interceptors.request.use(
    async (config) => {
        // 반환
        return config
    },
    (error) => {
        return Promise.reject(error)
    }
)

export default axiosInstance
