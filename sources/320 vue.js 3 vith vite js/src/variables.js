/**
 * NAME:    variables.js
 * DESC:    전역 변수 설정
 * DATE:    2021-11-18
 * CREATOR: hsohe74
 * HISTORY:
 *     2022-08-01 hsohe74 생성
 */
export const setting = {
    apiBaseURL: '/api',
    mobileMaxWidth: 900,
    defaultSubscriberId: 'DEFAULT',
    defaultPageSize: 20,
    defaultBlockSize: 10
}
