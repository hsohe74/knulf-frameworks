import { setting } from '@/variables.js'

/**
 * hostname에서 subdomain을 추출
 */
export const getSubDomain = () => {
    let subDomain = setting.defaultSubscriberId

    const found = window.location.hostname.match(/([^wwww][a-z0-9+\-_]+)?(.[^/:]+)/i)
    if (!found === false && found[0] !== 'localhost' && Number.isInteger(parseInt(found[1])) === false) {
        if (
            found[2].split('.').length === 2 ||
            (found[2].split('.').length === 3 && found[2].split('.')[2].length === 2)
        ) {
            // 서브도메인 없는 .com, .co.kr 의 경우
            subDomain = null
        } else {
            // 검색결과
            subDomain = found[1]
        }
    }

    return subDomain
}

/**
 * 주어진 사이트 호스트로 URL을 생성
 */
export const getFullMainUrlByHost = (siteHost) => {
    let mainUrl = ''
    let mainDomain = ''

    const found = window.location.hostname.match(/([a-z0-9+\-_]+)?(.[^/:]+)/i)
    if (found) {
        mainDomain = found[2]
    }

    if (window.location.port !== '80') {
        mainDomain = mainDomain + ':' + window.location.port
    }

    mainUrl = window.location.protocol + '//' + siteHost + mainDomain
    mainUrl = mainUrl.toLowerCase()

    return mainUrl
}

/**
 * 주어진 호스트명의 URL이 현재와 같은지 여부판단
 */
export const checkTargetHostUrlIsNotCurrent = (siteHost) => {
    let targetUrl = getFullMainUrlByHost(siteHost)
    targetUrl = targetUrl.toLowerCase()

    let currentUrl = window.location.protocol + '//' + window.location.host
    currentUrl = currentUrl.toLowerCase()

    return targetUrl === currentUrl
}

/**
 * URL에서 첫번째 경로를 읽어들임
 */
export const getFirstPath = () => {
    let firstPath = ''

    firstPath = window.location.pathname.replace(/^\/([^\\/]*).*$/, '$1')

    return firstPath
}
