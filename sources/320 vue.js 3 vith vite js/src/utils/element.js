/**
 * 엘리먼트 이동 시장
 * @param {*} evt
 */
export const startMoving = function (evt) {
    const elem = evt.target.parentElement

    const posX = evt.clientX
    const posY = evt.clientY

    const offsets = elem.getBoundingClientRect()
    const elemLeft = parseInt(offsets.left)
    const elemTop = parseInt(offsets.top)

    const diffX = posX - elemLeft
    const diffY = posY - elemTop

    elem.style.cursor = 'move'
    document.onmousemove = function (evt) {
        const posX = evt.clientX
        const posY = evt.clientY
        let aX = posX - diffX
        let aY = posY - diffY

        if (aX < 0) aX = 0
        if (aY < 0) aY = 0
        doMoving(elem, aX, aY)
    }
}

/**
 * 엘리먼트 이동
 * @param {*} elem
 * @param {*} aX
 * @param {*} aY
 */
export const doMoving = function (elem, aX, aY) {
    elem.style.left = aX + 'px'
    elem.style.top = aY + 'px'
}

/**
 * 엘리먼트 이동 중지
 * @param {*} evt
 */
export const stopMoving = function (evt) {
    const elem = evt.target.parentElement
    elem.style.cursor = 'default'
    document.onmousemove = function () {}
}
