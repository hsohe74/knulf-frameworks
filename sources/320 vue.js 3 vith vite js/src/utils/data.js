import i18n from '@/locales'

/**
 * FormData 를 가져옴
 */
export const getFormData = function (items) {
    const formData = new FormData()
    for (const key in items) {
        if (items[key] == null) {
            delete items[key]
        } else {
            formData.append(key, items[key])
        }
    }
    return formData
}

/**
 * 페이징 객체 구성 및 계산
 * pagingObj : {
 *   TotalRecord    : 전체 레코드 수 (설정값)
 *   PageSize       : 페이지당 레코드 수 (설정값)
 *   TotalPage      : 전체 페이지 수
 *   BlockSize      : 전체 블럭 수 (설정값)
 *   BlockNumber    : 현재 블럭
 *   PageNumbers    : 표시할 페이지
 * }
 */
export const setPagingData = function (pagingObj) {
    // TotalPage
    if (pagingObj.TotalRecord === 0) {
        pagingObj.TotalPage = 0
    } else {
        pagingObj.TotalPage = Math.floor((pagingObj.TotalRecord - 1) / pagingObj.PageSize + 1)
    }
    // TotalBlock
    if (pagingObj.TotalPage === 0) {
        pagingObj.TotalBlock = 0
    } else {
        pagingObj.TotalBlock = Math.floor((pagingObj.TotalPage - 1) / pagingObj.BlockSize + 1)
    }
    // BlockNumber
    if (pagingObj.TotalPage === 0) {
        pagingObj.BlockNumber = 0
    } else {
        pagingObj.BlockNumber = Math.floor((pagingObj.PageNumber - 1) / pagingObj.BlockSize + 1)
    }
    if (pagingObj.BlockNumber > pagingObj.TotalBlock) {
        pagingObj.BlockNumber = pagingObj.TotalBlock
    }
    // StartPage
    let StartPage = 0
    if (pagingObj.TotalPage > 0 && pagingObj.BlockNumber > 0) {
        StartPage = (pagingObj.BlockNumber - 1) * pagingObj.BlockSize + 1
    }
    // EndPage
    let EndPage = 0
    EndPage = pagingObj.BlockNumber * pagingObj.BlockSize
    if (EndPage > pagingObj.TotalPage) {
        EndPage = pagingObj.TotalPage
    }
    // Set page list
    pagingObj.PageNumbers = []
    if (pagingObj.TotalPage > 0) {
        for (let i = StartPage; i <= EndPage; i++) {
            pagingObj.PageNumbers.push(i)
        }
    }
}

/**
 * 페이징내의 게시물 번호 표시
 */
export const showPagingNo = function (listParam, index) {
    return listParam.TotalRecord - (listParam.PageNumber - 1) * listParam.PageSize - index
}

/**
 * Response 데이터를 가공처리
 */
export const getResponseData = function (response) {
    let returnData

    if (response !== undefined) {
        if (!response.data) {
            returnData = {}
        } else {
            returnData = response.data
        }

        if (response.status !== undefined && response.status === false) {
            returnData.status = response.status
            returnData.alertEnable = response.status !== 601
        }
    }

    return returnData
}

/**
 * response 에러 발생시 에러 출력
 * @param {*} response
 */
export const handleResponseError = function (response) {
    let errorMessage = ''
    let messageCode = ''
    if (!response.data) {
        errorMessage = response.statusText + ' (status: ' + response.status.toString() + ')'
    } else {
        errorMessage = response.data.Message
        messageCode = response.data.MessageCode
    }

    if (!messageCode) {
        alert(i18n.global.t('ui.message.error', { errorMessage: errorMessage }))
    } else {
        alert(i18n.global.t(messageCode))
    }
}

/**
 * 객체를 deep copy하여 반환
 * @param {*} object 복사할 대상
 */
export const copyObject = function (object) {
    return JSON.parse(JSON.stringify(object))
}

/**
 * Remove HTML Tag
 * @param {*} html html
 */
export const RemoveHtmlTag = function (html) {
    if (!html) {
        return ''
    } else {
        return html.replace(/(<([^>]+)>)/gi, '')
    }
}

/**
 * 초를 입력하면 mm:ss 로 반환
 * @param {*} secNum second number
 * @returns
 */
export const getTimeMMss = function (secNum) {
    let minutes = Math.floor(secNum / 60)
    let seconds = secNum - minutes * 60
    if (minutes < 10) {
        minutes = '0' + minutes
    }
    if (seconds < 10) {
        seconds = '0' + seconds
    }
    return minutes + ':' + seconds
}

/**
 * Format number
 * @param {*} value number
 * @returns
 */
export const formatNumber = function (value) {
    if (value) {
        return value.toFixed(0).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, '$1,')
    } else {
        return ''
    }
}

/**
 * Format phone number
 * @param {*} value phone number
 * @returns
 */
export const formatPhone = function (value) {
    if (!value) return value

    value = value.replace(/[^0-9]/g, '')

    if (value.length < 4) {
        return value
    } else if (value.length < 7) {
        // 123-4567
        return value.replace(/(\d{3})(\d{4})/, '$1-$2')
    } else if (value.length === 8) {
        // 1234-5678
        return value.replace(/(\d{4})(\d{4})/, '$1-$2')
    } else if (value.length < 10) {
        if (value.substring(0, 2) === '02') {
            // 02-123-5678
            return value.replace(/(\d{2})(\d{3})(\d{4})/, '$1-$2-$3')
        }
    } else if (value.length < 11) {
        if (value.substring(0, 2) === '02') {
            // 02-1234-5678
            return value.replace(/(\d{2})(\d{3})(\d{4})/, '$1-$2-$3')
        } else {
            // 010-123-4567
            return value.replace(/(\d{2})(\d{3})(\d{4})/, '$1-$2-$3')
        }
    } else {
        // 010-1234-5678
        return value.replace(/(\d{3})(\d{4})(\d{4})/, '$1-$2-$3')
    }
}

/**
 * Regular expression
 */
export const regAlphabet = /^[a-zA-Z]+$/
export const regKorean = /^[가-힣]+$/
export const regSpecial = /^[~`!@#$%^&*()_\-+={}[\]|\\;:'""<>,.?/]+$/
export const regText = /^[0-9a-zA-Z]+$/
export const regTextKorean = /^[0-9a-zA-Z가-힣]+$/
export const regFullText = /^[0-9a-zA-Z~`!@#$%^&*()_\-+={}[\]|\\;:'""<>,.?/]+$/
export const regFullTextKorean = /^[0-9a-zA-Z가-힣~`!@#$%^&*()_\-+={}[\]|\\;:'""<>,.?/]+$/
export const regLoginId = /^[a-zA-Z0-9_-]+$/
export const regTelephone = /^((0[1-9]\d?)?-?(\d{3,4})-?(\d{4}))$/
export const regTelephoneInternational = /^((\d{1,5})-(\d{1,3})-(\d{1,4})-(\d{1,4}))$/
export const regCellphone = /^((01\d)-?(\d{3,4})-?(\d{4}))$/
export const regCellphoneWithoutHyphen = /^((01\d)(\d{3,4})(\d{4}))$/
export const regEmail = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/
