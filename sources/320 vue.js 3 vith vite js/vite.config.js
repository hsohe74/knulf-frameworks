import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import mkcert from 'vite-plugin-mkcert'

export default defineConfig({
    plugins: [vue(), mkcert()],
    base: '/', // 배포 경로 (다른 서브폴더에 배포하는 경우 /path/ 와 같이 처리)
    resolve: {
        extensions: ['.js', '.vue', '.json'],
        alias: {
            '@': fileURLToPath(new URL('./src', import.meta.url))
        }
    },
    server: {
        port: 9193,
        https: true,
        proxy: {
            '/api': {
                target: 'http://localhost:9092',
                changeOrigin: true,
                secure: false
                //rewrite: (path) => path.replace('/api', '')
            }
        }
    },
    build: {
        assetsDir: 'resources/assets/'
    }
})
