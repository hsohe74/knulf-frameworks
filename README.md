# knulf-frameworks

frameworks (frontend, backend...)

## 100 .NET

    - Base version : 4.5

### knulf.Common

    - Common : 공통 클래스와 Utilities 클래스
    - External : 외부 라이브러리 사용한 Utilities
    - Language : 공통 로케일 언어

### knulf.Web

    - Common : 웹에 관련된 공통 클래스와 Utilities 클래스
    - Web : 웹에 관련된 JWT같은 것들

### knulf.AppCommon

    - Common
    - Dac
    - Models

